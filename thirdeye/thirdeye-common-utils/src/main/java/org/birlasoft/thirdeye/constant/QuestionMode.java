package org.birlasoft.thirdeye.constant;

public enum QuestionMode {
	
	FIXED ("Fixed"),
	MODIFIABLE ("Modifiable");
	
	private String value;
	
	private QuestionMode(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}

}
