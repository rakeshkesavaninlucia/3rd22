package org.birlasoft.thirdeye.constant;

public enum ExportPurpose {
	INV("Inventory"), QE("Questionnaire"), FM("Functional Map"),TCO("Total Cost Of Ownership");
	
	private final String value;       

    private ExportPurpose(String value) {
        this.value = value;
    }

	public String getValue() {
		return value;
	}
}
