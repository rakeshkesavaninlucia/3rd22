/**
 * @author shagun.sharma
 *
 */

package org.birlasoft.thirdeye.constant;

public enum CountryType {

	ALBANIA ("Albania"),
	ARGENTINA ("Argentina"),
	AUSTRALIA ("Australia"),
	AUSTRIA ("Austria"),
	BANGLADESH ("Bangladesh"),
	BELGIUM ("Belgium"),
	BHUTAN ("Bhutan"),
	BRAZIL ("Brazil"),
	CANADA("Canada"),
	CHILE("Chile"),
	CHINA("China"),
	DENMARK("Denmark"),
	EGYPT ("Egypt"),
	GERMANY("Germany"),
	GREECE("Greece"),
	INDIA("India"),
	ITALY("Italy"),
	JAPAN("Japan"),
	MALAYSIA("Malaysia"),
	MALDIVES("Maldives"),
	NETHERLANDS("Netherlands"),
	NEWZEALAND("New Zealand"),
	SINGAPORE("Singapore"),
	UAE("United Arab Emirates (UAE)"),
	USA("United States of America (USA)");

	private String value;

	private CountryType(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}
}
