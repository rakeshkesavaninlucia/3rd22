package org.birlasoft.thirdeye.constant;

public enum ApplicationLifeCycleStages {
	NEW("New"),
	EMERGING("Emerging"),
	MAINSTREAM("Mainstream"),
	CONTAINMENT("Containment"),
	SUNSET("Sunset"),
	PROHIBITED("Prohibited");
	
	private String stages;
	
	 ApplicationLifeCycleStages(String stages) {
		this.stages = stages;
	}

	public String getStages() {
		return stages;
	}

}
