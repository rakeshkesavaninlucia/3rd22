package org.birlasoft.thirdeye.constant;

public enum InterfaceDirection {

	UNI ("One Way"),
	BIDI ("Bi-directional");
	
	private String description;
	
	InterfaceDirection(String description){
		this.description = description;
	}

	public String getDescription() {
		return description;
	}

	
}
