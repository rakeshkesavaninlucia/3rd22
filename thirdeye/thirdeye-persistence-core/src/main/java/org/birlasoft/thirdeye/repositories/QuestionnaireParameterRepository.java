package org.birlasoft.thirdeye.repositories;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import org.birlasoft.thirdeye.entity.Parameter;
import org.birlasoft.thirdeye.entity.Questionnaire;
import org.birlasoft.thirdeye.entity.QuestionnaireParameter;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.EntityGraph.EntityGraphType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * Repository interface for questionnaire parameter.
 */
public interface QuestionnaireParameterRepository extends JpaRepository<QuestionnaireParameter, Serializable> {
	/**
	 * Find list of questionnaire parameter by questionnaire object. 
	 * @param questionnaire
	 * @return {@code List<QuestionnaireParameter>}
	 */
	public List<QuestionnaireParameter> findByQuestionnaire(Questionnaire questionnaire);
	
	@EntityGraph(value = "QuestionnaireParameter.parameter", type = EntityGraphType.LOAD)
	@Query("SELECT qp FROM QuestionnaireParameter qp WHERE qp.questionnaire = (:qe)")
	public Set<QuestionnaireParameter> findByQuestionnaireLoadedParameter(@Param("qe") Questionnaire qe);
	
	public List<QuestionnaireParameter> findByQuestionnaireAndParameterByParameterId(Questionnaire questionnaire, Parameter parameter);
	
	public List<QuestionnaireParameter> findByQuestionnaireAndParameterByParentParameterIdIsNull(Questionnaire questionnaire);

	public Long deleteByQuestionnaireAndParameterByRootParameterId(Questionnaire questionnaire, Parameter parameter);
	
	public QuestionnaireParameter findByQuestionnaireAndParameterByParameterIdAndParameterByParentParameterId(Questionnaire questionnaire, Parameter parameter,Parameter parentParameter);
	
	public Set<QuestionnaireParameter> findByParameterByRootParameterIdInAndQuestionnaire(Set<Parameter> parameters, Questionnaire questionnaire);
	
	@Query("SELECT qp FROM QuestionnaireParameter qp group by qp.parameterByParameterId")
	public Set<QuestionnaireParameter> getQuestionnaireParameterGroupByParameterId();
	
	public List<QuestionnaireParameter> findByQuestionnaireInAndParameterByParentParameterIdIsNull(List<Questionnaire> questionnaireList);
	
	@Query("SELECT qp.parameterByParameterId FROM QuestionnaireParameter qp where qp.questionnaire in (:qu) ")
	public List<Parameter> findParametersByQuestionnaireIn(@Param("qu") List<Questionnaire> questionnaireList);
	
	@Query("SELECT qp.questionnaire FROM QuestionnaireParameter qp where qp.parameterByParameterId = (:pa) ")
	public List<Questionnaire> findByParameterByParameterId(@Param("pa") Parameter parameter);
	
	public List<QuestionnaireParameter> getByParameterByParameterId(Parameter parameter);
	/**
	 * Fetch list of QP by Qes.
	 * @param questionnaireList
	 * @return {@code List<QuestionnaireParameter>}
	 */
	public List<QuestionnaireParameter> findByQuestionnaireIn(List<Questionnaire> questionnaireList);
}
