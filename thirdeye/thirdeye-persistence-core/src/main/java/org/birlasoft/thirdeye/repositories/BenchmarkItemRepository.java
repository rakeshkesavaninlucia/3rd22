package org.birlasoft.thirdeye.repositories;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import org.birlasoft.thirdeye.entity.Benchmark;
import org.birlasoft.thirdeye.entity.BenchmarkItem;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Repository interface for Benchmark Item.
 * @author samar.gupta
 */
public interface BenchmarkItemRepository extends JpaRepository<BenchmarkItem, Serializable> {
	/**
	 * Fetch list of Benchmark item by benchmark id. 
	 * @param benchmark
	 * @return {@code List<BenchmarkItem>}
	 */
	public List<BenchmarkItem> findByBenchmark(Benchmark benchmark);
	/**
	 * Fetch list of Benchmark item by benchmark item ids.
	 * @param setOfBenchmarkItemIds
	 * @return {@code List<BenchmarkItem>}
	 */
	public List<BenchmarkItem> findByIdIn(Set<Integer> setOfBenchmarkItemIds);
	/**
	 * 
	 * @param benchMarkId
	 * @return {@code List<BenchmarkItem>}
	 */
	public List<BenchmarkItem> findByBenchmarkId(Integer benchMarkId);
}
