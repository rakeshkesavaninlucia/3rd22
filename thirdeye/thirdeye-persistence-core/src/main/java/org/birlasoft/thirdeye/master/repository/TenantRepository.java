package org.birlasoft.thirdeye.master.repository;

import java.io.Serializable;

import org.birlasoft.thirdeye.master.entity.Tenant;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TenantRepository extends JpaRepository<Tenant, Serializable> {
	
	public Tenant findByTenantAccountId(String accountId); 

}
