package org.birlasoft.thirdeye.repositories;

import java.io.Serializable;
import java.util.Set;

import org.birlasoft.thirdeye.entity.Asset;
import org.birlasoft.thirdeye.entity.AssetData;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Repository interface for asset data.
 * @author samar.gupta
 */
public interface AssetDataRepository extends JpaRepository<AssetData, Serializable> {

	Set<AssetData> findByAsset(Asset asset);
}
