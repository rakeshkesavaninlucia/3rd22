package org.Birlasoft.Testcase.org.Birlasoft.Testcase;

import java.awt.AWTException;
import java.io.IOException;

import org.Birlasoft.POM.Available_Cost_Elements;
import org.Birlasoft.POM.Home_PageObjecet;
import org.Birlasoft.POM.Login_page_object;
import org.Birlasoft.POM.Modal_popup_createcostelement;
import org.Birlasoft.POM.TCO;
import org.Birlasoft.Utility.Util;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class TC_Create_Cost_Element extends Util{

	
	@BeforeClass
	public void browserinit() throws IOException{
		//intialization of the browser
		browser();
	}
	
	
	  @Test
	  (priority = 0)
	  public void login_page() throws InterruptedException  {
		
		  
		
			//login page
			Login_page_object obj1 = new Login_page_object(driver);
			obj1.verifyLogInPageTitle();
			
			Thread.sleep(1000);
			
			obj1.Login_username("manoj.shrivas@birlasoft.com");
			
			Thread.sleep(2000);
			obj1.Login_Password("welcome12#");
			Thread.sleep(2000);
			obj1.Login_Accountid1("qadd");
			Thread.sleep(1000);
			obj1.Login_Submit();
			//driver.navigate().to("http://130.1.4.252:8080/home"); 
			obj1.Login_ErrorMsg();
			Thread.sleep(1000);
  }
	
	
	  @Test(priority = 1)
	  public void Cost_element_tab() throws InterruptedException
	  {
		  Home_PageObjecet obj2 = new Home_PageObjecet(driver);
			obj2.Homepage_titleverify("Portfolio Home");
			
			obj2.Homepage_TCOManagement();
			obj2.Homepage_TCOManagement_CostElement();
			
	
}
	  

	   @Test(priority = 2 )
	  public void Create_CostElement() throws InterruptedException, AWTException
	  {
	  
	 	 TCO  objCele = new TCO (driver);
	 	  
	 	 objCele.get_PageTitle();
	 	 objCele.pageetitle_verify();
	 	 objCele.Click_Cost_element();
	 	  
	 	 	}
	  
	   @Test(priority = 3 )
	  public void Create_CostElement_modal() throws InterruptedException, AWTException
	  {
	  
	 	 Modal_popup_createcostelement  objCeleM = new Modal_popup_createcostelement (driver);
	 	  
	 	// objCeleM.get_PageTitle();
	 	 //objCeleM.pageetitle_verify();
	 	 objCeleM.Cost_elementname();
	 	 Thread.sleep(3000);
	 	 objCeleM.Cost_elementname1("Cost element_C");
	 	 Thread.sleep(3000);
	 	 objCeleM.Cost_elementdesc("Descone");
	 	 Thread.sleep(3000);
	 	 objCeleM.Click_create_element();
	 	  
	 	 	}
	  
	    @Test(priority = 4 )
	  public void CostElement_SearchEdit() throws InterruptedException, AWTException
	  {
	  
	 	 Available_Cost_Elements  objCele = new Available_Cost_Elements (driver);
	 	  
	 	 objCele.get_PageTitle();
	 	 objCele.pageetitle_verify();
	 	 objCele.Click_Cost_element("Cost element_C");
	 	 objCele.Click_Edit_Cost_element();
	 	 Thread.sleep(3000);
	 	 objCele.Close_modal();
	 	Thread.sleep(3000);
	 	 objCele.Close_modal1();
	 	 	}
	  
	  
	  
}
