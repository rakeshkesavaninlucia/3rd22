package org.Birlasoft.Testcase.org.Birlasoft.Testcase;



import java.io.IOException;

//import org.Pom.Home_PageObjecet;
//import org.Pom.Login_PageObject;
import org.Birlasoft.POM.Home_PageObjecet;
import org.Birlasoft.POM.Login_page_object;
import org.Birlasoft.POM.Template_Inventory;
import org.Birlasoft.POM.Template_Inventory_AddAsset;
import org.Birlasoft.POM.Template_ViewTemplate_Pageobject;
import org.Birlasoft.POM.Template_Viewtemplate_Updatetemplate;
import org.Birlasoft.Utility.Util;
//import org.Birlasoft.Utility.Util;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class TC_TemplateManagement extends Util
{
	//public WebDriver driver;
	@BeforeClass
	public void browserinit() throws IOException
	{
		//intialization of the browser
		browser();
		
	}
	
	
	   @Test(priority = 0)
	   public void f() throws InterruptedException  {
			
			  
			
			//login page
			Login_page_object obj1 = new Login_page_object(driver);
			obj1.verifyLogInPageTitle();
			
			Thread.sleep(1000);
			
			obj1.Login_username("rakesh1.k@birlasoft.com");
			
			Thread.sleep(2000);
			obj1.Login_Password("welcome12#");
			Thread.sleep(2000);
			obj1.Login_Accountid1("qadd");
			Thread.sleep(1000);
			obj1.Login_Submit();
			//driver.navigate().to("http://130.1.4.252:8080/home"); 
			obj1.Login_ErrorMsg();
			Thread.sleep(1000);
			
			
	}
	   
	   
	   
		@Test(priority = 1)
		public void Naviagate_To_CreateTemplate_Page() throws InterruptedException
		{
				 Home_PageObjecet obj2 = new Home_PageObjecet(driver);
				 
				 obj2.Homepage_titleverify("Portfolio Home");
				 //obj2.Homepage_Workspace_change("Workspace 1.2");
				 Thread.sleep(1000);
				 obj2.Homepage_TemplateManagement();
				 //obj2.Homepage_TemplateManagement_CreateTemplate();
				 obj2.Homepage_TemplateManagement_ViewTemplates();
				
		}
		
		/*@Test(priority = 2)
		public void Create_Asset_Template() throws InterruptedException
		{
			Template_CreateTemplate_PageObject obj3 = new Template_CreateTemplate_PageObject(driver);
			obj3.CreateTemplate_title_verify();
			
			obj3.CreateTemplate_Assettype_click("IT Services");
			obj3.CreateTemplate_template_name("Template_automation_1.6");
			obj3.CreateTemplate_template_description("Template Created through Automation");
			obj3.CreateTemplate_template_save();
			
		}*/
		
		@Test(priority = 2)
		public void View_Asset_Template() throws InterruptedException
		{
			Template_ViewTemplate_Pageobject obj4 = new Template_ViewTemplate_Pageobject(driver);
			obj4.pageetitle_verify();
			obj4.View_Template_Table_asset_verify("Template_automation_1.6");
			obj4.View_Template_Update_icon("Template_automation_1.6_1");
			
		}
		
		@Test(priority = 3)
		public void Update_Asset_Page()
		{
			
			Template_Viewtemplate_Updatetemplate obj5 = new Template_Viewtemplate_Updatetemplate(driver);
			obj5.pageetitle_verify();
			obj5.Update_template_name("Auto_Update_Template_automation_1.6");
			obj5.Update_template_description("Asset Template Updated");
			obj5.Update_template_save();
			
			
			
		}
		
		@Test(priority = 4)
		public void View_Update_Asset_Template() throws InterruptedException 
		{
			Template_ViewTemplate_Pageobject obj6 = new Template_ViewTemplate_Pageobject(driver);
			obj6.pageetitle_verify();
			obj6.View_Template_Table_asset_verify("Auto_Update_Template_automation_1.6");
			//obj6.View_Template_Update_icon("Auto_Update_Oct14_Buss01");
			
		}
	   
		@Test(priority = 5)
	   public void Navigate_To_Inventory() throws InterruptedException 
	   {
		   
		   Home_PageObjecet obj7 = new Home_PageObjecet(driver);
		   obj7.Homepage_TemplateManagement_Inventory();
		   Thread.sleep(1000);
		   obj7.Homepage_TemplateManagement_Inventorylist("Auto_Update_Template_automation_1.6");
		   
	   }
	   
		
	   @Test(priority = 6)
	   public void Add_Asset_To_Inventory()
	   {
		   Template_Inventory obj8 = new Template_Inventory(driver);
		   obj8.Inventory_title();
		   obj8.Inventory_AddAsset_Btn_clk(); 
		   
	   }
	   
	   @Test(priority = 7)
	   public void Add_Asset_Page()
	   {
		
		   Template_Inventory_AddAsset obj9 = new Template_Inventory_AddAsset(driver);
		   obj9.Inventory_addasset_title();
		   obj9.Inventory_addasset_textbox("Auto_Bis_Asset01");
		   obj9.Inventory_addasset_submit_btn();
		   obj9.Inventory_addasset_back_btn();   
		   
	   }
	   
	  @Test(priority = 8)
	   public void Verfiy_Asset_Table() throws InterruptedException
	   {
		   
		   Template_Inventory obj8 = new Template_Inventory(driver);
		   obj8.Inventory_title();
		   obj8.Inventory_table_Asset_check("Auto_Bis_Asset01");
		   
		   
	   }
	  
		@AfterClass(alwaysRun=true)
		public void Parameter_CloseBrowser()
		{
			
			closeBrowser();
			QuitBrowser();
		}

}

