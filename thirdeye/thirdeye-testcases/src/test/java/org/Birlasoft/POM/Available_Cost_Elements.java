package org.Birlasoft.POM;

import org.Birlasoft.Utility.Util;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Available_Cost_Elements extends Util{

	
	public  Available_Cost_Elements(WebDriver driver) 
	{
			
			PageFactory.initElements(driver,this);
			
		}
		
	//objects of Question Page
		
		
	     
	@FindBy(xpath = "//span[contains(text(),'Available Cost Elements')]")
    public WebElement verify_page_title;       
	
	@FindBy(xpath = "//*[@id='DataTables_Table_0_filter']/label/input")
    public WebElement SearchCost_element;
	              
	@FindBy(xpath = "//tr[1]/td[3]/span")
    public WebElement EditCost_element;
	
	@FindBy(xpath = "//*[@id='createNewCostElementForm']/div[1]/button/span")
    public WebElement open_modal;
	
	@FindBy(xpath = "//*[@id='createNewCostElementForm']/div[1]/button/span")
   public WebElement close_modal;
	       
	       
public String get_PageTitle() {
  	    	   
  	    	   String pageTitle = verify_page_title.getText();
  	   		
  	   		return pageTitle ;
  	   		
  	       }
	       
	       
	    //To verify whether the user is landing on the proper page or not
	   	public void pageetitle_verify()
	   	{
	   		try{
	   			String expectedTitle ="Available Cost Elements";
	   			if(expectedTitle.equals(get_PageTitle()))
	   			{
	   				System.out.println("Page Title is "+expectedTitle);
	   			}
	   			
	   		}catch(Exception e){

	   	      throw new AssertionError("A clear description of the failure", e);
	   		}
	   		}
	       
	   	   public void Click_Cost_element(String vaue) throws InterruptedException 
	   	   {
	   		Webtable_Search(SearchCost_element,vaue);
	   		
	       }
	   	   
	   	public void Click_Edit_Cost_element() {
	   		click_Method(EditCost_element);
	   		
	   		       }
	   	
	   	public void Close_modal() throws InterruptedException {
	   		Popup_table_close(open_modal);
	   		
	   		}
	   	
	   	public void Close_modal1() throws InterruptedException {
	   		click_Method(close_modal);
	   		
	   		}
	   	   
	   	
	   	   
	   	   
	   	   
}
