package org.Birlasoft.POM;
//package org.Birlasoft.Utility;


import org.Birlasoft.Utility.Util;
//import org.Birlasoft.Utility.Util;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;



public class FunctionalMap_Available_BCM_Template_Pageobject 
{
	public FunctionalMap_Available_BCM_Template_Pageobject(WebDriver driver)
    {
		
		PageFactory.initElements(driver,this);
		
	}
	
	
	
	@FindBy(xpath = "//span[contains(text(),'Available BCM Templates')]")
	public WebElement Available_BCM_Template_Pagetitle; 
	
	@FindBy(xpath = "//div[@id='listOfBCMTemplate_filter']/label/input[@type='search']")
	public WebElement Available_BCM_Template_SearchBox;
	
	@FindBy(xpath = "//table[@id='listOfBCMTemplate']/tbody/tr[1]/td[3]/a")
	public WebElement Available_BCM_Template_Action_Edit;
	
	@FindBy(xpath = "//a[contains(text(),'Add a new BCM Template')]")
	public WebElement Available_BCM_Template_Add_new_BCMBtn;
	
	@FindBy(id = "listOfBCMTemplate")
	public WebElement Available_BCM_Template_Table_Verify;
	
	@FindBy(xpath = "//div[@class='form-group']/input[@type='text' and @id='bcmName']")
	public WebElement Available_BCM_Template_Name_Txtbox;
	
	@FindBy(xpath = "//input[@type='submit' and @value='Create BCM Template']")
	public WebElement Available_BCM_Template_Name_SubmitBtn;
	
	@FindBy(xpath = "//h4[@class='modal-title' and  contains(text(),'Create BCM Template')]")
	public WebElement Available_BCM_Template_Popup_Title_verify;
	
	
	
	public String get_pagetitle()
	{
		String pageTitle = Available_BCM_Template_Pagetitle.getText();
		
		return pageTitle ;
		
		
	}
	
	
	
	//To verify whether the user is landing on the proper page or not
	public void Available_BCM_Template_pagetitle_verify()
	{
			try{
				String expectedTitle = "Available BCM Templates";
				if(expectedTitle.equals(get_pagetitle()))
				{
					System.out.println("Page Title is"+expectedTitle);
				}
				
			}catch(Exception e){

		      throw new AssertionError("A clear description of the failure", e);
			}
			
	}
	
	
	
	//click on add bcm button
	public void Available_BCM_Template_Add_new_BCMBtn()
	{
		Util.fn_CheckVisibility_Button(Available_BCM_Template_Add_new_BCMBtn);
		
		
	}
	
	
	
	//Verify the Created BCM name on the table
	public void Available_BCM_Template_Webtable_Verify(String Searchname) throws InterruptedException
	{
		Util.Webtable_Search(Available_BCM_Template_SearchBox,Searchname);
		Util.Webtable_Element_check(Available_BCM_Template_Table_Verify, Searchname);
		
	}
	
	
	public void Available_BCM_Template_Edit_BCM(String BCM_Template_Searchname ) throws InterruptedException
	{
		Util.Webtable_Search(Available_BCM_Template_SearchBox,BCM_Template_Searchname);
		Util.fn_CheckVisibility_Button(Available_BCM_Template_Action_Edit);
	}
	
	public void Available_BCM_Template_Popup_Create_BCM(String BCM_Name) throws InterruptedException
	{
		Thread.sleep(1000);
		Util. Popup_title_verify(Available_BCM_Template_Popup_Title_verify,"Create BCM Template");
		Util.Popup_Set_text(Available_BCM_Template_Name_Txtbox,BCM_Name);
		// try with this method , if it s not working plz start with modal popup
		Util.fn_CheckVisibility_Button(Available_BCM_Template_Name_SubmitBtn);
		//Util.Popup_Error_verify();
	}
	

	
}

