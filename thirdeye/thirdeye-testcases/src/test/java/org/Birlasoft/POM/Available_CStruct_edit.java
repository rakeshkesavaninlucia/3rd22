package org.Birlasoft.POM;

import org.Birlasoft.Utility.Util;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Available_CStruct_edit extends Util{

	
	public  Available_CStruct_edit (WebDriver driver) 
	{
			
			PageFactory.initElements(driver,this);
			
		}
		
	//objects of Question Page
		
		
	     
	@FindBy(xpath = "//span[contains(text(),'Available Cost Structure')]")
    public WebElement verify_page_title;       
	
	@FindBy(xpath = "//input[@type='search']")
    public WebElement Edit_cost_structure;
	
	
	@FindBy(xpath = "//tr[1]/td[5]/a")
    public WebElement Click_on_edit_icon;
	
	       
	       
public String get_PageTitle() {
  	    	   
  	    	   String pageTitle = verify_page_title.getText();
  	   		
  	   		return pageTitle ;
  	   		
  	       }
	       
	       
	    //To verify whether the user is landing on the proper page or not
	   public void pageetitle_verify()
	   	{
	   		try{
	   			String expectedTitle ="Available Cost Structure";
	   			if(expectedTitle.equals(get_PageTitle()))
	   			{
	   				System.out.println("Page Title is "+expectedTitle);
	   			}
	   			
	   		}catch(Exception e){

	   	      throw new AssertionError("A clear description of the failure", e);
	   		}
	   		}
	       
	    
	   public void Search_Cost_Structure(String cststruct) {
		   Edit_cost_structure.sendKeys(cststruct);
  		}
	   
	   public void Click_Edit_Cost_Structure() {
	   		click_Method(Click_on_edit_icon);
	   	}	
	   
	   
		
		
	   	
	   	   				
	
	
}
