package org.Birlasoft.POM;

import java.util.List;

import org.Birlasoft.Utility.Util;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Dashboard_Add_New_Dashboard extends Util{
	
	public Dashboard_Add_New_Dashboard(WebDriver driver) 
	{	
			PageFactory.initElements(driver,this);
			
	}

	

	//objects of Dashboard
 	
     	@FindBy(xpath = "//*[@id='createNewDashBoardForm']/div/h4")
     	public WebElement Page_title;

	    @FindBy(xpath="//*[@id='dashboardName']")
	    public WebElement Dashboard_Name;
	    
	    @FindBy(xpath="//*[@id='dashboardName']")
	    public WebElement Dashboard_Name1;
	    
	    @FindBy(xpath="//*[@class='btn btn-primary']")
		 public WebElement Save_Button;
	   
	    @FindBy(xpath="//*[@id='dashboardActionDropDown']")
		public WebElement Dropdown;
		
	    @FindBy(xpath="//*[@id='dashboardActionDropDown']")
		public WebElement Dropdown1;
	    
	     @FindBy(xpath="//*[@class='addWidget clickable']")
		 public WebElement Add_widget_click;
	   
//	     
//	     @FindBy(xpath="//*[@class='editDashboard clickable']")
//		 public WebElement Edit_widget_click;
//		   
//	     @FindBy(xpath="//*[@class='deleteDashboard clickable']")
//		 public WebElement Delete_widget_click;
//		
//	    

	    @FindBy(xpath="//*[@id='title']")
		public WebElement Name_for_widget;
		
        @FindBy(xpath="//*[@id='widgetType1']")
	 	 public WebElement Widget_popup_Raidobutton;
	 	     
	    
	    @FindBy(xpath="//*[@class='btn btn-primary']")
		 public WebElement Add_Widget_Button1;
	  
	  //div/button[@id="dashboardActionDropDown"]
	    @FindBy(xpath="//*[@id='mod-module-widget-1']/div[1]/div/div[1]/div/button[1]")
		 public WebElement Setting_Button;
	
	    @FindBy(xpath="//*[@class='form-control ajaxSelector']")
		 public WebElement Select_Questionnaire_Dropdown;
	
	    @FindBy(xpath="//*[@class='form-control ajaxPopulated questionSelector']")
	 	 public WebElement Select_Question_Dropdown;
	  
	    @FindBy(xpath="//*[@class='form-control ajaxPopulated paramSelector']")
		 public WebElement Select_Parameter_Dropdown;
		
	    
		@FindBy(xpath = "//*[@id='mod-module-widget-1']/div[1]/div/div[2]/div[1]/div[2]/form/div[4]/div[1]/label/input")
		public List<WebElement> Widget_popup_Raidobutton_list;
		
		 @FindBy(xpath="//*[@class='btn btn-primary plotChart']")
		 public WebElement Plot_Chart_Button;
		  
		 @FindBy(xpath="//*[@class='btn btn-primary saveWidget']")
		 public WebElement Save_Widget_Button;
		
	    
	    //calling of objects of dashboard
	    
	
	    	public String getLogInPageTitle() {
	 		String pageTitle = Page_title.getText();
			return pageTitle;
			
	    	}
		    public void verifyLogInPageTitle() {
			try{
			String expectedTitle = "createNewDashBoardForm";
			if(expectedTitle.equals(getLogInPageTitle())){
				System.out.println("Page Title is"+expectedTitle);
			}
			
	    	}catch(Exception e){

	       throw new AssertionError("A clear description of the failure", e);
	    	}
	    	}
		
  	   	
	    	public void Dashboard_Name() throws InterruptedException {
	   		Popup_table_close(Dashboard_Name);
	   		
	   		}

	       public void get_Text(String Dashbord_Text)
	       {
	    	   Dashboard_Name1.sendKeys(Dashbord_Text);
	       }
	
	       
	       public void Save_Button()
	       {
	       	Util.fn_CheckVisibility_Button(Save_Button);
	       }
	
	       
	       
		   	public void Dropdown() throws InterruptedException {
		   		Popup_table_close(Dropdown);
		   		
		   		}  
		       
	       
	           public void Dropdown1()
	    	{
	   		
	    		click_Method(Dropdown1);
	    	}
	   	
	       public void Add_widget_click()
	       {
	       	Util.fn_CheckVisibility_Button(Add_widget_click);
	       }
	
//	       
//	       public void Edit_widget_click()
//	       {
//	       	Util.fn_CheckVisibility_Button(Edit_widget_click);
//	       }
//	
//	       public void Delete_widget_click()
//	       {
//	       	Util.fn_CheckVisibility_Button(Delete_widget_click);
//	       }
//	
	       
	       public void get_name(String Widget_Text) {
	    	   Name_for_widget.sendKeys(Widget_Text);
	       }
	   

	       public void Add_Widget_Button1()
	       {
	       	Util.fn_CheckVisibility_Button(Add_Widget_Button1);
	       }
	
	       public void Setting_Button()
	       {
	       	Util.fn_CheckVisibility_Button(Setting_Button);
	       }
	
	       
	     
			public void Select_Questionnaire_Dropdown(String value) {
				Dropdownselect(Select_Questionnaire_Dropdown,value);
	  		}
			
		
			public void Select_Question_Dropdown(String value) {
				Dropdownselect(Select_Question_Dropdown,value);
	  		}
			
			
			public void Select_Parameter_Dropdown(String value) {
				Dropdownselect(Select_Parameter_Dropdown,value);
	  		}
			
			
			 public void Widget_popup_Raidobutton()
		       {
		       	Util.fn_CheckVisibility_Button(Widget_popup_Raidobutton);
		       }
		
			
//		    public void Widget_popup_Raidobutton_list()
//			   
//				{
//
//					Radiobutton_click(Widget_popup_Raidobutton_list);
//					
//				}
//		    
		    
		    public void finalmethod() throws InterruptedException
		       {
		             
		               int flag=0;
		              Thread.sleep(3000);
		                     List<WebElement>dashradiobutton= driver.findElements(By.xpath("//div[@class='radio']/label/input"));
		                     Thread.sleep(3000);
		                     for(int i=0 ; i<dashradiobutton.size();i++)
		                     {
		                           if((dashradiobutton.get(i).getAttribute("value").equals("bar")))
		                           {
		                                 
		                                  dashradiobutton.get(i).click();
		                                  System.out.println("element is");
		                                  flag=1;
		                                  break;
		                           }
		                           else
		                           {
		                                  flag=0;
		                           }
		                     }
		       }
			
			 public void Plot_Chart_Button()
			       {
			       	Util.fn_CheckVisibility_Button(Plot_Chart_Button);
			       }
 			
			 public void Save_Widget_Button()
		       {
		       	Util.fn_CheckVisibility_Button(Save_Widget_Button);
		       }
           }
