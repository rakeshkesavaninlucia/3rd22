package org.Birlasoft.POM;

import org.Birlasoft.Utility.Util;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Response_Status extends Util{
	
	public  Response_Status(WebDriver driver) 
	{
			
			PageFactory.initElements(driver,this);
			
		}
		//objects of Question Page
		
		
	     
	@FindBy(xpath = "/html/body/div/div/section[1]/h1/div/span")
    public WebElement verify_page_title;
	
	@FindBy(xpath = "//*[@id='DataTables_Table_0_filter']/label/input")
    public WebElement search_questionnaire;  
	
	@FindBy(xpath = "//tr[1]/td[4]/a")
    public WebElement click_edit;
	
	
	       
	       
	       
	        // @FindBy(id = "questionList")
	        // public WebElement ViewQuestion_table;
	       
	       
	       
public String get_PageTitle() {
  	    	   
  	    	   String pageTitle = verify_page_title.getText();
  	   		
  	   		return pageTitle ;
  	   		
  	       }
	       
	       
	    //To verify whether the user is landing on the proper page or not
	   	public void pageetitle_verify()
	   	{
	   		try{
	   			String expectedTitle ="Submit the Response";
	   			if(expectedTitle.equals(get_PageTitle()))
	   			{
	   				System.out.println("Page Title is "+expectedTitle);
	   			}
	   			
	   		}catch(Exception e){

	   	      throw new AssertionError("A clear description of the failure", e);
	   		}
	   		}
	       
	   	
	   	
	   	public String search_questionnaire(String text) {
	   		search_questionnaire.sendKeys(text);
			return text;
	       }
	   	
	    public void click_on_edit() 
	    {
	   		click_Method(click_edit);
	       }
	   	
}
	
	
	   	
	


