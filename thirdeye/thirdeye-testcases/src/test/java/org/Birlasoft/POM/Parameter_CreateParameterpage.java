package org.Birlasoft.POM;



import java.util.List;

import org.Birlasoft.Utility.Util;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Parameter_CreateParameterpage extends Util {

	
	public Parameter_CreateParameterpage(WebDriver driver)
    {
		
		PageFactory.initElements(driver,this);
		
	}
	
	/* Locators  */
	
	//page title
	@FindBy(xpath = "//span[contains(text(),'Create a New Parameter')]")
	public WebElement createParameter_pagetitle;
	
	//paramete/question/functional map popup title
	@FindBy(xpath = "//h4[@class='modal-title' and contains(text(),'Select Parameters')]")
	public WebElement createParmeter_AP_Paramter_popup;
	
	@FindBy(xpath = "//h4[@class='modal-title' and contains(text(),'Select Questions')]")
	public WebElement createParmeter_LP_Paramter_popup;
	
	@FindBy(xpath = "//h4[@class='modal-title' and contains(text(),'Select Functional Map')]")
	public WebElement createParmeter_FC_Paramter_popup;
	
	// Click workspace
	@FindBy(id = "select2-workspaceId-container")
	public WebElement CreateParameter_Workspace;
	
	// complete elements of workspace dropdown
	@FindBy(id = "select2-workspaceId-results")
	public WebElement CreateParameter_Workspace_List;
	
	//Parameter(mandatory)
	@FindBy(id = "select2-paramType-container")
	public WebElement CreateParameter_Parametertype;
	
	//complete elements of parameter dropdown
	@FindBy(id = "select2-paramType-results")
	public WebElement CreateParameter_Paramtertype_List;
	
	//uniquename(mandatory)
	@FindBy(id = "name")
	public WebElement CreateParameter_Uniquename;
	
	//display name (mandatory)
	@FindBy(id = "displayName")
	public WebElement CreateParameter_Displayname;
	
	//Description
	@FindBy(id = "description")
	public WebElement CreateParameter_Description;
	
	//Aggregate Parameter
	@FindBy(xpath = "//div/a[@data-type='buttonClick']")
	public WebElement CreateParameter_AP_Btn;
	
	//Leaf Parameter - Add question
	@FindBy(xpath = "//div/a[@data-type='buttonClick']")
	public WebElement CreateParameter_LP_Btn;
	
	//Functional coverage - Add Functional Map
	@FindBy(xpath = "//a[contains(text(),'Add Functional Map')]")
	public WebElement CreateParameter_FC_Btn;
	
	//parameter_Popup_search
	@FindBy(xpath = "//input[@type='search']")
	public WebElement CreateParameter_Popup_search;
	
	//select Radio button for AP
	@FindBy(xpath = "//input[@type='checkbox']")
	public List<WebElement> CreateParameter_popup_Raiobutton;
	
	@FindBy(xpath = "//input[@type='checkbox' and @name='parameter']")
	public List<WebElement>CreateParamter_LPpopup_Raiobutton;
	
	
	//AP_Submit
	@FindBy(xpath = "//input[@data-type='saveParametersTemporary']")
	public WebElement CreateParameter_AP_Submit;
	
	//LP_Submit
	@FindBy(xpath = "//input[@data-type='saveQuestionsTemporary']")
	public WebElement CreateParameter_LP_Submit;
	
	//FC_Radiobutton
	@FindBy(xpath = "//input[@type='radio']")
	public List<WebElement> CreateParameter_FC_Raiobutton;
	
	//FC_Submit
	@FindBy(xpath = "//input[@data-type='saveFunctionalMapTemporary']")
	public WebElement CreateParameter_FC_Submit;
	
	//LP and Ap 
	@FindBy(xpath = "//tr[1]/td[3]/input")
	public WebElement CreateParamete_Weight1strow;
	
	//LP and Ap 
	@FindBy(xpath = "//tr[2]/td[3]/input")
	public WebElement CreateParameter_Weight2ndrow;
	
	@FindBy(name = "functionalMapLevelType")
	public WebElement CreateParameter_Level;
	
	
	@FindBy(xpath = "//input[@type='submit']")
	public WebElement CreateParamete_submitbtn;
	
	
	public String get_pagetitle()
	{
		String pageTitle = createParameter_pagetitle.getText();
		
		return pageTitle ;
		
		
	}
	
	//To verify whether the user is landing on the proper page or not
	public void CreateParameter_pagetitle_verify()
	{
			try{
				String expectedTitle = "Create a New Parameter";
				if(expectedTitle.equals(get_pagetitle()))
				{
					System.out.println("Page Title is"+expectedTitle);
				}
				
			}catch(Exception e){

		      throw new AssertionError("A clear description of the failure", e);
			}
			
	}
	
	// 
	public void CreateParameter_Workspace_click(String Workspace) throws InterruptedException
	{
		if(CreateParameter_Workspace.isDisplayed())
			Thread.sleep(1000);
		CreateParameter_Workspace.click();
		//calling the method ajax auto select
		AjaxAuto_select(CreateParameter_Workspace_List,Workspace);//method(WebElement name , value)
		

	}
	
	//
	
	public void CreateParameter_Parametertype_click(String Select_Parameter) throws InterruptedException
	{
		if(CreateParameter_Parametertype.isDisplayed())
			Thread.sleep(1000);
		CreateParameter_Parametertype.click();
		//calling the method ajax auto select
		AjaxAuto_select(CreateParameter_Paramtertype_List,Select_Parameter);//method(WebElement name , value)
		

	}
	
	//
	//enter the template name

	public void CreateParameter_Uniquename(String textbox) 
	{
		
		if(CreateParameter_Uniquename.isDisplayed())
			
			//calling the method settext	
		Set_Text(CreateParameter_Uniquename, textbox);
		
		System.out.println("Entered Template name is" +textbox);
		
	 }
	//enter the template name

	public void CreateParameter_Displayname(String textbox) 
	{
			
		if(CreateParameter_Displayname.isDisplayed())
				
		//calling the method settext	
		Set_Text(CreateParameter_Displayname, textbox);
			
		System.out.println("Entered Template name is" +textbox);
			
	}
	
	//enter the template name

	public void CreateParameter_Description(String textbox) 
	{
				
	     if(CreateParameter_Description.isDisplayed())
					
	     //calling the method settext	
		 Set_Text(CreateParameter_Description, textbox);
				
	     System.out.println("Entered Template name is" +textbox);
				
	}
	
	//save the template 
	public void CreateParameter_AP_Btn()

	{
		if(CreateParameter_AP_Btn.isDisplayed())
			//calling click method
		click_Method(CreateParameter_AP_Btn);
		System.out.println("User click on add parameter button");
	}
	
	public String get_AP_Popup_pagetitle()
	{
		String pageTitle = createParmeter_AP_Paramter_popup.getText();
		
		return pageTitle ;
		
		
	}
	
	//To verify whether the user is landing on the proper page or not
	public void CreateParameter_AP_Parameter_Popup_pagetitle_verify()
	{
			try{
				String expectedTitle = "Select Parameters";
			
				
					
					
					System.out.println("Page Title is"+expectedTitle);
				
				
			}catch(Exception e){

		      throw new AssertionError("A clear description of the failure", e);
			}
			
	}
	
	public void CreateParameter_Allpopup_Radiobutton1(String Searchname) throws InterruptedException
	{
		Popup_table_Search(CreateParameter_Popup_search,Searchname);
		Radiobutton_click(CreateParameter_popup_Raiobutton);
		
	}
	
	public void CreateParameter_LPpopup_Radiobutton2(String Searchname1) throws InterruptedException
	{
		Popup_table_Search(CreateParameter_Popup_search,Searchname1);
		Radiobutton_click(CreateParamter_LPpopup_Raiobutton);
		
	}
	
	
	public void CreateParameter_FCPopup_Radiobutton(String Searchname) throws InterruptedException
	{
		Popup_table_Search(CreateParameter_Popup_search,Searchname);
		Radiobutton_click(CreateParameter_FC_Raiobutton);
	}
	
	public void CreateParameter_AP_Submit()

	{
		//if(CreateParameter_AP_Submit.isDisplayed())
			//calling click method
		click_Method(CreateParameter_AP_Submit);
		System.out.println("User click on Submit button");
	}
	
	public void CreateParamete_Weight1strow(String weight1)
	{
		Set_Number(CreateParamete_Weight1strow,weight1);
	}
	
	public void CreateParamete_Weight2ndrow(String weight2)
	{
		Set_Number(CreateParameter_Weight2ndrow,weight2);
	}
	
	//click on add question 
	public void CreateParameter_LP_Btn()

	{
		//if(CreateParameter_LP_Btn.isDisplayed())
			//calling click method
		click_Method(CreateParameter_LP_Btn);
		System.out.println("User click on add Question button");
	}
	
	public String get_LP_Popup_pagetitle()
	{
		String pageTitle = createParmeter_LP_Paramter_popup.getText();
		System.out.println("ttt"+pageTitle);
		return pageTitle ;
		
		
	}
	
	//To verify whether the user is landing on the proper LP page or not
	public void LPPopup_pagetitle_verify( )
	{
		driver.switchTo().activeElement();
		System.out.println("ttt"+get_LP_Popup_pagetitle());
			try{
				
				
				String expectedTitle = "Select Questions";
				if(expectedTitle.equals(get_LP_Popup_pagetitle()))
				{
					System.out.println("Page Title is"+expectedTitle);
				}
				
			}catch(Exception e){

		      throw new AssertionError("A clear description of the failure", e);
			}
			
	}
	
	public void CreateParameter_LP_Submit()

	{
		//if(CreateParameter_LP_Submit.isDisplayed())
			//calling click method
		click_Method(CreateParameter_LP_Submit);
		System.out.println("User click on Submit button");
	}
	
	
	//click on FC 
	public void CreateParameter_FC_Btn()

	{
		//if(CreateParameter_FC_Btn.isDisplayed())
			//calling click method
		click_Method(CreateParameter_FC_Btn);
		System.out.println("User click on Functional map button");
	}
	
	public String get_Fmap_Popup_pagetitle()
	{
		String pageTitle = createParmeter_FC_Paramter_popup.getText();
		
		return pageTitle ;
		
		
	}
	
	//To verify whether the user is landing on the proper LP page or not
	public void FmapPopup_pagetitle_verify( )
	{
			try{
				String expectedTitle = "Select Functional Map";
				if(expectedTitle.equals(get_Fmap_Popup_pagetitle()))
				{
					System.out.println("Page Title is"+expectedTitle);
				}
				
			}catch(Exception e){

		      throw new AssertionError("A clear description of the failure", e);
			}
			
	}
	
	
	
	public void CreateParameter_FC_Submit()

	{
		//if(CreateParameter_FC_Submit.isDisplayed())
			//calling click method
		click_Method(CreateParameter_FC_Submit);
		System.out.println("User click on Submit button");
	}
	
	
	public void CreateParameter_Level(String BCMLevel)
	{
		Dropdown_Text(CreateParameter_Level,BCMLevel);
	}
	
	public void CreateParameter_submitbtn()

	{
		//if(CreateParamete_submitbtn.isDisplayed())
			//calling click method
		click_Method(CreateParamete_submitbtn);
		System.out.println("User click on Submit button");
	}
	
	
}

