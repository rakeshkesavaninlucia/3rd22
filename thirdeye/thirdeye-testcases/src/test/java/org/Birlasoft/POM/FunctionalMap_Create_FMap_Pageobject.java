package org.Birlasoft.POM;




import org.Birlasoft.Utility.Util;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class FunctionalMap_Create_FMap_Pageobject extends Util
{
	
	public FunctionalMap_Create_FMap_Pageobject(WebDriver driver)
    {
		
		PageFactory.initElements(driver,this);
		
	}
	
	//Functional Map Page title
	@FindBy(xpath = "//h1/div/span[contains(text(),'Create Functional Map')]")
	public WebElement Create_FunctionalMap_Fmap_Pagetitle;
	
	//Fmap Name 
	@FindBy(xpath = "//tr/td/input[@id='name']")
	public WebElement Create_FunctionalMap_Fmap_Enter_Name;
	
	
	// Click Business Capability Map from dropdown
	@FindBy(id = "select2-bcmId-container")
	public WebElement Create_FunctionalMap_Click_BCM;
	
	// Choose Business Capability Map from dropdown
	@FindBy(id = "select2-bcmId-results")
	public WebElement Create_FunctionalMap_Choose_BCM;
	
	
	// Choose Asset Template from dropdown
	@FindBy(id = "select2-assetTemplateId-container")
	public WebElement Create_FunctionalMap_Click_Asset_Template;
	
	// Choose Asset Template from dropdown
	@FindBy(id = "select2-assetTemplateId-results")
	public WebElement Create_FunctionalMap_Choose_Asset_Template;
	
	
	// Click Type(L1,L2,L3 and L4) from dropdown
	@FindBy(id = "select2-type-container")
	public WebElement Create_FunctionalMap_Click_Level;
	
	// Choose Type(L1,L2,L3 and L4) from dropdown
	@FindBy(id = "select2-type-results")
	public WebElement Create_FunctionalMap_Choose_Level;
	
	
	// Click Question from dropdown
	@FindBy(id = "select2-questionId-container")
	public WebElement Create_FunctionalMap_Click_Question;
	
	// Choose Question from dropdown
	@FindBy(id = "select2-questionId-results")
	public WebElement Create_FunctionalMap_Choose_Question;
	
	
	//Fmap_save_Button
	@FindBy(xpath = "//input[@type='submit']")
	public WebElement Create_FunctionalMap_Submit;
	
	
	public String get_pagetitle()
	{
		String pageTitle = Create_FunctionalMap_Fmap_Pagetitle.getText();
		
		return pageTitle ;
		
	}
	
	//To verify whether the user is landing on the proper page or not
	public void CreateFMap_pagetitle_verify()
	{
			try{
				String expectedTitle = "Create Functional Map";
				if(expectedTitle.equals(get_pagetitle()))
				{
					System.out.println("Page Title is"+expectedTitle);
				}
				
			}catch(Exception e){

		      throw new AssertionError("A clear description of the failure", e);
			}
			
	}
	
	//To verify whether the user able to enter Fmap name or not
	public void Create_FunctionalMap_Fmap_Enter_Name(String textbox) 
	{
		
		if(Create_FunctionalMap_Fmap_Enter_Name.isDisplayed())
			
			//calling the method settext	
		Set_Text(Create_FunctionalMap_Fmap_Enter_Name, textbox);
		
		System.out.println("Entered Template name is" +textbox);
		
	 }
	
	// To verify whether the user able to choose BCM from dropdown
	public void Create_FunctionalMap_Choose_BCM(String BCM) throws InterruptedException
	{
		if(Create_FunctionalMap_Click_BCM.isDisplayed())
			Thread.sleep(1000);
		click_Method(Create_FunctionalMap_Click_BCM);
		//calling the method ajax auto select
		AjaxAuto_select(Create_FunctionalMap_Choose_BCM,BCM);//method(WebElement name , value)
		

	}
	
	
	// To verify whether the user able to choose Asset Template from dropdown
	public void Create_FunctionalMap_Choose_Asset_Template(String Asset_Template) throws InterruptedException
	{
		if(Create_FunctionalMap_Click_Asset_Template.isDisplayed())
			Thread.sleep(1000);
		click_Method(Create_FunctionalMap_Click_Asset_Template);
		//calling the method ajax auto select
		AjaxAuto_select(Create_FunctionalMap_Choose_Asset_Template,Asset_Template);//method(WebElement name , value)
		

	}
	
	
	// To verify whether the user able to choose Level type  from dropdown
	public void Create_FunctionalMap_Choose_Level(String Level_Type) throws InterruptedException
	{
		if(Create_FunctionalMap_Click_Level.isDisplayed())
			Thread.sleep(1000);
		click_Method(Create_FunctionalMap_Choose_Level);
		//calling the method ajax auto select
		AjaxAuto_select(Create_FunctionalMap_Choose_Level,Level_Type);//method(WebElement name , value)
		

	}
	
	
	// To verify whether the user able to choose Question from dropdown
	public void Create_FunctionalMap_Choose_Question(String Question_Name) throws InterruptedException
	{
		if(Create_FunctionalMap_Click_Question.isDisplayed())
			Thread.sleep(1000);
		click_Method(Create_FunctionalMap_Click_Question);
		//calling the method ajax auto select
		AjaxAuto_select(Create_FunctionalMap_Choose_Question,Question_Name);//method(WebElement name , value)
		

	}
	
	//To verify whether the user able to submit or not
	public void CreateParameter_submitbtn()

	{
		//if(CreateParamete_submitbtn.isDisplayed())
			//calling click method
		click_Method(Create_FunctionalMap_Submit);
		System.out.println("User click on Submit button");
	}
	

}
