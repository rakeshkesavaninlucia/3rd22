package org.Birlasoft.POM;

import org.Birlasoft.Utility.Util;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class View_Questionnaire extends Util {

	public  View_Questionnaire (WebDriver driver) 
	{
			
			PageFactory.initElements(driver,this);
			
		}
		//objects of Question Page
		
		
	     
	       
	       @FindBy(xpath = "//*[@id='templateColumnsofview_filter']/label/input")
		     public WebElement ViewQuestionnaire_Search;
	       
	       @FindBy(xpath = "//*[@id='templateColumnsofview_filter']/label/input")
		     public WebElement ViewQuestionnaire_Search_input;
	       
	       @FindBy(xpath ="//*[@id='173']/td[4]/a")
			WebElement Action_click;
	       
	       
	    public void Click_search()
	       {
	    	   
	    	   click_Method(ViewQuestionnaire_Search);
           }
	       
	     
	       public void Click_input(String Search) throws InterruptedException
	       {
	    	   Webtable_Search(ViewQuestionnaire_Search_input,Search);
	    	  
           } 
	      
	       public void Click_Action()
	       {
	    	   
	    	   click_Method(Action_click);
           }
	   
	       
}
