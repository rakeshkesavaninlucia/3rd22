package org.Birlasoft.POM;

import org.Birlasoft.Utility.Util;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Submit_Cost_of_Ownership extends Util{

	public  Submit_Cost_of_Ownership(WebDriver driver) 
	{
			
			PageFactory.initElements(driver,this);
			
		}
		
	//Handling Cost structure modal popup
		
		     
	
	@FindBy(xpath = "//span[contains(text(),'Submit Cost of Ownership')]")
    public WebElement Page_name;
	
	@FindBy(xpath = "//*[@id='uniqueName']")
    public WebElement Unique_name_text;
	              
	@FindBy(xpath = "//*[@id='displayName']")
    public WebElement Display_name_text;   
	   
	@FindBy(xpath = "//*[@id='description']")
    public WebElement Desc_text;
	
	@FindBy(xpath = "//input[@value='Create Cost Structure']")
    public WebElement Create_button;
	
	 	
	public String get_PageTitle() {
  	    	   
  	    	   String pageTitle = Page_name.getText();
  	   		
  	   		return pageTitle ;
  	   		
  	       }
	       
	       
	    //To verify whether the user is landing on the proper page or not
	   	public void pageetitle_verify()
	   	{
	   		try{
	   			String expectedTitle ="Submit Cost of Ownership";
	   			if(expectedTitle.equals(get_PageTitle()))
	   			{
	   				System.out.println("Page Title is "+expectedTitle);
	   			}
	   			
	   		}catch(Exception e){

	   	      throw new AssertionError("A clear description of the failure", e);
	   		}
	   		
	   	}
	      
	   		
	    public void Dname_Cost_Struct() 
	   	{
	   		   	}

	   	
	   	
	
}
