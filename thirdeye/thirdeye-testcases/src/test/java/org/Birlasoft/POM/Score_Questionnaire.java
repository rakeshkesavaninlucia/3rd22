package org.Birlasoft.POM;

import org.Birlasoft.Utility.Util;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Score_Questionnaire extends Util{

	public  Score_Questionnaire(WebDriver driver) 
	{
			
			PageFactory.initElements(driver,this);
			
		}
		//objects of Question Page
		
		
	     
	@FindBy(xpath = "/html/body/div/div/section[1]/h1/div/span")
    public WebElement verify_page_title;
	
	@FindBy(xpath = "//*[@class='editableParameter'][@name='qq_1539']")
    public WebElement score_one;
    
	       
public String get_PageTitle() {
  	    	   
  	    	   String pageTitle = verify_page_title.getText();
  	   		
  	   		return pageTitle ;
  	   		
  	       }
	       
	       
	    //To verify whether the user is landing on the proper page or not
	   	public void pageetitle_verify(String expectedTitle)
	   	{
	   		try{
	   			 
	   			if(expectedTitle.equals(get_PageTitle()))
	   			{
	   				System.out.println("Page Title is "+expectedTitle);
	   			}
	   			
	   		}catch(Exception e){

	   	      throw new AssertionError("A clear description of the failure", e);
	   		}
	   		}
	   //giving scores to the questions
	   	
	   public void Score_Questionnaire(String text)
	   {
		 
		   Dropdown_text(score_one, text);
	   }
	   	

}
	
	

