package org.Birlasoft.POM;

import org.Birlasoft.Utility.Util;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Create_Questionnaire extends Util{

	public  Create_Questionnaire(WebDriver driver) 
	{
			
			PageFactory.initElements(driver,this);
			
	}
		//objects of Question Page
	       
	       @FindBy(xpath = "//*[@id='name']")
		     public WebElement Questionnaire_name;
	       
	       @FindBy(xpath = "//*[@id='description']")
			 WebElement Description;
	       
	       @FindBy(xpath = "//*[@id='assetType.id']")
			 WebElement select_asset_type;
	       
	       @FindBy(xpath = "//div[@class='box-footer']/input")
			 WebElement click_next;
	       
	     	       
	       
	       
	       public void get_Name(String Questionnaire_title) 
	       {
	    	   fn_WaitForVisibility(Questionnaire_name,2);
	    	   Questionnaire_name.sendKeys(Questionnaire_title);
	       }
	  
	       public void get_Desc(String Questionnaire_Desc) 
	       {
	    	   Description.sendKeys(Questionnaire_Desc);
	       }
	       
	       public void select_asset_type() 
	       {
				click_Method(select_asset_type); 
			} 
		  
			public void select_asset_type(String value) 
			{
				Dropdownselect(select_asset_type,value);
	   		}
		    
	       public void get_Next() 
	       {
	    	   click_Method(click_next);
	       }
	       
	       @FindBy(xpath="//span[contains(text(),'Create a New Questionnaire')]")
	       public WebElement questionnaireTitle;
	       
	       @FindBy(xpath="//span[contains(text(),'Questionnaire Name already Exists')]")
	       public WebElement questionniareRedundancy;
	       
	       public void questionniareTitleVerify(String title)
	       {
	    	   waitForText(questionnaireTitle,3,title);
	       }
	       
	       public void questionniareRedundancy()
	       {
	    	   fn_WaitForVisibility(questionniareRedundancy,2);
				if(questionniareRedundancy.isDisplayed())
				{
					String Msg=questionniareRedundancy.getText();
					System.out.println(""+Msg);
				}else
				{
					System.out.println("Questionniare saved Successfully");
				}
	       }
	       
	       public void questioniareSubmit()
	       {
	    	   String startUrl = driver.getCurrentUrl();
	    	   click_Method(click_next);
	    	   if(driver.getCurrentUrl()==startUrl)
				{
	    		questionniareRedundancy();
				WaitforUrl(5,startUrl);
				}
				else
				{
				 System.out.println("Questionniare saved Successfully");
				}
	       }
	
}
