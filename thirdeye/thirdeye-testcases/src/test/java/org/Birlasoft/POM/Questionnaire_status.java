package org.Birlasoft.POM;

import org.Birlasoft.Utility.Util;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Questionnaire_status extends Util{

	public  Questionnaire_status (WebDriver driver) 
	{
			
			PageFactory.initElements(driver,this);
			
		}
		
	
	
	        @FindBy(xpath = "/html/body/div/div/section[1]/h1/div/div/span")
             public WebElement Status_Questionnaire;
	
	        @FindBy(xpath = "//*[@id='mod-module-questionnaireStatus-1']/div/div[2]/span")
		    public WebElement Verify_Status;
	       
	        @FindBy(xpath = "//a[contains(text(),'Editable')]")
		    public WebElement Change_status_to_editable;
	        
	        @FindBy(xpath = "//*[@id='mod-module-questionnaireStatus-1']/div/div[2]/span")
		    public WebElement Check_Changed_status_to_editable;
	        
	        @FindBy(xpath = "//a[contains(text(),'Published')]")
		    public WebElement Changed_status_to_pub;
	        
	        @FindBy(xpath = "//h1/div/a")
		    public WebElement Click_on_back_arrow;
	        
	       

	       
             public String get_PageTitle() {
  	    	   
  	    	   String pageTitle = Status_Questionnaire.getText();
  	   		
  	   		return pageTitle ;
  	   		
  	       }
	       
	       
	    //To verify whether the user is landing on the proper page or not
	   	public void pageetitle_verify(String expectedTitle)
	   	{
	   		try{
	   			
	   			if(expectedTitle.equals(get_PageTitle()))
	   			{
	   				System.out.println("Page Title is"+expectedTitle);
	   			}
	   			
	   		}catch(Exception e){

	   	      throw new AssertionError("A clear description of the failure", e);
	   		}
	   		
	   	}
	       
	   
	   //check the status of questionnaire
	   	
	      public String Check_status() throws InterruptedException
	      {
	    	  String Status = Verify_Status.getText();
	    	  return  Status; 
	    	  
	    	  
	   			} 
	     
	    //Verifying the status	  
	      public void Print_status(String expectedTitle) throws InterruptedException
	      {
	    	  
	    	  try{
		   			
		   			if(expectedTitle.equals(Check_status()))
		   			{
		   				System.out.println("Questionnaire Status is "+expectedTitle);
		   			}
		   			
		   		}catch(Exception e){

		   	      throw new AssertionError("A clear description of the failure", e);
		   		}
	    } 
	     
	       
	       	      

 public void Change_Questionnaire_status() throws InterruptedException {
	 click_Method(Change_status_to_editable);
 } 	   
	    	   
	
 public String Check_change_Questionnaire_status() throws InterruptedException {
	 String Status1 = Verify_Status.getText();
	  return  Status1; 
	 }
 
 public void Print_status1(String expectedTitle1) throws InterruptedException
 {
	  
	  try{
  			
  			if(expectedTitle1.equals(Check_status()))
  			{
  				System.out.println("Questionnaire Status is "+expectedTitle1);
  			}
  			
  		}catch(Exception e){

  	      throw new AssertionError("A clear description of the failure", e);
  		}
}
	
 public void Questionnaire_status_Published() throws InterruptedException {
	 click_Method(Changed_status_to_pub);
	 Thread.sleep(1000);
	 this.Check_status();
	 Thread.sleep(1000);
	 this.Print_status("PUBLISHED");
	 click_Method(Click_on_back_arrow);
 }

}
	
