package org.Birlasoft.POM;

import java.util.List;

import org.Birlasoft.Utility.Util;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Select_CostStructure_Modal extends Util {

	public  Select_CostStructure_Modal(WebDriver driver) 
	{
			
			PageFactory.initElements(driver,this);
			
		}
		
	//objects of Question Page
		
		
	     
	@FindBy(xpath = "//span[contains(text(),'Select Cost Structures')]")
    public WebElement verify_page_title;      
	
	@FindBy(xpath = "//*[@id='DataTables_Table_1']/tbody/tr")
    public List<WebElement> SearchCost_Structure;
	
	
	@FindBy(xpath = "//*[@id='parameterSelectorId']/div[3]/input")
	public WebElement AvaialbleCostStructure_ActionIcon;
	
	@FindBy(xpath = "//a[contains(text(),'Next')][@class='btn btn-default']")
	public WebElement Click_on_next;
	              
	       
	       
public String get_PageTitle() {
  	    	   
  	    	   String pageTitle = verify_page_title.getText();
  	   		
  	   		return pageTitle ;
  	   		
  	       }
	       
	       
	    //To verify whether the user is landing on the proper page or not
	   	public void pageetitle_verify()
	   	{
	   		try{
	   			String expectedTitle ="Select Cost Structures";
	   			if(expectedTitle.equals(get_PageTitle()))
	   			{
	   				System.out.println("Page Title is "+expectedTitle);
	   			}
	   			
	   		}catch(Exception e){

	   	      throw new AssertionError("A clear description of the failure", e);
	   		}
	   		}
	       
	   	
	   	
	   	public void Cost_StructureModal() throws InterruptedException {
	   		Popup_table_close(verify_page_title);
	   		
	   		}   	
	   	
	   /*	public void Click_Cost_Struct(String search) 
	   	{
	   		
	   		//click_Method(ClickCost_Structure);
	   		SearchCost_Structure.sendKeys(search);
	   	}*/
	   		
	   	public void Click_Cost_Struct(String cost_ele_table) throws InterruptedException {
   			
	  		Popup_Table_CheckBox(SearchCost_Structure, cost_ele_table );
		   	}
	    
	   	
	   	
	   	
	   	public void AvaialbleCostStructure_ActionIcon()
	   	{
	   		
	   		click_Method(AvaialbleCostStructure_ActionIcon);
	   		
	   		}
	   	
	   	public void Click_next_button()
	   	{
	   		
	   		click_Method(Click_on_next);
	   		
	   		}
	
}
