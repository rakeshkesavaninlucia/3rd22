package org.Birlasoft.POM;

import org.Birlasoft.Utility.Util;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Create_Question extends Util {

public  Create_Question(WebDriver driver) 
	{
		
		PageFactory.initElements(driver,this);
		
	}
	

         //objects of Question Page
	
	
     
       
       @FindBy(id = "select2-userWorkSpaceId-container")
	     public WebElement workspace;
       
       @FindBy(xpath = "//*[@id='select2-userWorkSpaceId-results']")
		 WebElement workspace_1;
     
       @FindBy(id = "select2-questionMode-container")
		 WebElement mode;
       
       @FindBy(id = "select2-questionMode-results")
		 WebElement mode1;
     
       @FindBy(xpath = "//span[starts-with(@id,'select2-questionCategory-')]")
		 WebElement category;
       
       @FindBy(xpath = "//ul[starts-with(@id,'select2-questionCategory-')]")
		 WebElement category1;
       
              
       @FindBy(xpath = "//*[@id='displayName']")
		 WebElement displayName;
       
       @FindBy(xpath = "//*[@id='title']")
		 WebElement title;
       
       @FindBy(xpath = "//*[@id='helpText']")
		 WebElement helpText;
       
       @FindBy(xpath = "//*[@id='select2-questionType-container']")
		 WebElement Question_type;
       
       @FindBy(id="select2-questionType-results")
		 WebElement Question_type1;
       
     /*  @FindBy(xpath = "//input[@name='min']")
		 WebElement Numbermin;
       
       @FindBy(xpath = "//input[@name='max']")
		 WebElement Numbermax;
		 */
		 
       @FindBy(xpath = "//*[@id='mod-module-createQuestion-1']/form/div/input")
	     WebElement done_button;
       
       @FindBy(xpath="//input[@data-type='addOption']")
         WebElement Add_Option;
       
       @FindBy(xpath="//div[@id='getFragment']/div[1]/div[1]/input[@type='text']")
         WebElement option1;
       
       @FindBy(xpath="//div[@id='getFragment']/div[1]/div[1]/input[@type='number']")
         WebElement option1_score;
       
       @FindBy(xpath="//*[@id='getOption']/div[1]/div/input[@type='text']")
         WebElement option2;
       
       @FindBy(xpath="//*[@id='getOption']/div[1]/div/input[@type='number']")
         WebElement option2_score;
       
       @FindBy(xpath="//*[@id='getOption']/div[2]/div/input[@type='text']")
         WebElement option3;
       
       @FindBy(xpath="//*[@id='getOption']/div[2]/div/input[@type='number']")
         WebElement option3_score;
       
       @FindBy(xpath="//*[@id='getOption']/div[3]/div/input[@type='text']")
        WebElement option4;
       
       @FindBy(xpath="//*[@id='getOption']/div[3]/div/input[@type='number']")
        WebElement option4_score;
       
       @FindBy(xpath="//*[@id='getOption']/div[4]/div/input[@type='text']")
        WebElement option5;
       
       @FindBy(xpath="//*[@id='getOption']/div[4]/div/input[@type='number']")
        WebElement option5_score;
       
       @FindBy(xpath = "//a[@class='btn btn-primary']")
	     WebElement Create_button;
      
       
       
     
       public void getWrkSpc() 
       {
     		
     		click_Method(workspace);
     		
        }
       
       

	public void getWrkSpcSelect(String workspace) 
	{
		AjaxAuto_select(workspace_1,workspace); 
		//workspace_1.click();
    		
    }
     
       public void getMode() {
    	   click_Method(mode);
    	   //mode.click();
   		
   }
       
       public void getMode1(String mode) {
    	   AjaxAuto_select(mode1,mode); 
    	   //mode.click();
      		
      }
       
       
       public void getcategory() {
    	   click_Method(category);
      		//category.click();
      		
      }
       
     
      	 public void getcategory1(String category) {
    	   AjaxAuto_select(category1,category); 
    	   //mode.click();
       }
       
       
       public void getdisName(String Display_Name) {
    		 displayName.sendKeys(Display_Name);
    		
    }
       
       public void get_title(String Question_title) {
   		title.sendKeys(Question_title);
   		
   }
       
       public void getHelpText(String Question_Title) {
      		helpText.sendKeys(Question_Title);
      		
      }
       
       public void getQuestion_type() {
    		Question_type.click();
    		
    }
      
       public void complete_creation()
	   {
    	   done_button.click();
        }
	   
	   
      public void Create_button ()
      {
      	Util.fn_CheckVisibility_Button(Create_button);
      }

	   
       public void getQuestion_type1(String Questiontype) throws InterruptedException {
    	  
    	
    	//String QuestionType = Question_type1.getText();
	   	  
    	   switch(Questiontype){  
	   	  case "Text": 
	   		    Question_type.click();
	   		    System.out.println("dropdown click");
	   		 Wait_sleep();
	   		    AjaxAuto_select(Question_type1,"Text");
	   		    System.out.println("Question Type choosen is Text");	
	   		    break;  
	   	    
	   	     case "Paragraph text": 
	   		  	Question_type.click();
	   		  	System.out.println("dropdown click");
	   		 Wait_sleep();
	   		  	AjaxAuto_select(Question_type1,"Paragraph text");
	   		    System.out.println("Question Type choosen is Paragraph text");	
   		    break;
	   	    	
	   	      
	      case "Number": 
	    	  	Question_type.click();
	    	  	System.out.println("dropdown click");
	    	  	Wait_sleep();
	   		    AjaxAuto_select(Question_type1,"Number" );
	   		    System.out.println("Question Type choosen is number");
	   		    
	   		    break;
	   		    
	      case "Date":
	    	  
	    	  	Question_type.click();
	    	  	System.out.println("dropdown click");
	    	  	Wait_sleep();
	   		    AjaxAuto_select(Question_type1,"Date" );
	   		    System.out.println("Question Type choosen is Date");	
	   		    break;	    
	   		    
	   	  
	   	  default:
	   	       System.out.println("The Status of the Chart of account is Reviewed"); 
	   	       break;
	   	    }
       }
       
       
       
       public void MCQ_Options(String options) throws InterruptedException{
    	   
    	  switch(options){  
 	   	  case "one":
 	   		Question_type.click(); 
 	   		
 	   		  
 	   	  AjaxAuto_select(Question_type1,"Multiple choice" );
 		   System.out.println("Question Type choosen is Text");
 		   System.out.println("dropdown click");
 	   		option1.sendKeys("test1");
 	   		option1_score.sendKeys("6");
 		   	Wait_sleep();
 	   		break; 
 	   		
 	   	  case "Two":
 	   		 
 	   		 Question_type.click();
 	   		AjaxAuto_select(Question_type1,"Multiple choice" );
 		    System.out.println("Question Type choosen is Text");
 		    System.out.println("dropdown click");  
 	   		option1.sendKeys("test1");
// 	   	     Util.Set_Number(option1_score,"6");
 	   		option1_score.sendKeys("6");
 		   	Wait_sleep();
 	   		Add_Option.click();
 	    	Wait_sleep();
 	   		option2.sendKeys("test2");
	   		option2_score.sendKeys("3");
		   	Wait_sleep();
 	   		
	   		break;
	   		
 	   	 case "Three":
 	   		 
 	   		 Question_type.click();
     	    AjaxAuto_select(Question_type1,"Multiple choice" );
    		   System.out.println("Question Type choosen is Text");
    		   System.out.println("dropdown click");
	   		  
  	   		option1.sendKeys("test1");
  	   		//Util.Set_Number(option1_score,"6");
  	   		option1_score.sendKeys("6");
  	    	Wait_sleep();
  	   		Add_Option.click();
  	    	Wait_sleep();
  	   	    option2.sendKeys("test2");
 	   		option2_score.sendKeys("3");
 	    	Wait_sleep();
 	   		Add_Option.click();
 	    	Wait_sleep();
	   		option3.sendKeys("test3");
	   		option3_score.sendKeys("3");
	   		break;
	   		
	   		
 	   	case "Four":
 	   		Question_type.click();
 	    	AjaxAuto_select(Question_type1,"Multiple choice" );
		   System.out.println("Question Type choosen is Text");
		   System.out.println("dropdown click"); 
  	   		option1.sendKeys("test1");
  	   		option1_score.sendKeys("6");
  	   		Wait_sleep();
  	   		Add_Option.click();
  	    	Wait_sleep();
  	   		option2.sendKeys("test2");
 	   		option2_score.sendKeys("3");
 	   		Add_Option.click();
 	     	Wait_sleep();
	   		option3.sendKeys("test3");
	   		option3_score.sendKeys("3");
	   		Add_Option.click();
	   		Wait_sleep();
	   		option4.sendKeys("test4");
	   		option4_score.sendKeys("3");
	   		break;
	   	
 	   case "Five":
 		   
 		   Question_type.click();
 		   AjaxAuto_select(Question_type1,"Multiple choice" );
		   System.out.println("Question Type choosen is Text");
		   System.out.println("dropdown click");  
 	   		option1.sendKeys("test1");
 	   		option1_score.sendKeys("6");
 	    	Wait_sleep();
 	   		Add_Option.click();
 	   	    Wait_sleep();
 	   		option2.sendKeys("test2");
	   		option2_score.sendKeys("3");
	   		Wait_sleep();
	   		Add_Option.click();
	   		Wait_sleep();
	   		option3.sendKeys("test3");
	   		option3_score.sendKeys("3");
	   		Wait_sleep();
	   		Add_Option.click();
	   		Wait_sleep();
	   		option4.sendKeys("test4");
	   		option4_score.sendKeys("3");
	   		Wait_sleep();
	   		Add_Option.click();
	   		Wait_sleep();
	   		option5.sendKeys("test5");
	   		option5_score.sendKeys("3");
	   		Wait_sleep();
	   		break;
    	   
       }
       }
       
       
   
    	   
       
       
 }
   
