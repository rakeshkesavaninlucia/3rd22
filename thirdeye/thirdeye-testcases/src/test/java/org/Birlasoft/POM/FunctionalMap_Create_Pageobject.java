package org.Birlasoft.POM;



import org.Birlasoft.Utility.Util;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class FunctionalMap_Create_Pageobject 
{
	
	public FunctionalMap_Create_Pageobject(WebDriver driver)
    {
		
		PageFactory.initElements(driver,this);
		
	}
	
	//Functional Map Page link
	@FindBy(xpath = "//h1/div/span[contains(text(),'Functional Maps')]")
	public WebElement Create_FunctionalMap_Pagetitle;
	
	
	//Search Box in the available template page
	@FindBy(xpath = "//div[@id='DataTables_Table_0_filter']/label/input[@type='search']")
	public WebElement Create_FunctionalMap_SearchBox;
	
	//Edit button for the corresponidng Functional Map
	@FindBy(xpath = "//table[@id='DataTables_Table_0']/tbody/tr[1]/td[5]/a")
	public WebElement Create_FucntionalMap_Action_Edit;
	
	//Create Functional Map page link btn
	@FindBy(xpath = "//a[contains(text(),'Create Functional Map')]")
	public WebElement Create_FunctionalMap_Page_Btn;
	
	//View Functional Map table
	@FindBy(id = "DataTables_Table_0")
	public WebElement Create_FunctionalMap_Table_Verify;
	
	
	public String get_pagetitle()
	{
		String pageTitle = Create_FunctionalMap_Pagetitle.getText();
		
		return pageTitle ;
			
	}
	
	
	//To verify whether the user is landing on the proper page or not
	public void Create_FunctionalMap_Pagetitle_verify()
	{
			try{
				String expectedTitle = "Functional Maps";
				if(expectedTitle.equals(get_pagetitle()))
				{
					System.out.println("Page Title is"+expectedTitle);
				}
				
			}catch(Exception e)
			
			{

		      throw new AssertionError("A clear description of the failure", e);
			}
			
	}
	
	
	//click on "ManageWorkspace BCM" Btn
	public void Create_FunctionalMap_Btn()
	
	{
		Util.fn_CheckVisibility_Button(Create_FunctionalMap_Page_Btn);	
		
	}
	
	//Verify the Created Functional Map on the table
	public void Available_Functional_Map_Webtable_Verify(String FunctionalMAP_Searchname) throws InterruptedException
	{
		Util.Webtable_Search(Create_FunctionalMap_SearchBox,FunctionalMAP_Searchname);
		Util.Webtable_Element_check(Create_FunctionalMap_Table_Verify, FunctionalMAP_Searchname);
		
	}
	
	
	//Verify and click on the Edit Icon
	public void Available_FunctionalMap(String FunctionalMAP_Searchname ) throws InterruptedException
	{
		Util.Webtable_Search(Create_FunctionalMap_SearchBox,FunctionalMAP_Searchname);
		Util.fn_CheckVisibility_Button(Create_FunctionalMap_Page_Btn);
	}
	

	
	


}

