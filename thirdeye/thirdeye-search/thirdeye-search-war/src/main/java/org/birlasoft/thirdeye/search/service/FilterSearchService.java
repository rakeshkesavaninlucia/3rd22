/**
 * 
 */
package org.birlasoft.thirdeye.search.service;

import java.util.List;
import java.util.Map;

import org.birlasoft.thirdeye.search.api.beans.FacetBean;
import org.birlasoft.thirdeye.search.api.beans.SearchConfig;
import org.elasticsearch.index.query.BoolQueryBuilder;

/**
 * @author shaishav.dixit
 *
 */
public interface FilterSearchService {

	public List<FacetBean> getFacetAndFilter(SearchConfig searchConfig);
	
	public BoolQueryBuilder extractBoolFilterQuery(Map<String, List<String>> filterMap);

}
