package org.birlasoft.thirdeye.search.service;

import org.birlasoft.thirdeye.search.api.beans.AssetParameterWrapper;
import org.birlasoft.thirdeye.search.api.beans.SearchConfig;

public interface ParameterEvaluatorSearchService {

	public AssetParameterWrapper getParameterValueForAssets(SearchConfig searchConfig);

}
