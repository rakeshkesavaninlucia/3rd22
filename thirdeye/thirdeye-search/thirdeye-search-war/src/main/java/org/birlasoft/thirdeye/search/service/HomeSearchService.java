package org.birlasoft.thirdeye.search.service;

import org.birlasoft.thirdeye.search.api.beans.AssetClassSearchBean;
import org.birlasoft.thirdeye.search.api.beans.AssetParameterWrapper;
import org.birlasoft.thirdeye.search.api.beans.ParameterSearchBean;
import org.birlasoft.thirdeye.search.api.beans.SearchConfig;

public interface HomeSearchService {

	public AssetParameterWrapper getAssetValueForParameter(SearchConfig searchConfig);

	public ParameterSearchBean getParameterAggregateValue(SearchConfig searchConfig);

	public AssetClassSearchBean getAssetClassDetailsForHomeScreen(SearchConfig searchConfig);

}
