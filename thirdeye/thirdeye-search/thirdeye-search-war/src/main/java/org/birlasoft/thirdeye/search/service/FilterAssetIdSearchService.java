package org.birlasoft.thirdeye.search.service;

import java.util.List;
import org.birlasoft.thirdeye.search.api.beans.SearchConfig;

/**
 * @author shagun.sharma
 *
 */
public interface FilterAssetIdSearchService {
	
	/**
	 * @param searchConfig
	 * @return list of filtered assetid
	 */
	public List<Integer> getAssetIds(SearchConfig searchConfig);
}
