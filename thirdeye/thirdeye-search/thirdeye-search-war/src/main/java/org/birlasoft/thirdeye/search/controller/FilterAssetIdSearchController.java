package org.birlasoft.thirdeye.search.controller;

import org.birlasoft.thirdeye.search.api.beans.SearchConfig;
import org.birlasoft.thirdeye.search.service.FilterAssetIdSearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;

@RestController
public class FilterAssetIdSearchController extends BaseSearchController {
	
	@Autowired
	private FilterAssetIdSearchService filterAssetIdSearchService;
	
	@RequestMapping( value = "/asset/id", method = RequestMethod.POST )
	public List<Integer> getAssetIds(@RequestBody SearchConfig searchConfig){		
		return filterAssetIdSearchService.getAssetIds(searchConfig);
	}
	
}
