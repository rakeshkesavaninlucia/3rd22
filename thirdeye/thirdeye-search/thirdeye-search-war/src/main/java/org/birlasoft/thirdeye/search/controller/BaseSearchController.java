package org.birlasoft.thirdeye.search.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/rest/search")
public abstract class BaseSearchController {
	
}
