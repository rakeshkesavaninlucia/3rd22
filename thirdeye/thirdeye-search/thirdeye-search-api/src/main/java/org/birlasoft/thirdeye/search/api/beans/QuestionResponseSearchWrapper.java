/**
 * 
 */
package org.birlasoft.thirdeye.search.api.beans;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

/**
 * @author shaishav.dixit
 *
 */
public class QuestionResponseSearchWrapper {
	
	private Integer questionId;
	private String displayName;
	private Map<Integer, BigDecimal> assetResponse = new HashMap<>();
	
	public Integer getQuestionId() {
		return questionId;
	}
	public void setQuestionId(Integer questionId) {
		this.questionId = questionId;
	}
	public String getDisplayName() {
		return displayName;
	}
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
	
	public void addAssetEvaluation (Integer ab, BigDecimal d){
		if (ab != null){
			assetResponse.put(ab, d);
		}
	}
	
	public BigDecimal fetchQuestionValueForAsset(Integer ab){
		return assetResponse.get(ab);
	}
	public Map<Integer, BigDecimal> getAssetResponse() {
		return assetResponse;
	}
}
