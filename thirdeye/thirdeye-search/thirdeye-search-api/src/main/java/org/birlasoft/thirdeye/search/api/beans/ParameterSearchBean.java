package org.birlasoft.thirdeye.search.api.beans;

import java.math.BigDecimal;

public class ParameterSearchBean {
	
	private Integer id;
	private String name;
	private BigDecimal aggregate;
	private String hexColor;
	
	public String getHexColor() {
		return hexColor;
	}

	public void setHexColor(String hexColor) {
		this.hexColor = hexColor;
	}

	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public BigDecimal getAggregate() {
		return aggregate;
	}
	
	public void setAggregate(BigDecimal aggregate) {
		this.aggregate = aggregate;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof ParameterSearchBean)) {
			return false;
		}
		ParameterSearchBean other = (ParameterSearchBean) obj;
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}

}
