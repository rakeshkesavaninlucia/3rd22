package org.birlasoft.thirdeye.search.api.constant;

/**
 * Standard tag keys that are going to be used in the elastic search JSON objects
 * 
 * @author dhruv.sood
 *
 */
public enum IndexBCMTags {

	
	BCMID("bcmId"),	
	BCMNAME("bcmName"),
	BCMLEVELS("bcmLevels");
	
	String tagKey;
	
	IndexBCMTags (String tagKey){
		this.tagKey = tagKey;
	}

	public String getTagKey() {
		return tagKey;
	}
	
}