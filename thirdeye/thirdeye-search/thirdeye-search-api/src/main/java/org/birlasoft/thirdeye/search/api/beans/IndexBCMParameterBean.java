package org.birlasoft.thirdeye.search.api.beans;

import java.util.HashSet;
import java.util.Set;

/**
 * @author dhruv.sood
 *
 */
public class IndexBCMParameterBean {
		
	private Integer parameterId;
	private Integer questionnaireId;
	private String displayName;
	private String uniqueName;
	private String type;
	private Set<IndexBCMAssetValueBean> values =  new HashSet<>();
	
	public Integer getParameterId() {
		return parameterId;
	}
	public void setParameterId(Integer parameterId) {
		this.parameterId = parameterId;
	}
	public String getDisplayName() {
		return displayName;
	}
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
	public String getUniqueName() {
		return uniqueName;
	}
	public void setUniqueName(String uniqueName) {
		this.uniqueName = uniqueName;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public Set<IndexBCMAssetValueBean> getValues() {
		return values;
	}
	public void setValues(Set<IndexBCMAssetValueBean> values) {
		this.values = values;
	}
	public Integer getQuestionnaireId() {
		return questionnaireId;
	}
	public void setQuestionnaireId(Integer questionnaireId) {
		this.questionnaireId = questionnaireId;
	}
}
