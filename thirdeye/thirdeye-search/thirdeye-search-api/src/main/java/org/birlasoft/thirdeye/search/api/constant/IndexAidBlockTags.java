package org.birlasoft.thirdeye.search.api.constant;

/**
 * Standard tag keys that are going to be used in the elastic search JSON objects
 * 
 * @author samar.gupta
 *
 */
public enum IndexAidBlockTags {
	
	ID("id"),
	TITLE("title"),
	SEQUENCENUMBER("sequenceNumber"),
	SUBBLOCKS("subBlocks");
	
	
	String tagKey;
	
	IndexAidBlockTags (String tagKey){
		this.tagKey = tagKey;
	}

	public String getTagKey() {
		return tagKey;
	}

	
	
}
