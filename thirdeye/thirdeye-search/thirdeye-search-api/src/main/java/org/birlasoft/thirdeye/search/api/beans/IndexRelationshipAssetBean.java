package org.birlasoft.thirdeye.search.api.beans;

import java.util.ArrayList;
import java.util.List;

/**
 * Bean class for relationship indexing
 * @author samar.gupta
 *
 */
public class IndexRelationshipAssetBean {
	
	private Integer assetId;
    private String assetName; 
    private String assetStyle;
    private List<IndexRelationshipAssetDataBean> relationshipAssetData = new ArrayList<>();
    
	public Integer getAssetId() {
		return assetId;
	}
	public void setAssetId(Integer assetId) {
		this.assetId = assetId;
	}
	public String getAssetName() {
		return assetName;
	}
	public void setAssetName(String assetName) {
		this.assetName = assetName;
	}
	public List<IndexRelationshipAssetDataBean> getRelationshipAssetData() {
		return relationshipAssetData;
	}
	public void setRelationshipAssetData(List<IndexRelationshipAssetDataBean> relationshipAssetData) {
		this.relationshipAssetData = relationshipAssetData;
	}
	public String getAssetStyle() {
		return assetStyle;
	}
	public void setAssetStyle(String assetStyle) {
		this.assetStyle = assetStyle;
	}
}
