package org.birlasoft.thirdeye.search.api.beans;

import java.util.List;


public class IndexQuestionBean {
	
	private Integer id;
	private String displayName;	
    private List<IndexQuestionValueTypeBean> values;   
  
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getDisplayName() {
		return displayName;
	}
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}   
	
	public List<IndexQuestionValueTypeBean> getValues() {
		return values;
	}
	public void setValues(List<IndexQuestionValueTypeBean> values) {
		this.values = values;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		IndexQuestionBean other = (IndexQuestionBean) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
    
	
}
