package org.birlasoft.thirdeye.search.api.constant;

/**
 * Enum for asset relationship of parents and children index tag
 * Standard tag keys that are going to be used in the elastic search JSON objects
 * @author samar.gupta
 *
 */
public enum IndexAssetParentChildTags {

	
	PARENTS("parents"),
	CHILDREN("children");
	
	String tagKey;
	
	IndexAssetParentChildTags (String tagKey){
		this.tagKey = tagKey;
	}

	public String getTagKey() {
		return tagKey;
	}
	
}
