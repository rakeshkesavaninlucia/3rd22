package org.birlasoft.thirdeye.search.api.beans;

/**
 * This is the abstract bean for those objects which will fall under the tenant and
 * workspace protection of the system.
 * 
 * @author tej.sarup
 *
 */
public abstract class AbstractProtectedEntityBean {

	protected String tenantURL;
	protected int workspaceId;
	protected int id;
	
	public AbstractProtectedEntityBean() {}
	
	public AbstractProtectedEntityBean(String tenantURL, int workspaceId) {
		this.tenantURL = tenantURL;
		this.workspaceId = workspaceId;
	}
	
	public String getTenantURL() {
		return tenantURL;
	}
	
	public int getWorkspaceId() {
		return workspaceId;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	
}
