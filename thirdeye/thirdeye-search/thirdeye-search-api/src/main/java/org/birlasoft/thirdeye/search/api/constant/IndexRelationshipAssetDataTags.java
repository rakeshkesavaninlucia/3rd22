package org.birlasoft.thirdeye.search.api.constant;

/**
 * Enum for asset relationship data index tag
 * Standard tag keys that are going to be used in the elastic search JSON objects
 * @author samar.gupta
 *
 */
public enum IndexRelationshipAssetDataTags {

	RELATIONSHIPNAME("relationshipName"),
	DATATYPE("dataType"),
	FREQUENCY("frequency"),
	RELATIONSHIPDISPLAYNAME("relationshipDisplayName"),
	DIRECTION("direction");
	
	
	String tagKey;
	
	IndexRelationshipAssetDataTags (String tagKey){
		this.tagKey = tagKey;
	}

	public String getTagKey() {
		return tagKey;
	}
	
}
