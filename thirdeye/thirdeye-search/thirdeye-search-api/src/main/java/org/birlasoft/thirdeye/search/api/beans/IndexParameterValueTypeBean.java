/**
 * 
 */
package org.birlasoft.thirdeye.search.api.beans;

import java.math.BigDecimal;

/**
 * @author sunil1.gupta
 *
 */
public class IndexParameterValueTypeBean {
	
	private Integer questionnaireId;
	private String valueDate;
	private BigDecimal value;
	private String color;
	private String colorDesc;
	
	public Integer getQuestionnaireId() {
		return questionnaireId;
	}
	public void setQuestionnaireId(Integer questionnaireId) {
		this.questionnaireId = questionnaireId;
	}
	
	public BigDecimal getValue() {
		return value;
	}
	public void setValue(BigDecimal value) {
		this.value = value;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public String getColorDesc() {
		return colorDesc;
	}
	public void setColorDesc(String colorDesc) {
		this.colorDesc = colorDesc;
	}
	public String getValueDate() {
		return valueDate;
	}
	public void setValueDate(String valueDate) {
		this.valueDate = valueDate;
	}
	
}
