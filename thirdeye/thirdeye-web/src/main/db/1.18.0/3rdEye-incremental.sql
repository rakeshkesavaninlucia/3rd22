-- MySQL Workbench Synchronization

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

DROP TABLE IF EXISTS `report`;
CREATE TABLE `report` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `reportName` varchar(45) NOT NULL,
  `reportType` varchar(45) NOT NULL,
  `description` text,
  `reportConfig` varchar(500) NOT NULL,
  `workspaceId` int(11) NOT NULL,
  `deletedStatus` tinyint(1) NOT NULL,
  `createdBy` int(11) NOT NULL,
  `createdDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updatedBy` int(11) NOT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `REPORT_CREATED_BY_FK_idx` (`createdBy`),
  KEY `REPORT_UPDATED_BY_FK_idx` (`updatedBy`),
  KEY `REPORT_WORKSPACEID_FK_idx_idx` (`workspaceId`),
  CONSTRAINT `REPORT_CREATED_BY_FK_idx` FOREIGN KEY (`createdBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `REPORT_UPDATED_BY_FK_idx` FOREIGN KEY (`updatedBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `REPORT_WORKSPACEID_FK_idx` FOREIGN KEY (`workspaceId`) REFERENCES `workspace` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Table structure for table `user_report`
--

DROP TABLE IF EXISTS `user_report`;
CREATE TABLE `user_report` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `reportId` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `REPORT_FK_idx_idx` (`reportId`),
  KEY `USER_FK_idx_idx` (`userId`),
  CONSTRAINT `REPORT_FK_idx` FOREIGN KEY (`reportId`) REFERENCES `report` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `USER_FK_idx` FOREIGN KEY (`userId`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



ALTER TABLE `role` 
ADD COLUMN `deleteStatus` TINYINT(1) NOT NULL DEFAULT '0' AFTER `roleDescription`;

ALTER TABLE `questionnaire` 
ADD COLUMN `deleteStatus` TINYINT(1) NOT NULL DEFAULT 0 AFTER `year`;

 
DELIMITER $$
drop procedure if exists deleteWorkspace$$
create procedure deleteWorkspace ( IN vWorkspaceId INT,OUT vres INT)
begin
DECLARE exit HANDLER FOR SQLEXCEPTION SET vres = 1;
set vres = 0;
set vres = deleteAidBlockAndAidForWorkspace(vWorkspaceId);
set vres = deleteWidgetAndDashboardForWorkspace(vWorkspaceId);
set vres = deleteQuestionnaireForWorkspace(vWorkspaceId);
set vres = deleteBenchmarkForWorkspace(vWorkspaceId);
set vres = deleteParameterForWorkspace(vWorkspaceId);
set vres = deleteQuestionForWorkspace(vWorkspaceId);
set vres =deleteAssetForWorkspace(vWorkspaceId);
set vres = deleteQualityGateForWorkspace(vWorkspaceId);
set vres = deleteFmForWorkspace(vWorkspaceId);
set vres =deleteExportLogForWorkspace(vWorkspaceId);
set vres =deleteGraphForWorkspace(vWorkspaceId);
set vres=deleteRelationshipTypeForWorkspace(vWorkspaceId);
set vres =deleteWorkspaceDashboard(vWorkspaceId);
set vres=deleteUserWorkspaceForWorkspace(vWorkspaceId);
call deletebcmForWorkspace(vWorkspaceId);
end$$

drop function if exists deleteAidBlockAndAidForWorkspace$$ 
create function deleteAidBlockAndAidForWorkspace ( vWorkspaceId INT)  returns int
begin
declare vres INT;
DECLARE EXIT HANDLER FOR SQLEXCEPTION SET vres = 1;
delete from aid_block  where aidid IN (select id from aid where workspaceId = vWorkspaceId);
delete from aid where workspaceId = vWorkspaceId;
RETURN vres;
end$$
 
drop function if exists deleteWidgetAndDashboardForWorkspace$$ 
create function deleteWidgetAndDashboardForWorkspace (vWorkspaceId INT)  returns int
begin
declare vres INT;
DECLARE EXIT HANDLER FOR SQLEXCEPTION SET vres = 1;
delete w from widget w join dashboard d on w.dashboardId = d.id where d.workspaceId = vWorkspaceId;
delete from dashboard where workspaceId = vWorkspaceId;
RETURN vres;
end$$

drop function if exists deleteQuestionnaireForWorkspace$$  
create function deleteQuestionnaireForWorkspace(vWorkspaceId INT ) returns int
begin
declare vres INT;
DECLARE EXIT HANDLER FOR SQLEXCEPTION SET vres = 1;
DELETE rd FROM response_data rd join questionnaire_question qq on rd.questionnaireQuestionId=qq.id
                                join questionnaire q on qq.questionnaireId=q.id
                                where q.workspaceId=vWorkspaceId;
                DELETE r FROM response r join questionnaire q on r.questionnaireId=q.id
        where q.workspaceId=vWorkspaceId;
                DELETE qq FROM questionnaire_question qq join questionnaire q on qq.questionnaireId=q.id
                                where q.workspaceId=vWorkspaceId;
                DELETE qa FROM questionnaire_asset qa join questionnaire q on qa.questionnaireId=q.id
                                where q.workspaceId=vWorkspaceId;
                DELETE qp FROM questionnaire_parameter qp join questionnaire q on qp.questionnaireId=q.id
                                where q.workspaceId=vWorkspaceId;
                DELETE qpa FROM questionnaire_parameter_asset qpa join questionnaire q on qpa.questionnaireId=q.id
                                where q.workspaceId=vWorkspaceId;
                DELETE fmd FROM functional_map_data fmd join questionnaire q on fmd.questionnaireId=q.id
                                where q.workspaceId=vWorkspaceId;
                DELETE q FROM questionnaire q where q.workspaceId=vWorkspaceId;
RETURN vres;
END$$
 
 
drop function if exists deleteBenchmarkForWorkspace$$  
create function deleteBenchmarkForWorkspace(vWorkspaceId INT) returns int
begin
declare vres INT;
DECLARE EXIT HANDLER FOR SQLEXCEPTION SET vres = 1;
delete bis from benchmark_item_score bis join benchmark_item bi on bis.benchmarkItemId = bi.id join benchmark b where b.workspaceId = vWorkspaceId;
delete bi from benchmark_item bi join benchmark b on bi.benchmarkId = b.id where b.workspaceId =  vWorkspaceId;
delete b from benchmark b where b.workspaceId = vWorkspaceId;
RETURN vres;
END$$
 
drop function if exists deleteParameterForWorkspace$$  
create function deleteParameterForWorkspace(vWorkspaceId INT) returns int
begin
declare vres INT;
DECLARE EXIT HANDLER FOR SQLEXCEPTION SET vres = 1;
DELETE pc FROM parameter_config pc join parameter p on pc.parameterId=p.id
                where p.workspaceId= vWorkspaceId;
 
DELETE pf FROM parameter_function pf join parameter p on pf.parentParameterId=p.id
                where p.workspaceId= vWorkspaceId;
DELETE pf FROM parameter_function pf join parameter p on pf.childparameterId=p.id
                where p.workspaceId= vWorkspaceId;
DELETE pqg FROM parameter_quality_gate pqg join parameter p on pqg.parameterId=p.id
                where p.workspaceId= vWorkspaceId;
Delete qp from questionnaire_parameter qp join parameter p on qp.parameterId = p.id where p.workspaceId =vWorkspaceId;
 
Delete qgc from quality_gate_condition qgc join parameter p on  qgc.parameterId = p.id where p.workspaceId =vWorkspaceId;
delete  from parameter where workspaceId = vWorkspaceId order by id desc;
RETURN vres;
END$$

drop function if exists deleteQuestionForWorkspace$$  
create function deleteQuestionForWorkspace(vWorkspaceId INT) returns int
begin
declare vres INT;
DECLARE EXIT HANDLER FOR SQLEXCEPTION SET vres = 1;
DELETE qq FROM questionnaire_question qq join question q on qq.questionId=q.id
                where q.workspaceId= vWorkspaceId;
DELETE pf FROM parameter_function pf join question q on pf.questionId=q.id
                where q.workspaceId= vWorkspaceId;
DELETE fm FROM functional_map fm join question q on fm.questionId=q.id
                where q.workspaceId= vWorkspaceId;
DELETE from question where workspaceId = vWorkspaceId order by id desc;
RETURN vres;
END$$
 
drop function if exists deleteAssetForWorkspace$$ 
create function deleteAssetForWorkspace(vWorkspaceId INT) returns int
begin
declare vres INT;
DECLARE EXIT HANDLER FOR SQLEXCEPTION SET vres = 1;
DELETE rad FROM relationship_asset_data rad join asset a on rad.assetId=a.id
                join asset_template ast on a.templateId=ast.id
                where ast.workspaceId=vWorkspaceId;
DELETE rad FROM relationship_asset_data rad join asset a on rad.parentAssetId=a.id
                join asset_template ast on a.templateId=ast.id
                where ast.workspaceId=vWorkspaceId;
DELETE rad FROM relationship_asset_data rad join asset a on rad.childAssetId=a.id
                join asset_template ast on a.templateId=ast.id
                where ast.workspaceId=vWorkspaceId;
DELETE qa FROM questionnaire_asset qa join asset a on qa.assetId=a.id
                join asset_template ast on a.templateId=ast.id
                where ast.workspaceId=vWorkspaceId;
DELETE qpa FROM questionnaire_parameter_asset qpa join asset a on qpa.assetId=a.id
                join asset_template ast on a.templateId=ast.id
                where ast.workspaceId=vWorkspaceId;
DELETE fmd FROM functional_map_data fmd join asset a on fmd.assetId=a.id
                join asset_template ast on a.templateId=ast.id
                where ast.workspaceId=vWorkspaceId;
DELETE ad FROM asset_data ad  join asset a on ad.assetId=a.id
                join asset_template ast on a.templateId=ast.id
                where ast.workspaceId=vWorkspaceId;
DELETE rt FROM relationship_template rt join asset_template ast on rt.assetTemplateId=ast.id
                where ast.workspaceId=vWorkspaceId;
DELETE gat FROM graph_asset_template gat join asset_template ast on gat.assetTemplateId=ast.id
                where ast.workspaceId=vWorkspaceId;
DELETE fm FROM functional_map fm join asset_template ast on fm.assetTemplateId=ast.id
                where ast.workspaceId=vWorkspaceId;
DELETE a FROM asset a join asset_template ast on a.templateId=ast.id
                where ast.workspaceId=vWorkspaceId;
DELETE atc FROM asset_template_column atc join asset_template ast on atc.assetTemplateId=ast.id
                where ast.workspaceId=vWorkspaceId;
                Delete a from aid a join asset_template at on a.assetTemplateId = at.id where at.workspaceId=vWorkspaceId;
DELETE at FROM asset_template at where at.workspaceId=vWorkspaceId;
RETURN vres;
END$$
 
drop function if exists deleteQualityGateForWorkspace$$  
create function deleteQualityGateForWorkspace(vWorkspaceId INT) returns int
begin
declare vres INT;
DECLARE EXIT HANDLER FOR SQLEXCEPTION SET vres = 1;
DELETE qgc FROM quality_gate_condition qgc join quality_gate qg on qgc.qualityGateId = qg.id  where qg.workspaceId = vWorkspaceId;
Delete pqg from parameter_quality_gate pqg join quality_gate qg on pqg.qualityGateId = qg.id where qg.workspaceId = vWorkspaceId;
DELETE qg from quality_gate qg where qg.workspaceId = vWorkspaceId;
RETURN vres;
END$$
 
drop function if exists deleteFmForWorkspace$$   
create function deleteFmForWorkspace(vWorkspaceId INT) returns int
begin
declare vres INT;
DECLARE EXIT HANDLER FOR SQLEXCEPTION SET vres = 1;
Delete fmd from functional_map_data fmd join functional_map fm on fmd.functionalMapId = fm.id where fm.workspaceId = vWorkspaceId;
Delete fm from functional_map fm where fm.workspaceId = vWorkspaceId;
RETURN vres;
END$$

drop function if exists deleteExportLogForWorkspace$$    
create function deleteExportLogForWorkspace(vWorkspaceId INT) returns int
begin
declare vres INT;
DECLARE EXIT HANDLER FOR SQLEXCEPTION SET vres = 1;
Delete el from export_log el where el.workspaceId = vWorkspaceId;
RETURN vres;
END$$

drop function if exists deleteGraphForWorkspace$$    
create function deleteGraphForWorkspace(vWorkspaceId INT) returns int
begin
declare vres INT;
DECLARE EXIT HANDLER FOR SQLEXCEPTION SET vres = 1;
Delete gat from graph_asset_template gat join graph g on gat.graphId = g.id where g.workspaceId = vWorkspaceId;
Delete g from graph g where g.workspaceId= vWorkspaceId;
RETURN vres;
END$$
 
drop function if exists deleteRelationshipTypeForWorkspace$$     
create function deleteRelationshipTypeForWorkspace(vWorkspaceId INT) returns int
begin
declare vres INT;
DECLARE EXIT HANDLER FOR SQLEXCEPTION SET vres = 1;
Delete rte from relationship_template rte join relationship_type rt on rte.relationshipTypeId = rt.id where rt.workspaceId = vWorkspaceId;
Delete rt from relationship_type rt where rt.workspaceId = vWorkspaceId;
RETURN vres;
END$$

drop function if exists deleteWorkspaceDashboard$$      
create function deleteWorkspaceDashboard(vWorkspaceId INT) returns int
begin
declare vres INT;
DECLARE EXIT HANDLER FOR SQLEXCEPTION SET vres = 1;
Delete wd from workspace_dashboard wd where wd.workspaceId = vWorkspaceId;
RETURN vres;
END$$

drop function if exists deleteUserWorkspaceForWorkspace$$       
Create function deleteUserWorkspaceForWorkspace(vWorkspaceId INT) returns int
begin
declare vres INT;
DECLARE EXIT HANDLER FOR SQLEXCEPTION SET vres = 1;
Delete uw from user_workspace uw where uw.workspaceId = vWorkspaceId;
RETURN vres;
END$$

drop procedure if exists deletebcmForWorkspace$$  
Create procedure deletebcmForWorkspace(vWorkspaceId INT)
begin
DECLARE done INT DEFAULT FALSE;
declare vres INT;
declare vbcm_id INT;
declare v_parentBcmLevelId INT;
 
DEClARE bcm_level_cursor CURSOR FOR
select parentBcmLevelId
from bcm_level b
where bcmid=vbcm_id and parentBcmLevelId is not null order by levelnumber desc;
 
DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
 
 
set vres = 0;
SELECT id
into vbcm_id
FROM bcm
where workspaceId=vWorkspaceId;
 
OPEN bcm_level_cursor;
 
 delete_parentBCMLevel: LOOP
 
 FETCH bcm_level_cursor INTO v_parentBcmLevelId;
 
 IF done THEN
      LEAVE delete_parentBCMLevel;
    END IF;

 
 delete from bcm_level where bcmid=vbcm_id and parentBcmLevelId=v_parentBcmLevelId;
 
 END LOOP delete_parentBCMLevel;
 
 CLOSE bcm_level_cursor;
 delete from bcm_level where bcmid=vbcm_id and parentBcmLevelId is null;
delete from bcm where workspaceId =vWorkspaceId;
delete from workspace where id = vWorkspaceId;
end;




SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;