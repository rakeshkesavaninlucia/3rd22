-- MySQL dump 10.13  Distrib 5.6.23, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: 3rdi
-- ------------------------------------------------------
-- Server version	5.6.24

-- To be executed after new fresh schema

-- user table 
INSERT INTO `3rdi`.`user` (`id`,`firstName`,`lastName`,`emailAddress`,`password`,`deleteStatus`,`accountExpired`,`accountLocked`,
`credentialExpired`,`accountEnabled`,`createdBy`,`updatedBy`) 
VALUES (1,'Admin','Admin','thirdeye-admin@birlasoft.com','$2a$10$2E3ud9YmM9U4EAcJd16lmusYswSzmx5DGhQ3tk6L9g4A/5eyvxCpO',0,1,1,0,0,-1,-1);

-- asset_type table
INSERT INTO `3rdi`.`asset_type`
(`id`,`assetTypeName`,`deleteStatus`,`createdBy`,`updatedBy`) 
VALUES (1,'Application',0,1,1),
(2,'Server',0,1,1),
(3,'Database',0,1,1),
(4,'Person',0,1,1),
(5,'Business Services',0,1,1),
(6,'IT Services',0,1,1),
(7,'Relationship',0,1,1);

-- category table
INSERT INTO `3rdi`.`category`
(`id`,`name`,`description`,`createdBy`,`updatedBy`)
VALUES (1,'Functional Assessment',NULL,1,1),
(2,'Application Evolution',NULL,1,1),
(3,'Technology Assessment',NULL,1,1),
(4,'Application Management',NULL,1,1),
(5,'Interface Assessment',NULL,1,1),
(6,'Server Assessment',NULL,1,1),
(7,'Cloud Readiness',NULL,1,1),
(8,'Business Benefit',NULL,1,1),
(9,'Maintenance Quotient',NULL,1,1),
(10,'Criticality Quotient',NULL,1,1),
(11,'Sourcing Alignment Index',NULL,1,1),
(12,'Utilization Quotient for Servers',NULL,1,1),
(13,'Infra Health Quotient',NULL,1,1),
(14,'Governance Quality',NULL,1,1);

-- role table
INSERT INTO `3rdi`.`role`
(`id`,`roleName`,`roleDescription`)
VALUES (1,'ROLE_APP_ADMIN',NULL),
(2,'ROLE_APP_USER',NULL),
(3,'ROLE_CONSULTANT',NULL),
(4,'ROLE_CUSTOMER',NULL);

-- user_role table
INSERT INTO `3rdi`.`user_role`
(`id`,`userId`,`roleId`)
VALUES (1,1,1);

-- workspace table
INSERT INTO `3rdi`.`workspace`
(`id`,`workspaceName`,`workspaceDescription`,`deletedStatus`,`createdBy`,`updatedBy`)
VALUES (1,'MyCust','Workspace for MyCust',0,1,1);

-- user_workspace table
INSERT INTO `3rdi`.`user_workspace`
(`id`,`userId`,`workspaceId`)
VALUES (1,1,1);

-- role_permission table
INSERT INTO `3rdi`.`role_permission`
(`ID`,`permissionname`,`roleid`)
VALUES 
(1,'APP_LOGIN',1),
(2,'USER_MODIFY',1),
(3,'USER_VIEW',1),
(4,'USER_ROLE_VIEW',1),
(5,'USER_ROLE_MODIFY',1),
(6,'TEMPLATE_VIEW',1),
(7,'TEMPLATE_MODIFY',1),
(8,'ASSET_FILLDETAILS',1),
(9,'REPORT_ASSET_GRAPH',1),
(10,'REPORT_SCATTER_GRAPH',1),
(11,'REPORT_FACET_GRAPH',1),
(12,'REPORT_PHP',1),
(13,'WORKSPACES_MODIFY',1),
(14,'WORKSPACES_VIEW',1),
(15,'WORKSPACES_VIEW_DASHBOARD',1),
(16,'WORKSPACES_MODIFY_DASHBOARD',1),
(17,'QUESTION_MODIFY',1),
(18,'QUESTION_VIEW',1),
(19,'QUESTIONNAIRE_MODIFY',1),
(20,'QUESTIONNAIRE_VIEW',1),
(21,'QUESTIONNAIRE_RESPOND',1),
(22,'AID_MODIFY',1),
(23,'AID_VIEW',1),
(24,'BCM_MODIFY',1),
(25,'BCM_VIEW',1),
(26,'RELATIONSHIP_MODIFY',1),
(27,'RELATIONSHIP_VIEW',1),
(28,'PARAMETER_MODIFY',1),
(29,'PARAMETER_VIEW',1),
(30,'FUNCTIONAL_MAP_MODIFY',1),
(31,'FUNCTIONAL_MAP_VIEW',1);


/*
-- Query: select * from asset_type_style
-- Date: 2016-06-08 10:43
*/
INSERT INTO `asset_type_style` (`id`,`asset_type_id`,`asset_colour`,`asset_image`,`workspaceId`) VALUES (1,1,'#8AC007',NULL,NULL);
INSERT INTO `asset_type_style` (`id`,`asset_type_id`,`asset_colour`,`asset_image`,`workspaceId`) VALUES (2,2,'#ADD8E6',NULL,NULL);
INSERT INTO `asset_type_style` (`id`,`asset_type_id`,`asset_colour`,`asset_image`,`workspaceId`) VALUES (3,3,'#FFFF00',NULL,NULL);
INSERT INTO `asset_type_style` (`id`,`asset_type_id`,`asset_colour`,`asset_image`,`workspaceId`) VALUES (4,4,'#FF8C00',NULL,NULL);
INSERT INTO `asset_type_style` (`id`,`asset_type_id`,`asset_colour`,`asset_image`,`workspaceId`) VALUES (5,5,'#FF8C00',NULL,NULL);
INSERT INTO `asset_type_style` (`id`,`asset_type_id`,`asset_colour`,`asset_image`,`workspaceId`) VALUES (6,6,'#FF8C00',NULL,NULL);
INSERT INTO `asset_type_style` (`id`,`asset_type_id`,`asset_colour`,`asset_image`,`workspaceId`) VALUES (7,7,'#FF8C00',NULL,NULL);