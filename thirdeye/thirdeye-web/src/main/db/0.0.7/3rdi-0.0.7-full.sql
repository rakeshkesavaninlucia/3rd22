CREATE DATABASE  IF NOT EXISTS `3rdi` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `3rdi`;
-- MySQL dump 10.13  Distrib 5.6.23, for Win32 (x86)
--
-- Host: 127.0.0.1    Database: 3rdi
-- ------------------------------------------------------
-- Server version	5.6.24

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `aid`
--

DROP TABLE IF EXISTS `aid`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aid` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `description` varchar(500) DEFAULT NULL,
  `assetTemplateId` int(11) NOT NULL,
  `aidTemplate` varchar(25) NOT NULL,
  `workspaceId` int(11) NOT NULL,
  `createdBy` int(11) NOT NULL,
  `createdDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedBy` int(11) NOT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `FK_AID_WORKSPACEID_idx` (`workspaceId`),
  KEY `FK_AID_ASSETTEMPLATID_idx` (`assetTemplateId`),
  KEY `FK_CREATEDBY_USERID_idx` (`createdBy`),
  KEY `FK_UPDATEDBY_USERID_idx` (`updatedBy`),
  CONSTRAINT `FK_AID_ASSETTEMPLATID` FOREIGN KEY (`assetTemplateId`) REFERENCES `asset_template` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_AID_WORKSPACEID` FOREIGN KEY (`workspaceId`) REFERENCES `workspace` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_CREATEDBY_USERID` FOREIGN KEY (`createdBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_UPDATEDBY_USERID` FOREIGN KEY (`updatedBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aid`
--

LOCK TABLES `aid` WRITE;
/*!40000 ALTER TABLE `aid` DISABLE KEYS */;
INSERT INTO `aid` VALUES (2,'dummy 1','dummy desc',1,'DEFAULT',1,2,'2015-09-16 07:31:01',2,'2015-09-16 07:31:01'),(3,'test AID1000','test',10,'DEFAULT',4,2,'2015-09-22 09:41:05',2,'2015-09-22 09:41:05'),(4,'dfd','',10,'DEFAULT',4,2,'2015-09-22 10:26:57',2,'2015-09-22 10:26:57'),(5,'dfd','',10,'DEFAULT',1,2,'2015-09-22 10:27:30',2,'2015-09-22 10:27:30'),(6,'test 1 AID','hellom desc',11,'DEFAULT',4,2,'2015-09-22 10:30:22',2,'2015-09-22 10:30:22'),(7,'Test AID 2','hello test aid 2',12,'DEFAULT',4,2,'2015-09-22 10:33:03',2,'2015-09-22 10:33:03'),(8,'test','',10,'DEFAULT',4,2,'2015-09-22 10:55:17',2,'2015-09-22 10:55:17'),(9,'test1','test desc',10,'DEFAULT',4,2,'2015-09-22 10:57:36',2,'2015-09-22 10:57:36'),(10,'test','',5,'DEFAULT',1,2,'2015-09-22 10:57:55',2,'2015-09-22 10:57:55'),(11,'test1','wedff',3,'DEFAULT',1,2,'2015-09-22 10:58:14',2,'2015-09-22 10:58:14'),(12,'gfg','',2,'DEFAULT',1,2,'2015-09-22 11:07:07',2,'2015-09-22 11:07:07'),(13,'test2','',1,'DEFAULT',1,2,'2015-09-22 11:21:34',2,'2015-09-22 11:21:34'),(14,'Demo AID','desv demo',5,'DEFAULT',1,2,'2015-09-22 13:26:22',2,'2015-09-22 13:26:22');
/*!40000 ALTER TABLE `aid` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `aid_block`
--

DROP TABLE IF EXISTS `aid_block`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aid_block` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `aidId` int(11) NOT NULL,
  `aidBlockType` varchar(20) NOT NULL,
  `sequenceNumber` int(11) NOT NULL,
  `blockJSONConfig` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_AIDID_AID_idx` (`aidId`),
  CONSTRAINT `FK_AIDID_AID` FOREIGN KEY (`aidId`) REFERENCES `aid` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aid_block`
--

LOCK TABLES `aid_block` WRITE;
/*!40000 ALTER TABLE `aid_block` DISABLE KEYS */;
INSERT INTO `aid_block` VALUES (33,2,'BLOCK_1',1,'{\"blockTitle\":\"block 1\",\"subBlock1\":{\"templateColumnId\":0,\"blockTitle\":\"title of subblock\",\"paramQEId\":23,\"paramId\":80,\"questionId\":0,\"questionQEId\":0},\"subBlock2\":{\"templateColumnId\":0,\"blockTitle\":null,\"paramQEId\":0,\"paramId\":0,\"questionId\":0,\"questionQEId\":0},\"subBlock3\":{\"templateColumnId\":0,\"blockTitle\":null,\"paramQEId\":0,\"paramId\":0,\"questionId\":0,\"questionQEId\":0},\"subBlock4\":{\"templateColumnId\":0,\"blockTitle\":null,\"paramQEId\":0,\"paramId\":0,\"questionId\":0,\"questionQEId\":0},\"subBlock5\":{\"templateColumnId\":0,\"blockTitle\":null,\"paramQEId\":0,\"paramId\":0,\"questionId\":0,\"questionQEId\":0},\"subBlock6\":{\"templateColumnId\":0,\"blockTitle\":null,\"paramQEId\":0,\"paramId\":0,\"questionId\":0,\"questionQEId\":0}}'),(34,2,'BLOCK_2',2,'{\"blockTitle\":\"block 2\",\"subBlock1\":{\"templateColumnId\":5,\"blockTitle\":\"\",\"paramQEId\":0,\"paramId\":0,\"questionId\":0,\"questionQEId\":0}}'),(35,2,'BLOCK_4',3,'');
/*!40000 ALTER TABLE `aid_block` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `asset`
--

DROP TABLE IF EXISTS `asset`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `asset` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `templateId` int(11) NOT NULL,
  `deleteStatus` tinyint(1) NOT NULL DEFAULT '0',
  `createdBy` int(11) NOT NULL,
  `createdDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updatedBy` int(11) NOT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `ASSET_TEMPLATE_FK_idx` (`templateId`),
  KEY `ASSET_CREATED_BY_FK_idx` (`createdBy`),
  KEY `ASSET_UPDATED_BY_FK_idx` (`updatedBy`),
  CONSTRAINT `ASSET_CREATED_BY_FK` FOREIGN KEY (`createdBy`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `ASSET_TEMPLATE_FK` FOREIGN KEY (`templateId`) REFERENCES `asset_template` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `ASSET_UPDATED_BY_FK` FOREIGN KEY (`updatedBy`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `asset`
--

LOCK TABLES `asset` WRITE;
/*!40000 ALTER TABLE `asset` DISABLE KEYS */;
INSERT INTO `asset` VALUES (1,1,0,2,'2015-08-13 17:57:34',2,'2015-08-13 17:57:34'),(2,1,0,2,'2015-08-13 18:02:00',2,'2015-08-13 18:02:00'),(3,1,0,2,'2015-08-13 18:03:32',2,'2015-08-13 18:03:32'),(4,1,0,2,'2015-08-13 18:04:29',2,'2015-08-13 18:04:29'),(5,1,0,2,'2015-08-13 18:05:52',2,'2015-08-13 18:05:52'),(6,1,0,2,'2015-08-13 18:07:35',2,'2015-08-13 18:07:35'),(7,1,0,2,'2015-08-13 18:08:51',2,'2015-08-13 18:08:51'),(8,1,0,2,'2015-08-13 18:10:23',2,'2015-08-13 18:10:23'),(9,1,0,2,'2015-08-13 18:12:38',2,'2015-08-13 18:12:38'),(10,3,0,2,'2015-08-13 18:29:57',2,'2015-08-13 18:29:57'),(11,3,0,2,'2015-08-13 18:30:19',2,'2015-08-13 18:30:19'),(12,3,0,2,'2015-08-13 18:30:59',2,'2015-08-13 18:30:59'),(13,3,0,2,'2015-08-13 18:31:48',2,'2015-08-13 18:31:48'),(14,3,0,2,'2015-08-13 18:32:47',2,'2015-08-13 18:32:47'),(15,1,0,2,'2015-08-13 18:45:07',2,'2015-08-13 18:45:07'),(16,1,0,2,'2015-08-13 18:45:45',2,'2015-08-13 18:45:45'),(17,15,0,2,'2015-09-22 13:24:38',2,'2015-09-22 13:24:38'),(18,15,0,2,'2015-09-22 13:25:21',2,'2015-09-22 13:25:21');
/*!40000 ALTER TABLE `asset` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `asset_data`
--

DROP TABLE IF EXISTS `asset_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `asset_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `data` text,
  `assetTemplateColId` int(11) NOT NULL,
  `assetId` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `assetTemplateColIdFk_idx` (`assetTemplateColId`),
  KEY `AssetData_Asset_FK_idx` (`assetId`),
  CONSTRAINT `AssetData_Asset_FK` FOREIGN KEY (`assetId`) REFERENCES `asset` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `assetTemplateColIdFk` FOREIGN KEY (`assetTemplateColId`) REFERENCES `asset_template_column` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=97 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `asset_data`
--

LOCK TABLES `asset_data` WRITE;
/*!40000 ALTER TABLE `asset_data` DISABLE KEYS */;
INSERT INTO `asset_data` VALUES (1,'',3,1),(2,'2010-08-05',7,1),(3,'Timothy Morrison ',6,1),(4,'Ranjan Mitra',5,1),(5,'Handling after sales data processing',2,1),(6,'After sales',4,1),(7,'Automobile Managem System ',1,1),(8,'Michael Flanke',6,2),(9,'Middleware',4,2),(10,'2010-05-01',7,2),(11,'IA-AG-01',1,2),(12,'Vohn Gutenberg',5,2),(13,'Integration architecture system bus in Germany',2,2),(14,'',3,2),(15,'Bondial',1,3),(16,'Frank Ziedle',6,3),(17,'',3,3),(18,'',7,3),(19,'Automobile policy data warehouse',2,3),(20,'Warranty',4,3),(21,'Frank Ziedle',5,3),(22,'Gerhard Miller',6,4),(23,'',7,4),(24,'',3,4),(25,'Claims processing factory system',2,4),(26,'Warranty',4,4),(27,'Frankhoff Gief',5,4),(28,'FAGA2',1,4),(29,'T-Systems',5,5),(30,'',3,5),(31,'2010-08-01',7,5),(32,'PMD BLI',1,5),(33,'Automobile ordering system',2,5),(34,'Sales',4,5),(35,'Amanda Connors',6,5),(36,'Frank Ziedle',6,6),(37,'Factory',4,6),(38,'SpyderCat',1,6),(39,'T-Systems',5,6),(40,'',3,6),(41,'2010-01-20',7,6),(42,'Factory design system',2,6),(43,'Middleware',4,7),(44,'',3,7),(45,'UK-ESB-01',1,7),(46,'2013-07-07',7,7),(47,'Birlasoft',5,7),(48,'UK Middleware service bus',2,7),(49,'John Miekleham',6,7),(50,'2009-07-07',7,8),(51,'Common Business Partner',1,8),(52,'Phillis Tarry',6,8),(53,'Customer Relations',4,8),(54,'Customer data warehouse',2,8),(55,'',3,8),(56,'Birlasoft',5,8),(57,'T-Systems',5,9),(58,'Roadside Support',4,9),(59,'',3,9),(60,'On The Go',1,9),(61,'Johnathon R',6,9),(62,'Mobility support for customers',2,9),(63,'2015-02-20',7,9),(64,'',13,10),(65,'Realitime',14,10),(66,'CBP Synchronization',12,10),(67,'Automobile Information',12,11),(68,'Realtime',14,11),(69,'',13,11),(70,'Realtime',14,12),(71,'Automobile Owner Service',12,12),(72,'Provides ownership details based on VIN',13,12),(73,'Automobile Information Service UK',12,13),(74,'',13,13),(75,'Realtime',14,13),(76,'PUK32-FAGA2',12,14),(77,'Batch',14,14),(78,'',13,14),(79,'T-Systems',5,15),(80,'Middleware',4,15),(81,'2010-04-01',7,15),(82,'',2,15),(83,'SFTP Germany',1,15),(84,'',3,15),(85,'Gerhard Miller',6,15),(86,'',3,16),(87,'Birlasoft',5,16),(88,'2010-02-04',7,16),(89,'',2,16),(90,'James Brown',6,16),(91,'SFTP UK',1,16),(92,'Middleware',4,16),(93,'Demo interafce',23,17),(94,'2',24,17),(95,'Demo interafce',23,18),(96,'2',24,18);
/*!40000 ALTER TABLE `asset_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `asset_template`
--

DROP TABLE IF EXISTS `asset_template`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `asset_template` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `assetTemplateName` varchar(45) NOT NULL,
  `assetTypeId` int(11) NOT NULL,
  `deleteStatus` tinyint(1) NOT NULL DEFAULT '0',
  `description` text,
  `workspaceId` int(11) NOT NULL,
  `createdBy` int(11) NOT NULL,
  `createdDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updatedBy` int(11) NOT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `assetTemplateName_createdBy_UNIQUE` (`assetTemplateName`,`createdBy`,`assetTypeId`),
  UNIQUE KEY `UK_a35ol4p1y14cnqk9j9yqcoj13` (`assetTemplateName`,`createdBy`,`assetTypeId`),
  KEY `id_idx` (`assetTypeId`),
  KEY `updatedByFk2_idx` (`updatedBy`),
  KEY `createdBy_UNIQUE` (`createdBy`),
  KEY `FK_ASSETTEMPLATE_WORKSPACEID_idx` (`workspaceId`),
  CONSTRAINT `FK_ASSETTEMPLATE_WORKSPACEID` FOREIGN KEY (`workspaceId`) REFERENCES `workspace` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `assetTypeIdFk` FOREIGN KEY (`assetTypeId`) REFERENCES `asset_type` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `createdByFk2` FOREIGN KEY (`createdBy`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `updatedByFk2` FOREIGN KEY (`updatedBy`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `asset_template`
--

LOCK TABLES `asset_template` WRITE;
/*!40000 ALTER TABLE `asset_template` DISABLE KEYS */;
INSERT INTO `asset_template` VALUES (1,'Application Inventory',1,0,'Inventory template for application portfolio',1,2,'2015-08-13 10:52:32',2,'2015-08-13 10:52:32'),(2,'Server Inventory',2,0,'',1,2,'2015-08-13 17:49:12',2,'2015-08-13 17:49:12'),(3,'Integration Services Repository',5,0,'',1,2,'2015-08-13 18:26:36',2,'2015-08-13 18:26:36'),(4,'Demo for DK',6,0,'',1,2,'2015-08-14 04:12:52',2,'2015-08-14 04:12:52'),(5,'template test',1,0,'et',1,2,'2015-08-26 11:46:33',2,'2015-08-26 11:46:33'),(6,'sdaffsdf',4,0,'sdfdffdf',1,2,'2015-08-26 11:47:04',2,'2015-08-26 11:47:04'),(7,'dcfsddsf',1,0,'dffdfgfdfg',1,2,'2015-08-26 11:53:46',2,'2015-08-26 11:53:46'),(8,'template 21',5,0,'test',1,2,'2015-08-26 12:07:57',2,'2015-08-26 12:07:57'),(9,'template test',5,0,'',1,2,'2015-08-26 12:09:27',2,'2015-08-26 12:09:27'),(10,'Test template in WS 123',1,0,'here we go!',4,2,'2015-08-30 09:31:41',2,'2015-08-30 09:31:41'),(11,'One template 123',1,0,'',4,2,'2015-08-30 09:32:25',2,'2015-08-30 09:32:25'),(12,'template in ws 123',2,0,'',4,2,'2015-08-30 09:33:43',2,'2015-08-30 09:33:43'),(13,'Template for application',1,0,'',1,2,'2015-08-31 11:16:05',2,'2015-08-31 11:16:05'),(14,'GG\'s template',3,0,'',4,2,'2015-08-31 11:50:03',2,'2015-08-31 11:50:03'),(15,'Customer Interface Repository',7,0,'',1,2,'2015-09-22 06:12:23',2,'2015-09-22 06:12:23');
/*!40000 ALTER TABLE `asset_template` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `asset_template_column`
--

DROP TABLE IF EXISTS `asset_template_column`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `asset_template_column` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `assetTemplateColName` varchar(45) NOT NULL,
  `dataType` varchar(45) NOT NULL,
  `length` int(11) NOT NULL,
  `assetTemplateId` int(11) NOT NULL,
  `deleteStatus` tinyint(1) NOT NULL DEFAULT '0',
  `mandatory` tinyint(1) NOT NULL DEFAULT '0',
  `sequenceNumber` int(11) NOT NULL,
  `createdBy` int(11) NOT NULL,
  `createdDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updatedBy` int(11) NOT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `assetTemplateColName_assetTemplateId_UNIQUE` (`assetTemplateColName`,`assetTemplateId`),
  UNIQUE KEY `UK_i7n89jntp5xq390elbs233689` (`assetTemplateColName`,`assetTemplateId`),
  KEY `createdByFk3_idx` (`createdBy`),
  KEY `updatedByFk3_idx` (`updatedBy`),
  KEY `assettemplateIdFk_idx` (`assetTemplateId`),
  CONSTRAINT `assetTemplateIdFk` FOREIGN KEY (`assetTemplateId`) REFERENCES `asset_template` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `createdByFk3` FOREIGN KEY (`createdBy`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `updatedByFk3` FOREIGN KEY (`updatedBy`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `asset_template_column`
--

LOCK TABLES `asset_template_column` WRITE;
/*!40000 ALTER TABLE `asset_template_column` DISABLE KEYS */;
INSERT INTO `asset_template_column` VALUES (1,'Application Name','TEXT',25,1,0,1,1,2,'2015-08-13 10:52:48',2,'2015-08-13 10:52:48'),(2,'Description','TEXT',50,1,0,0,2,2,'2015-08-13 10:53:06',2,'2015-08-13 10:53:06'),(3,'Category 1','TEXT',25,1,0,0,3,2,'2015-08-13 10:53:19',2,'2015-08-13 10:53:56'),(4,'Service Area / Function / Department','TEXT',25,1,0,1,4,2,'2015-08-13 10:53:51',2,'2015-08-13 10:53:51'),(5,'Application Lead','TEXT',50,1,0,1,5,2,'2015-08-13 10:54:15',2,'2015-08-13 10:54:15'),(6,'IT Manager','TEXT',50,1,0,1,6,2,'2015-08-13 10:55:12',2,'2015-08-13 10:55:12'),(7,'Date of Commissioning','DATE',1,1,0,0,7,2,'2015-08-13 10:55:34',2,'2015-08-13 10:55:34'),(8,'Server Name','TEXT',50,2,0,1,1,2,'2015-08-13 17:49:38',2,'2015-08-13 17:49:38'),(9,'FQDN','TEXT',50,2,0,1,2,2,'2015-08-13 17:49:58',2,'2015-08-13 17:49:58'),(10,'Operating System','TEXT',100,2,0,1,3,2,'2015-08-13 17:50:47',2,'2015-08-13 17:50:47'),(11,'Purpose','TEXT',200,2,0,0,4,2,'2015-08-13 17:51:12',2,'2015-08-13 17:51:12'),(12,'Service Name','TEXT',50,3,0,1,1,2,'2015-08-13 18:27:03',2,'2015-08-13 18:27:03'),(13,'Service Description','TEXT',100,3,0,0,2,2,'2015-08-13 18:27:37',2,'2015-08-13 18:27:37'),(14,'Realtime / Batch','TEXT',10,3,0,1,3,2,'2015-08-13 18:28:13',2,'2015-08-13 18:28:13'),(15,'Server Type','TEXT',15,2,0,1,5,2,'2015-08-13 18:29:25',2,'2015-08-13 18:29:25'),(16,'Service name','TEXT',50,4,0,1,1,2,'2015-08-14 04:13:06',2,'2015-08-14 04:13:06'),(17,'column 1','TEXT',50,10,0,1,1,2,'2015-08-30 09:39:16',2,'2015-08-30 09:39:16'),(18,'column 2','TEXT',25,10,0,0,2,2,'2015-08-30 09:39:31',2,'2015-08-30 09:39:31'),(19,'c1','TEXT',4,6,0,1,1,2,'2015-08-30 09:42:34',2,'2015-08-30 09:42:34'),(20,'x2','NUMBER',33,6,0,1,2,2,'2015-08-30 09:42:44',2,'2015-08-30 09:42:44'),(21,'db name','TEXT',25,14,0,1,1,2,'2015-08-31 11:50:17',2,'2015-08-31 11:50:17'),(22,'db platform','TEXT',25,14,0,0,2,2,'2015-08-31 11:50:28',2,'2015-08-31 11:50:28'),(23,'Interface Name','TEXT',50,15,0,1,1,2,'2015-09-22 06:14:14',2,'2015-09-22 06:14:14'),(24,'Interface ID','TEXT',30,15,0,0,2,2,'2015-09-22 06:14:28',2,'2015-09-22 06:14:28');
/*!40000 ALTER TABLE `asset_template_column` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `asset_type`
--

DROP TABLE IF EXISTS `asset_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `asset_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `assetTypeName` varchar(45) NOT NULL,
  `deleteStatus` tinyint(1) NOT NULL DEFAULT '0',
  `createdBy` int(11) NOT NULL,
  `createdDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updatedBy` int(11) NOT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `assetTypeName_UNIQUE` (`assetTypeName`),
  UNIQUE KEY `UK_q1gprhj5b79ufwicjvkvef4fw` (`assetTypeName`),
  KEY `createdByFk1_idx` (`createdBy`),
  KEY `updatedByFk1_idx` (`updatedBy`),
  CONSTRAINT `createdByFk1` FOREIGN KEY (`createdBy`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `updatedByFk1` FOREIGN KEY (`updatedBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `asset_type`
--

LOCK TABLES `asset_type` WRITE;
/*!40000 ALTER TABLE `asset_type` DISABLE KEYS */;
INSERT INTO `asset_type` VALUES (1,'Application',0,1,'2015-08-13 10:51:31',1,'2015-08-13 10:51:31'),(2,'Server',0,1,'2015-08-13 10:51:31',1,'2015-08-13 10:51:31'),(3,'Database',0,1,'2015-08-13 10:51:31',1,'2015-08-13 10:51:31'),(4,'Person',0,1,'2015-08-13 10:51:31',1,'2015-08-13 10:51:31'),(5,'Business Services',0,1,'2015-08-13 10:51:31',1,'2015-08-13 10:51:31'),(6,'IT Services',0,1,'2015-08-13 10:51:31',1,'2015-08-13 10:51:31'),(7,'Interfaces',0,1,'2015-08-13 10:51:31',1,'2015-08-13 10:51:31');
/*!40000 ALTER TABLE `asset_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `description` text,
  `createdBy` int(11) NOT NULL,
  `createdDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updatedBy` int(11) NOT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`),
  KEY `category_createdBy_fk_idx` (`createdBy`),
  KEY `category_updatedBy_fk_idx` (`updatedBy`),
  CONSTRAINT `category_createdBy_fk` FOREIGN KEY (`createdBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `category_updatedBy_fk` FOREIGN KEY (`updatedBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category`
--

LOCK TABLES `category` WRITE;
/*!40000 ALTER TABLE `category` DISABLE KEYS */;
INSERT INTO `category` VALUES (5,'Functional Assessment',NULL,1,'2015-08-13 10:36:33',1,'2015-08-13 10:36:33'),(6,'Application Evolution',NULL,1,'2015-08-13 10:36:33',1,'2015-08-13 10:36:33'),(7,'Technology Assessment',NULL,1,'2015-08-13 10:36:33',1,'2015-08-13 10:36:33'),(8,'Application Management',NULL,1,'2015-08-13 10:36:33',1,'2015-08-13 10:36:33');
/*!40000 ALTER TABLE `category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dashboard`
--

DROP TABLE IF EXISTS `dashboard`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dashboard` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dashboardName` varchar(45) NOT NULL,
  `workspaceId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_DASHBOARD_WORKSPACE_idx` (`workspaceId`),
  CONSTRAINT `FK_DASHBOARD_WORKSPACE` FOREIGN KEY (`workspaceId`) REFERENCES `workspace` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dashboard`
--

LOCK TABLES `dashboard` WRITE;
/*!40000 ALTER TABLE `dashboard` DISABLE KEYS */;
INSERT INTO `dashboard` VALUES (1,'Trial Dashboards',1),(3,'another one',1),(4,'Demos',1),(5,'Demo for Aditya',1),(6,'Test Dashboard',1),(8,'Test Dashboard',1);
/*!40000 ALTER TABLE `dashboard` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `graph`
--

DROP TABLE IF EXISTS `graph`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `graph` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `graphName` varchar(45) NOT NULL,
  `description` text,
  `workspaceId` int(11) NOT NULL,
  `graphType` varchar(45) NOT NULL,
  `deleteStatus` tinyint(1) NOT NULL DEFAULT '0',
  `createdBy` int(11) NOT NULL,
  `createdDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updatedBy` int(11) NOT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_GRAPHNAME_WORKSPACE` (`graphName`,`workspaceId`,`graphType`),
  KEY `Graph_Workspace_FK_idx` (`workspaceId`),
  KEY `Graph_User_FK_idx` (`createdBy`),
  KEY `Graph_Updated_By_FK_idx` (`updatedBy`),
  CONSTRAINT `Graph_Created_By_FK` FOREIGN KEY (`createdBy`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `Graph_Updated_By_FK` FOREIGN KEY (`updatedBy`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `Graph_Workspace_FK` FOREIGN KEY (`workspaceId`) REFERENCES `workspace` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=54 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `graph`
--

LOCK TABLES `graph` WRITE;
/*!40000 ALTER TABLE `graph` DISABLE KEYS */;
INSERT INTO `graph` VALUES (29,'Application Network view','Relation of the applications in the portfolio',1,'NW',0,2,'2015-08-13 18:13:49',2,'2015-08-13 18:46:32'),(30,'Demo Graph of Assets 2','',1,'NW',0,2,'2015-09-21 06:51:59',2,'2015-09-21 06:52:10'),(31,'test graph 1','',4,'NW',0,2,'2015-09-22 11:15:57',2,'2015-09-22 11:15:57'),(32,'test graph','',1,'NW',0,2,'2015-09-22 11:21:45',2,'2015-09-22 11:21:45'),(34,'test graph1','',1,'NW',0,2,'2015-09-22 11:27:01',2,'2015-09-22 11:27:01'),(35,'test graph5','',1,'NW',0,2,'2015-09-22 12:40:51',2,'2015-09-22 12:40:51'),(36,'test graph 12','sdadfsd',1,'NW',0,2,'2015-09-22 12:41:29',2,'2015-09-22 12:41:29'),(37,'test graph 23','333',1,'NW',0,2,'2015-09-22 12:42:25',2,'2015-09-22 12:42:25'),(38,'dfsdf','',1,'NW',0,2,'2015-09-22 12:45:48',2,'2015-09-22 12:45:48'),(39,'test graph 33','test graph',1,'NW',0,2,'2015-09-22 12:50:03',2,'2015-09-22 12:50:03'),(41,'fdsdfdf','',1,'NW',0,2,'2015-09-22 12:50:11',2,'2015-09-22 12:50:11'),(42,'5545645','',1,'NW',0,2,'2015-09-22 12:51:47',2,'2015-09-22 12:51:47'),(43,'rtrty','',4,'NW',0,2,'2015-09-22 12:52:21',2,'2015-09-22 12:52:21'),(44,'ghnnbnnbn','',4,'NW',0,2,'2015-09-22 12:52:29',2,'2015-09-22 12:52:29'),(45,'test graph 233213','eweweedw',4,'NW',0,2,'2015-09-22 12:53:57',2,'2015-09-22 12:53:57'),(46,'test graph 233213 2','eweweedw',4,'NW',0,2,'2015-09-22 12:54:20',2,'2015-09-22 12:54:20'),(47,'test graph demo for caterpille ws','sadfsdfsd',1,'NW',0,2,'2015-09-22 13:27:09',2,'2015-09-22 13:27:09'),(48,'test graph 34','dfedf',1,'NW',0,2,'2015-09-23 05:35:44',2,'2015-09-23 05:35:44'),(49,'test graph 4','',1,'NW',0,2,'2015-09-23 05:44:58',2,'2015-09-23 05:44:58'),(50,'test graph 5 5443','',1,'NW',0,2,'2015-09-23 06:28:23',2,'2015-09-23 06:28:23'),(51,'12345 graph','desc 132345',4,'NW',0,2,'2015-09-23 07:23:00',2,'2015-09-23 07:23:00'),(52,'123 graph','desc 132345',4,'NW',0,2,'2015-09-23 07:32:49',2,'2015-09-23 07:47:23'),(53,'new test graph 1','',1,'NW',0,2,'2015-09-23 07:49:19',2,'2015-09-23 07:49:36');
/*!40000 ALTER TABLE `graph` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `graph_asset_template`
--

DROP TABLE IF EXISTS `graph_asset_template`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `graph_asset_template` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `graphId` int(11) NOT NULL,
  `assetTemplateId` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_GRAPHID_ASSETTEMPLATEID` (`graphId`,`assetTemplateId`),
  KEY `FK_GRAPHID_GRAPHID_idx` (`graphId`),
  KEY `FK_ASSETTEMPLATEID_ASSETTEMPLATEID_idx` (`assetTemplateId`),
  CONSTRAINT `FK_ASSETTEMPLATEID_ASSETTEMPLATEID` FOREIGN KEY (`assetTemplateId`) REFERENCES `asset_template` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_GRAPHID_GRAPHID` FOREIGN KEY (`graphId`) REFERENCES `graph` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `graph_asset_template`
--

LOCK TABLES `graph_asset_template` WRITE;
/*!40000 ALTER TABLE `graph_asset_template` DISABLE KEYS */;
INSERT INTO `graph_asset_template` VALUES (1,36,1),(2,37,1),(3,37,5),(4,37,6),(5,37,7),(6,39,2),(7,39,6),(8,39,7),(9,45,10),(10,45,11),(11,45,12),(12,47,1),(13,47,5),(14,47,6),(15,47,7),(17,48,2),(18,48,3),(16,48,15),(19,50,15),(21,51,10),(22,51,11),(20,51,14),(34,52,12),(35,52,14),(45,53,4),(44,53,6),(43,53,13);
/*!40000 ALTER TABLE `graph_asset_template` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `interface`
--

DROP TABLE IF EXISTS `interface`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `interface` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `interfaceAssetId` int(11) NOT NULL,
  `assetSourceId` int(11) NOT NULL,
  `assetDestId` int(11) NOT NULL,
  `interfaceDirection` varchar(45) NOT NULL,
  `createdBy` int(11) NOT NULL,
  `createdDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updatedBy` int(11) NOT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_INTERFACE_SRC_DEST` (`interfaceAssetId`,`assetSourceId`,`assetDestId`),
  KEY `Interface_Asset_FK_idx` (`assetSourceId`),
  KEY `Interface_Asset_Dest_FK_idx` (`assetDestId`),
  KEY `Interface_Created_By_FK_idx` (`createdBy`),
  KEY `Interface_Updated_By_FK_idx` (`updatedBy`),
  KEY `FK_INTERFACEASSETID_ASSETID_idx` (`interfaceAssetId`),
  CONSTRAINT `FK_INTERFACEASSETID_ASSETID` FOREIGN KEY (`interfaceAssetId`) REFERENCES `asset` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `Interface_Asset_Dest_FK` FOREIGN KEY (`assetDestId`) REFERENCES `asset` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `Interface_Asset_Source_FK` FOREIGN KEY (`assetSourceId`) REFERENCES `asset` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `Interface_Created_By_FK` FOREIGN KEY (`createdBy`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `Interface_Updated_By_FK` FOREIGN KEY (`updatedBy`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `interface`
--

LOCK TABLES `interface` WRITE;
/*!40000 ALTER TABLE `interface` DISABLE KEYS */;
INSERT INTO `interface` VALUES (28,17,2,13,'UNI',2,'2015-09-22 13:24:38',2,'2015-09-22 13:24:38'),(29,18,2,14,'BIDI',2,'2015-09-22 13:25:21',2,'2015-09-22 13:25:21');
/*!40000 ALTER TABLE `interface` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `parameter`
--

DROP TABLE IF EXISTS `parameter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `parameter` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `description` text,
  `type` varchar(4) NOT NULL,
  `workspaceId` int(11) DEFAULT NULL,
  `questionnaireId` int(11) DEFAULT NULL,
  `templateFlag` tinyint(4) NOT NULL DEFAULT '0',
  `parentTemplateId` int(11) DEFAULT NULL,
  `createdBy` int(11) NOT NULL,
  `createdDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updatedBy` int(11) NOT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`,`questionnaireId`),
  KEY `parameter_workspaceId_fk_idx` (`workspaceId`),
  KEY `parameter_parentTemplateId_fk_idx` (`parentTemplateId`),
  KEY `parameter_createdBy_fk_idx` (`createdBy`),
  KEY `parameter_updatedBy_fk_idx` (`updatedBy`),
  KEY `parameter_questionnairId_fk_idx` (`questionnaireId`),
  CONSTRAINT `parameter_createdBy_fk` FOREIGN KEY (`createdBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `parameter_parentTemplateId_fk` FOREIGN KEY (`parentTemplateId`) REFERENCES `parameter` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `parameter_questionnairId_fk` FOREIGN KEY (`questionnaireId`) REFERENCES `questionnaire` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `parameter_updatedBy_fk` FOREIGN KEY (`updatedBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `parameter_workspaceId_fk` FOREIGN KEY (`workspaceId`) REFERENCES `workspace` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=92 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `parameter`
--

LOCK TABLES `parameter` WRITE;
/*!40000 ALTER TABLE `parameter` DISABLE KEYS */;
INSERT INTO `parameter` VALUES (80,'Functional Quotient','','LP',1,23,0,NULL,2,'2015-08-14 03:39:19',2,'2015-08-14 03:39:19'),(81,'Technical Quotient','','LP',1,23,0,NULL,2,'2015-08-14 03:39:32',2,'2015-08-14 03:39:32'),(82,'Management Quotient','','LP',1,23,0,NULL,2,'2015-08-14 03:39:54',2,'2015-08-14 03:39:54'),(83,'FQ 1','','AP',1,24,0,NULL,2,'2015-08-14 04:20:22',2,'2015-08-14 04:20:22'),(84,'TQ1','','AP',1,24,0,NULL,2,'2015-08-14 04:20:29',2,'2015-08-14 04:20:29'),(85,'FQ1.1','','LP',1,24,0,NULL,2,'2015-08-14 04:21:03',2,'2015-08-14 04:21:03'),(86,'P1','','LP',1,25,0,NULL,2,'2015-08-20 11:30:09',2,'2015-08-20 11:30:09'),(87,'Fq 1','','LP',1,27,0,NULL,2,'2015-08-31 11:24:19',2,'2015-08-31 11:24:19'),(88,'FQ2','','LP',1,27,0,NULL,2,'2015-08-31 11:24:24',2,'2015-08-31 11:24:24'),(89,'cp 1','','LP',1,24,0,NULL,2,'2015-09-01 05:40:50',2,'2015-09-01 05:40:50'),(90,'cp2','','LP',1,24,0,NULL,2,'2015-09-01 05:40:55',2,'2015-09-01 05:40:55'),(91,'Demo for ajit','','LP',1,28,0,NULL,2,'2015-09-16 03:57:59',2,'2015-09-16 03:57:59');
/*!40000 ALTER TABLE `parameter` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `parameter_function`
--

DROP TABLE IF EXISTS `parameter_function`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `parameter_function` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parentParameterId` int(11) NOT NULL,
  `weight` decimal(10,3) NOT NULL,
  `constant` int(11) NOT NULL,
  `questionId` int(11) DEFAULT NULL,
  `mandatoryQuestion` tinyint(1) NOT NULL DEFAULT '0',
  `childparameterId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `parameter_function_parentParameterId_fk_idx` (`parentParameterId`),
  KEY `parameter_function_childParameterId_fk_idx` (`childparameterId`),
  KEY `parameter_function_questionId_fk_idx` (`questionId`),
  CONSTRAINT `parameter_function_childParameterId_fk` FOREIGN KEY (`childparameterId`) REFERENCES `parameter` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `parameter_function_parentParameterId_fk` FOREIGN KEY (`parentParameterId`) REFERENCES `parameter` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `parameter_function_questionId_fk` FOREIGN KEY (`questionId`) REFERENCES `question` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=103 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `parameter_function`
--

LOCK TABLES `parameter_function` WRITE;
/*!40000 ALTER TABLE `parameter_function` DISABLE KEYS */;
INSERT INTO `parameter_function` VALUES (77,80,1.500,1,86,0,NULL),(78,80,1.000,1,88,0,NULL),(79,80,1.000,1,90,0,NULL),(80,81,1.000,1,94,0,NULL),(81,81,1.300,1,95,0,NULL),(82,81,0.900,1,96,0,NULL),(83,82,1.000,1,106,0,NULL),(84,82,1.000,1,109,0,NULL),(85,82,1.000,1,110,0,NULL),(86,84,1.000,1,NULL,0,85),(87,85,1.000,1,87,0,NULL),(88,85,1.000,1,88,0,NULL),(89,85,1.000,1,89,0,NULL),(90,80,1.000,1,102,0,NULL),(91,80,1.000,1,104,0,NULL),(92,80,1.000,1,93,0,NULL),(93,87,3.000,1,86,0,NULL),(94,87,1.000,1,87,0,NULL),(95,87,1.000,1,88,0,NULL),(96,83,1.000,1,NULL,0,89),(97,83,1.000,1,NULL,0,90),(98,89,2.000,1,86,0,NULL),(99,89,1.000,1,87,1,NULL),(100,91,1.000,1,86,1,NULL),(101,91,1.000,1,87,1,NULL),(102,91,1.000,1,88,1,NULL);
/*!40000 ALTER TABLE `parameter_function` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `question`
--

DROP TABLE IF EXISTS `question`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `question` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` text NOT NULL,
  `helpText` text,
  `workspaceId` int(11) DEFAULT NULL,
  `questionType` varchar(10) NOT NULL,
  `queTypeText` text,
  `level` smallint(6) NOT NULL,
  `questionCategoryId` int(11) DEFAULT NULL,
  `createdBy` int(11) NOT NULL,
  `createdDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updatedBy` int(11) NOT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `QUESTIONNAIRE_WORKSPACE_ID_FK_idx` (`workspaceId`),
  KEY `QUESTIONNAIRE_CREATED_BY_FK_idx` (`createdBy`),
  KEY `QUESTIONNAIRE_UPDATED_BY_FK_idx` (`updatedBy`),
  KEY `question_questioncategoryid_categoryid_idx` (`questionCategoryId`),
  CONSTRAINT `QUESTIONNAIRE_CREATED_BY_FK` FOREIGN KEY (`createdBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `QUESTIONNAIRE_UPDATED_BY_FK` FOREIGN KEY (`updatedBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `QUESTIONNAIRE_WORKSPACE_ID_FK` FOREIGN KEY (`workspaceId`) REFERENCES `workspace` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `question_questioncategoryid_categoryid` FOREIGN KEY (`questionCategoryId`) REFERENCES `category` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=112 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `question`
--

LOCK TABLES `question` WRITE;
/*!40000 ALTER TABLE `question` DISABLE KEYS */;
INSERT INTO `question` VALUES (86,'Please list the number of functions or business processes supported by the application? ','',NULL,'MULTCHOICE','{\"options\":[{\"sequenceNumber\":1,\"quantifier\":1.0,\"text\":\"0 - Only Supports peripherals\"},{\"sequenceNumber\":2,\"quantifier\":3.0,\"text\":\"1-2\"},{\"sequenceNumber\":3,\"quantifier\":5.0,\"text\":\"3-5\"},{\"sequenceNumber\":4,\"quantifier\":7.0,\"text\":\"6-10\"}]}',1,5,2,'2015-08-13 13:26:43',2,'2015-08-13 13:26:59'),(87,'What is the type of application?','',NULL,'MULTCHOICE','{\"options\":[{\"sequenceNumber\":1,\"quantifier\":3.0,\"text\":\"In house developed application\"},{\"sequenceNumber\":2,\"quantifier\":2.0,\"text\":\"Packaged application\"},{\"sequenceNumber\":3,\"quantifier\":1.0,\"text\":\"Third party developed application\"},{\"sequenceNumber\":4,\"quantifier\":1.5,\"text\":\"Others\"}]}',1,5,2,'2015-08-13 13:29:16',2,'2015-08-13 13:29:16'),(88,'What is the actual functional coverage vis-Ã -vis the planned coverage? ','Please state whether any more business functions/ processes were intended to be covered by this application as part of its implementation scope?',NULL,'MULTCHOICE','{\"options\":[{\"sequenceNumber\":1,\"quantifier\":5.0,\"text\":\"All envisaged functionality covered\"},{\"sequenceNumber\":2,\"quantifier\":3.0,\"text\":\"Core functionality implemented\"},{\"sequenceNumber\":3,\"quantifier\":1.0,\"text\":\"Peripheral functionality implemented\"}]}',1,5,2,'2015-08-13 13:39:16',2,'2015-08-13 13:39:16'),(89,'Are the business functions/ processes supported by this application also supported any other application implemented in this company?  ','',NULL,'MULTCHOICE','{\"options\":[{\"sequenceNumber\":1,\"quantifier\":1.0,\"text\":\"Yes\"},{\"sequenceNumber\":2,\"quantifier\":3.0,\"text\":\"No\"}]}',1,5,2,'2015-08-13 13:40:17',2,'2015-08-13 13:40:17'),(90,'Are there any functionality redundancies / overlap of functionality with other applications? ','',NULL,'MULTCHOICE','{\"options\":[{\"sequenceNumber\":1,\"quantifier\":1.0,\"text\":\"Significant overlap in functionality\"},{\"sequenceNumber\":2,\"quantifier\":3.0,\"text\":\"Minor functionality overlap\"},{\"sequenceNumber\":3,\"quantifier\":5.0,\"text\":\"No functionality overlap\"}]}',1,5,2,'2015-08-13 13:42:15',2,'2015-08-13 13:42:15'),(91,'How would you rate the ability of the application to support ad-hoc reporting requirements?','',NULL,'MULTCHOICE','{\"options\":[{\"sequenceNumber\":1,\"quantifier\":5.0,\"text\":\"The application is flexible and is easy to obtain ad-hoc reports\"},{\"sequenceNumber\":2,\"quantifier\":3.0,\"text\":\"The application is not very flexible and support from IT is required for ad-hoc reporting\"},{\"sequenceNumber\":3,\"quantifier\":1.0,\"text\":\"Code level changes are required to support ad-hoc reporting requirements\"}]}',1,5,2,'2015-08-13 13:44:57',2,'2015-08-13 13:44:57'),(92,'How would you rate the ability of the application to meet the overall current needs of users?','',NULL,'MULTCHOICE','{\"options\":[{\"sequenceNumber\":1,\"quantifier\":1.0,\"text\":\"New application required\"},{\"sequenceNumber\":2,\"quantifier\":2.0,\"text\":\"Major enhancement required\"},{\"sequenceNumber\":3,\"quantifier\":3.0,\"text\":\"Significant modifications required\"},{\"sequenceNumber\":4,\"quantifier\":4.0,\"text\":\"Minor Modifications required\"},{\"sequenceNumber\":5,\"quantifier\":5.0,\"text\":\"Acceptable as it is\"}]}',1,5,2,'2015-08-13 13:48:16',2,'2015-08-13 13:48:16'),(93,'What are the names of the modules / sub systems within the application?','',NULL,'PARATEXT',NULL,1,7,2,'2015-08-13 14:19:41',2,'2015-08-13 14:19:41'),(94,'What is the level of integration with other applications? ','',NULL,'MULTCHOICE','{\"options\":[{\"sequenceNumber\":1,\"quantifier\":3.0,\"text\":\"Yes\"},{\"sequenceNumber\":2,\"quantifier\":1.0,\"text\":\"No\"}]}',1,7,2,'2015-08-13 14:20:47',2,'2015-08-13 14:20:47'),(95,'Does the application import or export data to other applications? ','',NULL,'MULTCHOICE','{\"options\":[{\"sequenceNumber\":1,\"quantifier\":3.0,\"text\":\"Yes\"},{\"sequenceNumber\":2,\"quantifier\":1.0,\"text\":\"No\"}]}',1,7,2,'2015-08-13 14:21:50',2,'2015-08-13 14:21:50'),(96,'Is the application centrally deployed or is it distributed?','',NULL,'MULTCHOICE','{\"options\":[{\"sequenceNumber\":1,\"quantifier\":3.0,\"text\":\"Centrally\"},{\"sequenceNumber\":2,\"quantifier\":1.0,\"text\":\"Distributed\"},{\"sequenceNumber\":3,\"quantifier\":2.0,\"text\":\"Others\"}]}',1,7,2,'2015-08-13 14:22:50',2,'2015-08-13 14:22:50'),(97,'If distributed how is data synchronized across multiple locations?','',NULL,'MULTCHOICE','{\"options\":[{\"sequenceNumber\":1,\"quantifier\":5.0,\"text\":\"Real time synchronization\"},{\"sequenceNumber\":2,\"quantifier\":3.0,\"text\":\"Day end batch synchronization\"},{\"sequenceNumber\":3,\"quantifier\":-3.0,\"text\":\"No data synchronization\"},{\"sequenceNumber\":4,\"quantifier\":0.0,\"text\":\"Others\"}]}',1,7,2,'2015-08-13 14:24:37',2,'2015-08-13 14:24:37'),(98,'What are the response times of the application during the day / week? ','',NULL,'MULTCHOICE','{\"options\":[{\"sequenceNumber\":1,\"quantifier\":5.0,\"text\":\"< 1 sec\"},{\"sequenceNumber\":2,\"quantifier\":3.0,\"text\":\"2 - 4\"},{\"sequenceNumber\":3,\"quantifier\":0.0,\"text\":\">5 sec\"}]}',1,7,2,'2015-08-13 14:28:03',2,'2015-08-13 14:28:03'),(99,'What is the usage of system resources by the application?','',NULL,'MULTCHOICE','{\"options\":[{\"sequenceNumber\":1,\"quantifier\":1.0,\"text\":\"The application is very resource intensive and significant changes are required to make the application more efficient\"},{\"sequenceNumber\":2,\"quantifier\":3.0,\"text\":\"The application is resource intensive but minor changes are required to make the application more efficient\"},{\"sequenceNumber\":3,\"quantifier\":5.0,\"text\":\"The application uses resources optimally and does not load other IT resources\"}]}',1,7,2,'2015-08-13 14:30:13',2,'2015-08-13 14:30:13'),(100,'Has the application design considered good practices like following open architectures and standards etc.? Please provide a list of good practices followed.','',NULL,'MULTCHOICE','{\"options\":[{\"sequenceNumber\":1,\"quantifier\":3.0,\"text\":\"Yes\"},{\"sequenceNumber\":2,\"quantifier\":1.0,\"text\":\"No\"}]}',1,7,2,'2015-08-13 14:32:47',2,'2015-08-13 14:32:47'),(101,'How easily can the application be ported from the current environment to a new environment?','',NULL,'MULTCHOICE','{\"options\":[{\"sequenceNumber\":1,\"quantifier\":1.0,\"text\":\"The application would need to be designed and developed once again\"},{\"sequenceNumber\":2,\"quantifier\":3.0,\"text\":\"Major modifications would be required to port the application to another platform\"},{\"sequenceNumber\":3,\"quantifier\":5.0,\"text\":\"Minor modifications would be required to port the application\"}]}',1,7,2,'2015-08-13 14:34:12',2,'2015-08-13 14:34:12'),(102,'What are the modelling tools used for designing the system?','',NULL,'CHECKBOXES','{\"options\":[{\"sequenceNumber\":1,\"quantifier\":2.0,\"text\":\"Rational Rose\"},{\"sequenceNumber\":2,\"quantifier\":2.0,\"text\":\"Erwin\"},{\"sequenceNumber\":3,\"quantifier\":1.0,\"text\":\"Designer 2000\"},{\"sequenceNumber\":4,\"quantifier\":3.0,\"text\":\"System Architect\"},{\"sequenceNumber\":5,\"quantifier\":1.5,\"text\":\"Others\"}]}',1,7,2,'2015-08-13 14:36:24',2,'2015-08-13 14:36:24'),(103,'Please state the level of vendor support for the tools deployed?','',NULL,'MULTCHOICE','{\"options\":[{\"sequenceNumber\":1,\"quantifier\":4.0,\"text\":\"On site 24X7 support\"},{\"sequenceNumber\":2,\"quantifier\":3.0,\"text\":\"On site only during office hours\"},{\"sequenceNumber\":3,\"quantifier\":5.0,\"text\":\"On call\"},{\"sequenceNumber\":4,\"quantifier\":2.0,\"text\":\"Off shore monitoring & onsite remedial\"},{\"sequenceNumber\":5,\"quantifier\":1.0,\"text\":\"No support\"}]}',1,7,2,'2015-08-13 14:38:26',2,'2015-08-13 14:38:26'),(104,'What is the type of production support? ','',NULL,'CHECKBOXES','{\"options\":[{\"sequenceNumber\":1,\"quantifier\":2.0,\"text\":\"Helpdesk\"},{\"sequenceNumber\":2,\"quantifier\":3.0,\"text\":\"Hands-on support to some of the production activities like generating MIS reports\"},{\"sequenceNumber\":3,\"quantifier\":2.0,\"text\":\"batch execution\"},{\"sequenceNumber\":4,\"quantifier\":1.0,\"text\":\"System maintenance support to maintain application\"}]}',1,7,2,'2015-08-13 14:41:17',2,'2015-08-13 14:41:17'),(105,'Does the application have IPR issues that may be a hindrance for outsourcing?','',NULL,'MULTCHOICE','{\"options\":[{\"sequenceNumber\":1,\"quantifier\":1.0,\"text\":\"Yes\"},{\"sequenceNumber\":2,\"quantifier\":3.0,\"text\":\"No\"}]}',1,8,2,'2015-08-13 14:46:40',2,'2015-08-13 14:48:21'),(106,'Does the application have any licensing issues that could be a hindrance in migrating offshore? If yes, please provide details.','',NULL,'MULTCHOICE','{\"options\":[{\"sequenceNumber\":1,\"quantifier\":1.0,\"text\":\"Yes\"},{\"sequenceNumber\":2,\"quantifier\":3.0,\"text\":\"No\"}]}',1,8,2,'2015-08-13 14:53:17',2,'2015-08-13 14:53:17'),(107,'For the change control  process also ascertain whether the: ','',NULL,'MULTCHOICE','{\"options\":[{\"sequenceNumber\":1,\"quantifier\":5.0,\"text\":\"Process defined\"},{\"sequenceNumber\":2,\"quantifier\":4.0,\"text\":\"documented and followed consistently\"},{\"sequenceNumber\":3,\"quantifier\":3.0,\"text\":\"Process defined\"},{\"sequenceNumber\":4,\"quantifier\":1.0,\"text\":\"documented but not followed consistently\"}]}',1,8,2,'2015-08-13 14:58:19',2,'2015-08-13 14:58:19'),(108,'Are change request registers maintained and updated regularly? ','',NULL,'MULTCHOICE','{\"options\":[{\"sequenceNumber\":1,\"quantifier\":5.0,\"text\":\"Change request registers are maintained and updated with all change requests\"},{\"sequenceNumber\":2,\"quantifier\":3.0,\"text\":\"Change request registers are maintained but only some change requests are logged\"},{\"sequenceNumber\":3,\"quantifier\":1.0,\"text\":\"Change request registers are not maintained\"}]}',3,8,2,'2015-08-13 14:59:18',2,'2015-08-13 14:59:18'),(109,'What % of application budget is allocated for third party service providers?','',NULL,'MULTCHOICE','{\"options\":[{\"sequenceNumber\":1,\"quantifier\":1.0,\"text\":\"< 5%\"},{\"sequenceNumber\":2,\"quantifier\":3.0,\"text\":\"5 - 15%\"},{\"sequenceNumber\":3,\"quantifier\":5.0,\"text\":\"15 - 30%\"},{\"sequenceNumber\":4,\"quantifier\":6.0,\"text\":\"> 30%\"}]}',3,8,2,'2015-08-13 15:01:46',2,'2015-08-13 15:01:46'),(110,'Are documentation standards clearly defined?  Are these standards diligently followed while making enhancements/ changes?','',NULL,'MULTCHOICE','{\"options\":[{\"sequenceNumber\":1,\"quantifier\":5.0,\"text\":\"Documentation standards defined and followed\"},{\"sequenceNumber\":2,\"quantifier\":3.0,\"text\":\"Documentation standards defined but not followed\"},{\"sequenceNumber\":3,\"quantifier\":1.0,\"text\":\"Documentation standards not defined\"}]}',3,8,2,'2015-08-13 15:03:56',2,'2015-08-13 15:03:56'),(111,'what is your next week looking like','',NULL,'MULTCHOICE','{\"options\":[{\"sequenceNumber\":1,\"quantifier\":2.0,\"text\":\"Option 1\"},{\"sequenceNumber\":2,\"quantifier\":5.0,\"text\":\"Option 2\"},{\"sequenceNumber\":3,\"quantifier\":6.0,\"text\":\"Option 3\"},{\"sequenceNumber\":4,\"quantifier\":8.0,\"text\":\"Option 4\"}]}',1,NULL,2,'2015-08-14 04:18:41',2,'2015-08-14 04:18:41');
/*!40000 ALTER TABLE `question` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `questionnaire`
--

DROP TABLE IF EXISTS `questionnaire`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `questionnaire` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `description` text,
  `workspaceId` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `createdBy` int(11) NOT NULL,
  `createdDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updatedBy` int(11) NOT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `questionnaire_name_workspace_unique` (`name`,`workspaceId`),
  KEY `questionnaire_workspaceId_fk_idx` (`workspaceId`),
  KEY `questionnaire_createdBy_fk_idx` (`createdBy`),
  KEY `questionnaire_updatedBy_fk_idx` (`updatedBy`),
  CONSTRAINT `questionnaire_createdBy_fk` FOREIGN KEY (`createdBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `questionnaire_updatedBy_fk` FOREIGN KEY (`updatedBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `questionnaire_workspaceId_fk` FOREIGN KEY (`workspaceId`) REFERENCES `workspace` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `questionnaire`
--

LOCK TABLES `questionnaire` WRITE;
/*!40000 ALTER TABLE `questionnaire` DISABLE KEYS */;
INSERT INTO `questionnaire` VALUES (23,'IT Portfolio Survey - 1','',1,1,2,'2015-08-14 03:38:18',2,'2015-09-22 11:33:48'),(24,'Questionnaire for Aditya\'s first day1 :)','',1,1,2,'2015-08-14 04:19:55',2,'2015-09-01 05:40:03'),(25,'This is for test.','',1,0,2,'2015-08-19 11:46:38',2,'2015-08-24 06:12:57'),(26,'First for 123','testing',4,0,2,'2015-08-30 10:03:47',2,'2015-08-30 10:03:47'),(27,'Demo for ajit','',1,0,2,'2015-08-31 11:23:54',2,'2015-09-03 05:52:09'),(28,'Assessment for SITA','',1,1,2,'2015-09-16 03:57:09',2,'2015-09-16 03:57:09');
/*!40000 ALTER TABLE `questionnaire` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `questionnaire_asset`
--

DROP TABLE IF EXISTS `questionnaire_asset`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `questionnaire_asset` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `questionnaireId` int(11) NOT NULL,
  `assetId` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `questionnaire_asset_questionnaireAssetId_uq` (`questionnaireId`,`assetId`),
  KEY `questionnaire_asset_questionnaireId_fk_idx` (`questionnaireId`),
  KEY `questionnaire_asset_assetId_fk_idx` (`assetId`),
  CONSTRAINT `questionnaire_asset_assetId_fk` FOREIGN KEY (`assetId`) REFERENCES `asset` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `questionnaire_asset_questionnaireId_fk` FOREIGN KEY (`questionnaireId`) REFERENCES `questionnaire` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=150 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `questionnaire_asset`
--

LOCK TABLES `questionnaire_asset` WRITE;
/*!40000 ALTER TABLE `questionnaire_asset` DISABLE KEYS */;
INSERT INTO `questionnaire_asset` VALUES (111,23,1),(112,23,2),(113,23,3),(114,23,4),(115,23,5),(116,23,6),(117,23,7),(118,23,8),(119,23,9),(106,23,10),(107,23,11),(108,23,12),(109,23,13),(110,23,14),(120,23,15),(121,23,16),(132,24,2),(133,24,3),(134,24,4),(135,24,5),(136,24,7),(137,24,8),(138,24,9),(127,24,10),(128,24,11),(129,24,12),(130,24,13),(131,24,14),(88,25,1),(89,25,2),(87,25,14),(139,27,10),(140,27,11),(141,27,12),(142,27,13),(143,27,14),(144,28,1),(145,28,2),(146,28,3),(147,28,4),(148,28,5),(149,28,6);
/*!40000 ALTER TABLE `questionnaire_asset` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `questionnaire_question`
--

DROP TABLE IF EXISTS `questionnaire_question`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `questionnaire_question` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `questionnaireId` int(11) DEFAULT NULL,
  `questionId` int(11) NOT NULL,
  `questionnaireAssetId` int(11) DEFAULT NULL,
  `parameterId` int(11) NOT NULL,
  `sequenceNumber` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `questionnaire_question_questionnaireId_questionId_parameterId` (`questionnaireId`,`parameterId`,`questionId`,`questionnaireAssetId`),
  KEY `questionnaire_question_questionnaireId_fk_idx` (`questionnaireId`),
  KEY `questionnaire_question_questionId_fk_idx` (`questionId`),
  KEY `questionnaire_question_assetId_fk_idx` (`questionnaireAssetId`),
  KEY `questionnaire_question_parameterId_fk_idx` (`parameterId`),
  CONSTRAINT `questionnaire_question_parameterId_fk` FOREIGN KEY (`parameterId`) REFERENCES `parameter` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `questionnaire_question_questionId_fk` FOREIGN KEY (`questionId`) REFERENCES `question` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `questionnaire_question_questionnaireAssetId_fk` FOREIGN KEY (`questionnaireAssetId`) REFERENCES `questionnaire_asset` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `questionnaire_question_questionnaireId_fk` FOREIGN KEY (`questionnaireId`) REFERENCES `questionnaire` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=1328 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `questionnaire_question`
--

LOCK TABLES `questionnaire_question` WRITE;
/*!40000 ALTER TABLE `questionnaire_question` DISABLE KEYS */;
INSERT INTO `questionnaire_question` VALUES (1118,23,90,108,80,1),(1119,23,102,108,80,2),(1120,23,86,108,80,3),(1121,23,88,108,80,4),(1122,23,104,108,80,5),(1123,23,93,108,80,6),(1124,23,94,108,81,7),(1125,23,96,108,81,8),(1126,23,95,108,81,9),(1127,23,106,108,82,10),(1128,23,110,108,82,11),(1129,23,109,108,82,12),(1130,23,90,112,80,13),(1131,23,102,112,80,14),(1132,23,86,112,80,15),(1133,23,88,112,80,16),(1134,23,104,112,80,17),(1135,23,93,112,80,18),(1136,23,94,112,81,19),(1137,23,96,112,81,20),(1138,23,95,112,81,21),(1139,23,106,112,82,22),(1140,23,110,112,82,23),(1141,23,109,112,82,24),(1142,23,90,120,80,25),(1143,23,102,120,80,26),(1144,23,86,120,80,27),(1145,23,88,120,80,28),(1146,23,104,120,80,29),(1147,23,93,120,80,30),(1148,23,94,120,81,31),(1149,23,96,120,81,32),(1150,23,95,120,81,33),(1151,23,106,120,82,34),(1152,23,110,120,82,35),(1153,23,109,120,82,36),(1154,23,90,111,80,37),(1155,23,102,111,80,38),(1156,23,86,111,80,39),(1157,23,88,111,80,40),(1158,23,104,111,80,41),(1159,23,93,111,80,42),(1160,23,94,111,81,43),(1161,23,96,111,81,44),(1162,23,95,111,81,45),(1163,23,106,111,82,46),(1164,23,110,111,82,47),(1165,23,109,111,82,48),(1166,23,90,109,80,49),(1167,23,102,109,80,50),(1168,23,86,109,80,51),(1169,23,88,109,80,52),(1170,23,104,109,80,53),(1171,23,93,109,80,54),(1172,23,94,109,81,55),(1173,23,96,109,81,56),(1174,23,95,109,81,57),(1175,23,106,109,82,58),(1176,23,110,109,82,59),(1177,23,109,109,82,60),(1178,23,90,107,80,61),(1179,23,102,107,80,62),(1180,23,86,107,80,63),(1181,23,88,107,80,64),(1182,23,104,107,80,65),(1183,23,93,107,80,66),(1184,23,94,107,81,67),(1185,23,96,107,81,68),(1186,23,95,107,81,69),(1187,23,106,107,82,70),(1188,23,110,107,82,71),(1189,23,109,107,82,72),(1190,23,90,110,80,73),(1191,23,102,110,80,74),(1192,23,86,110,80,75),(1193,23,88,110,80,76),(1194,23,104,110,80,77),(1195,23,93,110,80,78),(1196,23,94,110,81,79),(1197,23,96,110,81,80),(1198,23,95,110,81,81),(1199,23,106,110,82,82),(1200,23,110,110,82,83),(1201,23,109,110,82,84),(1202,23,90,114,80,85),(1203,23,102,114,80,86),(1204,23,86,114,80,87),(1205,23,88,114,80,88),(1206,23,104,114,80,89),(1207,23,93,114,80,90),(1208,23,94,114,81,91),(1209,23,96,114,81,92),(1210,23,95,114,81,93),(1211,23,106,114,82,94),(1212,23,110,114,82,95),(1213,23,109,114,82,96),(1214,23,90,106,80,97),(1215,23,102,106,80,98),(1216,23,86,106,80,99),(1217,23,88,106,80,100),(1218,23,104,106,80,101),(1219,23,93,106,80,102),(1220,23,94,106,81,103),(1221,23,96,106,81,104),(1222,23,95,106,81,105),(1223,23,106,106,82,106),(1224,23,110,106,82,107),(1225,23,109,106,82,108),(1226,23,90,113,80,109),(1227,23,102,113,80,110),(1228,23,86,113,80,111),(1229,23,88,113,80,112),(1230,23,104,113,80,113),(1231,23,93,113,80,114),(1232,23,94,113,81,115),(1233,23,96,113,81,116),(1234,23,95,113,81,117),(1235,23,106,113,82,118),(1236,23,110,113,82,119),(1237,23,109,113,82,120),(1238,23,90,117,80,121),(1239,23,102,117,80,122),(1240,23,86,117,80,123),(1241,23,88,117,80,124),(1242,23,104,117,80,125),(1243,23,93,117,80,126),(1244,23,94,117,81,127),(1245,23,96,117,81,128),(1246,23,95,117,81,129),(1247,23,106,117,82,130),(1248,23,110,117,82,131),(1249,23,109,117,82,132),(1250,23,90,116,80,133),(1251,23,102,116,80,134),(1252,23,86,116,80,135),(1253,23,88,116,80,136),(1254,23,104,116,80,137),(1255,23,93,116,80,138),(1256,23,94,116,81,139),(1257,23,96,116,81,140),(1258,23,95,116,81,141),(1259,23,106,116,82,142),(1260,23,110,116,82,143),(1261,23,109,116,82,144),(1262,23,90,121,80,145),(1263,23,102,121,80,146),(1264,23,86,121,80,147),(1265,23,88,121,80,148),(1266,23,104,121,80,149),(1267,23,93,121,80,150),(1268,23,94,121,81,151),(1269,23,96,121,81,152),(1270,23,95,121,81,153),(1271,23,106,121,82,154),(1272,23,110,121,82,155),(1273,23,109,121,82,156),(1274,23,90,115,80,157),(1275,23,102,115,80,158),(1276,23,86,115,80,159),(1277,23,88,115,80,160),(1278,23,104,115,80,161),(1279,23,93,115,80,162),(1280,23,94,115,81,163),(1281,23,96,115,81,164),(1282,23,95,115,81,165),(1283,23,106,115,82,166),(1284,23,110,115,82,167),(1285,23,109,115,82,168),(1286,23,90,118,80,169),(1287,23,102,118,80,170),(1288,23,86,118,80,171),(1289,23,88,118,80,172),(1290,23,104,118,80,173),(1291,23,93,118,80,174),(1292,23,94,118,81,175),(1293,23,96,118,81,176),(1294,23,95,118,81,177),(1295,23,106,118,82,178),(1296,23,110,118,82,179),(1297,23,109,118,82,180),(1298,23,90,119,80,181),(1299,23,102,119,80,182),(1300,23,86,119,80,183),(1301,23,88,119,80,184),(1302,23,104,119,80,185),(1303,23,93,119,80,186),(1304,23,94,119,81,187),(1305,23,96,119,81,188),(1306,23,95,119,81,189),(1307,23,106,119,82,190),(1308,23,110,119,82,191),(1309,23,109,119,82,192),(1310,28,88,145,91,1),(1311,28,87,145,91,2),(1312,28,86,145,91,3),(1313,28,88,149,91,4),(1314,28,87,149,91,5),(1315,28,86,149,91,6),(1316,28,88,146,91,7),(1317,28,87,146,91,8),(1318,28,86,146,91,9),(1319,28,88,148,91,10),(1320,28,87,148,91,11),(1321,28,86,148,91,12),(1322,28,88,144,91,13),(1323,28,87,144,91,14),(1324,28,86,144,91,15),(1325,28,88,147,91,16),(1326,28,87,147,91,17),(1327,28,86,147,91,18);
/*!40000 ALTER TABLE `questionnaire_question` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `response`
--

DROP TABLE IF EXISTS `response`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `response` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `questionnaireId` int(11) NOT NULL,
  `createdBy` int(11) NOT NULL,
  `createdDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updatedBy` int(11) NOT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `response_questionnaireId_fk_idx` (`questionnaireId`),
  KEY `response_createdBy_fk_idx` (`createdBy`),
  KEY `response_updatedBy_fk_idx` (`updatedBy`),
  CONSTRAINT `response_createdBy_fk` FOREIGN KEY (`createdBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `response_questionnaireId_fk` FOREIGN KEY (`questionnaireId`) REFERENCES `questionnaire` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `response_updatedBy_fk` FOREIGN KEY (`updatedBy`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `response`
--

LOCK TABLES `response` WRITE;
/*!40000 ALTER TABLE `response` DISABLE KEYS */;
INSERT INTO `response` VALUES (4,23,2,'2015-08-14 03:43:11',2,'2015-09-16 03:15:41'),(5,25,2,'2015-08-24 06:13:14',2,'2015-08-24 06:13:14'),(6,28,2,'2015-09-24 09:03:00',2,'2015-09-24 09:13:40'),(7,24,2,'2015-09-24 09:03:06',2,'2015-09-24 09:03:06');
/*!40000 ALTER TABLE `response` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `response_data`
--

DROP TABLE IF EXISTS `response_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `response_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `questionnaireQuestionId` int(11) NOT NULL,
  `responseId` int(11) NOT NULL,
  `responseData` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UQ_response_data_questionnaireQuestionId_responseId` (`questionnaireQuestionId`,`responseId`),
  KEY `response_data_questionnaireId_fk_idx` (`questionnaireQuestionId`),
  KEY `response_data_responseId_fk_idx` (`responseId`),
  CONSTRAINT `response_data_questionnaireQuestionId_fk` FOREIGN KEY (`questionnaireQuestionId`) REFERENCES `questionnaire_question` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `response_data_responseId_fk` FOREIGN KEY (`responseId`) REFERENCES `response` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=latin1 COMMENT='	';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `response_data`
--

LOCK TABLES `response_data` WRITE;
/*!40000 ALTER TABLE `response_data` DISABLE KEYS */;
INSERT INTO `response_data` VALUES (1,1123,4,'{\"responseParaText\":\"\",\"score\":4.0}'),(2,1118,4,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":1.0,\"text\":\"Significant overlap in functionality\"}],\"otherOptionResponseText\":null,\"score\":null}'),(3,1119,4,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":2.0,\"text\":\"Rational Rose\"},{\"sequenceNumber\":4,\"quantifier\":3.0,\"text\":\"System Architect\"}]}'),(4,1120,4,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":1.0,\"text\":\"0 - Only Supports peripherals\"}],\"otherOptionResponseText\":null,\"score\":null}'),(5,1121,4,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":5.0,\"text\":\"All envisaged functionality covered\"}],\"otherOptionResponseText\":null,\"score\":null}'),(6,1122,4,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":2.0,\"text\":\"Helpdesk\"},{\"sequenceNumber\":3,\"quantifier\":2.0,\"text\":\"batch execution\"}]}'),(7,1126,4,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":1.0,\"text\":\"No\"}],\"otherOptionResponseText\":null,\"score\":null}'),(8,1127,4,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":1.0,\"text\":\"Yes\"}],\"otherOptionResponseText\":null,\"score\":null}'),(9,1132,4,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":5.0,\"text\":\"3-5\"}],\"otherOptionResponseText\":null,\"score\":null}'),(10,1144,4,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":5.0,\"text\":\"3-5\"}],\"otherOptionResponseText\":null,\"score\":null}'),(11,1156,4,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":3.0,\"text\":\"1-2\"}],\"otherOptionResponseText\":null,\"score\":null}'),(12,1168,4,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":3.0,\"text\":\"1-2\"}],\"otherOptionResponseText\":null,\"score\":null}'),(13,1180,4,'{\"options\":[{\"sequenceNumber\":4,\"quantifier\":7.0,\"text\":\"6-10\"}],\"otherOptionResponseText\":null,\"score\":null}'),(14,1192,4,'{\"options\":[{\"sequenceNumber\":4,\"quantifier\":7.0,\"text\":\"6-10\"}],\"otherOptionResponseText\":null,\"score\":null}'),(15,1204,4,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":5.0,\"text\":\"3-5\"}],\"otherOptionResponseText\":null,\"score\":null}'),(16,1216,4,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":1.0,\"text\":\"0 - Only Supports peripherals\"}],\"otherOptionResponseText\":null,\"score\":null}'),(17,1228,4,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":3.0,\"text\":\"1-2\"}],\"otherOptionResponseText\":null,\"score\":null}'),(18,1240,4,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":1.0,\"text\":\"0 - Only Supports peripherals\"}],\"otherOptionResponseText\":null,\"score\":null}'),(19,1252,4,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":3.0,\"text\":\"1-2\"}],\"otherOptionResponseText\":null,\"score\":null}'),(20,1264,4,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":3.0,\"text\":\"1-2\"}],\"otherOptionResponseText\":null,\"score\":null}'),(21,1276,4,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":5.0,\"text\":\"3-5\"}],\"otherOptionResponseText\":null,\"score\":null}'),(22,1288,4,'{\"options\":[{\"sequenceNumber\":4,\"quantifier\":7.0,\"text\":\"6-10\"}],\"otherOptionResponseText\":null,\"score\":null}'),(23,1300,4,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":3.0,\"text\":\"1-2\"}],\"otherOptionResponseText\":null,\"score\":null}'),(24,1133,4,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":3.0,\"text\":\"Core functionality implemented\"}],\"otherOptionResponseText\":null,\"score\":null}'),(25,1145,4,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":1.0,\"text\":\"Peripheral functionality implemented\"}],\"otherOptionResponseText\":null,\"score\":null}'),(26,1157,4,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":3.0,\"text\":\"Core functionality implemented\"}],\"otherOptionResponseText\":null,\"score\":null}'),(27,1169,4,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":3.0,\"text\":\"Core functionality implemented\"}],\"otherOptionResponseText\":null,\"score\":null}'),(28,1181,4,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":1.0,\"text\":\"Peripheral functionality implemented\"}],\"otherOptionResponseText\":null,\"score\":null}'),(29,1193,4,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":1.0,\"text\":\"Peripheral functionality implemented\"}],\"otherOptionResponseText\":null,\"score\":null}'),(30,1205,4,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":1.0,\"text\":\"Peripheral functionality implemented\"}],\"otherOptionResponseText\":null,\"score\":null}'),(31,1217,4,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":5.0,\"text\":\"All envisaged functionality covered\"}],\"otherOptionResponseText\":null,\"score\":null}'),(32,1229,4,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":3.0,\"text\":\"Core functionality implemented\"}],\"otherOptionResponseText\":null,\"score\":null}'),(33,1241,4,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":3.0,\"text\":\"Core functionality implemented\"}],\"otherOptionResponseText\":null,\"score\":null}'),(34,1253,4,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":5.0,\"text\":\"All envisaged functionality covered\"}],\"otherOptionResponseText\":null,\"score\":null}'),(35,1265,4,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":1.0,\"text\":\"Peripheral functionality implemented\"}],\"otherOptionResponseText\":null,\"score\":null}'),(36,1277,4,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":3.0,\"text\":\"Core functionality implemented\"}],\"otherOptionResponseText\":null,\"score\":null}'),(37,1289,4,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":1.0,\"text\":\"Peripheral functionality implemented\"}],\"otherOptionResponseText\":null,\"score\":null}'),(38,1301,4,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":3.0,\"text\":\"Core functionality implemented\"}],\"otherOptionResponseText\":null,\"score\":null}'),(39,1326,6,'{\"options\":[{\"sequenceNumber\":1,\"quantifier\":3.0,\"text\":\"In house developed application\"}],\"otherOptionResponseText\":null,\"score\":null}'),(40,1327,6,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":5.0,\"text\":\"3-5\"}],\"otherOptionResponseText\":null,\"score\":null}'),(41,1325,6,'{\"options\":[{\"sequenceNumber\":2,\"quantifier\":3.0,\"text\":\"Core functionality implemented\"}],\"otherOptionResponseText\":null,\"score\":null}'),(42,1324,6,'{\"options\":[{\"sequenceNumber\":3,\"quantifier\":5.0,\"text\":\"3-5\"}],\"otherOptionResponseText\":null,\"score\":null}');
/*!40000 ALTER TABLE `response_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role`
--

DROP TABLE IF EXISTS `role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `roleName` varchar(45) NOT NULL,
  `roleDescription` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role`
--

LOCK TABLES `role` WRITE;
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
INSERT INTO `role` VALUES (11,'ROLE_APP_USER',NULL),(12,'ROLE_APP_ADMIN',NULL);
/*!40000 ALTER TABLE `role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_permission`
--

DROP TABLE IF EXISTS `role_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_permission` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `permissionname` varchar(45) NOT NULL,
  `roleid` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UK_ROLE_PERMISSION` (`permissionname`,`roleid`),
  KEY `FK_ROLEPERM_ROLEID_idx` (`roleid`),
  CONSTRAINT `FK_ROLEPERM_ROLEID` FOREIGN KEY (`roleid`) REFERENCES `role` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=105 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_permission`
--

LOCK TABLES `role_permission` WRITE;
/*!40000 ALTER TABLE `role_permission` DISABLE KEYS */;
INSERT INTO `role_permission` VALUES (12,'APP_LOGIN',11),(93,'APP_LOGIN',12),(90,'ASSET_FILLDETAILS',12),(91,'QUESTIONNAIRE_MODIFY',12),(100,'QUESTIONNAIRE_RESPOND',12),(103,'QUESTIONNAIRE_VIEW',12),(97,'QUESTION_MODIFY',12),(95,'QUESTION_VIEW',12),(88,'REPORT_ASSET_GRAPH',12),(102,'REPORT_FACET_GRAPH',12),(87,'REPORT_SCATTER_GRAPH',12),(104,'TEMPLATE_MODIFY',12),(85,'TEMPLATE_VIEW',12),(101,'USER_MODIFY',12),(11,'USER_ROLE_MODIFY',11),(86,'USER_ROLE_MODIFY',12),(99,'USER_ROLE_VIEW',12),(89,'USER_VIEW',12),(96,'WORKSPACES_MODIFY',12),(98,'WORKSPACES_MODIFY_DASHBOARD',12),(92,'WORKSPACES_VIEW',12),(94,'WORKSPACES_VIEW_DASHBOARD',12);
/*!40000 ALTER TABLE `role_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `firstName` varchar(45) NOT NULL,
  `lastName` varchar(45) NOT NULL,
  `emailAddress` varchar(45) NOT NULL,
  `password` varchar(60) NOT NULL,
  `deleteStatus` tinyint(1) NOT NULL DEFAULT '0',
  `accountExpired` tinyint(1) NOT NULL DEFAULT '0',
  `accountLocked` tinyint(1) NOT NULL DEFAULT '0',
  `credentialExpired` tinyint(1) NOT NULL DEFAULT '0',
  `accountEnabled` tinyint(1) NOT NULL DEFAULT '0',
  `createdBy` int(11) NOT NULL,
  `createdDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updatedBy` int(11) NOT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `emailAddress_UNIQUE` (`emailAddress`),
  UNIQUE KEY `UK_t40ack6c3x1y4m2nhaju018jg` (`emailAddress`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'System','System','thirdeye-system@birlasoft.com','b!rl@s0fT',0,1,1,0,0,-1,'2015-08-13 10:29:36',-1,'2015-08-13 10:29:36'),(2,'Dharmender','Kapoor','dharmender.k@birlasoft.com','welcome12#',0,0,0,0,0,1,'2015-08-13 10:45:54',3,'2015-08-26 09:35:27'),(3,'Samar','Gupta','samar.gupta@birlasoft.com','welcome12#',0,0,0,0,0,2,'2015-08-26 07:58:00',2,'2015-08-26 07:58:06');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_role`
--

DROP TABLE IF EXISTS `user_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `roleId` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_role_unique` (`userId`,`roleId`),
  KEY `roleIdFk_idx` (`roleId`),
  KEY `userIdFk_idx` (`userId`),
  CONSTRAINT `roleIdFk` FOREIGN KEY (`roleId`) REFERENCES `role` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `userIdFk` FOREIGN KEY (`userId`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_role`
--

LOCK TABLES `user_role` WRITE;
/*!40000 ALTER TABLE `user_role` DISABLE KEYS */;
INSERT INTO `user_role` VALUES (9,2,11),(10,2,12),(6,3,11),(7,3,12);
/*!40000 ALTER TABLE `user_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_workspace`
--

DROP TABLE IF EXISTS `user_workspace`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_workspace` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `workspaceId` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_USER_WORKSPACE` (`userId`,`workspaceId`),
  KEY `FK_WORKSPACEID_WORKSPACEID_JOIN_idx` (`workspaceId`),
  CONSTRAINT `FK_USERID_USERID_JOIN` FOREIGN KEY (`userId`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `FK_WORKSPACEID_WORKSPACEID_JOIN` FOREIGN KEY (`workspaceId`) REFERENCES `workspace` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_workspace`
--

LOCK TABLES `user_workspace` WRITE;
/*!40000 ALTER TABLE `user_workspace` DISABLE KEYS */;
INSERT INTO `user_workspace` VALUES (4,2,1),(3,2,4);
/*!40000 ALTER TABLE `user_workspace` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `widget`
--

DROP TABLE IF EXISTS `widget`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `widget` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `widgetType` varchar(20) NOT NULL,
  `dashboardId` int(11) NOT NULL,
  `sequenceNumber` int(11) NOT NULL,
  `widgetConfig` varchar(500) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_WIDGET_DASHBOARD_idx` (`dashboardId`),
  CONSTRAINT `FK_WIDGET_DASHBOARD` FOREIGN KEY (`dashboardId`) REFERENCES `dashboard` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `widget`
--

LOCK TABLES `widget` WRITE;
/*!40000 ALTER TABLE `widget` DISABLE KEYS */;
INSERT INTO `widget` VALUES (15,'FACETGRAPH',4,1,''),(16,'FACETGRAPH',4,2,''),(17,'FACETGRAPH',4,3,'{\"id\":17,\"width\":0,\"height\":0,\"title\":\"test 3\",\"questionnaireId\":23,\"questionId\":88,\"graphType\":\"pie\"}'),(19,'FACETGRAPH',5,1,'{\"id\":19,\"width\":0,\"height\":0,\"title\":\"Functionality\",\"questionnaireId\":23,\"questionId\":88,\"graphType\":\"bar\"}'),(20,'FACETGRAPH',5,2,'{\"id\":20,\"width\":0,\"height\":0,\"title\":\"Now I start showing off\",\"questionnaireId\":23,\"questionId\":110,\"graphType\":\"pie\"}'),(21,'FACETGRAPH',5,3,'{\"id\":21,\"width\":0,\"height\":0,\"title\":\"Another on\",\"questionnaireId\":23,\"questionId\":104,\"graphType\":\"pie\"}');
/*!40000 ALTER TABLE `widget` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `workspace`
--

DROP TABLE IF EXISTS `workspace`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `workspace` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `workspaceName` varchar(45) NOT NULL,
  `workspaceDescription` text,
  `deletedStatus` tinyint(1) NOT NULL DEFAULT '0',
  `createdBy` int(11) NOT NULL,
  `createdDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedBy` int(11) NOT NULL,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `FK_WORKSPACE_USER_idx` (`createdBy`),
  KEY `FK_WORKSPACE_USER_UPDATE_idx` (`updatedBy`),
  CONSTRAINT `FK_WORKSPACE_USER_CREATE` FOREIGN KEY (`createdBy`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `FK_WORKSPACE_USER_UPDATE` FOREIGN KEY (`updatedBy`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `workspace`
--

LOCK TABLES `workspace` WRITE;
/*!40000 ALTER TABLE `workspace` DISABLE KEYS */;
INSERT INTO `workspace` VALUES (1,'Demo for Caterpillar','Workspace for Caterpillar',0,2,'2015-08-13 10:49:14',2,'2015-08-13 10:49:14'),(2,'Another workspace for testing','',0,2,'2015-08-28 11:12:16',2,'2015-08-28 11:12:16'),(3,'Another workspace for 2','',0,2,'2015-08-28 11:15:37',2,'2015-08-28 11:15:37'),(4,'123','',0,2,'2015-08-28 11:19:42',2,'2015-08-28 11:19:42');
/*!40000 ALTER TABLE `workspace` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `workspace_dashboard`
--

DROP TABLE IF EXISTS `workspace_dashboard`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `workspace_dashboard` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `workspaceId` int(11) NOT NULL,
  `dashboardId` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_WORKSPACE_DASHBOARD` (`workspaceId`,`dashboardId`),
  KEY `FK_WORKSPACE_ID_idx` (`workspaceId`),
  KEY `FK_DASHBOARD_ID_idx` (`dashboardId`),
  CONSTRAINT `FK_DASHBOARD_ID` FOREIGN KEY (`dashboardId`) REFERENCES `dashboard` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_WORKSPACE_ID` FOREIGN KEY (`workspaceId`) REFERENCES `workspace` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `workspace_dashboard`
--

LOCK TABLES `workspace_dashboard` WRITE;
/*!40000 ALTER TABLE `workspace_dashboard` DISABLE KEYS */;
/*!40000 ALTER TABLE `workspace_dashboard` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-11-02 14:13:09
