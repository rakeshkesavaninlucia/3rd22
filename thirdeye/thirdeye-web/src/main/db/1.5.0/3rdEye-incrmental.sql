-- MySQL Workbench Synchronization

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';


update   bcm_level set levelNumber = '4' where levelNumber = '3';
update   bcm_level set levelNumber = '3' where levelNumber = '2';
update   bcm_level set levelNumber = '2' where levelNumber = '1';
update   bcm_level set levelNumber = '1' where levelNumber = '0';


ALTER TABLE `bcm_level` 
ADD COLUMN `description` TEXT NULL DEFAULT NULL AFTER `category`;

ALTER TABLE `questionnaire_question` 
ADD COLUMN `boostValue` DECIMAL(5,2) NULL DEFAULT 0 AFTER `questionnaireParameterId`;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;