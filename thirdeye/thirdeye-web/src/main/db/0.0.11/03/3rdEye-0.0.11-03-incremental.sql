-- MySQL Workbench Synchronization

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

ALTER TABLE `3rdi`.`parameter` 
DROP FOREIGN KEY `parameter_questionnairId_fk`,
DROP FOREIGN KEY `parameter_parentTemplateId_fk`;

ALTER TABLE `3rdi`.`parameter` 
DROP COLUMN `templateFlag`,
CHANGE COLUMN `name` `displayName` VARCHAR(250) NOT NULL ,
CHANGE COLUMN `parentTemplateId` `parentParameterId` INT(11) NULL DEFAULT NULL ,
ADD COLUMN `uniqueName` VARCHAR(45) NOT NULL AFTER `id`,
ADD INDEX `parameter_parentParameterId_idx` (`parentParameterId` ASC),
DROP INDEX `parameter_parentTemplateId_fk_idx` ,
DROP INDEX `name_UNIQUE` ;

update `3rdi`.`parameter` set uniqueName=concat(SUBSTR(displayName, 1, 20),'-',createdDate);

ALTER TABLE `3rdi`.`parameter` 
ADD UNIQUE INDEX `parameter_uniqueName_workspaceId_uq` (`uniqueName` ASC, `workspaceId` ASC),
ADD CONSTRAINT `parameter_parentParameterId`
  FOREIGN KEY (`parentParameterId`)
  REFERENCES `3rdi`.`parameter` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;
  
 
CREATE TABLE IF NOT EXISTS `3rdi`.`questionnaire_parameter` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `questionnaireId` INT(11) NOT NULL,
  `parameterId` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `qp_questionnaireId_parameterId_uq` (`questionnaireId` ASC, `parameterId` ASC),
  INDEX `qp_questionnaireId_fk_idx` (`questionnaireId` ASC),
  INDEX `qp_parameterId_fk_idx` (`parameterId` ASC),
  CONSTRAINT `qp_parameterId_fk`
    FOREIGN KEY (`parameterId`)
    REFERENCES `3rdi`.`parameter` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `qp_questionnaireId_fk`
    FOREIGN KEY (`questionnaireId`)
    REFERENCES `3rdi`.`questionnaire` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

  ALTER TABLE `3rdi`.`parameter_function` 
		CHANGE COLUMN `constant` `constant` INT(11) NOT NULL DEFAULT 1 ;
		
  ALTER TABLE `3rdi`.`parameter_function` 
		ADD UNIQUE INDEX `pf_parentParam_que_childParam_uq` (`parentParameterId` ASC, `questionId` ASC, `childparameterId` ASC);
		
ALTER TABLE `3rdi`.`parameter` 
DROP COLUMN `questionnaireId`;

-- creating function to get all child parameters
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `GetParameterTree`(GivenID INT) RETURNS varchar(1024) CHARSET latin1
    DETERMINISTIC
BEGIN

    DECLARE rv,q,queue,queue_children VARCHAR(1024);
    DECLARE queue_length,front_id,pos INT;

    SET rv = '';
    SET queue = GivenID;
    SET queue_length = 1;

    WHILE queue_length > 0 DO
        
        IF queue_length = 1 THEN
            SET front_id = FORMAT(queue,0);
            SET queue = '';
        ELSE
			SET front_id = SUBSTR(queue,1,LOCATE(',',queue)-1);
            SET pos = LOCATE(',',queue) + 1;
            SET q = SUBSTR(queue,pos);
            SET queue = q;
        END IF;
        SET queue_length = queue_length - 1;

        SELECT IFNULL(qc,'') INTO queue_children
        FROM (SELECT GROUP_CONCAT(childparameterId) qc
        FROM parameter_function WHERE parentParameterId = front_id) A;

        IF LENGTH(queue_children) = 0 THEN
            IF LENGTH(queue) = 0 THEN
                SET queue_length = 0;
            END IF;
        ELSE
            IF LENGTH(rv) = 0 THEN
                SET rv = queue_children;
            ELSE
                SET rv = CONCAT(rv,',',queue_children);
            END IF;
            IF LENGTH(queue) = 0 THEN
                SET queue = queue_children;
            ELSE
                SET queue = CONCAT(queue,',',queue_children);
            END IF;
            SET queue_length = LENGTH(queue) - LENGTH(REPLACE(queue,',','')) + 1;
        END IF;
    END WHILE;

    RETURN rv;

END ;;
DELIMITER ;

-- procedure to call function and return tree
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `getParameterTree`(IN GivenID int, OUT rv VARCHAR(1024))
BEGIN

    set rv = getParameterTree(GivenID);

END ;;
DELIMITER ;

ALTER TABLE `3rdi`.`questionnaire_parameter` 
DROP INDEX `qp_questionnaireId_parameterId_uq` ;

ALTER TABLE `3rdi`.`questionnaire_parameter` 
ADD COLUMN `rootParameterId` INT(11) NULL AFTER `parameterId`,
ADD COLUMN `parentParameterId` INT(11) NULL AFTER `rootParameterId`,
ADD INDEX `qp_rootParameterId_fk_idx` (`rootParameterId` ASC),
ADD INDEX `qp_parentParameterId_fk_idx` (`parentParameterId` ASC),
ADD UNIQUE INDEX `qp_questionnaireId_parameterId_parentParameterId_uq` (`questionnaireId` ASC, `parameterId` ASC, `parentParameterId` ASC);
ALTER TABLE `3rdi`.`questionnaire_parameter` 
ADD CONSTRAINT `qp_rootParameterId_fk`
  FOREIGN KEY (`rootParameterId`)
  REFERENCES `3rdi`.`parameter` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `qp_parentParameterId_fk`
  FOREIGN KEY (`parentParameterId`)
  REFERENCES `3rdi`.`parameter` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
