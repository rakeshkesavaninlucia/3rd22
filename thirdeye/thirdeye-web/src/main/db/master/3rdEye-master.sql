CREATE DATABASE  IF NOT EXISTS `3rdi_master` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `3rdi_master`;
-- MySQL dump 10.13  Distrib 5.6.23, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: 3rdi_master
-- ------------------------------------------------------
-- Server version	5.6.24

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tenant`
--

DROP TABLE IF EXISTS `3rdi_master`.`tenant`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `3rdi_master`.`tenant` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tenantUrlId` varchar(45) NOT NULL,
  `tenantAccountId` varchar(45) NOT NULL,
  `tenantName` varchar(45) NOT NULL,
  `tenantDesc` varchar(255) NOT NULL,
  `tenantDbUrl` varchar(255) NOT NULL,
  `tenantDbUserName` varchar(45) NOT NULL,
  `tenantDbPassword` varchar(60) NOT NULL,
  `deleteStatus` tinyint(1) NOT NULL DEFAULT '0',
  `accountLocked` tinyint(1) NOT NULL DEFAULT '0',
  `accountExpired` tinyint(1) NOT NULL DEFAULT '0',
  `createdDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `tenantId_UNIQUE` (`tenantUrlId`),
  UNIQUE KEY `tenantName_UNIQUE` (`tenantName`),
  UNIQUE KEY `tenantAccountId_UNIQUE` (`tenantAccountId`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tenant`
--

LOCK TABLES `3rdi_master`.`tenant` WRITE;
/*!40000 ALTER TABLE `3rdi_master`.`tenant` DISABLE KEYS */;
INSERT INTO `3rdi_master`.`tenant` VALUES (1,'123','123456','Shaishav','Shaishav','jdbc:mysql://127.0.0.1:3306/3rdi','3rdi_app','welcome12#',0,0,0,'2016-05-11 05:20:01','2016-05-11 05:20:01');
/*!40000 ALTER TABLE `3rdi_master`.`tenant` ENABLE KEYS */;
UNLOCK TABLES;

-- Creating user for master schema
CREATE USER '3rdi_master'@'localhost' IDENTIFIED BY 'welcome12#';
GRANT ALL PRIVILEGES ON 3rdi_master.* To '3rdi_master'@'localhost';

--
-- Dumping routines for database '3rdi_master'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-05-16 17:27:37
