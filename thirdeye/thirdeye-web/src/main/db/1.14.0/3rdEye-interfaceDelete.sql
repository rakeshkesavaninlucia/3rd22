-- Delete response for interfaces
DELETE from response_data where questionnaireQuestionId in (select id from questionnaire_question where questionnaireId in (select id from questionnaire where assetTypeId=7));
DELETE from response_data where questionnaireQuestionId in (select id from questionnaire_question where questionnaireAssetId in (select id from questionnaire_asset where assetId in (select id from asset where templateId in (select id from asset_template where assetTypeId=7))));
DELETE from response where questionnaireId in (select id from questionnaire where assetTypeId=7);

-- Delete questionnaire related data for interfaces
DELETE from questionnaire_question where questionnaireId in (select id from questionnaire where assetTypeId=7);
DELETE from questionnaire_question where questionnaireAssetId in (select id from questionnaire_asset where assetId in (select id from asset where templateId in (select id from asset_template where assetTypeId=7)));
DELETE from questionnaire_parameter_asset where questionnaireId in (select id from questionnaire where assetTypeId=7);
DELETE from questionnaire_asset where questionnaireId in (select id from questionnaire where assetTypeId=7);
DELETE from questionnaire_asset where assetId in (select id from asset where templateId in (select id from asset_template where assetTypeId=7));
DELETE from questionnaire_parameter where questionnaireId in (select id from questionnaire where assetTypeId=7);
DELETE from questionnaire where assetTypeId=7;

-- Delete graphs for interfaces
DELETE from graph_asset_template where assetTemplateId in (select id from asset_template where assetTypeId=7);
DELETE from graph where id in (select graphId from graph_asset_template where assetTemplateId in (select id from asset_template where assetTypeId=7));

-- Delete functional maps for interfaces
DELETE from functional_map_data where functionalMapId in (select id from functional_map where assetTemplateId in (select id from asset_template where assetTypeId=7));
DELETE from functional_map where assetTemplateId in (select id from asset_template where assetTypeId=7);

-- Delete asset data for interfaces
DELETE from asset_data where assetId in (select id from asset where templateId in (select id from asset_template where assetTypeId=7));
DELETE from asset where templateId in (select id from asset_template where assetTypeId=7);
DELETE from asset_template_column where assetTemplateId in (select id from asset_template where assetTypeId=7);
DELETE from asset_template where assetTypeId=7;
DELETE from asset_type_style where asset_type_id=7;
DELETE from asset_type where assetTypeName='Interfaces';