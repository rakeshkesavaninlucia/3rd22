-- MySQL Workbench Synchronization

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

ALTER TABLE `3rdi`.`aid` 
ADD UNIQUE INDEX `UQ_AID_Name_WorkspaceId` (`name` ASC, `workspaceId` ASC);

CREATE TABLE IF NOT EXISTS `3rdi`.`bcm` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `bcmId` INT(11) NULL DEFAULT NULL,
  `bcmName` VARCHAR(255) NOT NULL,
  `workspaceId` INT(11) NULL DEFAULT NULL,
  `createdBy` INT(11) NOT NULL,
  `createdDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedBy` INT(11) NOT NULL,
  `updatedDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  INDEX `bcm_workspaceId_idx` (`workspaceId` ASC),
  INDEX `bcm_createdBy_idx` (`createdBy` ASC),
  INDEX `bcm_updatedBy_idx` (`updatedBy` ASC),
  INDEX `bcm_bcmId_idx` (`bcmId` ASC),
  CONSTRAINT `bcm_bcmId`
    FOREIGN KEY (`bcmId`)
    REFERENCES `3rdi`.`bcm` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `bcm_createdBy`
    FOREIGN KEY (`createdBy`)
    REFERENCES `3rdi`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `bcm_updatedBy`
    FOREIGN KEY (`updatedBy`)
    REFERENCES `3rdi`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `bcm_workspaceId`
    FOREIGN KEY (`workspaceId`)
    REFERENCES `3rdi`.`workspace` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 9
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

CREATE TABLE IF NOT EXISTS `3rdi`.`bcm_level` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `parentBcmLevelId` INT(11) NULL DEFAULT NULL,
  `bcmId` INT(11) NOT NULL,
  `levelNumber` INT(2) NOT NULL,
  `sequenceNumber` INT(11) NOT NULL,
  `bcmLevelName` VARCHAR(60) NOT NULL,
  `category` VARCHAR(30) NULL DEFAULT NULL,
  `createdBy` INT(11) NOT NULL,
  `createdDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedBy` INT(11) NOT NULL,
  `updatedDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  INDEX `bcm_level_parentBcmLevelId_fk_idx` (`parentBcmLevelId` ASC),
  INDEX `bcm_level_bcmId_fk_idx` (`bcmId` ASC),
  INDEX `bcm_level_createdBy_fk_idx` (`createdBy` ASC),
  INDEX `bcm_level_updatedBy_fk_idx` (`updatedBy` ASC),
  CONSTRAINT `bcm_level_bcmId_fk`
    FOREIGN KEY (`bcmId`)
    REFERENCES `3rdi`.`bcm` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `bcm_level_createdBy_fk`
    FOREIGN KEY (`createdBy`)
    REFERENCES `3rdi`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `bcm_level_parentBcmLevelId_fk`
    FOREIGN KEY (`parentBcmLevelId`)
    REFERENCES `3rdi`.`bcm_level` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `bcm_level_updatedBy_fk`
    FOREIGN KEY (`updatedBy`)
    REFERENCES `3rdi`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 15
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
