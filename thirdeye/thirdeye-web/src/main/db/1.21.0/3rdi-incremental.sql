-- MySQL Workbench Synchronization

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';


ALTER TABLE `report` ADD COLUMN `status` TINYINT(1) NOT NULL DEFAULT '0' AFTER `deletedStatus`;


ALTER TABLE `user` ADD COLUMN `homePageURL` VARCHAR(45) NULL AFTER `firstLogin`;


ALTER TABLE `user` 
ADD COLUMN `selectedHomePage` VARCHAR(45) NULL AFTER `homePageURL`;



SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
