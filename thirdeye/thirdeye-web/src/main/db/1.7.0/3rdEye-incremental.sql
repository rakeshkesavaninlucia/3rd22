-- MySQL Workbench Synchronization

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

CREATE TABLE `benchmark` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `workspaceId` INT(11) NULL,
  `createdBy` INT(11) NOT NULL,
  `createdDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedBy` INT(11) NOT NULL,
  `updatedDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  INDEX `benchmark_workspaceId_fk_idx` (`workspaceId` ASC),
  INDEX `benchmark_createdBy_fk_idx` (`createdBy` ASC),
  INDEX `benchmark_updatedBy_fk_idx` (`updatedBy` ASC),
  UNIQUE INDEX `benchmark_name_wsid_uq` (`name` ASC, `workspaceId` ASC),
  CONSTRAINT `benchmark_workspaceId_fk`
    FOREIGN KEY (`workspaceId`)
    REFERENCES `workspace` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `benchmark_createdBy_fk`
    FOREIGN KEY (`createdBy`)
    REFERENCES `user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `benchmark_updatedBy_fk`
    FOREIGN KEY (`updatedBy`)
    REFERENCES `user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);
    
  CREATE TABLE `benchmark_item` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `baseName` VARCHAR(45) NOT NULL,
  `version` VARCHAR(45) NULL,
  `displayName` VARCHAR(100) NOT NULL,
  `sequenceNumber` INT(11) NOT NULL,
  `benchmarkId` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `benchmark_item_benchmarkId_fk_idx` (`benchmarkId` ASC),
  UNIQUE INDEX `benchmark_item_displayName_benchmarkId_uq` (`displayName` ASC, `benchmarkId` ASC),
  CONSTRAINT `benchmark_item_benchmarkId_fk`
    FOREIGN KEY (`benchmarkId`)
    REFERENCES `benchmark` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);

CREATE TABLE `benchmark_item_score` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `benchmarkItemId` INT(11) NOT NULL,
  `startYear` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `endYear` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `score` DECIMAL(10,3) NULL,
  PRIMARY KEY (`id`),
  INDEX `bis_benchmarkItemId_fk_idx` (`benchmarkItemId` ASC),
  CONSTRAINT `bis_benchmarkItemId_fk`
    FOREIGN KEY (`benchmarkItemId`)
    REFERENCES `benchmark_item` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);


ALTER TABLE `asset_template_column` 
ADD COLUMN `filterable` TINYINT(1) NOT NULL DEFAULT 0 AFTER `mandatory`;



SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;