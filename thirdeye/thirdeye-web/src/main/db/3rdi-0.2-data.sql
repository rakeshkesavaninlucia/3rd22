-- MySQL dump 10.13  Distrib 5.6.23, for Win32 (x86)
--
-- Host: 127.0.0.1    Database: 3rdi
-- ------------------------------------------------------
-- Server version	5.6.24

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `asset`
--

LOCK TABLES `asset` WRITE;
/*!40000 ALTER TABLE `asset` DISABLE KEYS */;
INSERT INTO `asset` VALUES (1,1,0,7,'2015-07-14 13:06:47',7,'2015-07-14 13:06:47'),(2,2,0,7,'2015-07-20 10:52:36',7,'2015-07-20 10:52:36'),(3,2,0,7,'2015-07-21 04:39:25',7,'2015-07-21 04:39:25'),(4,2,0,7,'2015-07-21 04:39:38',7,'2015-07-21 04:39:38');
/*!40000 ALTER TABLE `asset` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `asset_data`
--

LOCK TABLES `asset_data` WRITE;
/*!40000 ALTER TABLE `asset_data` DISABLE KEYS */;
INSERT INTO `asset_data` VALUES (1,'9874',2,1),(2,'12345',3,1),(3,'test',1,1),(4,'12445555',20,2),(5,'2015-07-20',21,2),(6,'gfggghfghf',19,2),(7,'4545645',20,3),(8,'dfgdfg',26,3),(9,'gfggghfghf',19,3),(10,'2015-07-21',21,3),(11,'sdfsdg',22,3),(12,'retret',22,4),(13,'rtrtrt',19,4),(14,'2015-07-21',21,4),(15,'4564567',20,4),(16,'ertert',26,4);
/*!40000 ALTER TABLE `asset_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `asset_template`
--

LOCK TABLES `asset_template` WRITE;
/*!40000 ALTER TABLE `asset_template` DISABLE KEYS */;
INSERT INTO `asset_template` VALUES (1,'Template-1',5,0,'This is the test Template -1 ',1,7,'2015-07-14 13:05:23',7,'2015-07-14 13:05:23'),(2,'Template 2',5,0,'',2,7,'2015-07-20 10:50:33',7,'2015-07-20 10:50:33');
/*!40000 ALTER TABLE `asset_template` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `asset_template_column`
--

LOCK TABLES `asset_template_column` WRITE;
/*!40000 ALTER TABLE `asset_template_column` DISABLE KEYS */;
INSERT INTO `asset_template_column` VALUES (1,'col27','text',64,1,0,1,1,7,'2015-07-14 13:06:04',7,'2015-07-21 07:15:46'),(2,'col2','number',20,1,0,1,2,7,'2015-07-14 13:06:04',7,'2015-07-20 10:44:20'),(3,'col245','number',10,1,0,1,3,7,'2015-07-14 13:06:04',7,'2015-07-20 12:57:25'),(4,'sdsdrer','text',87,1,0,1,4,7,'2015-07-16 09:24:56',7,'2015-07-21 07:20:45'),(5,'col4','date',44,1,0,1,5,7,'2015-07-16 10:05:47',7,'2015-07-20 06:38:32'),(7,'col6','text',17,1,0,1,7,7,'2015-07-16 10:12:24',7,'2015-07-21 07:23:46'),(8,'col7','text',15,1,0,1,8,7,'2015-07-16 10:17:48',7,'2015-07-21 07:13:33'),(12,'Today','text',36,1,0,1,10,7,'2015-07-16 10:30:07',7,'2015-07-16 11:18:45'),(14,'ffd','text',15,1,0,1,12,7,'2015-07-16 10:55:35',7,'2015-07-16 10:55:35'),(15,'Today1','text',54,1,0,1,13,7,'2015-07-16 11:08:44',7,'2015-07-20 06:27:52'),(16,'vv','text',34,1,0,1,14,7,'2015-07-16 12:35:01',7,'2015-07-16 12:35:01'),(17,'today8','text',35,1,0,1,15,7,'2015-07-20 06:27:53',7,'2015-07-20 06:19:38'),(18,'dsds','text',12,1,0,1,16,7,'2015-07-20 06:58:06',7,'2015-07-21 07:06:03'),(19,'col21','TEXT',10,2,0,1,1,7,'2015-07-20 10:51:24',7,'2015-07-21 06:51:55'),(20,'col22','NUMBER',10,2,0,0,2,7,'2015-07-20 10:51:24',7,'2015-07-20 10:51:24'),(21,'col3','DATE',34,2,0,0,3,7,'2015-07-20 10:51:24',7,'2015-07-20 10:51:24'),(22,'col222','text',10,2,0,1,4,7,'2015-07-20 12:59:33',7,'2015-07-20 13:13:32'),(26,'col2update','text',12,2,0,1,5,7,'2015-07-20 13:13:48',7,'2015-07-20 13:14:16'),(27,'col24','NUMBER',15,2,0,0,6,7,'2015-07-21 06:53:07',7,'2015-07-21 06:53:07'),(28,'Today12','NUMBER',21,1,0,1,17,7,'2015-07-21 07:17:26',7,'2015-07-21 07:17:25');
/*!40000 ALTER TABLE `asset_template_column` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `asset_type`
--

LOCK TABLES `asset_type` WRITE;
/*!40000 ALTER TABLE `asset_type` DISABLE KEYS */;
INSERT INTO `asset_type` VALUES (5,'Application',0,7,'2015-07-14 13:04:01',7,'2015-07-14 13:04:01'),(6,'Server',0,7,'2015-07-14 13:04:50',7,'2015-07-14 13:04:50'),(7,'DB',0,7,'2015-07-14 13:04:50',7,'2015-07-14 13:04:50'),(8,'Person',0,7,'2015-07-14 13:04:50',7,'2015-07-14 13:04:50'),(9,'Business services',0,7,'2015-07-14 13:04:50',7,'2015-07-14 13:04:50'),(10,'IT services',0,7,'2015-07-14 13:04:50',7,'2015-07-14 13:04:50'),(11,'Interfaces',0,7,'2015-07-14 13:04:50',7,'2015-07-14 13:04:50');
/*!40000 ALTER TABLE `asset_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `graph`
--

LOCK TABLES `graph` WRITE;
/*!40000 ALTER TABLE `graph` DISABLE KEYS */;
INSERT INTO `graph` VALUES (1,'graph-1','This is the test graph-1',1,'pc',0,7,'2015-07-14 13:07:14',7,'2015-07-21 06:10:29'),(2,'graph-2','This is the graph-2.',1,'pc',0,7,'2015-07-15 04:17:30',7,'2015-07-20 11:09:20'),(3,'','',1,'select',0,7,'2015-07-20 11:10:30',7,'2015-07-21 04:40:45'),(6,'sdwefr','refrff',1,'PC',0,7,'2015-07-20 11:16:09',7,'2015-07-20 11:16:09');
/*!40000 ALTER TABLE `graph` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `graph_asset`
--

LOCK TABLES `graph_asset` WRITE;
/*!40000 ALTER TABLE `graph_asset` DISABLE KEYS */;
INSERT INTO `graph_asset` VALUES (6,1,1),(3,2,1),(5,3,1);
/*!40000 ALTER TABLE `graph_asset` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `interface`
--

LOCK TABLES `interface` WRITE;
/*!40000 ALTER TABLE `interface` DISABLE KEYS */;
INSERT INTO `interface` VALUES (1,'interface-1',1,1,1,'NW',7,'2015-07-14 13:07:45',7,'2015-07-14 13:07:45'),(2,'interface-2',1,1,2,'NW',7,'2015-07-15 04:30:06',7,'2015-07-15 04:30:06');
/*!40000 ALTER TABLE `interface` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `question`
--

LOCK TABLES `question` WRITE;
/*!40000 ALTER TABLE `question` DISABLE KEYS */;
INSERT INTO `question` VALUES (1,'test question','help text',NULL,'TEXT',NULL,7,'2015-07-15 05:40:08',7,'2015-07-15 05:40:08'),(2,'test question','help text',NULL,'TEXT',NULL,7,'2015-07-15 05:43:13',7,'2015-07-15 05:43:13'),(3,'test question','help text',NULL,'TEXT',NULL,7,'2015-07-15 06:27:20',7,'2015-07-15 06:27:20'),(5,'question-2','this is the test que-2',NULL,'TEXT',NULL,7,'2015-07-15 06:35:49',7,'2015-07-15 06:35:49'),(6,'This  is New Test Question for workspace2','Hi there is new Question for 2',2,'PARATEXT',NULL,7,'2015-07-15 10:01:35',7,'2015-07-17 08:50:22'),(7,'','',NULL,'-1',NULL,7,'2015-07-15 10:04:37',7,'2015-07-15 10:04:37'),(8,'This is Second Question for workspace 2','This is Second Test 2',2,'TEXT',NULL,7,'2015-07-15 10:07:08',7,'2015-07-17 08:55:57'),(10,'This is Third Question','This is New Test Third',NULL,'TEXT',NULL,7,'2015-07-15 10:17:59',7,'2015-07-15 10:17:59'),(11,'tttret11','ertert11',2,'TEXT',NULL,7,'2015-07-15 11:09:34',7,'2015-07-17 09:33:35'),(12,'ggdfg','gfgfgggg',1,'MULTCHOICE','fghgfh;gfhgfh;gfhg',7,'2015-07-15 11:11:13',7,'2015-07-20 08:27:03'),(13,'wafdrgfghfth3456','sdfsdff',1,'CHECKBOXES','2314,efsd,sdfsd,sdfgsdf,sdddd',7,'2015-07-15 11:36:14',7,'2015-07-15 11:36:14'),(14,'wafdrgfghfth3456','sdfsdff',NULL,'CHECKBOXES','edfsdf,dfdsf,dsfsdf,sdfsdf',7,'2015-07-15 11:36:55',7,'2015-07-15 11:36:55'),(15,'wafdrgfghfth3456','sdfsdff',NULL,'MULTCHOICE',',,',7,'2015-07-15 11:40:47',7,'2015-07-15 11:40:47'),(16,'wefgrthtyjh','tyjhtyjtyjh',1,'MULTCHOICE','swq,rfer',7,'2015-07-15 11:43:00',7,'2015-07-15 11:43:00'),(17,'Test 1','help test 1',1,'PARATEXT',NULL,7,'2015-07-15 11:43:37',7,'2015-07-15 11:43:37'),(18,'test que 1','help text 1',1,'MULTCHOICE','test 1;test 2,option 3,option 4',7,'2015-07-15 13:14:50',7,'2015-07-15 13:14:50'),(19,'fdgdfgdfg','fghfgh',1,'MULTCHOICE',NULL,7,'2015-07-15 13:20:10',7,'2015-07-17 09:38:51'),(20,'thfgh','hfghfgh',1,'TEXT',NULL,7,'2015-07-15 13:22:07',7,'2015-07-20 07:52:27'),(21,'ffghf','hfghfghfgh',1,'MULTCHOICE','op1;op2;op3;op4;op5',7,'2015-07-15 13:28:16',7,'2015-07-15 13:28:16'),(22,'dgrtjuyk','tyjuyjk',NULL,'TEXT',NULL,7,'2015-07-15 13:34:57',7,'2015-07-15 13:34:57'),(23,'wqfrtjhujku','uyjuyk',1,'PARATEXT',NULL,7,'2015-07-15 13:35:27',7,'2015-07-15 13:35:27'),(24,'ghfgjh','jghjghj',1,'MULTCHOICE','op1;op 3;op4;op5',7,'2015-07-15 13:36:13',7,'2015-07-15 13:36:13'),(25,'dvh','hgfhgh',1,'CHECKBOXES','jwqo1;nkjnhkj213;nnkn',7,'2015-07-15 13:37:21',7,'2015-07-15 13:37:21'),(26,'xdsdfvdfgh','thfghfgh',1,'MULTCHOICE','576;bhkj;nljl;bkhkhkh',7,'2015-07-15 13:38:26',7,'2015-07-15 13:38:26'),(27,'2efgrthtyuj6456','tyhtyjhghj',1,'MULTCHOICE','nkjn%3Bnjkbhb;bkjhk%3Bnmnjk;bnkb+++%3Bbjbjhb;nkjbkjh%3Bnhbjh',7,'2015-07-15 13:51:39',7,'2015-07-15 13:51:39'),(28,'weferghfgjh','jhghjghj',1,'CHECKBOXES','ghdfh;gh%3Bfghfghfgh;ghfgh%23%24%24%25%25%5E%5E%26%5E',7,'2015-07-15 13:54:01',7,'2015-07-15 13:54:01'),(29,'test question','',NULL,'CHECKBOXES','empty;rtgg',7,'2015-07-15 13:56:38',7,'2015-07-20 09:39:27'),(30,'','',NULL,'CHECKBOXES','',7,'2015-07-15 13:56:38',7,'2015-07-15 13:56:38'),(31,'','',NULL,'-1',NULL,7,'2015-07-15 13:56:47',7,'2015-07-15 13:56:47'),(32,'3445g','fdfgfg',1,'MULTCHOICE','fdgdfg435;dfdffgfdgg',7,'2015-07-16 04:23:14',7,'2015-07-16 04:23:14'),(33,'3445g','fdfgfg',1,'MULTCHOICE','fdgdfg435;dfdffgfdgg',7,'2015-07-16 04:23:14',7,'2015-07-16 04:23:14'),(34,'','',1,'PARATEXT',NULL,7,'2015-07-16 05:14:19',7,'2015-07-16 05:14:19'),(35,'New Test Question','hello',1,'PARATEXT',NULL,7,'2015-07-16 08:23:24',7,'2015-07-16 08:23:24'),(36,'This is Test Question ','Hello',NULL,'TEXT',NULL,7,'2015-07-16 08:32:47',7,'2015-07-20 07:05:47'),(37,'test question','tiuui',1,'CHECKBOXES','hello+empty',7,'2015-07-16 09:05:09',7,'2015-07-20 09:34:50'),(38,'new','reer',1,'TEXT',NULL,7,'2015-07-16 09:11:55',7,'2015-07-16 09:11:55'),(39,'What is the business criticality of the application ','Please select the criticality from low medium / high',NULL,'MULTCHOICE','Low;medium;High',7,'2015-07-17 04:55:22',7,'2015-07-20 05:35:22'),(40,'what is CPU cost','Please select any thing',1,'PARATEXT',NULL,7,'2015-07-17 07:17:39',7,'2015-07-17 07:17:39'),(41,'DSSDSS','SDSDSD',1,'TEXT',NULL,7,'2015-07-17 08:50:51',7,'2015-07-17 08:50:51'),(42,'This is the multipole','help text for ',NULL,'MULTCHOICE','',7,'2015-07-17 09:41:13',7,'2015-07-17 09:41:13'),(43,'test question','rwerr',1,'CHECKBOXES','ere;ewrewr;ewre',7,'2015-07-17 09:53:33',7,'2015-07-17 09:53:33'),(44,'test question','rwerr',1,'CHECKBOXES','ere;ewrewr;ewre',7,'2015-07-17 09:53:33',7,'2015-07-17 09:53:33'),(45,'This is new Question Test ','Hi Please select any thing here',1,'MULTCHOICE','Test1;Test2;Test3;Test4',7,'2015-07-17 10:20:31',7,'2015-07-17 10:20:31'),(46,'New Test Question Here fo Testing','Hi this is Me',1,'CHECKBOXES','Test1;Test2;Test3',7,'2015-07-17 10:25:13',7,'2015-07-17 10:25:13'),(47,'what is the cost of server','Hello This cost Question',1,'TEXT',NULL,7,'2015-07-17 10:47:08',7,'2015-07-17 10:47:08'),(48,'dfjsdlkfj','kljsd',NULL,'PARATEXT',NULL,7,'2015-07-17 11:01:15',7,'2015-07-17 11:01:15'),(49,'What is the status of CPU and Server','Very fine if you selected any one ...',2,'CHECKBOXES','very;low;high;nothing',7,'2015-07-17 11:57:51',7,'2015-07-17 11:57:51'),(50,'This is new Question for Server','This is Test for Server',2,'CHECKBOXES','high;low;very+low;very+high;nothing',7,'2015-07-20 05:33:39',7,'2015-07-20 05:33:39'),(51,'tergggrt','dfgdfgg',1,'TEXT',NULL,7,'2015-07-20 05:44:32',7,'2015-07-20 06:20:17'),(52,'Server ',' About Server ',1,'MULTCHOICE','High;Low;very+Low;very+High',7,'2015-07-20 05:56:48',7,'2015-07-20 05:56:48'),(53,'This is Test for Server application','Please select some text',2,'TEXT',NULL,7,'2015-07-20 06:22:28',7,'2015-07-20 06:22:28'),(54,'SERVERE','TEST SERVERW',1,'MULTCHOICE','HIGH',7,'2015-07-20 07:02:30',7,'2015-07-20 07:03:49'),(55,'This is Second Question for workspace','hello',2,'CHECKBOXES','hello;hello+1;hello+3',7,'2015-07-20 07:17:38',7,'2015-07-20 07:17:38'),(56,'Management update','Test update here',2,'MULTCHOICE','eew',7,'2015-07-20 08:23:01',7,'2015-07-20 08:26:55'),(57,'ssasa','sasasa',NULL,'PARATEXT',NULL,7,'2015-07-20 08:27:40',7,'2015-07-20 08:27:40'),(58,'hgjhj','',NULL,'CHECKBOXES',NULL,7,'2015-07-20 10:00:39',7,'2015-07-20 10:00:39'),(59,'ergttrg','',NULL,'MULTCHOICE',NULL,7,'2015-07-20 10:02:28',7,'2015-07-20 10:02:28'),(60,'What is the cost of all Server ','Please select a choice ',1,'CHECKBOXES','one;two;three;four',7,'2015-07-21 05:52:52',7,'2015-07-21 05:52:52'),(61,'Update','update',2,'MULTCHOICE','one;two;three',7,'2015-07-21 06:11:27',7,'2015-07-21 06:11:50'),(62,'Test update','Test',1,'CHECKBOXES','q;q;q',7,'2015-07-21 06:28:01',7,'2015-07-21 06:28:01'),(63,'Test Update One','Test Update',2,'MULTCHOICE','r;r;r;r',7,'2015-07-21 06:28:56',7,'2015-07-21 06:28:56'),(64,'Question for Workspace Management','This is Test application',1,'MULTCHOICE','one;two;three;four;five',7,'2015-07-21 07:03:47',7,'2015-07-21 07:04:28');
/*!40000 ALTER TABLE `question` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `questionnaire`
--

LOCK TABLES `questionnaire` WRITE;
/*!40000 ALTER TABLE `questionnaire` DISABLE KEYS */;
/*!40000 ALTER TABLE `questionnaire` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `questionnaire_question`
--

LOCK TABLES `questionnaire_question` WRITE;
/*!40000 ALTER TABLE `questionnaire_question` DISABLE KEYS */;
/*!40000 ALTER TABLE `questionnaire_question` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `response`
--

LOCK TABLES `response` WRITE;
/*!40000 ALTER TABLE `response` DISABLE KEYS */;
/*!40000 ALTER TABLE `response` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `response_data`
--

LOCK TABLES `response_data` WRITE;
/*!40000 ALTER TABLE `response_data` DISABLE KEYS */;
/*!40000 ALTER TABLE `response_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `role`
--

LOCK TABLES `role` WRITE;
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
INSERT INTO `role` VALUES (6,'ROLE_APP_USER'),(7,'ROLE_APP_ADMIN');
/*!40000 ALTER TABLE `role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (7,'Samar','Gupta','samar.gupta@birlasoft.com','welcome12#',0,0,0,0,0,-1,'2015-07-14 12:30:25',-1,'2015-07-14 12:30:25');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `user_role`
--

LOCK TABLES `user_role` WRITE;
/*!40000 ALTER TABLE `user_role` DISABLE KEYS */;
INSERT INTO `user_role` VALUES (2,7,6),(3,7,7);
/*!40000 ALTER TABLE `user_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `user_workspace`
--

LOCK TABLES `user_workspace` WRITE;
/*!40000 ALTER TABLE `user_workspace` DISABLE KEYS */;
INSERT INTO `user_workspace` VALUES (1,7,1),(2,7,2);
/*!40000 ALTER TABLE `user_workspace` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `workspace`
--

LOCK TABLES `workspace` WRITE;
/*!40000 ALTER TABLE `workspace` DISABLE KEYS */;
INSERT INTO `workspace` VALUES (1,'Workspace-1','This is the workspace-1',0,7,'2015-07-14 12:58:48',7,'2015-07-14 12:58:48'),(2,'workspace-2','This is the test workspace -2 ',0,7,'2015-07-17 07:20:31',7,'2015-07-17 07:20:31');
/*!40000 ALTER TABLE `workspace` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-07-21 12:59:58
