package org.birlasoft.thirdeye.validators;

import org.birlasoft.thirdeye.entity.Password;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 * This {@code class} validate the {@code necessary}  fields ,
 * logic of {@code validation} is here.
 */
@Component
public class ChangePasswordValidator implements Validator {
	

	private static final String CURRENT_USER_PASS = "currentPassword";
	private static final String NEW_USER_PASS = "newPassword";
	private static final String CONFIRM_USER_PASS = "confirmPassword";

	@Autowired
	private Environment env;
	
	@Override
	public boolean supports(Class<?> clazz) {
		return Password.class.equals(clazz);
	}
	
	@Override
    public void validate(Object target, Errors errors) {
			Password password = (Password)target;
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, CURRENT_USER_PASS, "error.currentPassword", env.getProperty("error.currentPassword"));
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, NEW_USER_PASS, "error.newPassword", env.getProperty("error.newPassword"));
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, CONFIRM_USER_PASS, "error.confirmPassword", env.getProperty("error.confirmPassword"));
            
            //new & confirm password
            if( !password.getNewPassword().equals(password.getConfirmPassword())){
              errors.rejectValue(CONFIRM_USER_PASS, "error.confirmPassword.mismatch", env.getProperty("error.confirmPassword.mismatch")); 
            }
            //current & db password
            BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
			if (!passwordEncoder.matches(password.getCurrentPassword(), password.getDbPassword())) {

				errors.rejectValue(CURRENT_USER_PASS, "error.currentPassword.mismatch",env.getProperty("error.currentPassword.mismatch"));
			}
			// current & new password
			if( password.getCurrentPassword().equals(password.getNewPassword())){
	              errors.rejectValue(NEW_USER_PASS, "error.Password.mismatch", env.getProperty("error.Password.mismatch")); 
	            } 
	}	
}
