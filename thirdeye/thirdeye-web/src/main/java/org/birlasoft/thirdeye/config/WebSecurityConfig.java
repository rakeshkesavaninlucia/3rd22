package org.birlasoft.thirdeye.config;

import org.birlasoft.thirdeye.filters.AccessDeniedFilter;
import org.birlasoft.thirdeye.security.CustomAuthenticationFailureHandler;
import org.birlasoft.thirdeye.security.CustomAuthenticationSuccessHandler;
import org.birlasoft.thirdeye.security.CustomDetailsFilter;
import org.birlasoft.thirdeye.service.CustomUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = false, prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	private CustomUserDetailsService customUserDetailsService;

	@Bean
	@Override
	public AuthenticationManager authenticationManagerBean() throws Exception {
		return super.authenticationManagerBean();
	}

	/*
	 * @Bean public AuthenticationProvider getCustomAuthenticationProvider(){
	 * return new CustomAuthenticationProvider(); }
	 */

	@Override
	public void configure(WebSecurity web) throws Exception {
		web.ignoring().antMatchers("/static/**");
		web.ignoring().antMatchers("/forgot/**");
		web.ignoring().antMatchers("/questionAnswer/**");
	}

	protected void configure(AuthenticationManagerBuilder registry) throws Exception {
		// registry.authenticationProvider(getCustomAuthenticationProvider());
		registry.userDetailsService(customUserDetailsService).passwordEncoder(passwordEncoder());

	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		// TODO We can change this later
		http.authorizeRequests()
		    .antMatchers("/resources/**").permitAll()
		    .anyRequest().authenticated()
		    .and()
		        .formLogin()
			    .loginPage("/login")
			    .defaultSuccessUrl("/home", false) // We can change this later
				.permitAll().
		    and()
		       .logout()
		       .logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
			   .logoutSuccessUrl("/login?logout")
			   .deleteCookies("JSESSIONID","filterURL")
			   .invalidateHttpSession(true)
			   .permitAll()
			.and()
			   .exceptionHandling().accessDeniedHandler(accessDeniedHandler());
		http.addFilter(customUsernamePasswordAuthenticationFilter());

		// http.addFilterBefore(,
		// UsernamePasswordAuthenticationFilter.class).authenticationProvider(getCustomAuthenticationProvider());

		// http.addFilterAfter(getFilterForDefaultWorkspaceInSession() ,
		// SwitchUserFilter.class);
	}

	@Bean
	public UsernamePasswordAuthenticationFilter customUsernamePasswordAuthenticationFilter() throws Exception {
		UsernamePasswordAuthenticationFilter uNameFilter = new CustomDetailsFilter();
		uNameFilter.setAuthenticationManager(authenticationManagerBean());
		uNameFilter.setAuthenticationSuccessHandler(customAuthenticationSuccessHandler());
		uNameFilter.setAuthenticationFailureHandler(customAuthenticationFailureHandler());
		return uNameFilter;
	}

	@Bean
	public AccessDeniedHandler accessDeniedHandler() {
		return new AccessDeniedFilter("403");
	}

	@Bean
	public PasswordEncoder passwordEncoder() {
		PasswordEncoder encoder = new BCryptPasswordEncoder();
		return encoder;
	}
	@Bean
	public AuthenticationSuccessHandler customAuthenticationSuccessHandler() throws Exception{
		return new CustomAuthenticationSuccessHandler();		
	}
	@Bean
	public CustomAuthenticationFailureHandler customAuthenticationFailureHandler() {
	    return new CustomAuthenticationFailureHandler();
	}

}
