package org.birlasoft.thirdeye.controller.add;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.birlasoft.thirdeye.beans.add.ADDWrapper;
import org.birlasoft.thirdeye.constant.GraphType;
import org.birlasoft.thirdeye.entity.Graph;
import org.birlasoft.thirdeye.service.GraphService;
import org.birlasoft.thirdeye.service.SecurityService;
import org.birlasoft.thirdeye.service.WorkspaceSecurityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *Controller for {@code showing} a {@code graph}
 */
@RestController
@RequestMapping(value="/add/json")
public class AddJSONController {
	
	@Autowired
	private GraphService graphService;
	
	@Autowired
	private WorkspaceSecurityService workspaceSecurityService; 
	
	/**
	 * Showing a {@code graph} using {@code Json}
	 * @param model
	 * @param idOfGraph
	 * @return {@code Map<String, Object>}
	 */
	@RequestMapping(value = "/graphData/{id}", method = { RequestMethod.POST, RequestMethod.GET })
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'REPORT_ASSET_GRAPH'})")
	public Map<String, Object> jsonGraphOne(@PathVariable(value = "id") Integer idOfGraph, HttpServletRequest request) {		
		Graph graph = graphService.findOne(idOfGraph,true);		
		// Check if the user can access this graph.
		workspaceSecurityService.authorizeGraphAccess(graph);
		//get filter data
		Map<String,String[]> requestParamMap = request.getParameterMap();
		Map<String,List<String>> filterMap = new HashMap<>();
		
		for(Map.Entry<String,String[]> oneEntry: requestParamMap.entrySet()){
			if (!"parameterIds".equals(oneEntry.getKey()) && !"questionnaireId".equals(oneEntry.getKey()) && !"assetTemplateIds".equals(oneEntry.getKey()) && !"_csrf".equals(oneEntry.getKey()))
				filterMap.put(oneEntry.getKey(), Arrays.asList(oneEntry.getValue()));
		}
		
		Map<String, Object> jsonMapToBeReturned = new HashMap<>();

		Set<ADDWrapper> addWrappers = null;
		if (graph.getGraphType().equals(GraphType.NW.toString())){
			addWrappers = graphService.getAddGraph(graph, filterMap);
		}
		
		jsonMapToBeReturned.put("data", addWrappers);
		return jsonMapToBeReturned;	
	}
}
