package org.birlasoft.thirdeye.formatter;

import java.text.ParseException;
import java.util.Locale;

import org.birlasoft.thirdeye.entity.Category;
import org.springframework.format.Formatter;

/**
 * {@code Formatter} , which allows you to define custom parse and print capabilities for just about any type
 */
public class CategoryFormatter implements Formatter<Category> {

	@Override
	public String print(Category category, Locale locale) {
		return category.getId().toString();
	}

	@Override
	public Category parse(String id, Locale locale) throws ParseException {
		Category category = new Category();
		category.setId(Integer.parseInt(id));
		return category;
	}

}
