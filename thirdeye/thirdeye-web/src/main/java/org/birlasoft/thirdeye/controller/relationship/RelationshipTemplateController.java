package org.birlasoft.thirdeye.controller.relationship;

import java.util.ArrayList;
import java.util.List;

import org.birlasoft.thirdeye.beans.AssetTemplateBean;
import org.birlasoft.thirdeye.constant.AssetTypes;
import org.birlasoft.thirdeye.entity.AssetTemplate;
import org.birlasoft.thirdeye.entity.AssetType;
import org.birlasoft.thirdeye.entity.Workspace;
import org.birlasoft.thirdeye.service.AssetTemplateService;
import org.birlasoft.thirdeye.service.AssetTypeService;
import org.birlasoft.thirdeye.service.CustomUserDetailsService;
import org.birlasoft.thirdeye.service.RelationshipTypeService;
import org.birlasoft.thirdeye.service.SecurityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Controller class to handle request for creation, list of
 * relationship template stuff. 
 * @author samar.gupta
 *
 */
@Controller
@RequestMapping(value="/relationshipTemplate")
public class RelationshipTemplateController {
	
	private CustomUserDetailsService customUserDetailsService;
	private AssetTypeService assetTypeService;
	private RelationshipTypeService relationshipTypeService;
	private AssetTemplateService assetTemplateService;

	/**
	 * @param customUserDetailsService
	 * @param assetTypeService
	 * @param relationshipTypeService
	 * @param assetTemplateService
	 */
	@Autowired
	public RelationshipTemplateController(CustomUserDetailsService customUserDetailsService,
			AssetTypeService assetTypeService,
			RelationshipTypeService relationshipTypeService,
			AssetTemplateService assetTemplateService) {
		this.customUserDetailsService = customUserDetailsService;
		this.assetTypeService = assetTypeService;
		this.relationshipTypeService = relationshipTypeService;
		this.assetTemplateService = assetTemplateService;
	}

	/**
	 * Create relationship template.
	 * @param model
	 * @param idOfAssetType
	 * @return {@code String}
	 */
	@RequestMapping(value = "/create", method = {RequestMethod.GET, RequestMethod.POST})
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'TEMPLATE_MODIFY'})")
	public String createRelationshipTemplate (Model model, @RequestParam(value="assetTypeId") Integer idOfAssetType) {
		AssetType assetType = assetTypeService.findOne(idOfAssetType);
		boolean isRelationshipAssetType = false;
		if(assetType != null && assetType.getAssetTypeName().equalsIgnoreCase(AssetTypes.RELATIONSHIP.name())){
			AssetTypes[] assetTypes = new AssetTypes[]{AssetTypes.RELATIONSHIP};
			model.addAttribute("assetTemplateBean", new AssetTemplateBean());
			model.addAttribute("listOfAssetType", assetTypeService.extractAssetTypes(assetTypes));
			model.addAttribute("listOfRType", relationshipTypeService.findByWorkspaceIsNullOrWorkspace(customUserDetailsService.getActiveWorkSpaceForUser()));
			isRelationshipAssetType = true;
			model.addAttribute("isRelationshipAssetType", isRelationshipAssetType);
		}else{
			model.addAttribute("isRelationshipAssetType", isRelationshipAssetType);
		}
		return "relationship/relationshipTemplateFragments :: createRelationshipTemplate";
	}
	
	/**
	 * List of all relationship template.
	 * @param model
	 * @return {@code String}
	 */
	@RequestMapping(value = "/list", method = RequestMethod.POST)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'TEMPLATE_VIEW'})")
	public String listOfRelationshipTemplate(Model model) {
		List<Workspace> listOfWorkspaces = new ArrayList<>();
		listOfWorkspaces.add(customUserDetailsService.getActiveWorkSpaceForUser());
		// List those templates which the user has access to in the active workspace
		List<AssetTemplate> listOfRelationshipTemplate = assetTemplateService.findByAssetTypeAndWorkspaceIn(assetTypeService.findByAssetTypeName(AssetTypes.RELATIONSHIP.getValue()), listOfWorkspaces);
		model.addAttribute("listOfRelationshipTemplate", listOfRelationshipTemplate);
		return "relationship/relationshipTemplateFragments :: relationshipTemplateListModal";
	}
}
