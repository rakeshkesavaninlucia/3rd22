package org.birlasoft.thirdeye.validators;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.birlasoft.thirdeye.entity.BenchmarkItemScore;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

/**
 * This {@code class} validate the {@code necessary} fields , logic of
 * {@code validation} is here.
 */
@Component
public class BenchmarkItemScoreValidator implements Validator {
	private static Logger logger = LoggerFactory.getLogger(BenchmarkItemScoreValidator.class);
	private static final String BENCHMARK_ITEM_SCORE_STARTYEAR = "startYear";
	private static final String BENCHMARK_ITEM_SCORE_ENDYEAR = "endYear";
	private static final String BENCHMARK_ITEM_SCORE = "score";	
	@Autowired
	private Environment env;

	@Override
	public boolean supports(Class<?> clazz) {
		return BenchmarkItemScore.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		BenchmarkItemScore benchmarkItemScore = (BenchmarkItemScore) target;
		
		validateEmptyYear(errors, benchmarkItemScore);
		if(!errors.hasErrors()){
		  validateYearAlreadyExist(errors, benchmarkItemScore);		
		  validateDate(errors, benchmarkItemScore);
	     }			
		validateScore(errors, benchmarkItemScore);
	}

	private void validateYearAlreadyExist(Errors errors, BenchmarkItemScore benchmarkItemScore) {
		if(benchmarkItemScore.getStartYear() != null && isStartDateExist(benchmarkItemScore)) {		
			errors.rejectValue(BENCHMARK_ITEM_SCORE_STARTYEAR,"error.benchmark.item.score.start.year.exist", env.getProperty("error.benchmark.item.score.start.year.exist"));	
		}
		if(benchmarkItemScore.getEndYear() != null && isEndDateExist(benchmarkItemScore)) {		
			errors.rejectValue(BENCHMARK_ITEM_SCORE_ENDYEAR,"error.benchmark.item.score.end.year.exist", env.getProperty("error.benchmark.item.score.end.year.exist"));	
		}
		if(!errors.hasErrors()){
			if(benchmarkItemScore.getStartYear() != null && isDateWithinRange(benchmarkItemScore,benchmarkItemScore.getStartYear())) {		
				errors.rejectValue(BENCHMARK_ITEM_SCORE_STARTYEAR,"error.benchmark.item.score.start.year.invalid", env.getProperty("error.benchmark.item.score.start.year.invalid"));	
			}
			if(benchmarkItemScore.getEndYear() != null && isDateWithinRange(benchmarkItemScore ,benchmarkItemScore.getEndYear())) {		
				errors.rejectValue(BENCHMARK_ITEM_SCORE_ENDYEAR,"error.benchmark.item.score.end.year.invalid", env.getProperty("error.benchmark.item.score.end.year.invalid"));	
			}
		}
	}

	private void validateDate(Errors errors, BenchmarkItemScore benchmarkItemScore) {
		if(benchmarkItemScore.getEndYear() != null && benchmarkItemScore.getStartYear() != null && benchmarkItemScore.getStartYear().equals(benchmarkItemScore.getEndYear())) {
			errors.rejectValue(BENCHMARK_ITEM_SCORE_ENDYEAR,"error.benchmark.item.score.year.equal", env.getProperty("error.benchmark.item.score.year.equal"));	
		}
		if(benchmarkItemScore.getEndYear() != null && benchmarkItemScore.getStartYear() != null && benchmarkItemScore.getStartYear().after(benchmarkItemScore.getEndYear())) {
			errors.rejectValue(BENCHMARK_ITEM_SCORE_STARTYEAR,"error.benchmark.item.score.year.greater", env.getProperty("error.benchmark.item.score.year.greater"));	
			errors.rejectValue(BENCHMARK_ITEM_SCORE_ENDYEAR,"error.benchmark.item.score.year.less", env.getProperty("error.benchmark.item.score.year.less"));	
		}
	}

	private void validateEmptyYear(Errors errors, BenchmarkItemScore benchmarkItemScore) {
		if(benchmarkItemScore.getStartYear() == null || benchmarkItemScore.getStartYear().equals(new Date(0))) {
			benchmarkItemScore.setStartYear(null);
			errors.rejectValue(BENCHMARK_ITEM_SCORE_STARTYEAR,"error.benchmark.item.score.start.year", env.getProperty("error.benchmark.item.score.start.year"));	
		}
		if(benchmarkItemScore.getEndYear() == null || benchmarkItemScore.getEndYear().equals(new Date(0))) {
			benchmarkItemScore.setEndYear(null);
			errors.rejectValue(BENCHMARK_ITEM_SCORE_ENDYEAR,"error.benchmark.item.score.end.year", env.getProperty("error.benchmark.item.score.end.year"));	
		}
	}

	private void validateScore(Errors errors, BenchmarkItemScore benchmarkItemScore) {
		if(benchmarkItemScore.getScore() == null){
			errors.rejectValue(BENCHMARK_ITEM_SCORE,"error.benchmark.item.score", env.getProperty("error.benchmark.item.score"));	
		}
		if(benchmarkItemScore.getScore() != null && (benchmarkItemScore.getScore().intValue() < 0 || benchmarkItemScore.getScore().intValue() > 10)) {
			errors.rejectValue(BENCHMARK_ITEM_SCORE,"error.benchmark.item.score.value", env.getProperty("error.benchmark.item.score.value"));	
		}
	}

	private boolean isStartDateExist(BenchmarkItemScore benchmarkItemScore) {
		return benchmarkItemScore.getBenchmarkItem().getBenchmarkItemScores().stream()
	            .anyMatch(t -> (formatDate(t.getStartYear()).equals(benchmarkItemScore.getStartYear()) || formatDate(t.getEndYear()).equals(benchmarkItemScore.getStartYear())) && t.getId() != benchmarkItemScore.getId());		
	}
	private boolean isEndDateExist(BenchmarkItemScore benchmarkItemScore) {
		return benchmarkItemScore.getBenchmarkItem().getBenchmarkItemScores().stream()
	            .anyMatch(t -> (formatDate(t.getEndYear()).equals(benchmarkItemScore.getEndYear()) || formatDate(t.getStartYear()).equals(benchmarkItemScore.getEndYear())) &&  t.getId() != benchmarkItemScore.getId());
	}
	
	private boolean isDateWithinRange(BenchmarkItemScore benchmarkItemScore,Date date) {
		return benchmarkItemScore.getBenchmarkItem().getBenchmarkItemScores().stream()
	            .anyMatch(t -> date.after(formatDate(t.getStartYear())) && date.before(formatDate(t.getEndYear())) &&  t.getId() != benchmarkItemScore.getId());		  
	}

	private Date formatDate(Date date) {
	    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	    Date d = new Date();
	    try {
			 d = format.parse(format.format(date));
		} catch (ParseException e) {
			logger.error("Parse exception in BenchmarkItemScoreValidator" + e);
		}	   
		return d;
	}

}
