package org.birlasoft.thirdeye.validators;

import org.birlasoft.thirdeye.beans.report.ReportConfigBean;
import org.birlasoft.thirdeye.entity.Benchmark;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 * This {@code class} validate the {@code necessary} fields , logic of
 * {@code validation} is here.
 */
@Component
public class ReportValidator implements Validator {
	private static final String REPORT_NAME = "reportName";

	@Autowired
	private Environment env;

	@Override
	public boolean supports(Class<?> clazz) {
		return Benchmark.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		ReportConfigBean reportConfigBean = (ReportConfigBean) target;
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, REPORT_NAME, "error.reprt.name",env.getProperty("error.report.name"));

	}
}
