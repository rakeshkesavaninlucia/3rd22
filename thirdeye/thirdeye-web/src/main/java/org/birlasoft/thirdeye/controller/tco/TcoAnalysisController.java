/**
 * 
 */
package org.birlasoft.thirdeye.controller.tco;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.birlasoft.thirdeye.beans.tco.TcoAnalysisGraphBean;
import org.birlasoft.thirdeye.constant.AssetTypes;
import org.birlasoft.thirdeye.constant.QuestionnaireType;
import org.birlasoft.thirdeye.constant.TcoGraphTypes;
import org.birlasoft.thirdeye.entity.AssetType;
import org.birlasoft.thirdeye.entity.Questionnaire;
import org.birlasoft.thirdeye.entity.Workspace;
import org.birlasoft.thirdeye.service.AssetTypeService;
import org.birlasoft.thirdeye.service.CustomUserDetailsService;
import org.birlasoft.thirdeye.service.QuestionnaireService;
import org.birlasoft.thirdeye.service.ScatterGraphService;
import org.birlasoft.thirdeye.service.SecurityService;
import org.birlasoft.thirdeye.service.TcoAnalysisService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author shaishav.dixit
 *
 */
@Controller
@RequestMapping(value="/graph/tco")
public class TcoAnalysisController {
	
	@Autowired
	private CustomUserDetailsService customUserDetailsService;
	@Autowired
	private QuestionnaireService questionnaireService;
	@Autowired
	private TcoAnalysisService tcoAnalysisService;
	@Autowired
	private ScatterGraphService scatterGraphService;
	@Autowired
	private AssetTypeService assetTypeService;
	
	@RequestMapping(value = "/view", method = RequestMethod.GET)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'VIEW_TCO'})")
	public String viewGraph(Model model) {
		// List those questionnaires which the user has access to in the active workspace
		Set<Workspace> listOfWorkspaces = new HashSet<>();
		listOfWorkspaces.add(customUserDetailsService.getActiveWorkSpaceForUser());
		
		// List of asset type
		List<AssetType> listOfAssetType =  assetTypeService.findAll();
		listOfAssetType.removeIf(e -> e.getAssetTypeName().equalsIgnoreCase(AssetTypes.RELATIONSHIP.name()));
		//Convert list to set for year drop down
		List<Questionnaire> listofchartOfAccount =  questionnaireService.findByWorkspaceInAndQuestionnaireType(listOfWorkspaces,QuestionnaireType.TCO.toString());
		Set<String> setOfyear = listofchartOfAccount.stream().map(Questionnaire::getYear).collect(Collectors.toCollection(HashSet::new));
		
		if(!listOfWorkspaces.isEmpty()){
			model.addAttribute("AssetTypeList", listOfAssetType);
		    model.addAttribute("SetOfChartOfAccountYear", setOfyear);
		}
		List<Questionnaire> listOfQuestionnaire = scatterGraphService.getQuestionnaireforScatter();
		model.addAttribute("listOfQuestionnaire", listOfQuestionnaire);
		return "tco/viewTcoAnalysis";
	}
	
	@RequestMapping(value="/plot", method = RequestMethod.POST)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'VIEW_TCO'})")
	@ResponseBody
    public TcoAnalysisGraphBean plotGraph(@RequestParam(value = "chartOfAccountYear") String chartOfAccountYear,@RequestParam(value = "assetTypeId") Integer idOfAssetType, @RequestParam(value = "graphType") String graphType,
    		@RequestParam(value = "parameterId", required = false) Integer idOfParameter, @RequestParam(value = "questionnaireId", required = false) Integer idOfQuestionnaire){
				
		TcoAnalysisGraphBean analysisGraphBean =new TcoAnalysisGraphBean();
		Questionnaire questionnnaire = questionnaireService.findByYearAndAssetTypeIdAndWorkSpace(chartOfAccountYear, idOfAssetType,customUserDetailsService.getActiveWorkSpaceForUser());
		if(questionnnaire !=null){
			String name = questionnnaire.getName();
			if(graphType.equals(TcoGraphTypes.COMBI.toString())) {
				analysisGraphBean = tcoAnalysisService.getDataForCombiGraph(questionnnaire.getId(), idOfQuestionnaire, idOfParameter);
				analysisGraphBean.setGraphType(TcoGraphTypes.COMBI.toString());
				name = name + " vs " + analysisGraphBean.getGraphName();
			} else {			
				analysisGraphBean = tcoAnalysisService.getTotalCostOfAssets(questionnnaire.getId());
				analysisGraphBean.setGraphType(TcoGraphTypes.SORTED.toString());
			}
			analysisGraphBean.setGraphName(name);
		}
		return analysisGraphBean;
	}	
}
