package org.birlasoft.thirdeye.config;

import java.text.SimpleDateFormat;
import java.util.Date;
import org.springframework.aop.Advisor;
import org.springframework.aop.aspectj.AspectJExpressionPointcut;
import org.springframework.aop.support.DefaultPointcutAdvisor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.scheduling.annotation.EnableScheduling;


/**
 * @author dhruv.sood
 *
 */

@Configuration
@EnableScheduling
@EnableAspectJAutoProxy(proxyTargetClass=true)
public class PerformanceMonitoring {

	/**
	 * @return {customizableTraceInterceptor}
	 * This interceptor is the setter for Entry and Exit message for any method.
	 */
	@Bean
    public MyCustomizableTraceInterceptor customizableTraceInterceptor() {
				
			MyCustomizableTraceInterceptor customizableTraceInterceptor = new MyCustomizableTraceInterceptor();
			customizableTraceInterceptor.setLoggerName("PerformanceLogs");
			customizableTraceInterceptor.setEnterMessage(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date())+" ; Enter ; $[targetClassName] ; $[methodName]() ; $[arguments]");
			customizableTraceInterceptor.setExitMessage(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date())+" ; Exit ; $[targetClassName] ; $[methodName]() ; $[invocationTime] ; $[returnValue]");
			return customizableTraceInterceptor;	
    }

    /**
     * @return
     * This bean is the point cut for monitoring the entry and exit of a method.
     */
    @Bean
    public Advisor pointcutForEntryExit() {
        AspectJExpressionPointcut pointcut = new AspectJExpressionPointcut();
        pointcut.setExpression("within(org.birlasoft.thirdeye.controller..*) || within(org.birlasoft.thirdeye.service..*) || within(org.birlasoft.thirdeye.repositories..*)");
        return new DefaultPointcutAdvisor(pointcut, customizableTraceInterceptor());
    }
}
