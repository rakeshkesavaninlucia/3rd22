package org.birlasoft.thirdeye.controller;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.birlasoft.thirdeye.beans.ParameterBean;
import org.birlasoft.thirdeye.constant.Constants;
import org.birlasoft.thirdeye.constant.QuestionnaireConstants;
import org.birlasoft.thirdeye.constant.QuestionnaireType;
import org.birlasoft.thirdeye.entity.Parameter;
import org.birlasoft.thirdeye.entity.Questionnaire;
import org.birlasoft.thirdeye.service.ParameterService;
import org.birlasoft.thirdeye.service.QuestionnaireParameterService;
import org.birlasoft.thirdeye.service.QuestionnaireService;
import org.birlasoft.thirdeye.service.SecurityService;
import org.birlasoft.thirdeye.service.WorkspaceSecurityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Controller for {@code show parameter} page to {@code load tree} , add a new {@code parameter}  to save , get {@code model}
 * content for {@code adding parameter} , get {@code model} content for {@code adding questions} , get {@code model}
 * for {@code updating} {@code parameter} {@code function}
 *
 */
@Controller
@RequestMapping(value="/questionnaire")
public class QuestionnaireParameterController {
	
	private ParameterService parameterService;
	private QuestionnaireService questionnaireService;
	private final WorkspaceSecurityService workspaceSecurityService;	
	private QuestionnaireParameterService qpService;

	/**
	 * Constructor to initialize services 
	 * @param parameterService
	 * @param parameterFunctionService
	 * @param questionnaireService
	 * @param questionService
	 * @param workspaceSecurityService
	 * @param qpService
	 */
	@Autowired
	public QuestionnaireParameterController(ParameterService parameterService,			
			QuestionnaireService questionnaireService,		
			WorkspaceSecurityService workspaceSecurityService,QuestionnaireParameterService qpService){
		this.parameterService = parameterService;
		this.questionnaireService = questionnaireService;	
		this.workspaceSecurityService = workspaceSecurityService;
		this.qpService = qpService;
	}
	
	/**
	 * Show parameter page to load tree.
	 * @param model
	 * @param idOfQuestionnaire
	 * @return {@code String}
	 */
	@RequestMapping(value = "/{id}/parameters", method = RequestMethod.GET)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'QUESTIONNAIRE_MODIFY'})")
	public String showParameterPage(Model model,  @PathVariable(value = "id") Integer idOfQuestionnaire) {
		
		// Prepare the view for the parameters that are on the questionnaire.
		Questionnaire qe = questionnaireService.findOne(idOfQuestionnaire);
		// Check if the user can access this questionnaire.
		workspaceSecurityService.authorizeQuestionnaireType(qe,QuestionnaireType.DEF.toString());
		workspaceSecurityService.authorizeQuestionnaireAccess(qe);
		List<Parameter> parameterList = parameterService.findByQuestionnaireAndParameterByParentParameterIdIsNull(qe,true) ;	
		// Get root parameters of QE
		List<ParameterBean> listOfRootParameters = parameterService.getParametersOfQuestionnaire(parameterList);
		
		model.addAttribute("rootParameterList", listOfRootParameters);
		model.addAttribute(QuestionnaireConstants.QUESTIONNAIRE_ID, idOfQuestionnaire);
		model.addAttribute(QuestionnaireConstants.QUESTIONNAIRE_TYPES, QuestionnaireType.DEF.toString());
		model.addAttribute(QuestionnaireConstants.QUESTIONNAIRE_ACTION, QuestionnaireType.DEF.getAction());
		model.addAttribute(Constants.PAGE_TITLE, "Questionnaire Management - Parameter Selection for Questionnaire");
		
		return "questionnaire/questionnaireParameters";
	}

	/** 
	 * Delete node from tree.
	 * @param model
	 * @param idOfQuestionnaire
	 * @param parentParameterId
	 * @param parameterId
	 * @param questionId
	 */
	@RequestMapping(value = "{id}/parameterquestion/deleteNode", method = RequestMethod.POST)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'QUESTIONNAIRE_MODIFY'})")
	@ResponseStatus(value = HttpStatus.OK)
	public void deleteNodeFromTree(		@PathVariable(value = "id") Integer idOfQuestionnaire,
										@RequestParam(value = "paramId", required=false) Integer parameterId){		

		Questionnaire questionnaire = questionnaireService.findOne(idOfQuestionnaire);
		// Check if the user can access this questionnaire.
		workspaceSecurityService.authorizeQuestionnaireType(questionnaire,QuestionnaireType.DEF.toString());
		workspaceSecurityService.authorizeQuestionnaireAccess(questionnaire);		
		// Are we going to delete a question ?
		if (idOfQuestionnaire != null && parameterId != null){
			Parameter parameter = parameterService.findFullyLoaded(parameterId);
			Set<Parameter> parameters = new HashSet<>();
			parameters.add(parameter);
			questionnaireService.updateQuestionnaireEntity(questionnaire, null,parameters);
		}
	}
	
    /**
     * method to save QuestionnaireParameter
     * @param model
     * @param result
     * @param questionnaireId
     * @param setOfParameters
     * @param session
     * @return
     */
	@RequestMapping(value="/{id}/qparameters/save", method = RequestMethod.POST)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'QUESTIONNAIRE_MODIFY'})")
	public String saveQuestionnaireParameter(Model model,@PathVariable(value = "id") Integer questionnaireId, @RequestParam(value = "parameter[]") Set<Integer> setOfParameters) {
				
		Questionnaire questionnaire = questionnaireService.findOneFullyLoaded(questionnaireId);
		// Check if the user can access this questionnaire.
		workspaceSecurityService.authorizeQuestionnaireType(questionnaire,QuestionnaireType.DEF.toString());
		workspaceSecurityService.authorizeQuestionnaireAccess(questionnaire);		
		List<Parameter> listOfParametersToUpdate = parameterService.findFullyLoadedParametersByIds(setOfParameters);		
		qpService.updateParametersOnQuestionnaire(questionnaire, listOfParametersToUpdate);	
		
		List<Parameter> parameterList = parameterService.findByQuestionnaireAndParameterByParentParameterIdIsNull(questionnaire,true) ;	
		// Get root parameters of QE
		List<ParameterBean> listOfRootParameters = parameterService.getParametersOfQuestionnaire(parameterList);
		model.addAttribute("rootParameterList", listOfRootParameters);
		model.addAttribute(QuestionnaireConstants.QUESTIONNAIRE_ID, questionnaireId);
		model.addAttribute(QuestionnaireConstants.QUESTIONNAIRE_TYPES, QuestionnaireType.DEF.toString());
		model.addAttribute(QuestionnaireConstants.QUESTIONNAIRE_ACTION, QuestionnaireType.DEF.getAction());
		return "questionnaire/questionnaireParameters :: questionnaireParameterFragment(rootParameterList=${rootParameterList})";
	}

}
