package org.birlasoft.thirdeye.validators;

import java.util.List;

import org.birlasoft.thirdeye.entity.Asset;
import org.birlasoft.thirdeye.entity.AssetType;
import org.birlasoft.thirdeye.entity.Role;
import org.birlasoft.thirdeye.service.AssetService;
import org.birlasoft.thirdeye.service.AssetTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;


/**
 * This {@code class} validate the {@code necessary}  fields ,
 * logic of {@code validation} is here.
 * unique asset name
 */
@Component
public class AssetValidation implements Validator {

	private static final String UID = "uid";
	private static final String ASSETTYPENAME ="assetTypeName";
	private static final String IMAGEPATH = "imagePath";

	@Autowired
	private Environment env;

	@Autowired
	private AssetService assetService;
	@Autowired
	private AssetTypeService assetTypeService;

	@Override
	public boolean supports(Class<?> clazz) {
		return Asset.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors){

		Asset asset = (Asset) target;
		if(asset.getUid() != null){			
			Asset assetUid  = assetService.findByUid(asset.getUid());	
			if(assetUid != null) {
				errors.rejectValue(UID,"error.asset.name.unique", env.getProperty("error.asset.name.unique"));	
			}
		}
	}
	

	/**
	 * @param target asset
	 * @return boolean(true/false)
	 */
	public boolean isAssetDuplicate(Object target){

		Asset asset = (Asset) target;
		if(asset.getUid() != null){			
			Asset assetUid  = assetService.findByUid(asset.getUid());	
			if(assetUid != null) {
				return true;
			}
		}
		return false;
	}
	
	public boolean isAssetActive(Object target){

		Asset asset = (Asset) target;
		if(asset.getUid() != null){			
			Asset assetUid  = assetService.findByUidAndDeleteStatus(asset.getUid(),true);	
			if(assetUid != null) {
				return true;
			}
		}
		return false;
	}
	
public void validateAsseTypeModel(AssetType assetType,Errors errors) {
		
		if (assetType.getAssetTypeName() != null && assetType.getAssetTypeName().isEmpty()) {			  
			  ValidationUtils.rejectIfEmptyOrWhitespace(errors, ASSETTYPENAME, "error.assetTypeName.title", env.getProperty("error.assetTypeName.title "));
		}
		if (assetType.getImagePath()==null) {			  
			errors.rejectValue(IMAGEPATH,"error.imagePath", env.getProperty("error.imagePath"));	 
		}
		List<AssetType> listAssetType = assetTypeService.findAll();
		listAssetType.stream().forEach(oneAssetType->{
			if(oneAssetType.getAssetTypeName().equals(assetType.getAssetTypeName())) {
				errors.rejectValue(ASSETTYPENAME,"error.assetTypeName.unique", env.getProperty("error.assetTypeName.unique"));
				
			}
			
		});
		
		
	}
}