package org.birlasoft.thirdeye.controller.tco;

import java.util.List;
import java.util.Map;

import org.birlasoft.thirdeye.beans.ParameterBean;
import org.birlasoft.thirdeye.constant.QuestionnaireConstants;
import org.birlasoft.thirdeye.constant.QuestionnaireType;
import org.birlasoft.thirdeye.entity.Questionnaire;
import org.birlasoft.thirdeye.service.CustomUserDetailsService;
import org.birlasoft.thirdeye.service.QuestionnaireLifeCycleService;
import org.birlasoft.thirdeye.service.QuestionnaireService;
import org.birlasoft.thirdeye.service.SecurityService;
import org.birlasoft.thirdeye.service.WorkspaceSecurityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Controller for view  the chart of account Status {@code viewChartOfAccountStatus}
 * 
 */
@Controller
@RequestMapping("/tco")
public class ChartOfAccountLifeCycleController {
	
	private QuestionnaireLifeCycleService questionnaireLifeCycleService ;
	
	private QuestionnaireService questionnaireService;
	
	private final WorkspaceSecurityService workspaceSecurityService;
	
	private CustomUserDetailsService customUserDetailsService;
	
	private static final String VIEW_QUESTIONNAIRE_STATUS = "questionnaire/viewQuestionnaireStatus";

	
	/**
	 * Constructor to initialize services
	 * @param questionnaireService
	 * @param questionnaireLifeCycleService
	 * @param workspaceSecurityService
	 * @param customUserDetailsService
	 */
	 
	@Autowired
	public ChartOfAccountLifeCycleController(QuestionnaireService questionnaireService,
			QuestionnaireLifeCycleService questionnaireLifeCycleService,
			WorkspaceSecurityService workspaceSecurityService,
			CustomUserDetailsService customUserDetailsService){
 
		this.questionnaireService = questionnaireService;
		this.questionnaireLifeCycleService = questionnaireLifeCycleService;
		this.workspaceSecurityService = workspaceSecurityService;
		this.customUserDetailsService = customUserDetailsService;
	}
	

	
	/**
	 * method to view the chart of account Status
	 * @param idOfChartOfAccount
	 * @param model
	 * @return {@code String}
	 */
	@RequestMapping(value = "/{id}/status", method = RequestMethod.GET)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'MODIFY_TCO'})")
	public String viewChartOfAccountStatus(@PathVariable(value = "id") Integer idOfChartOfAccount, Model model){
		
		Questionnaire coa = questionnaireService.findOneFullyLoaded(idOfChartOfAccount);
		// Check if the user can access this questionnaire.
		workspaceSecurityService.authorizeQuestionnaireType(coa,QuestionnaireType.TCO.toString());
		workspaceSecurityService.authorizeQuestionnaireAccess(coa);
		
		List<ParameterBean> listOfRootParameters  = questionnaireLifeCycleService.getRootParameterForQuestionnaire(coa);		
		Map<String,Integer> mapOfQuestionCategory = questionnaireLifeCycleService.getMapOfCategoryAndNoOfQuetion(coa);
		
		model.addAttribute("rootParameterList", listOfRootParameters);		
		model.addAttribute("mapOfQC", mapOfQuestionCategory);
		model.addAttribute("questionnaireForm", coa);
		model.addAttribute(QuestionnaireConstants.QUESTIONNAIRE_TYPES, QuestionnaireType.TCO.toString());
		model.addAttribute(QuestionnaireConstants.QUESTIONNAIRE_ACTION,QuestionnaireType.TCO.getAction());
		
		return VIEW_QUESTIONNAIRE_STATUS;
	}
	

	/**
	 * method to change the chart of account Status
	 * @param idOfChartOfAccount
	 * @param status
	 * @param model
	 * @return {@code String}
	 */
	@RequestMapping(value = "/{id}/modify/{status}", method = RequestMethod.GET)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'MODIFY_TCO'})")
	public String changeChartOfAccountStatus(@PathVariable(value = "id") Integer idOfChartOfAccount, @PathVariable(value = "status") String status, Model model){
		
		Questionnaire coa = questionnaireService.findOneFullyLoaded(idOfChartOfAccount);
		// Check if the user can access this questionnaire.
		workspaceSecurityService.authorizeQuestionnaireType(coa,QuestionnaireType.TCO.toString());
		workspaceSecurityService.authorizeQuestionnaireAccess(coa);
		coa.setStatus(status);
		Questionnaire savedQuestionnaire = questionnaireService.save(questionnaireService.updateQuestionnaireObject(coa, customUserDetailsService.getCurrentUser()));
		
		return  "redirect:/tco/"+savedQuestionnaire.getId()+"/status";
	}
}

