package org.birlasoft.thirdeye.validators;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.birlasoft.thirdeye.entity.Aid;
import org.birlasoft.thirdeye.entity.Workspace;
import org.birlasoft.thirdeye.service.AIDService;
import org.birlasoft.thirdeye.service.CustomUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 * This {@code class} validate the {@code necessary}  fields ,
 * logic of {@code validation} is here.
 */
@Component
public class AIDValidator implements Validator {
	private static final String AID_NAME = "name";
	
	@Autowired
	private Environment env;
	@Autowired
	private AIDService aidService;
	@Autowired
	private CustomUserDetailsService customUserDetailsService; 
	
	
	@Override
	public boolean supports(Class<?> clazz) {
		return Aid.class.equals(clazz);
	}
                
	@Override
	public void validate(Object target, Errors errors) {
		  Aid aid= (Aid)target;
		  
		  if(aid.getName() != null && aid.getName().isEmpty()){
			    ValidationUtils.rejectIfEmptyOrWhitespace(errors, AID_NAME, "error.aid.name", env.getProperty("error.aid.name"));
		  }else  if(aid.getName()!= null && aid.getName().length() > 45){
				 errors.rejectValue(AID_NAME, "error.aid.name.length", env.getProperty("error.aid.name.length")); 
		  }else if(aid.getAssetTemplate() == null){
			  errors.rejectValue("assetTemplate" ,"error.aid.template.select",env.getProperty("error.aid.template.select"));
		  
		  }else if(!aid.getName().isEmpty()){
			// List those templates which the user has access to in the active workspace
			Set<Workspace> listOfWorkspaces = new HashSet<Workspace> ();
			listOfWorkspaces.add(customUserDetailsService.getActiveWorkSpaceForUser());
			List<Aid> aids = aidService.findByWorkspaceIn(listOfWorkspaces);
			for (Aid aidObject : aids) {
				if(aidObject.getName().equalsIgnoreCase(aid.getName().trim())){
					errors.rejectValue(AID_NAME ,"error.aid.name.unique", env.getProperty("error.aid.name.unique"));
					break;
				}
			}
		  }
	}
}
