package org.birlasoft.thirdeye.controller.parameter.lp;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArrayList;

import javax.servlet.http.HttpSession;

import org.birlasoft.thirdeye.entity.Question;
import org.birlasoft.thirdeye.service.CustomUserDetailsService;
import org.birlasoft.thirdeye.service.QuestionService;
import org.birlasoft.thirdeye.service.SecurityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping(value = "/parameter/lp")
public class LeafParameterController {
	
	private CustomUserDetailsService customUserDetailsService;
	private QuestionService questionService;
	
	@Autowired
	public LeafParameterController(CustomUserDetailsService customUserDetailsService,
			QuestionService questionService) {
		this.customUserDetailsService = customUserDetailsService;
		this.questionService = questionService;
	}

	@RequestMapping(value="/showButton", method = RequestMethod.POST)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'PARAMETER_MODIFY'})")
	public String showAddQuestionButton(Model model){
		return "param/paramFragment :: addQuestionButton";
	}
	
	@RequestMapping(value="/showModal", method = RequestMethod.POST)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'PARAMETER_MODIFY'})")
	public String getModalContentForAddingQuestions(Model model, HttpSession session, @RequestParam(value = "workspaceId", required = false) Integer workspaceId){
		List<Question> listOfQuestions = new ArrayList<Question>();
		if(String.valueOf(workspaceId).equals("null") || workspaceId == -1){
			listOfQuestions = questionService.findByWorkspace(null);
		}else if(workspaceId != null && workspaceId > 0){
			listOfQuestions = questionService.findByWorkspaceIsNullOrWorkspace(customUserDetailsService.getActiveWorkSpaceForUser());
		}
		List<Question> questions = new CopyOnWriteArrayList<Question>(listOfQuestions);
		Set<Question> listOfSelectedQuestions = (HashSet<Question>)session.getAttribute("_selectedQuestions");
		if(listOfSelectedQuestions.size() > 0){
			for (Question q : questions) {
				for(Question selectedQuestion : listOfSelectedQuestions){
					if(selectedQuestion.getId().equals(q.getId())){
						questions.remove(q);
						break;
					}
				}
			}
		}
		model.addAttribute("listOfQuestions", questions);
		return "param/paramFragment :: questionsModal";
	}
	
	@RequestMapping(value="/saveTemporary", method = RequestMethod.POST)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'PARAMETER_MODIFY'})")
	public String saveSelectedQuestions(Model model, @RequestParam(value = "question[]") Set<Integer> setOfQuestions,
			@RequestParam(value = "mandatoryQuestion[]", required = false) Set<Integer> setOfMandatoryQuestion, HttpSession session){
		Set<Question> listOfQuestionsToUpdate = (HashSet<Question>)session.getAttribute("_selectedQuestions");
		listOfQuestionsToUpdate.addAll(questionService.findByIdIn(setOfQuestions));
		session.setAttribute("_selectedQuestions", listOfQuestionsToUpdate);
		
		Set<Integer> setOfMandatoryQuestionsToUpdate = (HashSet<Integer>)session.getAttribute("_selectedMandatoryQuestions");
		if(setOfMandatoryQuestion != null)
		{
			for (Integer qMandateId : setOfMandatoryQuestion) {
				if(setOfQuestions.contains(qMandateId)){
					setOfMandatoryQuestionsToUpdate.add(qMandateId);					
				}
			}
		}
		session.setAttribute("_selectedMandatoryQuestions", setOfMandatoryQuestionsToUpdate);
		model.addAttribute("mapOfWeight", new HashMap<Integer, Double>());
		model.addAttribute("selectedQuestions", session.getAttribute("_selectedQuestions"));
		return "param/paramFragment :: selectedQuestion";
	}
}
