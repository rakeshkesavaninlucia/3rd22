package org.birlasoft.thirdeye.controller.asset;

import java.util.List;
import java.util.Map;

import org.birlasoft.thirdeye.entity.AssetTemplate;
import org.birlasoft.thirdeye.service.AssetLifeCycleService;
import org.birlasoft.thirdeye.service.SecurityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping(value="/assetLifeCycle")
public class AssetLifeCycleController {

	@Autowired
	private AssetLifeCycleService assetLifeCycleService;

	@RequestMapping( value = "/report", method = RequestMethod.GET ) 
	public String viewLifeCycleReprt(Model model) {
		List<AssetTemplate> listOfAssetTemplate = assetLifeCycleService.getListOfAssetTemplate();
		model.addAttribute("listOfAssetTemplate", listOfAssetTemplate);

		return "lifeCycle/lifeCycleReport";
	}

	@RequestMapping( value = "/getAsset", method = RequestMethod.POST) 
	public String getAssetForAssetTemplate(Model model,@RequestParam(value="assettemplateid") Integer assetTemplateId) {

		//get year range
		List<String> yearRange = assetLifeCycleService.getYearRange(assetTemplateId);
		//get map of Asset life cycle
		Map<String,Map<String,List<String>>> mapofAssetLifeCycle = assetLifeCycleService.getAssetlifeCycle(assetTemplateId);
		model.addAttribute("mapofassetLifeCycle", mapofAssetLifeCycle);
		model.addAttribute("yearRange",yearRange);
		return "lifeCycle/lifeCycleReport::assetTemplateAsset";
	}
}
