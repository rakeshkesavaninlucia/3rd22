package org.birlasoft.thirdeye.controller.tco;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletResponse;

import org.birlasoft.thirdeye.beans.ParameterBean;
import org.birlasoft.thirdeye.constant.Constants;
import org.birlasoft.thirdeye.constant.ParameterType;
import org.birlasoft.thirdeye.entity.Parameter;
import org.birlasoft.thirdeye.entity.ParameterFunction;
import org.birlasoft.thirdeye.entity.Question;
import org.birlasoft.thirdeye.entity.Workspace;
import org.birlasoft.thirdeye.service.CategoryService;
import org.birlasoft.thirdeye.service.CostStructureService;
import org.birlasoft.thirdeye.service.CustomUserDetailsService;
import org.birlasoft.thirdeye.service.ParameterFunctionService;
import org.birlasoft.thirdeye.service.ParameterService;
import org.birlasoft.thirdeye.service.QuestionService;
import org.birlasoft.thirdeye.service.SecurityService;
import org.birlasoft.thirdeye.service.WorkspaceSecurityService;
import org.birlasoft.thirdeye.validators.ParameterBeanValidator;
import org.birlasoft.thirdeye.views.JSONView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;

/**
 * This controller for TCO cost structure stuffs. 
 * @author samar.gupta
 *
 */
@Controller
@RequestMapping(value="/coststructure")
public class CostStructureController {
	
	private static final String ROOT_PARAMETER_ID = "rootParameterId";
	private static final String MODE = "mode";

	private CustomUserDetailsService customUserDetailsService;
	private CostStructureService costStructureService;
	private ParameterBeanValidator parameterBeanValidator;
	private ParameterService parameterService;
	private ParameterFunctionService parameterFunctionService;
	private QuestionService questionService;
	private CategoryService categoryService;
	@Autowired
	private WorkspaceSecurityService workspaceSecurityService;
	
	/**
	 * @param customUserDetailsService
	 * @param costStructureService
	 * @param parameterBeanValidator
	 * @param parameterService
	 * @param parameterFunctionService
	 * @param questionService
	 * @param categoryService
	 */
	@Autowired
	public CostStructureController(CustomUserDetailsService customUserDetailsService,
			CostStructureService costStructureService,
			ParameterBeanValidator parameterBeanValidator,
			ParameterService parameterService,
			ParameterFunctionService parameterFunctionService,
			QuestionService questionService,
			CategoryService categoryService) {
		this.customUserDetailsService = customUserDetailsService;
		this.costStructureService = costStructureService;
		this.parameterBeanValidator = parameterBeanValidator;
		this.parameterService = parameterService;
		this.parameterFunctionService = parameterFunctionService;
		this.questionService = questionService;
		this.categoryService = categoryService;
	}

	/**
	 * Show the list of cost structure. 
	 * @param model
	 * @return {@code String}
	 */
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'VIEW_TCO'})")
	public String listCostStructure (Model model) {	
		Set<Workspace> listOfWorkspaces = new HashSet<>();
		listOfWorkspaces.add(customUserDetailsService.getActiveWorkSpaceForUser());
		Set<Parameter> parameterSet   = parameterService.getCostStructureByWorkspaceInAndType(listOfWorkspaces,ParameterType.TCOA.toString());
		model.addAttribute("listOfCostStructure", parameterSet);
		return "tco/listCostStructure";
	}
	
	/**
	 * Create root/child cost structure/parameter.
	 * @param model
	 * @param selectedParentNodeId
	 * @param rootParameterId
	 * @return {@code String}
	 */
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'MODIFY_TCO'})")
	public String createCostStructure (Model model, @RequestParam(value = "baseParameterId", required = false) Integer selectedParentNodeId,
			@RequestParam(value = ROOT_PARAMETER_ID, required = false) Integer rootParameterId,
			@RequestParam(value = MODE, required = false) String mode) {
		ParameterBean parameterBean = new ParameterBean(); 
		if(selectedParentNodeId != null){
			parameterBean.setBaseParameterId(selectedParentNodeId);
			model.addAttribute(ROOT_PARAMETER_ID, rootParameterId);
		}
		
		if("edit".equalsIgnoreCase(mode)){
			Parameter parameter = parameterService.findOne(selectedParentNodeId);
			parameterBean = new ParameterBean(parameter);
		}
		model.addAttribute("costStructureForm", parameterBean);
		return "tco/tcoFragment :: createCostStructure";
	}
	
	/**
	 * Save root and child cost_structure/parameter. 
	 * @param incomingCostStructure
	 * @param response
	 * @param result
	 * @param rootParameterId
	 * @param model
	 * @return {@code Object}
	 */
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'MODIFY_TCO'})")
	public Object saveCostStructure (@ModelAttribute("costStructureForm") ParameterBean incomingCostStructure,
			HttpServletResponse response, BindingResult result,
			@RequestParam(value = ROOT_PARAMETER_ID, required = false) Integer rootParameterId, Model model) {
		
		Integer rootIdOfParameter = rootParameterId;
		parameterBeanValidator.validate(incomingCostStructure, result);
		if(result.hasErrors()){
			model.addAttribute(ROOT_PARAMETER_ID, rootParameterId);
			return new ModelAndView("tco/tcoFragment :: createCostStructure");
		}
		
		Parameter parameter;
		if(incomingCostStructure.getId() != null){
			parameter = parameterService.save(costStructureService.updateCostStructure(incomingCostStructure, customUserDetailsService.getCurrentUser()));
		}else{
			parameter = parameterService.save(costStructureService.createNewCostStructure(incomingCostStructure));
		}
		
		if(incomingCostStructure.getId() == null){
			if(incomingCostStructure.getBaseParameterId() != null){
				parameterFunctionService.save(costStructureService.createNewParameterFunction(incomingCostStructure, parameter));
			}else {
				rootIdOfParameter = parameter.getId();
			}
		}
		// Now that the save is done simply return the JSON okay and redirect
		Map<String, String> jsonToReturn = new HashMap<>();
		jsonToReturn.put("redirect", "coststructure/edit/"+rootIdOfParameter);
		
		return JSONView.render(jsonToReturn, response);
	}

	/**
	 * Edit cost structure to add child or cost elements.
	 * @param model
	 * @param idOfParameter
	 * @return {@code String}
	 */
	@RequestMapping(value = "/edit/{id}", method = RequestMethod.GET)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'MODIFY_TCO'})")
	public String editCostStructure (Model model, @PathVariable("id") Integer idOfParameter) {
		Parameter parameter = parameterService.findFullyLoaded(idOfParameter);
		ParameterBean parameterBean = parameterService.generateParameterTree(parameter);
		model.addAttribute("parameterBean", parameterBean);
		model.addAttribute("listOfParameters", parameterBean.getChildParameters());
		model.addAttribute("listOfQuestions", parameterBean.getChildQuestions());
		return "tco/editCostStructure";
	}
	
	/**
	 * Show available cost_elements in modal pop-up.
	 * @param model
	 * @param selectedParentNodeId
	 * @param rootParameterId
	 * @return {@code String}
	 */
	@RequestMapping(value = "/view", method = RequestMethod.POST)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'MODIFY_TCO'})")
	public String viewCostElement(Model model, @RequestParam(value = "baseParameterId") Integer selectedParentNodeId,
			@RequestParam(value = ROOT_PARAMETER_ID) Integer rootParameterId) {
		
		//Get the active Workspace	
		Set<Workspace> setOfWorkspace = new HashSet<>();
		setOfWorkspace.add(customUserDetailsService.getActiveWorkSpaceForUser());
		
		//Finding all questions by SYS_TCO category as per current user workspace
		List<Question> listCostElement = questionService.findByWorkspaceInAndCategory(setOfWorkspace, categoryService.getCategoryFromName(Constants.__SYS_TCO));
		
		// Fetch cost element which are added already
		List<ParameterFunction> alreadyAddedCostElements = parameterFunctionService.findByParameterByParentParameterId(parameterService.findOne(selectedParentNodeId));
		
		List<Question> listOfRemainingCostElementsToDisplay = costStructureService.extractCostElementToDisplay(listCostElement, alreadyAddedCostElements);
				
		model.addAttribute("listCostElement", listOfRemainingCostElementsToDisplay);
		model.addAttribute("baseParameterId", selectedParentNodeId);
		model.addAttribute(ROOT_PARAMETER_ID, rootParameterId);
		return "tco/tcoFragment :: listCostElement";
	}

	/**
	 * Save selected cost elements within TCOL type cost_structure.
	 * @param response
	 * @param setOfCostElements
	 * @param selectedParentNodeId
	 * @param rootParameterId
	 * @return {@code Object}
	 */
	@RequestMapping(value = "/selectedCostElements/save", method = RequestMethod.POST)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'MODIFY_TCO'})")
	public Object saveCostElements(HttpServletResponse response,
			@RequestParam(value = "costElement[]") Set<Integer> setOfCostElements,
			@RequestParam(value = "baseParameterId") Integer selectedParentNodeId,
			@RequestParam(value = "rootParameterId") Integer rootParameterId) {
		
		Parameter selectedParameter = parameterService.findOne(selectedParentNodeId);
		List<ParameterFunction> listOfPfs = parameterFunctionService.findByParameterByParentParameterId(selectedParameter);
		int maxSequenceNumber = costStructureService.getMaxSequenceNumber(listOfPfs);
		List<ParameterFunction> listOfParameterFunctions = new ArrayList<>();
		List<Question> listOfQuestions = questionService.findByIdIn(setOfCostElements);
		for (Question question : listOfQuestions) {
			ParameterFunction pf = new ParameterFunction();
			pf.setParameterByParentParameterId(selectedParameter);
			pf.setQuestion(question);
			pf.setWeight(Constants.DEFAULT_WEIGHT_VALUE);
			++maxSequenceNumber;
			pf.setSequenceNumber(maxSequenceNumber);
			listOfParameterFunctions.add(pf);
		}
		
		parameterFunctionService.save(listOfParameterFunctions);
		selectedParameter.setType(ParameterType.TCOL.name());
		parameterService.save(selectedParameter);
		
		// Now that the save is done simply return the JSON okay and redirect
		Map<String, String> jsonToReturn = new HashMap<>();
		jsonToReturn.put("redirect", "coststructure/edit/"+rootParameterId);
		
		return JSONView.render(jsonToReturn, response);
	}
	
	/**
	 * Delete a {@code parameter}
	 * @param idOfParameter
	 */
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'MODIFY_TCO'})")
	@ResponseStatus(value = HttpStatus.OK)
	public void deleteCostStructure(@RequestParam(value = "baseParameterId") Integer idOfParameter){
		Parameter parameter = parameterService.findOne(idOfParameter);
		// Check if the user can access this parameter.
		workspaceSecurityService.authorizeParameterAccess(parameter);
		
		// Parameters which are on Questionnaire
		Map<Integer, Integer> mapOfParams = parameterService.getParamsOnQuestionnaire();
		
		if(mapOfParams.containsKey(idOfParameter)){
			throw new AccessDeniedException("User is not allowed to delete this parameter");
		}
		costStructureService.deleteParamFunctionByParent(parameter);
		costStructureService.deleteParamFunctionByChild(parameter);
		//delete parameter quality gate
		parameterService.deleteParameterQualityGate(parameter);
		parameterService.deleteParameter(idOfParameter);
	}
}
