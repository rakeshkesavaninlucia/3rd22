package org.birlasoft.thirdeye.validators;

import org.birlasoft.thirdeye.entity.Parameter;
import org.birlasoft.thirdeye.service.ParameterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 * This {@code class} validate the {@code necessary}  fields ,
 * logic of {@code validation} is here.
 */
@Component
public class ParameterValidator implements Validator {
	
	@Autowired
	private Environment env;

	@Autowired
	private ParameterService parameterService;
	
	@Override
	public boolean supports(Class<?> clazz) {
		return Parameter.class.equals(clazz);
	}
                
	@Override
	public void validate(Object target, Errors errors) {
		  Parameter parameter = (Parameter) target;
		  
		  if (parameter.getUniqueName() != null && parameter.getUniqueName().isEmpty()) {
			  
			    ValidationUtils.rejectIfEmptyOrWhitespace(errors, "uniqueName", "error.parameter.name", env.getProperty("error.parameter.name"));
		  }/*else if(!parameter.getUniqueName().isEmpty()){		  
			 
		      List<Parameter> parameters = parameterService.findByQuestionnaire(parameter.getQuestionnaire(), false);
		      for(Parameter param : parameters){
		    	  if(param.getUniqueName().trim().equals(parameter.getUniqueName().trim())){
		    		  errors.rejectValue("uniqueName", "error.parameter.name.unique",env.getProperty("error.parameter.name.unique"));
		    		  break;
		    	  }
		      }
		  }*/
	}
}
