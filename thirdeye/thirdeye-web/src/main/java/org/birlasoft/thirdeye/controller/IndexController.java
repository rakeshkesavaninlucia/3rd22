package org.birlasoft.thirdeye.controller;

import org.birlasoft.thirdeye.service.IndexService;
import org.birlasoft.thirdeye.service.SecurityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 * The basic entry page to the application. Will be forwarded to the login page
 * of the application.
 * 
 * @author tej.sarup
 *
 */
@Controller
@RequestMapping("/index")
public class IndexController {
	
	@Autowired 
	private IndexService indexService;
	
	/**
	 * @return {@code ModelAndView}
	 */
	@RequestMapping(value = { "/", "/index.htm" }, method = RequestMethod.GET)
	public ModelAndView IndexPage() {
		
		ModelAndView model = new ModelAndView();
		model.addObject("message", "This is message from the server");
		model.setViewName("login");
		return model;
 
	}
	
	@RequestMapping(value = "/indexHome", method = RequestMethod.GET)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'INDEX_CREATION'})")
	public String indexHome(Model model){		
		return "index/indexHome";
	}
	
	@RequestMapping(value = "/createIndex", method = RequestMethod.POST)
	@PreAuthorize("@" + SecurityService.SECURITY_SERVICE_BEAN +".hasPermission({'INDEX_CREATION'})")
	public String createIndex(Model model){	
		
		String message = indexService.buildIndexForTenant(); 
	    model.addAttribute("message",message);
		return "index/indexHome";
		
	}
	
}