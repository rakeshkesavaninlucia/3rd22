/*
 * Wrapper for the whole JS File
 */
(function(jQuery){
	
//Make sure jQuery has been loaded before app.js
if (typeof jQuery === "undefined") {
  throw new Error("3rdi requires jQuery");
};

var csrf_token = $('meta[name="_csrf"]').attr('content');

$.ajaxPrefilter(function(options, originalOptions, jqXHR){
    if (options.type.toLowerCase() === "post") {
    	// Check if you are firing a JSON request then you should
    	// put it as request headers
    	if (options.contentType === "application/json; charset=utf-8") {
    		jqXHR.setRequestHeader('X-CSRF-TOKEN',  $('meta[name="_csrf"]').attr('content'));
    	} else {
    		// add leading ampersand if `data` is non-empty
    		options.data += options.data?"&":"";
    		// add _token entry
    		options.data += "_csrf=" + csrf_token;
    	}
    }
});

/*
 * Create the namespace for the application
 */
$.ThirdEye = {};

/*
 * The utility which will load the controller related actions for the specific pages.
 */
$.ThirdEye.ControllerUtil = {

		init: function() {
			Box.Application.init({
			    debug: true
			});
		}
};

// Trigger page specifc actions and controllers
$( document ).ready( $.ThirdEye.ControllerUtil.init );

/*
 * Graph visualizations are loaded only if the configuration is there on the page
 * otherwise it will remain dormant. 
 */
(function ($) {}(jQuery));

$(function(){
	// Init functions should be called here.
});

}(window, document, jQuery));