/**
 * @author: dhruv.sood
 */
Box.Application
		.addModule(
				'module-exportQuestionnaire',
				function(context) {

					var $, moduleEl;
					var commonUtils;
					var pageUrl = "/exportImport/fetchModal";
					var id;

					function bindQuestionnaireExportImportModalRequirements(
							$newModal) {
						$('#importQuestionnaireForm', $newModal)
								.ajaxForm(
										function(htmlResponse) {

											if (htmlResponse.redirect) {
												window.location
														.replace(commonUtils
																.getAppBaseURL(htmlResponse.redirect));
											} else {
												var $replacement = commonUtils
														.swapModalContent(
																"exportImportQuestionnaireModal",
																htmlResponse);
												$replacement.modal({
													backdrop : false
												});
												bindQuestionnaireExportImportModalRequirements($replacement);
											}
										});
					}

					function createQuestionnaireExportImportModal(id) {

						var url = commonUtils.getAppBaseURL("questionnaire/"
								+ id + pageUrl);

						commonUtils
								.getHTMLContent(url)
								.then(
										function(content) {

											var $newModal = commonUtils
													.appendModalToPage(
															"exportImportQuestionnaireModal",
															content, true);

											bindQuestionnaireExportImportModalRequirements($newModal);

											$newModal.modal({
												backdrop : false,
												show : true
											});
										});
					}

					return {

						init : function() {
							// retrieve a reference to jQuery
							$ = context.getGlobal('jQuery');
							commonUtils = context.getService('commonUtils');
							moduleEl = context.getElement();
						},

						onclick : function(event, element, elementType) {
							if (elementType === 'exportImportQuestionnaire') {
								id = $(element).data("qid");
								createQuestionnaireExportImportModal(id);
							}
						}
					};
				});