Box.Application.addModule('module-tcoAnalysisGraphs', function(context) {
	
	var $, moduleEl, title,moduleConfig;
	var dataObj = {};
	
	function hideFragment(optionValue){
		if(optionValue === "-1" || optionValue === "SORTED")
			$( "#getFragment" ).hide();
		else{
			$("#getFragment").show();
		}
	}
	
	function showSortedGraph(tcoWrapper) {
		var longest = tcoWrapper.tcoAssetBeans.sort(function (a, b) { return b.assetName.length - a.assetName.length; })[0];
		$(".chart").css("width", (tcoWrapper.tcoAssetBeans.length*20)+50);
		var margin = {top: 20, right: 20, bottom: (longest.assetName.length * 5.5) > 70 ? (longest.assetName.length * 5.5) : 70, left: 100},
	    width = tcoWrapper.tcoAssetBeans.length > 30 ? (20*tcoWrapper.tcoAssetBeans.length)+50 : 960 - margin.lefreft - margin.right,
	    height = 500 - margin.top - margin.bottom + (longest.assetName.length * 5.5) - 120;
		var x = d3.scale.ordinal()
		    .rangeRoundBands([0, tcoWrapper.tcoAssetBeans.length > 30 ? (20*tcoWrapper.tcoAssetBeans.length)+50 : width], .1, 1);
	
		var y = d3.scale.linear()
		    .range([height, 0]);
	
		var xAxis = d3.svg.axis()
		    .scale(x)
		    .orient("bottom");
	
		var yAxis = d3.svg.axis()
		    .scale(y)
		    .orient("left")
		    .tickFormat(function(d) { return '$' + d3.format(',f')(d) });
	
		d3.select("svg").remove();
		
		var tooltip = d3.select("body").append("div").attr("class", "nvtooltip");
		
		var svg = d3.select(".chart-responsive").append("svg")
		    .attr("width", width + margin.left + margin.right)
		    .attr("height", height + margin.top + margin.bottom)
		    .attr("id", "graphDown")
		    
		  .append("g")
		    .attr("transform", "translate(" + margin.left + "," + margin.top + ")")
		    .style("overflow-x","scroll");
		
		x.domain(tcoWrapper.tcoAssetBeans.map(function(d) { return d.assetName; }));
		y.domain([0, d3.max(tcoWrapper.tcoAssetBeans, function(d) { return d.totalCost; })]);
		
		svg.append("g")
		.attr("class", "x1 axis")
		.attr("transform", "translate(0," + height + ")")
		.call(xAxis);
		
		svg.append("g")
		.attr("class", "y axis")
		.call(yAxis)
		.append("text")
		.attr("transform", "rotate(-90)")
		.attr("y", 6)
		.attr("dy", ".71em")
		.style("text-anchor", "end")
		.text("Cost");
		
		svg.selectAll(".bar")
		.data(tcoWrapper.tcoAssetBeans)
		.enter().append("rect")
		.attr("class", "bar")
		.attr("x", function(d) { return x(d.assetName); })
		.attr("width", x.rangeBand())
		.attr("y", function(d) { return y(d.totalCost); })
		.attr("height", function(d) { return height - y(d.totalCost); })
		.on("mousemove", function(d){
            tooltip
            .style("left", d3.event.pageX - 50 + "px")
            .style("top", d3.event.pageY - 70 + "px")
            .style("display", "inline-block")
            .html((d.assetName) + "<br>" + "$" + d3.format(',f')(d.totalCost));
      })
  		.on("mouseout", function(d){ tooltip.style("display", "none");});
		
		d3.select("input").on("change", change);
		
		var sortTimeout = setTimeout(function() {
			d3.select("input").property("checked", true).each(change);
		}, 2);
		
		var xLabels = d3.select('.x1.axis').selectAll('g');
        xLabels.selectAll('text').attr('transform', function() { return 'translate (-10, 0) rotate(-60,0,0) ' });
		
		function change() {
			clearTimeout(sortTimeout);
			
			// Copy-on-write since tweens are evaluated after a delay.
			var x0 = x.domain(tcoWrapper.tcoAssetBeans.sort(this.checked
					? function(a, b) { return b.totalCost - a.totalCost; }
			: function(a, b) { return d3.ascending(a.assetName, b.assetName); })
			.map(function(d) { return d.assetName; }))
			.copy();
			
			svg.selectAll(".bar")
			.sort(function(a, b) { return x0(a.assetName) - x0(b.assetName); });
			
			var transition = svg.transition().duration(250),
			delay = function(d, i) { return i * 50; };
			
			transition.selectAll(".bar")
			.delay(delay)
			.attr("x", function(d) { return x0(d.assetName); });
			
			transition.select(".x1.axis")
			.call(xAxis)
			.selectAll("g")
			.delay(delay);
		}
	}
	
    
	
	function showCombiGraph(tcoWrapper) {
		var data = tcoWrapper;
		var longest = tcoWrapper[0].assets.sort(function (a, b) { return b.length - a.length; })[0];
		
		nv.addGraph(function() {
		    var chart = nv.models.linePlusBarChart()
		      .margin({top: 30, right: 60, bottom: (longest.length * 5.5) > 40 ? (longest.length * 5.5) : 40, left: 70})
		      .x(function(d,i) { return i })
		      .y(function(d) { return d[1] })
		      .color(d3.scale.category10().range())
		      .options({focusEnable: true});

		    chart.xAxis
		      .showMaxMin(false)
		      .ticks(data[0].assets.length)
		      .tickFormat(function(d) {
		    	  return data[0].values[d] && data[0].values[d][0];
		      });

		    chart.y1Axis
		      .tickFormat(function(d) { return '$' + d3.format(',f')(d) });

		    chart.y2Axis
		      .tickFormat(function(d) { 
		    	  return d3.format('.3n')(d) });
		    
		    chart.bars.forceY([0, d3.max(data[0].values, function(d) { return d[1]; })]);
		    chart.lines.forceY([0, d3.max(data[1].values, function(d) { return d[1]; })]);
		    
		    //chart.tooltip.enabled(false)
		    
		    d3.select("svg").remove();
		    
		    d3.select(".chart-responsive").append("svg")
		    .attr("height", 500 + (longest.length * 5.5))
		    .attr("id", "graphDown");

		    d3.select('.chart-responsive svg')
		      .datum(data)
		      .transition().duration(500)
		      .call(chart);
		    
		    var xLabels = d3.select('.nv-x.nv-axis').selectAll('g');
	        xLabels.selectAll('text').attr('transform', function() { return 'translate (-10, 0) rotate(-60,0,0) ' });

		    nv.utils.windowResize(chart.update);

		    return chart;
		});
		
	}
	
	function initPageBindings() {
		var ajaxFormOptions = {
			success: function(tcoWrapper){
				plotTcoGraph(tcoWrapper);
			}
		}
		$("#tcoAnalysisGraph", moduleEl).ajaxForm(ajaxFormOptions); 
	}
	
	function plotTcoGraph(tcoWrapper){
		
		if($('#nodataavailable').length) {					
			$('#nodataavailable').remove();
		}
		if(tcoWrapper.graphName == null && tcoWrapper.graphType==null){
			$(".chart-responsive svg").remove();
			$(".chart-responsive").append("<p id='nodataavailable' style='font-size:16px;text-align: center;'><b>No Data Available.</b></p>");
		}
		if(tcoWrapper.graphType === "SORTED" && tcoWrapper.tcoAssetBeans.length > 0){					
			showSortedGraph(tcoWrapper);
			title = "RankByGraph-" + tcoWrapper.graphName;
		} else if(tcoWrapper.graphType === "SORTED" && tcoWrapper.tcoAssetBeans.length === 0 ) {
			$(".chart-responsive svg").remove();
			$(".chart-responsive").append("<p id='nodataavailable' style='font-size:16px;text-align: center;'><b>No Data Available.</b></p>");
		} else if(tcoWrapper.graphType === "COMBI"){
			showCombiGraph(tcoWrapper.tcoCombiGraphBeans);
			title = "CombiGraph-" + tcoWrapper.graphName;
		}
		if((tcoWrapper.tcoAssetBeans != null && tcoWrapper.tcoAssetBeans.length > 0) || (tcoWrapper.tcoCombiGraphBeans != null && tcoWrapper.tcoCombiGraphBeans.length > 0)){
			$("#saveAs").show();
		}else{
			$("#saveAs").hide();
		}
	
	}
	
	function getReportConfiguration(){
		var configData= {};
		configData.assetTypeId = moduleEl.querySelector('[name="assetTypeId"]').value;
		configData.chartOfAccountYear= moduleEl.querySelector('[name="chartOfAccountYear"]').value;
		configData.graphType= moduleEl.querySelector('[name="graphType"]').value;
		configData.parameterId= moduleEl.querySelector('[name="parameterId"]').value;
		configData.questionnaireId= moduleEl.querySelector('[name="questionnaireId"]').value;
		return JSON.stringify(configData);
	}
	
	return {
		
		behaviors : ['behavior-fetchParameterGraph', 'behavior-select2' ],
		messages: ['repo::TCOAnalysis'],
		
		init : function(){
			// retrieve a reference to jQuery
			$ = context.getGlobal('jQuery');
			// Accessing the service
			$( "#getFragment" ).hide();
			moduleEl = context.getElement();
			moduleConfig = context.getConfig();
			initPageBindings();
			
		},
		
		onchange:function(event,element,elementType){
			if(elementType === 'graphType'){
				hideFragment(element.value);
			}
		},
		
		onclick: function(event, element, elementType) {
			if (elementType === "download"){
				saveSvgAsPng(document.getElementById("graphDown"), title+".png")
			}
			else if(elementType === "saveReport"){
				dataObj = {};
				dataObj.id = moduleConfig.id;
				dataObj.reportType = moduleConfig.reportType.replace(/[']/g, "");
				dataObj.reportConfig = getReportConfiguration(); 
				context.broadcast('repo::savereport', dataObj);
			}
		},
		
		onmessage: function(name, data) {
			 if(name === 'repo::TCOAnalysis'){
				 plotTcoGraph(data);
				 context.broadcast('repo:ViewSuccess',"");
			 }
		 }
	}
	
});