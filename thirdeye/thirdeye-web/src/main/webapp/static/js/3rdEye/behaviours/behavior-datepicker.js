/**
 * 
 * @author: samar.gupta  
 */
Box.Application.addBehavior('behavior-datepicker', function(context) {
	'use strict';

	var $;
	
	function datePicker() {
		$( ".datepicker" ).datepicker({
			  dateFormat: "yy-mm-dd"
		});
		
	}
	
	return {
		init : function() {
			$ = context.getGlobal('jQuery');
			datePicker();
		}
	}

});
