/**
 * module to validate asset type list drop down and to add select2 behavior
 * @author: akshay.mathur
 */
Box.Application.addModule('module-adminView', function(context) {

	'use strict';
	var $;
	// private methods here
	function validateDropDown() {
		$("#errorAid").hide();
		$("#errorAdd").hide();
		
		$(".assetTypeSubmit").click(function () {
			if($(this).attr('id')==='aidButton'){
				if( $("#dropDownAid").val() == "-1"){
					$("#errorAid").css('color', 'red').show()
					return false;
				}
			}

			if($(this).attr('id')==='addButton'){

				if($("#dropDownAdd").val() == "-1"){
					$("#errorAdd").css('color', 'red').show()
					return false;
				}
			}
			return true;
		});
	}
	return {
		behaviors : [ 'behavior-select2'],

		init: function (){
			// retrieve a reference to jQuery
			$ = context.getGlobal('jQuery');  
			
			validateDropDown();
		}
	};
});