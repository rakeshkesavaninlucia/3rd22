/**
 * This module is for Parameter Value Boost - Selection of Asset
 * @author dhruv.sood
 */

Box.Application.addModule('module-pvbAsset', function(context) {

	// private methods here
	var $, moduleDiv;	
	
	var dataObj ={};
	
	return {
		behaviors : [ 'behavior-select2' ], 
		init: function (){
			
			// retrieve a reference to jQuery
			$ = context.getGlobal('jQuery');
			moduleDiv = $(context.getElement());

		},  	
		onchange: function(event, element, elementType) {
			
			if(elementType === 'assetChanged' && $(element).val()>0){
        		dataObj.assetid = $(element).val();
        		dataObj.qeid = $('.select2', moduleDiv).data("qeid");
        		context.broadcast('assetSelected', dataObj);
    	}		
        },
	};
});