/**
 * 
 */

Box.Application.addModule('module-bulletChart', function(context) {

    // private methods here
	var $,d3,nv;	
	var dataObj={};
	var $element,_moduleConfig,jsonService, commonUtils;
	function drawBulletChart(data){
		   var width = _moduleConfig.width,
	        height = _moduleConfig.height,
	        margin = _moduleConfig.margin;//{top: 5, right: 80, bottom: 20, left: 300};
	    var chart = nv.models.bulletChart()
	            .width(width - margin.right - margin.left)
	            .height(height - margin.top - margin.bottom);
	    var div = d3.select("body").append("div")   
	    .attr("class", "tooltip")               
	    .style("opacity", 0);
	    //TODO: to be consistent with other models, should be appending a g to an already made svg, not creating the svg element
	    var vis = d3.select($element.get(0)).selectAll("svg")
	        .data(data)
	        .enter().append("svg")
	        .attr("class", "bullet nvd3")
	        .attr("width", width)
	        .attr("height", height)
	        .attr("data-type",function(d){ 
	        	if(d.parameterId)
	        		return "drilldown" }
	        )
	        .attr("data-parentparameterid",_moduleConfig.display.parameter.parameterId)
	        .attr("data-rootparameterid",_moduleConfig.display.parameter.rootparameterId)
	        .attr("data-questionnaireid",_moduleConfig.display.parameter.questionnaireId)
	        .attr("data-parameterid", function(d){
	        	return d.parameterId}
	        )
	        .attr("data-assetid",_moduleConfig.display.parameter.assetId)
	        .on('mouseover', function(d){
	        	  div.transition()        
	             .duration(200)      
	             .style("opacity", 1.2)
	             .style("left", (d3.event.pageX) + "px")    //Set X  
	             .style("top", (d3.event.pageY) + "px");  //Set Y
	              div.html("<br/> "  +d.title);    
           })
           .on('mouseout', function(d){
        	   div.transition()        
	             .duration(200)      
	             .style("opacity", 0.0);
	          div.html("<br/> "  +d.title);  
    })

	         vis.transition().duration(1000).call(chart);
	    
	   }
	function fetchData(dataObj){
		var pageUrl = "parameter/drill/down/response";		
		var deffMassage = $.Deferred();
		var dataPromise = jsonService.getJsonResponse(commonUtils.getAppBaseURL(pageUrl),dataObj); 
		dataPromise.done ( function (data) {
	        // need to massage it
			deffMassage.resolve (drawBulletChart(data.data));    
	    })
	    
	    .fail (function (error) {
	        console.log (error);
	        deffMassage.reject(error);
	    });
		
	    return deffMassage.promise();
	}
    return {
    	
		 init: function (){
	     	// retrieve a reference to jQuery
	         $ = context.getGlobal('jQuery');
	         d3 = context.getGlobal('d3');
	         nv = context.getGlobal('nv');
	         jsonService = context.getService('jsonService');
	         commonUtils = context.getService('commonUtils');
	         $element = $(context.getElement());
	         _moduleConfig = context.getConfig();            
	         this.drawChart(_moduleConfig.display);
	     },       
       
        drawChart: function(display) {
        	if (display.entity === 'bulletChart'){
        		
        		dataObj.questionnaireId = display.parameter.questionnaireId;
        		dataObj.parameterId = display.parameter.parameterId;
        		dataObj.assetId = display.parameter.assetId;
        		fetchData(dataObj);
        	}
        }

    };
});