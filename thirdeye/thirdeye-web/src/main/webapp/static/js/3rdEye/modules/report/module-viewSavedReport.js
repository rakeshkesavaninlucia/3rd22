/**
 * This module is to view the saving reports.
 * @author: sunil1.gupta
 */
Box.Application.addModule('module-viewSavedReport', function(context) {
	 'use strict';
	 
	 var $, _moduleConfig,commonUtils,waveAxis,noOfWaves,notifyService;
	 var spinner = null;
	
	 function viewReport(reportType,reportConfig){
		 switch (reportType) {
		    case "Health Analysis":
		        fetchWrapperData(reportConfig,"repo::HealthAnalysis",commonUtils.getAppBaseURL("graph/scatter/plot"));
		        break;
		    case "Wave Analysis":
		    	fetchWrapperData(getWaveConfigParam(reportConfig),"repo::WaveAnalysis",commonUtils.getAppBaseURL("graph/scatter/plot"));
		        break;
		    case "TCO Analysis":
		    	fetchWrapperData(reportConfig,"repo::TCOAnalysis",commonUtils.getAppBaseURL("graph/tco/plot"));
		        break;
		    case "Portfolio Health":
		    	fetchWrapperData(reportConfig,"repo::PortfolioHealth",commonUtils.getAppBaseURL("portfoliohealth/plot"));
		        break;
		}
	 }
	 
	 function getWaveConfigParam(config){
		 var waveConfigParam =  JSON.parse(config) ;
		 var dataObj = {};
		 dataObj.questionnaireIds = waveConfigParam.questionnaireIds;
		 dataObj.parameterIds = waveConfigParam.parameterIds;
		 waveAxis = waveConfigParam.waveAxis;
		 noOfWaves = waveConfigParam.noOfWaves;
		 return JSON.stringify(dataObj);
	 }
	 
	 function fetchWrapperData(reportConfig,reportType,pageUrl){
		 var search =JSON.parse(reportConfig) ;
		 $.ajax({
			    type : "POST",
			    dataType : "json",
	            url : pageUrl,
	            data : search,           
	            success : function(response) {
	            	brodcastMessageForReport(reportType, response);
	            },
	            error : function() {
	                console.log("error getting response.....");
	            }
	        });
		 
	 }
	 
	 function brodcastMessageForReport(reportType, response){
		 if(reportType === 'repo::WaveAnalysis'){
			 var waveResponse = {}
			 waveResponse.response = response;
			 waveResponse.waveAxis = waveAxis;
			 waveResponse.noOfWaves = noOfWaves
			 context.broadcast(reportType, waveResponse);
		 }else{
			 context.broadcast(reportType, response);
		 }
	 }

		function saveSavedReportasLandingPage(data){
			
			commonUtils = context.getService('commonUtils');
			 notifyService = context.getService('service-notify');
			var savedreportUrl=commonUtils.getAppBaseURL("report/updateReportStatus");
			var postRequest = $.post(savedreportUrl,data);
			postRequest.done();
		    notifyService.setNotification("Saved", "", "success");
		
		}//eof

	 function showLoading(container, show){
	   var spinService = context.getService('service-spinner');
	   spinner = spinService.getSpinner(container,show,spinner);   	
	}
    return {
    	messages: ['repo:ViewSuccess','buttonOnSavedReportlandingpage'],
		init: function(){
			$ = context.getGlobal('jQuery');
			commonUtils = context.getService('commonUtils');
			 _moduleConfig = context.getConfig(); 
			 viewReport(_moduleConfig.reportType.replace(/[']/g, ""),_moduleConfig.reportConfig.replace(/[']/g, ""));
		},
		onmessage: function(name, data) {
			 if(name === 'repo:ViewSuccess') {
				 showLoading($('.scatterPlot'), false);
			 }
		 },
	    destroy: function() {
	    	waveAxis = null;
	    	noOfWaves = null;
		},
		onmessage: function(name, data){
			
			if (name === 'buttonOnSavedReportlandingpage') {
			    saveSavedReportasLandingPage(data);
			    
			}
			
		}  
    
    };
 });