/**
 * @author: samar.gupta
 * 
 */
Box.Application.addModule('module-relationshipTemplateList',function(context) {
	'use strict';
	var $, commonUtils,moduleEl;
	var pageUrl = "relationshipTemplate/list";
	var dataObj = {};
	
	function showRelationshipTemplatesModal(){	
		 var modalId = moduleEl.id;
		 commonUtils.populateModalWithURL(modalId, commonUtils.getAppBaseURL(pageUrl), dataObj)
		.then(function(){
			Box.Application.startAll(moduleEl);
			// Show the modal
			$(moduleEl).modal({
				keyboard: true
			})			
		});	
	}

	return {

		messages: [ 'rtm::relationshipTemplateListModal'],
		
		// Initialize the page
		init : function() {
			// retrieve a reference to jQuery
			$ = context.getGlobal('jQuery');
			// Accessing the service
			commonUtils = context.getService('commonUtils');
			moduleEl = context.getElement();
			
			$('#goBack').click(function(){
				history.back();
			});
		},
	     
	     destroy: function() {
        	 moduleEl = null;
	     },   
	     
         onmessage: function(name) {

            if (name === 'rtm::relationshipTemplateListModal') {
            	showRelationshipTemplatesModal();
            }
         }
	}
});