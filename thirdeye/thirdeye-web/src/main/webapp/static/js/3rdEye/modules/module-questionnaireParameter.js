/**
 * 
 */
Box.Application.addModule('module-questionnaireParameter', function(context) {
	
	var $, commonUtils,moduleEl;
	var dataObj = {};
	
	function addParamOnQuestionnaire(data){	
		var url;
		if(data.questionnaireType ==='DEF')
		{
			url ="questionnaire/"+data.questionnaireId+"/qparameters/save";
		}else if(data.questionnaireType ==='TCO'){
			url ="tco/"+data.questionnaireId+"/qparameters/save";
		}
		var postRequest = $.post( commonUtils.getAppBaseURL(url), data);
		postRequest.done(function(htmlResponse){			
			 $(moduleEl).empty();
			 $(moduleEl).append(htmlResponse);
			 Box.Application.startAll(moduleEl);
			 context.broadcast('pm::paramProcessComplete', data);
		})
	 }
	
	function deleteParamOnQuestinnaire(element){
		    var questionnaireId =  $(element).data("questionnaireid");
		    var questionnaireType = $(element).data("questionnairetype");
			dataObj.paramId = $(element).closest('tr').attr('id');
			handleDelete(questionnaireId,questionnaireType, dataObj).then(updateGUIForNodeDelete(element));
    }
	
	function handleDelete(questionnaireId,questionnaireType,dataObj){
		var url;
		if(questionnaireType ==='DEF')
		{
			url ="questionnaire/"+questionnaireId+"/parameterquestion/deleteNode";
		}else if(questionnaireType ==='TCO'){
			url ="tco/"+questionnaireId+"/parameterquestion/deleteNode";
		}
		var deferred = $.Deferred();
		var postRequest = $.post( commonUtils.getAppBaseURL(url), dataObj);
		postRequest.done(function(){
			deferred.resolve();
		})		
		return deferred.promise();
	} 
	
	function updateGUIForNodeDelete(element){
		var $trToDelete = $(element).closest('tr');
		$trToDelete.fadeOut(300, function() { $trToDelete.remove();});
	}
	
	return {
		
		behaviors: ['behaviour-showsParamTree'],
	    messages: [ 'pm::parameterSelected' ],
        onmessage: function(name, data) {
            if (name === 'pm::parameterSelected') {
            	dataObj.questionnaireId =  data.questionnaireId;
        		dataObj.parameter = data.parameter;
        		dataObj.questionnaireType = data.questionnaireType;
            	addParamOnQuestionnaire(dataObj);
            }
        },  
        
		init : function(){
			  $ = context.getGlobal('jQuery');
	          commonUtils = context.getService('commonUtils');
	          moduleEl = context.getElement();
	   	},
	   	
		destroy: function() {
			 dataObj = {};
        },   
        
		onclick: function(event, element, elementType) {
			if(elementType==="addParamOnQuestionnaire" ){
				dataObj.questionnaireId = $(element).data("questionnaireid");
				dataObj.questionnaireType = $(element).data("questionnairetype");
				context.broadcast('pm::showParameterSelector', dataObj);				
			}else if(elementType==="deleteParamOnQuestionnaire" ){			
				deleteParamOnQuestinnaire($(element));
			}
		}
		
	}
})