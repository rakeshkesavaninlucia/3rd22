/**
 * @author: ananta.sethi
 *  Behavior for Download Image as PNG/JPEG
 *  This behavior allows us to download the graph/charts  as image  
 */
Box.Application.addBehavior('behavior-download', function(context) {
	'use strict';

	var $;
	var $html2canvas,$canvas2Image;
	var _moduleConfig;
	var $imagename

	function downloadButton()
	{
		$('.box-header').append('<div class="download"><i title = "Download" class="glyphicon glyphicon-download clickable"/></div>');
		//on click of download button
		$('.download').click( function() {
			$html2canvas($('.box-body'), {
				onrendered: function(canvas) {
					if(_moduleConfig.pngimagename === "null")
						$imagename = 'Download';
					else
						$imagename =_moduleConfig.pngimagename.replace(/'/g,"");
					return $canvas2Image.saveAsPNG(canvas,canvas.width,canvas.height,$imagename);
				},
				background :'#FFFFFF',
			});
		})
	}

	return {
		init: function (){

			// retrieve a reference to jQuery
			$ = context.getGlobal('jQuery');
			$html2canvas = context.getGlobal('html2canvas');
			$canvas2Image = context.getGlobal('Canvas2Image');
			_moduleConfig = context.getConfig();
			downloadButton();
		}
	}
})