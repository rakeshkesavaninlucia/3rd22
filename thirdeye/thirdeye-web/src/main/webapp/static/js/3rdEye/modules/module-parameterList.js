/**
 * 
 */
Box.Application.addModule('module-parameterList', function(context) {
	
	// private methods here
	var $ , notifyService, commonUtils;
	var dataObj = {};
	
	function handleDelete(dataObj){
		var deferred = $.Deferred();
		var postRequest = $.post( commonUtils.getAppBaseURL("parameter/delete"), dataObj);
		postRequest.done(function(){
			window.location.href = commonUtils.getAppBaseURL("parameter/list");
			deferred.resolve();
			notifyService.setNotification("Parameter Deleted", "", "success");
		}).fail(function(){
			notifyService.setNotification("Something went wrong", "", "error");
		});	
		return deferred.promise();
	}
	
return {
	behaviors: ['behaviour-showsParamTree'],
	
    init: function (){
    	// retrieve a reference to jQuery
        $ = context.getGlobal('jQuery'); 
        notifyService = context.getService('service-notify');
        commonUtils = context.getService('commonUtils');
      },
      
      onclick: function(event, element, elementType) {
    	  if(elementType === "deleteParameter" && confirm("This item will be permanently deleted and cannot be recovered. Are you sure?") === true){
    		  dataObj.parameterId = $(element).data("parameterid");
    		  handleDelete(dataObj);
    	  }
      }
	};
});