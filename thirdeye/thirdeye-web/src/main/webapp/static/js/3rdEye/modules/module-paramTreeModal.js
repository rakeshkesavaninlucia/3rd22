/**
 * 
 */

Box.Application.addModule('module-paramTreeModal', function(context) {

    // private methods here
	var $, commonUtils,moduleEl;
	var dataObj = {};
	var pageUrl = "parameter/treeview";
	
	function showParameterTreeViewModal(data){	
		 var modalId = moduleEl.id;
		 commonUtils.populateModalWithURL(modalId, commonUtils.getAppBaseURL(pageUrl),data)
		.then(function(){
			Box.Application.startAll(moduleEl);
			$( "div.containerParameterTreeView" ).find( "li" ).attr("data-jstree", '{"opened" : true }');
			$('#container').jstree();

			// Show the modal
			var $modalDiv = $(moduleEl).modal({
				keyboard: true
			})
			$('[data-toggle="popover"]', $modalDiv).popover();
		});	
	}
	function hideParameterTreeViewModal(){
		$(moduleEl).modal('hide');
	}
   
    return {

    	messages: [ 'pm::showTree'],

        onmessage: function(name, data) {
            if (name === 'pm::showTree') {
            		dataObj.parameterId = data.parameterId;
            		showParameterTreeViewModal(dataObj);
            }
        },
        
        init: function (){
        	// retrieve a reference to jQuery
            $ = context.getGlobal('jQuery');
            commonUtils = context.getService('commonUtils');
            moduleEl = context.getElement();
        },
        
        destroy: function() {
        	 dataObj = {};
        	 moduleEl= null;
       }
    };
});