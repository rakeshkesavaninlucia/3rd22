/**
 * @author: ananta.sethi 
 * module for edit workspace-()
 * 
 */
Box.Application.addModule('module-editworkSpace',function(context) {
	var dataobj = {};
	var $,commonUtils,wsid;
	var createUrl = "workspace/edit";	
	//Function to generate modal for cost element
	function createWorkspaceModal(wsid){		
		
		dataobj.wsid = wsid;		
    	var url = commonUtils.getAppBaseURL(createUrl);
    	
		commonUtils.getHTMLContent(url, dataobj).then(function(content){
			
			var $newModal = commonUtils.appendModalToPage("editWorkspaceModal", content, true);
			bindWorkspaceModal($newModal);
			$newModal.modal({backdrop:false,show:true});
		});
	}
	
	function bindWorkspaceModal ($newModal){
		// Bind the ajax form
		$('#editWorkspaceForm', $newModal).ajaxForm(function(htmlResponse) {

			if (htmlResponse.redirect){
				window.location.replace(commonUtils.getAppBaseURL(htmlResponse.redirect));
			} else{ 
				var $replacement = commonUtils.swapModalContent("editWorkspaceModal", htmlResponse);
				$replacement.modal({backdrop:false});
				bindWorkspaceModal($replacement);
			}
		}); 
	
	}
	return{
		behaviors : [ 'behavior-select2'],
		
		
		// Initialize the page
		init:function(){
			// retrieve a reference to jQuery
			$ = context.getGlobal('jQuery');
			// Accessing the service
			commonUtils = context.getService('commonUtils');  
			var clickHandlerConfiguration = {};
			clickHandlerConfiguration.deleteClass = "deleteRow";
			clickHandlerConfiguration.deleteURL = commonUtils.getAppBaseURL("workspace/delete");

			$("#userWorkspaceView").click(clickHandlerConfiguration,commonUtils.handleTableRowClick);
		},
		
	 onclick: function(event, element, elementType) {
     	if (elementType === 'workspaceEdit') {           		
     		wsid = $(element).data("wsid");
    		createWorkspaceModal(wsid);
    	}
     	
     }

	}


});