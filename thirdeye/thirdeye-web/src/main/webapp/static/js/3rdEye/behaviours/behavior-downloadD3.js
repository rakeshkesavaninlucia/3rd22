/**
 * @author: ananta.sethi
 *  Behavior for Download Image as PNG/JPEG
 *  This behavior allows us to download the graph/charts  as image  
 */
Box.Application.addBehavior('behavior-downloadD3', function(context) {
	'use strict';

	var $,d3;
	var _moduleConfig;
	var $imagename,moduleEl;

	function downloadButton()
	{
		$('.box-header').append('<div class="download"><i title = "Download" class="glyphicon glyphicon-download clickable"/></div>');
		//on click of download button
		$('.download').click( function() {

			var html = d3.select("svg")
							.attr("version", 1.1)
							.attr("xmlns", "http://www.w3.org/2000/svg")
							.node().parentNode.innerHTML;

			var svgTag = $.find('svg');
			var canvas = moduleEl.querySelector('[name="canvas"]');
			context = canvas.getContext("2d");
			//Canvas Height & Width 
			canvas.height = svgTag[0].clientHeight;
			canvas.width = svgTag[0].clientWidth;
			var image = new Image;
			image.src = 'data:image/svg+xml;base64,'+ btoa(html);
			image.onload = function() {
				context.drawImage(image, 0, 0);
				var canvasdata = canvas.toDataURL("image/png");
				var a = $('<a></a>');
				if(_moduleConfig.pngimagename === "null")
					$imagename = 'Download';
				else
					$imagename =_moduleConfig.pngimagename.replace(/'/g,"");
				a[0].download = $imagename;
				a[0].href = canvasdata;
				a[0].click();
			};
		});
		
		
	}
	return {
		init: function (){

			// retrieve a reference to jQuery
			$ = context.getGlobal('jQuery');
			d3 = context.getGlobal('d3');
			_moduleConfig = context.getConfig();
			moduleEl = context.getElement();
			downloadButton();
		}
	}
})