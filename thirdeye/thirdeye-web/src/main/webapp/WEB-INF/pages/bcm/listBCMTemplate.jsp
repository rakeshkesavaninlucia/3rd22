<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:th="http://www.thymeleaf.org"
      th:include="layouts/baseLayout :: baseLayoutContainer (dataController='',dataAction='',pageTitle = #{pages.functionalmaps.viewbcmtemplates.nav })">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
</head>
<body>
<div th:fragment="pageTitle"><span th:text="#{pages.functionalmaps.viewbcmtemplates.title}"></span></div>
<div th:fragment="pageSubTitle"><span th:text="#{pages.functionalmaps.viewbcmtemplate.subtitle}"></span></div>
	<div class="container" th:fragment="contentContainer">
		<div class="row">
        	<div class="col-sm-12">
	        	<div class="box box-primary">
	            	<div class="box-body">
 						<div class="table-responsive" data-module="common-data-table">
							<div class="overlay">
								<i class="fa fa-refresh fa-spin"></i>
							</div>
							<table class="table table-bordered table-condensed table-hover" id="listOfBCMTemplate">
								<thead>
									<tr>
										<th><span th:text="#{bcm.name}"></span></th>
										<th><span th:text="#{bcm.update.date}"></span></th>
										<th><span th:text="#{bcm.action}" ></span></th>
									</tr>
								</thead>
								<tbody>
									<tr  th:each="oneBcm : ${listOfBCMTemplate}" th:id="${oneBcm.id}">
									<td><span th:text="${oneBcm.bcmName}">Test</span></td>
									<td><span th:text="${#dates.format(oneBcm.updatedDate,'dd-MMM-yyyy')}">Test</span></td>
									<!-- <td><a class="editBCMTemplate fa fa-pencil-square-o" th:attr="data-bcmid=${oneBcm.id}"></a></td> -->
									<td><a class="fa fa-pencil-square-o" th:href="@{/bcmlevel/{id}/levels(id=${oneBcm.id})}" th:title="#{icon.title.edit}" sec:authorize="@securityService.hasPermission({'BCM_MODIFY'})"></a></td>
									</tr>					
								</tbody>
							</table>
						</div>
					</div>
					<div class="box-footer clearfix" data-module="module-modalCreation">
				    	<div>
							<a class="createBCMTemplate btn btn-primary" data-type="createModal" sec:authorize="@securityService.hasPermission({'BCM_MODIFY'})">Add a new BCM Template</a>
							<script type="text/x-config" th:inline="javascript">{"url":"bcm/create", "modal": "createNewBCMTemplateModal"}</script>
						</div>
					</div>
				</div>
			</div>
		</div>
	<div  id="new" data-module ="module-showHelp">
			<button class="btn btn-primary btn-sm glyphicon glyphicon-question-sign"  data-type ="help" style="float:right;top: -640px;"></button>
	</div>
	</div>
	<div th:fragment="scriptsContainer"  th:remove="tag">
		<script	th:src="@{/static/js/3rdEye/modules/module-modalCreation.js}"></script>
	</div>

</body>
</html>