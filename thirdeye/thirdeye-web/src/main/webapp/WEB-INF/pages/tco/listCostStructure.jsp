<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:th="http://www.thymeleaf.org"
      th:include="layouts/baseLayout :: baseLayoutContainer (dataController='',dataAction='',pageTitle = #{pages.tcomanagement.viewcoststructure.nav})">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
</head>
<body>
<div th:fragment="pageTitle"><span th:text="#{pages.tcomanagement.viewcoststructure.title}"></span></div>
<div th:fragment="pageSubTitle"><span th:text="#{pages.tcomanagement.viewcoststructure.subtitle}"></span></div>
	<div class="container" th:fragment="contentContainer">
	  <div class="row">
        	<div class="col-sm-12">
	        	<div class="box box-primary">
	                <div class="box-body">
				 		<div class="table-responsive" data-module="common-data-table">
							<div class="overlay">
								<i class="fa fa-refresh fa-spin"></i>
							</div>
							<table class="table table-bordered table-condensed table-hover">
								<thead>
									<tr>
										<th><span th:text="#{tco.display.name}"></span></th>
										<th><span th:text="#{tco.unique.name}"></span></th>
										<th><span th:text="#{tco.description}"></span></th>
										<th><span th:text="#{tco.updated.date}"></span></th>
										<th><span th:text="#{tableheader.actions}"></span></th>
									</tr>
								</thead>
								<tbody>
									<tr th:each="oneCostStructure : ${listOfCostStructure}" th:id="${oneCostStructure.id}">
										<td><span th:text="${oneCostStructure.displayName}">Test</span></td>
										<td><span th:text="${oneCostStructure.uniqueName}">Test</span></td>
										<td><span th:text="${oneCostStructure.description}">Test</span></td>
										<td><span th:text="${#dates.format(oneCostStructure.updatedDate,'dd-MMM-yyyy')}">Test</span></td>
										<td><a class="fa fa-pencil-square-o" th:href="@{/coststructure/edit/{id}(id=${oneCostStructure.id})}" th:title="#{icon.title.edit}" sec:authorize="@securityService.hasPermission({'MODIFY_TCO'})"></a></td>
									</tr>					
								</tbody>
							</table>
						</div>
					</div>
					<div class="box-footer clearfix">
	                  <div data-module="module-costStructure">
				          <a class="createCostStructure btn btn-primary" data-type="costStructure" sec:authorize="@securityService.hasPermission({'MODIFY_TCO'})">Create Cost Structure</a>
				      </div>
					</div>
			</div>
		</div>
		</div>
	</div>
	<div th:fragment="scriptsContainer"  th:remove="tag">
       <script	th:src="@{/static/js/3rdEye/modules/module-costStructure.js}"></script>
    </div>
</body>
</html>