<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:th="http://www.thymeleaf.org"
      th:include="layouts/baseLayout :: baseLayoutContainer (dataController='',dataAction='')">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
</head>
<body>
	<div th:fragment="chartOfAccountTreeView">
		<div class="modal-dialog" role="document">
			    <div class="modal-content">
				      <div class="modal-header">
				        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				        <h4 class="modal-title" th:text="${name}"></h4>
				      </div>
				      <div class="modal-body" style="overflow-y:auto;height: 420px;">
				      <div class="containerParameterTreeView" id="container">
				      	<ul>
						  <div th:replace="tco/tcolevelfragment :: oneLevelAdded(listOfLevels=${listsubcostsrtucture})"></div>
						</ul>
				      </div>
			    </div>
			    <div class="modal-footer">	
				      <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>			       
				      </div>
		 </div>
	</div>
	</div>
</body>
</html>


