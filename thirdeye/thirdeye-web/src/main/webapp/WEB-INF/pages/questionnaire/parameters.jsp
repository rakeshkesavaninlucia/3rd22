<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:th="http://www.thymeleaf.org"
      th:include="layouts/baseLayout :: baseLayoutContainer (dataController='QuestionnaireParameter',dataAction='')">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
</head>
<body>
	<div th:fragment="pageTitle"><span th:text="#{questionnaire.parameter}"></span></div>
	<div class="container"  th:fragment="contentContainer">
	   <input type="hidden" name="questionnaireId" th:value="${questionnaireId}"/>
		<div class="row">
			<div class="col-md-6">
				<div class="parameterTree box box-default" >
					<div class="box-body">
						<ul class="parameterTreeMenu" id="questionnaireParameterTree" th:attr="data-questionnaireid=${questionnaireId}">
							<div th:replace="questionnaire/questionnaireParameterFragements :: oneParameterAdded(listOfParameters=${rootParameterList})"></div>
						</ul>
	                   <div class="box-footer">
	                   <a th:href="@{/questionnaire/{questionnaireId}/assets(questionnaireId=${questionnaireId})}" class="btn btn-default" th:text="#{button.previous}"></a>
						<a class="addParameter btn btn-primary">Add Parameters</a>
						<a th:href="@{/questionnaire/{questionnaireId}/orderquestions(questionnaireId=${questionnaireId})}" class="btn btn-default" th:text="#{button.next}"></a>
	                  </div>
	                </div>
				</div>
				
			</div>
		</div>
		<div class="modal fade" id="pageModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" style="display: none;">
	</div>
	</div>
</body>
</html>
