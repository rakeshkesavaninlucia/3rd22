<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:th="http://www.thymeleaf.org"
      th:include="layouts/baseLayout :: baseLayoutContainer (dataController='',dataAction='',pageTitle=#{pages.questionnaire.sresponse.nav})">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
</head>
<body>
	<div th:fragment="pageTitle">
	 <a class="pull-left fa fa-chevron-left btn btn-default" href="/thirdeye-web/questionnaire/submit/response/list" style="margin-left: 10px;"></a>
	 <span th:text="#{pages.questionnaire.sresponse.title}"></span>
	</div>
	<div th:fragment="pageSubTitle"><span th:text="#{pages.questionnaire.sresponse.subtitle}"></span></div>
	
	<div class="container"  th:fragment="contentContainer">
	<div class="row">
		 <div class=" col-lg-12 col-md-12 col-sm-8 col-xs-12">
		 <div class="box box-primary">
		  <div class="box-header with-border">
             <h3 class="box-title" th:text="#{questionnaire.excel.response}"></h3>
             <div class="box-tools pull-right">
				<button class="btn btn-box-tool" data-widget="collapse">
					<i class="fa fa-minus"></i>
				</button>
			</div>
         </div><!-- /.box-header -->
		 <div class="box-body">		 
 			<div class="nav-tabs-custom">
				<ul class="nav nav-tabs" style="width: 100%">
					<li th:class="${activeTab == 'exportTab' ? 'active' : ''} ">
						<a href="#selectExport" data-toggle="tab" aria-expanded="true" th:text="#{export.tab}"> Export </a>
					</li>
					<li th:class="${activeTab == 'importTab' ? 'active' : ''} ">
						<a href="#selectImport" data-toggle="tab" aria-expanded="false" th:text="#{import.tab}"> Import </a>
					</li>
				</ul>
				<div class="tab-content">
					<div class="tab-pane" th:classappend="${activeTab == 'exportTab' ? 'active' : ''} " id="selectExport">
						<h3 th:text="#{export.tab.heading}"></h3>
						<a class="btn btn-primary" th:href="@{/questionnaire/{id}/response/exportQuestionnaire(id=${questionnaireId})}" th:text="#{export.button}"></a>
						<h4>Please Note:</h4>
						<ol>
						    <li>Only you can upload from this template.</li>
							<li>Please do not make any modifications to the structure of this file.</li>
						</ol>
					</div><!-- /.tab-pane -->
					<div class="tab-pane" th:classappend="${activeTab == 'importTab' ? 'active' : ''} " id="selectImport">
						<h3 th:text="#{import.tab.heading}">Test</h3>
						<form th:action="@{/questionnaire/{id}/import?_csrf={_csrf.token}(id=${questionnaireId},_csrf.token=${_csrf.token})}" method="post" id="importQuestionnaireForm" enctype="multipart/form-data">
							<input type="file" name="fileImport" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel"/>
							<input type="submit" value="Upload"/>
							<div th:if="${activeTab == 'importTab'}">
								<div th:if="${#lists.isEmpty(listOfErrors)}">
									<div class="alert alert-success alert-dismissable">
										<h4><i class="icon fa fa-check"></i> Alert!</h4>
										<span th:text="#{import.success}"></span>
									</div>
								</div>
								<div th:if="${not #lists.isEmpty(listOfErrors)}">
									<div class="alert alert-danger alert-dismissable">
										<h4><i class="icon fa fa-ban"></i> Alert!</h4>
										<ul th:each="errorCell : ${listOfErrors}">
											<span th:if="${errorCell.row ne null and errorCell.column ne null}">Column: <span th:text="${errorCell.column }"></span> Row: <span th:text="${errorCell.row }"></span></span>
											<ul th:each="error : ${errorCell.errors}">				
												<li><span th:text="${error}"></span></li>
											</ul>
										</ul>
									</div>
								</div>
								<span th:text="${Message}" style="color:red;"></span>
							</div>
						</form>
					</div><!-- /.tab-pane -->
				</div><!-- /.tab-content -->
			</div>	
			</div>	
			</div>	
		 </div>
		</div>
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"  data-module='module-questionnaireResponse'>
				<div class="box box-primary" th:fragment="gridQuestionnaire">
					<div class="box-header">
		                 <h3 class="box-title" th:text="${questionnaire.name}">This is the title of the questionnaire</h3>
		                 <!-- <div class="box-tools pull-right">
		                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
		                  </div> -->
		                 <small th:text="${questionnaire.description}"></small>
		            </div>
	             	<div class="box-body" >
					<form  method="POST" th:action="@{/questionnaire/response/saveQuestionnaire}" th:object ="${questionnaireQuestionWrapper}">
						<div>
						<table class="table table-bordered table-striped table-condensed table-hover dataTable"	id="coaList">
							<thead>
								<tr>	
									<th><span th:text="#{questionnaire.question} +'/'+ #{questionnaire.asset}"></span></th>														
									<th th:each="assetName : ${AssetName}"><span th:text="${assetName}"></span></th>
								</tr>
							</thead>
							<tbody>
								<tr th:each="cs,loop : *{questionnaireQuestionResponseBean}">								
									<td>
									<span th:text="${cs.questionTitle}" th:field="${cs.questionTitle}"></span></td>
									<td th:each ="coa1, stat : *{questionnaireQuestionResponseBean[__${loop.index}__].questionnaireQuestionGridBean}">
						<th:block th:switch="${coa1.questionType}">                                                            
                                    <input th:case="NUMBER" type="number" th:id="'resp_'+${coa1.response}" th:value="${coa1.response}" th:field="*{questionnaireQuestionResponseBean[__${loop.index}__].questionnaireQuestionGridBean[__${stat.index}__].response}" class="form-control editableParameter numeric" placeholder="Enter your amount Here" />
                                     <select class="select2" th:case="MULTCHOICE" th:field="*{questionnaireQuestionResponseBean[__${loop.index}__].questionnaireQuestionGridBean[__${stat.index}__].response}">
                                      <option value="">--Select--</option>
                                       <option th:each="dataType :${coa1.questionOptions}" th:id="'resp_'+${coa1.response}" th:value="${dataType}" th:text="${dataType}" th:selected="${dataType.equals(coa1.response)}"/>
                                          </select> 
                                 		    <input th:case = "TEXT" type="text" th:id="'resp_'+${coa1.response}" th:value="${coa1.response}" th:field="*{questionnaireQuestionResponseBean[__${loop.index}__].questionnaireQuestionGridBean[__${stat.index}__].response}" class="form-control editableParameter" placeholder="Enter your text Here" />
                               			  <input th:case = "PARATEXT" type="text" th:id="'resp_'+${coa1.response}" th:value="${coa1.response}" th:field="*{questionnaireQuestionResponseBean[__${loop.index}__].questionnaireQuestionGridBean[__${stat.index}__].response}" class="form-control editableParameter" placeholder="Enter your text Here" />
                                        <input th:case = "DATE" type="date" th:id="'resp_'+${coa1.response}" th:value="${coa1.response}" th:field="*{questionnaireQuestionResponseBean[__${loop.index}__].questionnaireQuestionGridBean[__${stat.index}__].response}" class="form-control editableParameter" placeholder="Enter your text Here" />
                         </th:block> 
								 <input type="hidden"  th:id="'qqid_'+${coa1.questionnaireQuestionId}" th:value="${coa1.questionnaireQuestionId}" th:field="*{questionnaireQuestionResponseBean[__${loop.index}__].questionnaireQuestionGridBean[__${stat.index}__].questionnaireQuestionId}"/></td>
								</tr>
							</tbody>
						</table>
						</div>
						<div class="box-footer" style="float:right;">
								<div th:if="${T(org.birlasoft.thirdeye.constant.QuestionnaireStatusType).REVIEWED.name().equalsIgnoreCase(questionnaire.status) or T(org.birlasoft.thirdeye.constant.QuestionnaireStatusType).EDITABLE.name().equalsIgnoreCase(questionnaire.status) or T(org.birlasoft.thirdeye.constant.QuestionnaireStatusType).CLOSED.name().equalsIgnoreCase(questionnaire.status)}">
									<input  style="display: none;" type="submit" value="SAVE" class="btn btn-primary" />
							</div>
							<div th:if="${status eq false and T(org.birlasoft.thirdeye.constant.QuestionnaireStatusType).PUBLISHED.name().equalsIgnoreCase(questionnaire.status)}">
									<input style="display: none;" type="submit" value="SAVE" class="btn btn-primary" />
							</div>
							<div th:if="${status eq true and T(org.birlasoft.thirdeye.constant.QuestionnaireStatusType).PUBLISHED.name().equalsIgnoreCase(questionnaire.status)}">
									<input  type="submit" value="SAVE" class="btn btn-primary" />
							</div>
						</div>				
						</form>	
					</div>
		        </div> 
			</div>
		</div>
		
		<div class="row">
			<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2" ></div>
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4" th:if="${fcExists}">
				<a class="pull-right fa fa-chevron-right btn btn-block btn-primary" style="content:after;margin-right: 125px;" th:href="@{/questionnaire/{id}/functionalCoverage(id=${questionnaireId})}" th:text="#{view.functional.coverage}"></a>						
			</div>
		</div>
	</div>
	<div th:fragment="gridQuestionnairePrePublish">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="box box-primary" th:fragment="gridQuestionnairePrePublish">
					<div class="box-header">
		                 <h3 class="box-title" th:text="${questionnaire.name}">This is the title of the questionnaire</h3>
		                 <!-- <div class="box-tools pull-right">
		                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
		                  </div> -->
		                 <small th:text="${questionnaire.description}"></small>
		            </div>
	             	<div class="box-body" >
					<form  method="POST" th:action="@{/questionnaire/response/saveQuestionnaire}" th:object ="${questionnaireQuestionWrapper}">
						<div>
						<table class="table table-bordered table-striped table-condensed table-hover dataTable"	id="coaList">
							<thead>
								<tr>	
									<th><span th:text="#{questionnaire.question} +'/'+ #{questionnaire.asset}"></span></th>														
									<th th:each="assetName : ${AssetName}"><span th:text="${assetName}"></span></th>
								</tr>
							</thead>
							<tbody>
								<tr th:each="cs,loop : *{questionnaireQuestionResponseBean}">								
									<td>
									<span th:text="${cs.questionTitle}" th:field="${cs.questionTitle}"></span>
									</td>																			
									<td th:each ="coa1, stat : *{questionnaireQuestionResponseBean[__${loop.index}__].questionnaireQuestionGridBean}">
						<th:block th:switch="${coa1.questionType}">                                                            
                                    <input th:case="NUMBER" type="number" th:id="'resp_'+${coa1.response}" th:value="${coa1.response}" th:field="*{questionnaireQuestionResponseBean[__${loop.index}__].questionnaireQuestionGridBean[__${stat.index}__].response}" class="form-control editableParameter numeric nonClick greyLight" placeholder="Enter your amount Here" />
                                     <select class="select2"  th:disabled="disabled"  th:case="MULTCHOICE" th:field="*{questionnaireQuestionResponseBean[__${loop.index}__].questionnaireQuestionGridBean[__${stat.index}__].response}">
                                      <option value="">--Select--</option>
                                       <option  th:each="dataType :${coa1.questionOptions}" th:id="'resp_'+${coa1.response}" th:value="${dataType}" th:text="${dataType}" th:selected="${dataType.equals(coa1.response)}"/>
                                          </select> 
                                 		    <input th:case = "TEXT" type="text" th:id="'resp_'+${coa1.response}" th:value="${coa1.response}" th:field="*{questionnaireQuestionResponseBean[__${loop.index}__].questionnaireQuestionGridBean[__${stat.index}__].response}" class="form-control editableParameter nonClick greyLight" placeholder="Enter your text Here" />
                               			  <input th:case = "PARATEXT" type="text" th:id="'resp_'+${coa1.response}" th:value="${coa1.response}" th:field="*{questionnaireQuestionResponseBean[__${loop.index}__].questionnaireQuestionGridBean[__${stat.index}__].response}" class="form-control editableParameter nonClick greyLight" placeholder="Enter your text Here" />
                                        <input th:case = "DATE" type="date" th:id="'resp_'+${coa1.response}" th:value="${coa1.response}" th:field="*{questionnaireQuestionResponseBean[__${loop.index}__].questionnaireQuestionGridBean[__${stat.index}__].response}" class="form-control editableParameter nonClick greyLight" placeholder="Enter your text Here" />
                         </th:block> 
								 <input type="hidden"  th:id="'qqid_'+${coa1.questionnaireQuestionId}" th:value="${coa1.questionnaireQuestionId}" th:field="*{questionnaireQuestionResponseBean[__${loop.index}__].questionnaireQuestionGridBean[__${stat.index}__].questionnaireQuestionId}"/></td>
								</tr>
							</tbody>
						</table>
						</div>
						<div class="box-footer" style="float:right;">
								<div th:if="${T(org.birlasoft.thirdeye.constant.QuestionnaireStatusType).REVIEWED.name().equalsIgnoreCase(questionnaire.status) or T(org.birlasoft.thirdeye.constant.QuestionnaireStatusType).EDITABLE.name().equalsIgnoreCase(questionnaire.status) or T(org.birlasoft.thirdeye.constant.QuestionnaireStatusType).CLOSED.name().equalsIgnoreCase(questionnaire.status)}">
									<input  style="display: none;" type="submit" value="SAVE" class="btn btn-primary" />
							</div>
							<div th:if="${status eq false and T(org.birlasoft.thirdeye.constant.QuestionnaireStatusType).PUBLISHED.name().equalsIgnoreCase(questionnaire.status)}">
									<input style="display: none;" type="submit" value="SAVE" class="btn btn-primary" />
							</div>
							<div th:if="${status eq true and T(org.birlasoft.thirdeye.constant.QuestionnaireStatusType).PUBLISHED.name().equalsIgnoreCase(questionnaire.status)}">
									<input  type="submit" value="SAVE" class="btn btn-primary" />
							</div>
						</div>				
						</form>	
					</div>
		        </div> 
			</div>
		</div>
		<div class="row">
			<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2" ></div>
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4" th:if="${fcExists}">
				<a class="pull-right fa fa-chevron-right btn btn-block btn-primary" style="content:after;margin-right: 125px;" th:href="@{/questionnaire/{id}/functionalCoverage(id=${questionnaireId})}" th:text="#{view.functional.coverage}"></a>						
			</div>
		</div>
	</div>
	<div th:fragment="scriptsContainer"  th:remove="tag">
		<script th:src="@{/static/js/3rdEye/modules/module-questionnaireResponse.js}"></script>
		<script	th:src="@{/static/js/3rdEye/modules/module-exportQuestionnaire.js}"></script>
	</div>
</body>
</html>