<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:th="http://www.thymeleaf.org"
      th:include="layouts/baseLayout :: baseLayoutContainer (dataController='',dataAction='',pageTitle = #{pages.reports.liefeCycle.reports.title.nav})">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
</head>
<body>
	<div th:fragment="pageTitle">
	<span th:text="#{pages.reports.liefeCycle.title}"></span></div>
		<div th:fragment="pageSubTitle">
		<span th:text="#{pages.reports.liefeCycle.subtitle}"></span>
	</div>
	<div class="container"  th:fragment="contentContainer">
	<div data-module="module-lifeCycleReport">
		<div class="row">
			<div class="col-sm-12">	
				<div class="box box-primary">					
					 <table class="table table-bordered table-condensed table-hover">					
						<tr>
							<td colspan="3">
							<select class="select2" data-type="assetTemplate">
								<option value="-1">--Select an Asset Template--</option>							
								<option th:each="oneAssetTemplate : ${listOfAssetTemplate}" th:value="${oneAssetTemplate.id}" th:text="${oneAssetTemplate.assetTemplateName}" />
							</select></td>
						</tr>
					</table>				
				</div>	
			</div>	
		</div>
	</div>
		<div data-module="module-lifeCycleAssetReport">
			<div th:fragment="assetTemplateAsset">
				<div class="row" th:if="${not #maps.isEmpty(mapofassetLifeCycle)}" id="assetlifeCycle">
			       	<div class="col-sm-12">
			        	<div class="box box-primary">
			                <div class="box-body">	              
								<div class="table-responsive">
									<table class="table table-bordered table-striped table-condensed table-hover dataTable"	id="assetlifeCycle1" >
							<thead>
								<tr >	
									<th><span th:text="#{heading.subtitle}"></span></th>														
									<th th:each="oneyear:${yearRange}"><span th:text="${oneyear}"></span></th>
								</tr>
							</thead>
							<tbody>
							<tr th:each ="oneMap:${mapofassetLifeCycle}">								
							<td style="white-space: nowrap;"><span th:text="${oneMap.key}"></span></td>																			
							<td th:each="twoMap:${oneMap.value}">
							<table>
      						  <tr>
       					  	 <td  th:each="threemap:${twoMap.value}">
       					  	 <td  bgcolor ="#FFFFFF"  th:if = "${threemap eq 'NA'}" th:text ="${threemap}"></td>
       					  	 <td  bgcolor ="#3498DB"  th:if = "${threemap eq 'NEW' }" th:text ="${threemap}"></td>
       					  	 <td  bgcolor ="#F1C40F"  th:if = "${threemap eq 'EMERGING'}"  th:text ="${threemap}"></td>
       					  	 <td  bgcolor = "#E67E22" th:if = "${threemap eq 'MAINSTREAM'}"  th:text ="${threemap}"></td>
       					  	 <td  bgcolor = "#1ABC9C" th:if = "${threemap eq 'CONTAINMENT'}"  th:text ="${threemap}"></td>
       					  	 <td  bgcolor =  "#EC7063" th:if = "${threemap eq 'SUNSET'}"  th:text ="${threemap}"></td>
       					  	 <td  bgcolor =  "#E74C3C" th:if = "${threemap eq 'PROHIBITED'}"  th:text ="${threemap}"></td>
       					  														 </td>
       																	  	 </tr>
     																 </table>
																</td>
														</tr>
												</tbody>
											</table>
										</div>		
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
	<div th:fragment="scriptsContainer"  th:remove="tag">
	<script th:src="@{/static/js/3rdEye/modules/module-lifeCycleReport.js}"></script> 
	 <script th:src="@{/static/js/3rdEye/modules/module-lifeCycleAssetReport.js}"></script>     	
    </div>
</body>
</html>