
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
	xmlns:th="http://www.thymeleaf.org">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
</head>
<body>
	<div th:fragment="wavePointFragment(noOfPartition)">
		  <table class="table table-bordered table-condensed row">
		      <tr>
		        <td class="col-md-2 horizontal"></td>
		        <td class="col-md-5">
		           <div>	
		             <label>Point 1 (x,y)</label>
				   </div>
		       </td>
		         <td class="col-md-5">
		          <div>	
		             <label>Point 2 (x,y)</label>
		          </div>
		       </td>
		      </tr>
		      <tr th:each="i: ${#numbers.sequence(1, noOfPartition)}">
					 <td class="col-md-2 horizontal"><label th:text="'Partition '+${i}"></label></td>
					 <td class="col-md-5">
					     <div>	
					      <input type="number" min="0" max="10" step=".1" th:name="'w'+${i}+'x1'" required="required" />
					      <input type="number" min="0" max="10" step=".1" th:name="'w'+${i}+'y1'" required="required"/>
					     </div>
					 </td>
					 <td class="col-md-5">
					      <div>	
					        <input type="number" min="0" max="10" step=".1" th:name="'w'+${i}+'x2'" required="required" />
					        <input type="number" min="0" max="10" step=".1" th:name="'w'+${i}+'y2'" required="required"/>
					      </div>
					 </td>
			   </tr>
	      </table>
       
        <div>
		       <span class="error_msg"></span>
	    </div>
        <div class = "pull-right">
		        <button type="submit" class="btn btn-primary" data-type="applyWaves">Apply Waves</button>
	    </div>
	</div>
	<div th:fragment="waveReportFragment">
	  <table class="table table-bordered table-condensed table-hover" id="waveTableView">
				<thead>
					<tr>
						<th><span th:text="#{waveanalysis.wave}"></span></th>
						<th><span th:text="#{waveanalysis.asset.name}"></span></th>
						<th><span th:text="${xAxisLabel}"></span></th>
						<th><span th:text="${yAxisLabel}"></span></th>
					</tr>
				</thead>
	    </table>
	</div>
</body>
</html>