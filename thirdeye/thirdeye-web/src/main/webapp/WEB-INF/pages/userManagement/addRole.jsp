<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:th="http://www.thymeleaf.org">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
 <style th:fragment="onPageStyles">
  </style>
<title>New Role</title>
</head>
<body>
	<div th:fragment="addNewRole">
		<div class="modal fade" id="addNewRoleModal" role="dialog" aria-labelledby="addNewRoleModal">
			<div class="modal-dialog" role="document" >
				    <div class="modal-content">
						<form th:action="@{/user/roles/saveUserRole}" method="post" th:object="${addNewRoleForm}" id="addNewRoleForm">
						      <div class="modal-header">
						        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						        <h4 class="modal-title" th:text="#{pages.usermanagement.userroles.title}"></h4>
						      </div>
						      <div class="modal-body">						          
						          <div class="form-group">
						          	<label for="title" class="control-label"> Role Name:</label>
						            <input type="text" class="form-control" th:field="*{roleName}"/>
						            <span th:if="${#fields.hasErrors('roleName')}" th:errors="*{roleName}" style="color: red;"></span>
						            </div>
						            <div class="form-group">
						            <label for="title" class="control-label">Description:</label>
						            <input type="text" class="form-control" th:field="*{roleDescription}"/>		
						            </div>
						    		</div>
						    	
						      <div class="modal-footer">
						     	  <input type="submit" th:value = "ADD" class="btn btn-primary"></input>
						     </div>
					    </form>
				    </div>
			 </div>
		 </div>
	</div>
</body>
</html>