<!DOCTYPE html >
<html xmlns="http://www.w3.org/1999/xhtml"
	xmlns:th="http://www.thymeleaf.org"
	th:include="layouts/baseLayout :: baseLayoutContainer (dataController='',dataAction='',pageTitle = #{pages.templatemanagement.createtemplate.nav})">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
</head>
<body>
	<div th:fragment="pageTitle"></div>
	<div th:fragment="pageSubTitle">
		<span th:text="#{pages.templatemanagement.definetemplate.subtitle}"></span>
	</div>

	<div class="container" th:fragment="contentContainer">
		<div class="row">
			<div class="col-md-6">
				<div class="box box-primary">
					<div class="box-body" data-module="module-assetDataEntry">
						<div th:if="${#sets.size(assetTemplateModel.assetTemplateColumns) gt 0 or T(org.birlasoft.thirdeye.constant.AssetTypes).RELATIONSHIP.name().equalsIgnoreCase(assetTemplateModel.assetType.assetTypeName)}" th:fragment="populateAssetDataForm">
							<form th:action="@{/templates/relationform/save}" method="post">
								<input type="hidden" name="templateId" th:value="${assetTemplateModel.id}" />
								<input type = "hidden" name ="assetid"  th:value="${assetid}"/>
								<div class="form-group" data-module="module-assetDataEntry">
									<div>
										<a class="fa fa-chevron-left btn btn-default" th:href="@{/templates/data/view/{id}(id=${assetTemplateModel.id})}" style="margin-left: 10px;"></a> 
										<input class="btn btn-primary" style="float: right;" type="submit" value="Save" id="submitButton"
											th:if="${assetTemplateModel.assetTemplateColumns ne null}" />
									</div>

									<table th:if="${listOfParentAssets ne null and listOfChildAssets ne null}" class="table table-bordered table-condensed table-hover">
										<tr	th:each="templatecol : ${assetTemplateModel.assetTemplateColumns}">
											<td class="col-md-4 horizontal">
											<span th:text="${templatecol.assetTemplateColName}"></span></td>
											<td class="col-md-8">
											<input 	th:if="${templatecol.dataType ne T(org.birlasoft.thirdeye.constant.DataType).DATE.name()}" th:name="${templatecol.id}" th:type="${templatecol.dataType}" class="form-control" th:maxlength="${templatecol.length}" th:required="${templatecol.mandatory}" th:value="${mapOfColIdAndData.get(templatecol.id)}"></input>
											<input	th:if="${templatecol.dataType eq T(org.birlasoft.thirdeye.constant.DataType).DATE.name()}" th:name="${templatecol.id}" th:type="date" class="form-control" th:maxlength="${templatecol.length}" th:required="${templatecol.mandatory}" th:value="${mapOfColIdAndData.get(templatecol.id)}"></input>
											</td>
										</tr>
										<tr>
											<td class="col-md-4 horizontal"><span>Parent Asset</span></td>
											<td class="col-md-8">
											<select	class="form-control select2 parentassetdropdown" name="parentAsset" required="required">
													<option value="">--Select--</option>
													<option th:each="oneAsset : ${listOfParentAssets}"	th:value="${oneAsset.id}" th:selected="${oneAsset.id==Parentasset}"	th:text="${oneAsset.assetTypeName} + ' :: ' + ${oneAsset.shortName}" />
											</select></td>
										</tr>
										<tr>
											<td class="col-md-4 horizontal"><span>Child Asset</span></td>
											<td class="col-md-8">
											<select	class="form-control select2 childassetdropdown"	name="childAsset" required="required">
													<option value="">--Select--</option>
													<option th:each="oneAsset : ${listOfChildAssets}" th:value="${oneAsset.id}"	th:selected="${oneAsset.id==Childasset}" th:text="${oneAsset.assetTypeName} + ' :: ' + ${oneAsset.shortName}" />
											</select> <br />
										<div id="errorParentChild" style="display: none;">Child	asset name cannot be same as Parent asset</div></td>
										</tr>
									</table>
									<span th:if="${error eq 1}" style="color: red;"> Name already exists</span>
								</div>
							</form>
						</div>
					</div>
					<!-- /.box-body -->
				</div>
			</div>
		</div>
	</div>
	<div th:fragment="scriptsContainer">
		<script	th:src="@{/static/js/3rdEye/modules/module-assetDataEntry.js}"></script>
	</div>
</body>
</html>