<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
	xmlns:th="http://www.thymeleaf.org"
	th:include="layouts/baseLayout :: baseLayoutContainer (dataController='',dataAction='',pageTitle = #{pages.admin.reports.title.nav} )">
<body>
	<div th:fragment="pageTitle">
		<div>
			<span th:text="#{pages.admin.reports.title}"></span>
		</div>
	</div>
	<div th:fragment="pageSubTitle">
		<span th:text="#{pages.admin.reports.subtitle}"></span>
	</div>
	<div class="container" th:fragment="contentContainer">
		<div data-module="module-adminView">
			<form th:action="@{/adminaid}" method="post">
				<div class="row">
					<div class="col-md-12">
						<div class="box box-primary">
							<div class="box-header with-border">
								<h3 class="box-title"
									th:text="#{pages.admin.reports.header.aid}"></h3>

							</div>
							<div class="box-body">
								<div>
									<span th:text="#{asset.type}"></span> <br />
									 <select id="dropDownAid" class="form-control select2 aidDropDownClass" name="assetTypeId">
										<option id="one" value="-1">--Select--</option>
										<option th:each="assetType : ${assetTypeList}" th:value="${assetType.id}" th:text="${assetType.assetTypeName}" />
									</select> <br />
									<div id="errorAid" style="display: none;">Please select and option</div> <br /> <input id="aidButton" data-type="aidDataType"
										class="btn btn-primary assetTypeSubmit" type="submit"
										value="Submit" /> <br />
								</div>
							</div>
						</div>
					</div>
				</div>
			</form>
			<form th:action="@{/adminadd}" method="post">
				<div class="row">
					<div class="col-md-12">
						<div class="box box-primary">
							<div class="box-header with-border">
								<h3 class="box-title"
									th:text="#{pages.admin.reports.header.add}"></h3>

							</div>
							<div class="box-body">
								<div>
									<span th:text="#{asset.type}"></span>
									<br /> <select id="dropDownAdd" class="form-control select2 addDropDownClass"
										name="assetTypeId">
										<option id="two" value="-1">--Select--</option>
										<option th:each="assetType : ${assetTypeList}" th:value="${assetType.id}" th:text="${assetType.assetTypeName}" />
									</select> <br />
									<div id="errorAdd" style="display: none;">Please select and option</div>  <br /> <input id="addButton" data-type="addDataType"
										class="btn btn-primary assetTypeSubmit" type="submit"
										value="Submit" /> <br />
								</div>
							</div>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
	<div th:fragment="scriptsContainer">
		<script th:src="@{/static/js/3rdEye/modules/module-adminView.js}"></script>
	</div>
</body>
</html>
