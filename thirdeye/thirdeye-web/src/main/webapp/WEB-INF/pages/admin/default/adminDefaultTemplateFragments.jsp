<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
	xmlns:th="http://www.thymeleaf.org">
<head>
<title>Insert title here</title>
</head>
<body>

	<div th:fragment="centralApp(aidWrapper)" class="defaultTemplate centralAssetWrapper">
		<div class="centralAsset" th:attr="data-aidreportid=${aidWrapper.id}">
			<!-- Code in the blocks -->
			<div th:each="oneBlock: ${aidWrapper.listOfBlocksInMainApp}" class="aidblockwrapper">
				<div th:if="${showEdit != null}" class="dropdown">
				  <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
				    <i class="fa fa-cog"></i>
				    <span class="caret"></span>
				  </button>
				  <ul class="dropdown-menu" aria-labelledby="dashboardActionDropDown">
				    <li><a th:attr="data-aidblockid=${oneBlock.id}, data-aidblockcontrol='editblock'">Edit block configuration</a></li>
				    <li><a th:attr="data-aidblockid=${oneBlock.id}, data-aidblockcontrol='moveup'">Move up</a></li>
				    <li><a th:attr="data-aidblockid=${oneBlock.id}, data-aidblockcontrol='movedown'">Move down</a></li>
				    <li><a th:attr="data-aidblockid=${oneBlock.id}, data-aidblockcontrol='deleteblock'">Delete block</a></li>
				  </ul>
				</div>
				<div th:if="${oneBlock.aidBlockType ==  T(org.birlasoft.thirdeye.constant.aid.AIDBlockType).BLOCK_1}">
				    <div th:replace="aid/blocks/BLOCK_1 :: BLOCK_1_view(blockForDisplay=${oneBlock})"></div>
				</div>
				
				<div th:if="${oneBlock.aidBlockType ==  T(org.birlasoft.thirdeye.constant.aid.AIDBlockType).BLOCK_2}">
				    <div th:replace="aid/blocks/BLOCK_2 :: BLOCK_2_view(blockForDisplay=${oneBlock})"></div>
				</div>
				
				<div th:if="${oneBlock.aidBlockType ==  T(org.birlasoft.thirdeye.constant.aid.AIDBlockType).BLOCK_3}">
				    <div th:replace="aid/blocks/BLOCK_3 :: BLOCK_3_view(blockForDisplay=${oneBlock})"></div>
				</div>
				
				<div th:if="${oneBlock.aidBlockType ==  T(org.birlasoft.thirdeye.constant.aid.AIDBlockType).BLOCK_4}">
				    <div th:replace="aid/blocks/BLOCK_4 :: BLOCK_4_view(blockForDisplay=${oneBlock})"></div>
				</div>
			</div>
		</div>
	</div>
	
	
	<div th:fragment="subBlockConfiguration(subBlockForm)">
		<div class="modal fade" id="subBlockEditModal" tabindex="-1" role="dialog" aria-labelledby="subBlockEditModal">
			<div class="modal-dialog" role="document" >
				    <div class="modal-content">
						<form th:action="@{/aid/configure/subBlockEdit/save}" th:object="${subBlockForm}" method="post" id="subBlockForm">
							<input type="hidden" th:field="*{parentBlockId}"/>
							<input type="hidden" th:field="*{subBlockIdentifier}"/>
							<input type="hidden" name="activeTab" th:value="${activeTab}"/>
								<div class="modal-header">
							        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							        <h4 class="modal-title">Configure the sub-block</h4>
						      	</div>
						      	<div class="modal-body">
						      		<div>
						      			<div class="form-group">
								            <input type="text" class="form-control" th:field="*{blockTitle}" placeholder="Add a title"/>
					            			<span th:if="${#fields.hasErrors('blockTitle')}" th:errors="*{blockTitle}" style="color: red;">Test</span>
								        </div>
							        </div>
									<div class="nav-tabs-custom">
						                <ul class="nav nav-tabs" style="width: 100%">
						                  <li th:class="${activeTab == T(org.birlasoft.thirdeye.constant.aid.SubBlockTabs).selectQuestion ? 'active' : ''} ">
							                  <a href="#selectQuestion" data-toggle="tab" aria-expanded="true">
							                  	Questions
							                  </a>
						                  </li>
						                  <li th:class="${activeTab == T(org.birlasoft.thirdeye.constant.aid.SubBlockTabs).selectParameter ? 'active ' : ''}"><a href="#selectParameter" data-toggle="tab" aria-expanded="false">
							                  	Parameter
						                  </a></li>
						                  <li th:class="${activeTab == T(org.birlasoft.thirdeye.constant.aid.SubBlockTabs).selectColumn? 'active ' : ''} "><a href="#selectColumn" data-toggle="tab" aria-expanded="false">
						                  	Inventory
						                  </a></li>
						                </ul>
						                <div class="tab-content">
						                  <div class="tab-pane" th:classappend="${activeTab == T(org.birlasoft.thirdeye.constant.aid.SubBlockTabs).selectQuestion ? 'active ' : ''}" id="selectQuestion">
						                  		
						                  		<div class="form-group">
													<label>Please select the questionnaire</label>
													<select class="form-control ajaxSelector" th:field="*{questionQEId}" data-dependency="qeForQuestion">
														<option value="-1">--Select--</option>
											          <option th:each="oneQuestionnaire : ${listOfQuestionnaire}" th:value="${oneQuestionnaire.id}" th:text="${oneQuestionnaire.name}" />
											        </select>
										        </div>
										        <div class="form-group">
													<label>Select the question</label>
											        <select class="form-control ajaxPopulated" th:field="*{questionId}" data-dependson="qeForQuestion" th:fragment="questionList(listOfQuestions)" th:remove="${ajaxRequest}? tag : none" >
											        	<option value="-1">--Select--</option>
												        <option th:each="oneQuestion : ${listOfQuestions}" th:value="${oneQuestion.id}" th:text="${oneQuestion.title}" />
												    </select>
										        </div>
						                  		
						                  </div><!-- /.tab-pane -->
						                  <div class="tab-pane" th:classappend="${activeTab == T(org.birlasoft.thirdeye.constant.aid.SubBlockTabs).selectParameter ? 'active ' : ''}" id="selectParameter">
						                  	<div class="form-group">
												<label>Please select the questionnaire</label>
												<select class="form-control ajaxSelector" th:field="*{paramQEId}" data-dependency="qeForParameter">
													<option value="-1">--Select--</option>
										          <option th:each="oneQuestionnaire : ${listOfQuestionnaire}" th:value="${oneQuestionnaire.id}" th:text="${oneQuestionnaire.name}" />
										        </select>
									        </div>
									        <div class="form-group" >
												<label>Select the question</label>
										        <select class="form-control ajaxPopulated" th:field="*{paramId}" data-dependson="qeForParameter" th:fragment="parameterList(listOfParameters)" th:remove="${ajaxRequest}? tag : none" >
										        	<option value="-1">--Select--</option>
											        <option th:each="oneParameter : ${listOfParameters}" th:value="${oneParameter.id}" th:text="${oneParameter.displayName}" />
											    </select>
									        </div>
						                  </div><!-- /.tab-pane -->
						                  <div class="tab-pane" th:classappend="${activeTab == T(org.birlasoft.thirdeye.constant.aid.SubBlockTabs).selectColumn ? 'active ' : ''}" id="selectColumn">
						                  	<select class="form-control" th:field="*{templateColumnId}">
									          <option th:each="oneTempmlateColumn : ${listOfTemplateColumns}"  th:value="${oneTempmlateColumn.id}" th:text="${oneTempmlateColumn.assetTemplateColName}" />
									        </select>
						                  </div><!-- /.tab-pane -->
						                </div><!-- /.tab-content -->
						              </div>
								</div>
								
							  <div class="modal-footer">
						        	<a id="submitSubBlockForm" class="btn btn-primary">Update Sub Block</a>
						      </div>
					    </form>
				    </div>
			 </div>
		 </div>
	</div>
	
</body>
</html>