<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:th="http://www.thymeleaf.org"
      th:include="layouts/baseLayout :: baseLayoutContainer (dataController='',dataAction='',pageTitle ='')">
<head>
 <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
</head>
<body>
	<div th:fragment="drillDownModal">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
				 <tr>
	                <th style="width: 10px"><button class="btn btn-default btn-sm" ><i class="fa fa-reply" data-type="go-up" th:attr="data-assetid=${cRParamBean.assetId},data-questionnaireid=${cRParamBean.questionnaireId},data-parameterid=${cRParamBean.parentParameterId},data-parentparameterid=${cRParamBean.parentParentParameterId},data-rootparameterid=${cRParamBean.rootParameterId}" ></i></button></th>&nbsp;&nbsp;&nbsp;
	                <th ><label  th:text="${paramName}"></label></th>
			      </tr>
			       <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				</div>
				<div class="modal-body">
					<div class='gallery with-transitions' data-module="module-bulletChart">
					  <script type="text/x-config" th:inline="javascript">
                        {"height":70, "width":960 , "display": {"entity":"bulletChart", "parameter":{"assetId":[[${cRParamBean.assetId}]],"questionnaireId":[[${cRParamBean.questionnaireId}]],"parameterId":[[${cRParamBean.parameterId}]],"rootparameterId":[[${cRParamBean.rootParameterId}]]}}, "margin" : {"top": 5, "right": 80, "bottom": 20, "left": 300}}
                      </script>
				    </div>
				</div>				
			</div>
		</div>
	</div>
</body>
</html>