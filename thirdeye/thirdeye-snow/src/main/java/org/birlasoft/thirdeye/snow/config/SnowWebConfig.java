/**
 * 
 */
package org.birlasoft.thirdeye.snow.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * @author shaishav.dixit
 *
 */
@EnableWebMvc
@Configuration
@Import({MasterPersistenceConfig.class, PropertyLoader.class})
public class SnowWebConfig extends WebMvcConfigurerAdapter {
	
	
	@Override
	public void configureDefaultServletHandling( DefaultServletHandlerConfigurer configurer ){
		configurer.enable();
	}

}
