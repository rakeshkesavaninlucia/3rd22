/**
 * 
 */
package org.birlasoft.thirdeye.snow.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author shaishav.dixit
 *
 */
@RestController
@RequestMapping("/snow")
public class SnowController {
	
	@RequestMapping( value = "/home", method = RequestMethod.POST )
	public String snowHome() {
		return "hello";
	}

}
