package org.birlasoft.thirdeye.beans;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.birlasoft.thirdeye.beans.bcm.BcmResponseBean;
import org.birlasoft.thirdeye.beans.fm.FunctionalMapDataBean;
import org.birlasoft.thirdeye.beans.parameter.FunctionalCoverageParameterEvaluator;
import org.birlasoft.thirdeye.constant.QuestionnaireType;
import org.birlasoft.thirdeye.entity.Asset;
import org.birlasoft.thirdeye.entity.Bcm;
import org.birlasoft.thirdeye.entity.BcmLevel;
import org.birlasoft.thirdeye.entity.FunctionalMap;
import org.birlasoft.thirdeye.entity.FunctionalMapData;
import org.birlasoft.thirdeye.entity.Parameter;
import org.birlasoft.thirdeye.entity.ParameterConfig;
import org.birlasoft.thirdeye.entity.Questionnaire;
import org.birlasoft.thirdeye.service.BCMLevelService;
import org.birlasoft.thirdeye.service.FunctionalMapService;
import org.birlasoft.thirdeye.service.ParameterService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class FunctionalCoverageParameterEvaluatorTest {

	private List<Parameter> listOfParameters;
	@Mock
	private ParameterService parameterService;
	@Mock
	private FunctionalMapService functionalMapService;
	@Mock
	private BCMLevelService bcmLevelService;
	
	FunctionalCoverageParameterEvaluator fcParameterEvaluator;
	private Parameter dummyParameter;
	private ParameterConfig dummyConfig;
	private FunctionalMap dummyFm;
	private List<FunctionalMapDataBean> fmDataBeans;
	private Set<Integer> levelNumbers;
	private List<BcmLevel> listOfBcmLevel;
	private AssetBean dummyAssetBean;
	private Asset dummyAsset;
	private Questionnaire dummyQuestionnaire;
	Exception ex;
	
	@Before
	public void setup() {
		dummyParameter = new Parameter();
		dummyParameter.setDisplayName("Dummy Parameter");
		dummyParameter.setType("FC");
		
		dummyQuestionnaire = new Questionnaire();
		dummyQuestionnaire.setId(1);
		dummyQuestionnaire.setQuestionnaireType(QuestionnaireType.DEF.name());
		
		dummyConfig = new ParameterConfig();
		dummyConfig.setParameter(dummyParameter);
		dummyConfig.setParameterConfig("{\"functionalMapId\":5,\"functionalMapLevelType\":\"L2\"}");
		
		dummyFm = new FunctionalMap();
		dummyFm.setId(5);
		dummyFm.setType("L4");
		
		levelNumbers = new HashSet<Integer>();
		levelNumbers.add(1);
		levelNumbers.add(2);
		levelNumbers.add(3);
		levelNumbers.add(4);
		
		setupAsset();
		setupBcmLevelBean();
		//setupFmDataBean();
		
		Bcm dummyBcm = new Bcm();
		dummyBcm.setId(20);
		dummyBcm.setBcmLevels(new HashSet<BcmLevel>(listOfBcmLevel));
		
		dummyFm.setBcm(dummyBcm);
		
		//create dummy data for fmDataBeans and listOfBcmLevel
		listOfParameters = new ArrayList<>();
		listOfParameters.add(dummyParameter);
		
		fcParameterEvaluator = new FunctionalCoverageParameterEvaluator(listOfParameters, parameterService, functionalMapService, bcmLevelService, dummyQuestionnaire);
	}
	
	private void setupBcmLevelBean() {
		listOfBcmLevel = new ArrayList<>();
		
		BcmLevel bcmLevel = new BcmLevel();
		bcmLevel.setBcmLevelName("L One");
		bcmLevel.setId(328);
		bcmLevel.setLevelNumber(1);
		bcmLevel.setSequenceNumber(1);
		bcmLevel.setCategory("");
		listOfBcmLevel.add(bcmLevel);
		
		BcmLevel bcmLevel1 = new BcmLevel();
		bcmLevel1.setBcmLevelName("L Two 1");
		bcmLevel1.setId(329);
		bcmLevel1.setLevelNumber(2);
		bcmLevel1.setSequenceNumber(1);
		bcmLevel1.setCategory("C1");
		bcmLevel1.setBcmLevel(bcmLevel);
		listOfBcmLevel.add(bcmLevel1);
		
		BcmLevel bcmLevel2 = new BcmLevel();
		bcmLevel2.setBcmLevelName("L Three 1");
		bcmLevel2.setId(330);
		bcmLevel2.setLevelNumber(3);
		bcmLevel2.setSequenceNumber(1);
		bcmLevel2.setCategory("");
		bcmLevel2.setBcmLevel(bcmLevel1);
		listOfBcmLevel.add(bcmLevel2);
		
		BcmLevel bcmLevel3 = new BcmLevel();
		bcmLevel3.setBcmLevelName("L Four 1");
		bcmLevel3.setId(331);
		bcmLevel3.setLevelNumber(4);
		bcmLevel3.setSequenceNumber(1);
		bcmLevel3.setCategory("");
		bcmLevel3.setBcmLevel(bcmLevel2);
		listOfBcmLevel.add(bcmLevel3);
		
		BcmLevel bcmLevel4 = new BcmLevel();
		bcmLevel4.setBcmLevelName("L Four 2");
		bcmLevel4.setId(332);
		bcmLevel4.setLevelNumber(4);
		bcmLevel4.setSequenceNumber(1);
		bcmLevel4.setCategory("");
		bcmLevel4.setBcmLevel(bcmLevel2);
		listOfBcmLevel.add(bcmLevel4);
		
		BcmLevel bcmLevel5 = new BcmLevel();
		bcmLevel5.setBcmLevelName("L Four 3");
		bcmLevel5.setId(333);
		bcmLevel5.setLevelNumber(4);
		bcmLevel5.setSequenceNumber(1);
		bcmLevel5.setCategory("");
		bcmLevel5.setBcmLevel(bcmLevel2);
		listOfBcmLevel.add(bcmLevel5);
		
		BcmLevel bcmLevel6 = new BcmLevel();
		bcmLevel6.setBcmLevelName("L Three 2");
		bcmLevel6.setId(334);
		bcmLevel6.setLevelNumber(3);
		bcmLevel6.setSequenceNumber(1);
		bcmLevel6.setCategory("");
		bcmLevel6.setBcmLevel(bcmLevel1);
		listOfBcmLevel.add(bcmLevel6);
		
		BcmLevel bcmLevel7 = new BcmLevel();
		bcmLevel7.setBcmLevelName("L Four 4");
		bcmLevel7.setId(335);
		bcmLevel7.setLevelNumber(4);
		bcmLevel7.setSequenceNumber(1);
		bcmLevel7.setCategory("");
		bcmLevel7.setBcmLevel(bcmLevel6);
		listOfBcmLevel.add(bcmLevel7);
		
		BcmLevel bcmLevel8 = new BcmLevel();
		bcmLevel8.setBcmLevelName("L Four 5");
		bcmLevel8.setId(336);
		bcmLevel8.setLevelNumber(4);
		bcmLevel8.setSequenceNumber(1);
		bcmLevel8.setCategory("");
		bcmLevel8.setBcmLevel(bcmLevel6);
		listOfBcmLevel.add(bcmLevel8);
		
		BcmLevel bcmLevel9 = new BcmLevel();
		bcmLevel9.setBcmLevelName("L Four 6");
		bcmLevel9.setId(337);
		bcmLevel9.setLevelNumber(4);
		bcmLevel9.setSequenceNumber(1);
		bcmLevel9.setCategory("");
		bcmLevel9.setBcmLevel(bcmLevel6);
		listOfBcmLevel.add(bcmLevel9);
		
		BcmLevel bcmLevel10 = new BcmLevel();
		bcmLevel10.setBcmLevelName("L Four 7");
		bcmLevel10.setId(338);
		bcmLevel10.setLevelNumber(4);
		bcmLevel10.setSequenceNumber(1);
		bcmLevel10.setCategory("");
		bcmLevel10.setBcmLevel(bcmLevel6);
		listOfBcmLevel.add(bcmLevel10);
		
		BcmLevel bcmLevel11 = new BcmLevel();
		bcmLevel11.setBcmLevelName("L Four 8");
		bcmLevel11.setId(339);
		bcmLevel11.setLevelNumber(4);
		bcmLevel11.setSequenceNumber(1);
		bcmLevel11.setCategory("");
		bcmLevel11.setBcmLevel(bcmLevel6);
		listOfBcmLevel.add(bcmLevel11);
		
		BcmLevel bcmLevel12 = new BcmLevel();
		bcmLevel12.setBcmLevelName("L Three 3");
		bcmLevel12.setId(340);
		bcmLevel12.setLevelNumber(3);
		bcmLevel12.setSequenceNumber(1);
		bcmLevel12.setCategory("");
		bcmLevel12.setBcmLevel(bcmLevel1);
		listOfBcmLevel.add(bcmLevel12);
		
		BcmLevel bcmLevel13 = new BcmLevel();
		bcmLevel13.setBcmLevelName("L Four 9");
		bcmLevel13.setId(341);
		bcmLevel13.setLevelNumber(4);
		bcmLevel13.setSequenceNumber(1);
		bcmLevel13.setCategory("");
		bcmLevel13.setBcmLevel(bcmLevel12);
		listOfBcmLevel.add(bcmLevel13);
		
		BcmLevel bcmLevel14 = new BcmLevel();
		bcmLevel14.setBcmLevelName("L Four 10");
		bcmLevel14.setId(342);
		bcmLevel14.setLevelNumber(4);
		bcmLevel14.setSequenceNumber(1);
		bcmLevel14.setCategory("");
		bcmLevel14.setBcmLevel(bcmLevel12);
		listOfBcmLevel.add(bcmLevel14);
		
		BcmLevel bcmLevel15 = new BcmLevel();
		bcmLevel15.setBcmLevelName("L Three 4");
		bcmLevel15.setId(343);
		bcmLevel15.setLevelNumber(3);
		bcmLevel15.setSequenceNumber(1);
		bcmLevel15.setCategory("");
		bcmLevel15.setBcmLevel(bcmLevel1);
		listOfBcmLevel.add(bcmLevel15);
		
		BcmLevel bcmLevel16 = new BcmLevel();
		bcmLevel16.setBcmLevelName("L Four 11");
		bcmLevel16.setId(344);
		bcmLevel16.setLevelNumber(4);
		bcmLevel16.setSequenceNumber(1);
		bcmLevel16.setCategory("");
		bcmLevel16.setBcmLevel(bcmLevel15);
		listOfBcmLevel.add(bcmLevel16);
		
		BcmLevel bcmLevel17 = new BcmLevel();
		bcmLevel17.setBcmLevelName("L Four 12");
		bcmLevel17.setId(345);
		bcmLevel17.setLevelNumber(4);
		bcmLevel17.setSequenceNumber(1);
		bcmLevel17.setCategory("");
		bcmLevel17.setBcmLevel(bcmLevel15);
		listOfBcmLevel.add(bcmLevel17);
		
		BcmLevel bcmLevel18 = new BcmLevel();
		bcmLevel18.setBcmLevelName("L Three 13");
		bcmLevel18.setId(346);
		bcmLevel18.setLevelNumber(4);
		bcmLevel18.setSequenceNumber(1);
		bcmLevel18.setCategory("");
		bcmLevel18.setBcmLevel(bcmLevel15);
		listOfBcmLevel.add(bcmLevel18);
		
		fmDataBeans = new ArrayList<>();
		
		FunctionalMapData functionalMapData = new FunctionalMapData();
		functionalMapData.setId(84);
		functionalMapData.setAsset(dummyAsset);
		functionalMapData.setData("{\"options\":[{\"sequenceNumber\":2,\"quantifier\":7.5,\"text\":\"Partially Supported\"}],\"otherOptionResponseText\":null,\"score\":null}");
		functionalMapData.setBcmLevel(bcmLevel3);
		fmDataBeans.add(new FunctionalMapDataBean(functionalMapData));
		
		FunctionalMapData functionalMapData1 = new FunctionalMapData();
		functionalMapData1.setId(85);
		functionalMapData1.setAsset(dummyAsset);
		functionalMapData1.setData("{\"options\":[{\"sequenceNumber\":2,\"quantifier\":7.5,\"text\":\"Partially Supported\"}],\"otherOptionResponseText\":null,\"score\":null}");
		functionalMapData1.setBcmLevel(bcmLevel4);
		fmDataBeans.add(new FunctionalMapDataBean(functionalMapData1));
		
		FunctionalMapData functionalMapData2 = new FunctionalMapData();
		functionalMapData2.setId(86);
		functionalMapData2.setAsset(dummyAsset);
		functionalMapData2.setData("{\"options\":[{\"sequenceNumber\":4,\"quantifier\":1.0,\"text\":\"Desired\"}],\"otherOptionResponseText\":null,\"score\":null}");
		functionalMapData2.setBcmLevel(bcmLevel5);
		fmDataBeans.add(new FunctionalMapDataBean(functionalMapData2));
		
		FunctionalMapData functionalMapData3 = new FunctionalMapData();
		functionalMapData3.setId(87);
		functionalMapData3.setAsset(dummyAsset);
		functionalMapData3.setData("{\"options\":[{\"sequenceNumber\":3,\"quantifier\":5.0,\"text\":\"Sparsely Supported\"}],\"otherOptionResponseText\":null,\"score\":null}");
		functionalMapData3.setBcmLevel(bcmLevel13);
		fmDataBeans.add(new FunctionalMapDataBean(functionalMapData3));
		
		FunctionalMapData functionalMapData4 = new FunctionalMapData();
		functionalMapData4.setId(88);
		functionalMapData4.setAsset(dummyAsset);
		functionalMapData4.setData("{\"options\":[{\"sequenceNumber\":1,\"quantifier\":9.0,\"text\":\"Fully Supported\"}],\"otherOptionResponseText\":null,\"score\":null}");
		functionalMapData4.setBcmLevel(bcmLevel14);
		fmDataBeans.add(new FunctionalMapDataBean(functionalMapData4));
		
		FunctionalMapData functionalMapData5 = new FunctionalMapData();
		functionalMapData5.setId(89);
		functionalMapData5.setAsset(dummyAsset);
		functionalMapData5.setData("{\"options\":[{\"sequenceNumber\":3,\"quantifier\":5.0,\"text\":\"Sparsely Supported\"}],\"otherOptionResponseText\":null,\"score\":null}");
		functionalMapData5.setBcmLevel(bcmLevel16);
		fmDataBeans.add(new FunctionalMapDataBean(functionalMapData5));
		
		FunctionalMapData functionalMapData6 = new FunctionalMapData();
		functionalMapData6.setId(84);
		functionalMapData6.setAsset(dummyAsset);
		functionalMapData6.setData("{\"options\":[{\"sequenceNumber\":2,\"quantifier\":7.5,\"text\":\"Partially Supported\"}],\"otherOptionResponseText\":null,\"score\":null}");
		functionalMapData6.setBcmLevel(bcmLevel17);
		fmDataBeans.add(new FunctionalMapDataBean(functionalMapData6));
		
		FunctionalMapData functionalMapData7 = new FunctionalMapData();
		functionalMapData7.setId(84);
		functionalMapData7.setAsset(dummyAsset);
		functionalMapData7.setData("{\"options\":[{\"sequenceNumber\":2,\"quantifier\":7.5,\"text\":\"Partially Supported\"}],\"otherOptionResponseText\":null,\"score\":null}");
		functionalMapData7.setBcmLevel(bcmLevel18);
		fmDataBeans.add(new FunctionalMapDataBean(functionalMapData7));
		
	}

	private void setupAsset() {
		dummyAsset = new Asset();
		dummyAsset.setId(100);
		dummyAssetBean = new AssetBean();
		List<TemplateColumnBean> assetDatas = new ArrayList<TemplateColumnBean>();
		TemplateColumnBean tcb = new TemplateColumnBean();
		tcb.setData("Dummy Data");
		tcb.setName("Test Name");
		tcb.setSequenceNumber(1);
		assetDatas.add(tcb);
		dummyAssetBean.setAssetDatas(assetDatas);
		dummyAssetBean.setShortName("Test");
		
	}
	
	private List<BcmLevelBean> getBcmLevelBean() {
		Map<Integer, BcmLevelBean> levelMap = new HashMap<>();
		// Load the BCM level into the hashmap for quick access
		for (BcmLevel oneBcmLevel : listOfBcmLevel) {
			levelMap.put(oneBcmLevel.getId(), new BcmLevelBean(oneBcmLevel));
		}

		// Now create tree properly with the beans
		for (BcmLevel oneBcmLevel : listOfBcmLevel) {
			if (oneBcmLevel.getBcmLevel() != null) {
				// Link up the parameter bean to the parent
				BcmLevelBean parent = levelMap.get(oneBcmLevel.getBcmLevel().getId());
				BcmLevelBean child = levelMap.get(oneBcmLevel.getId());

				if (parent != null) {
					parent.addChildLevel(child);
					child.setParent(parent);
				}
			}
		}

		// Find the BCM root levels
		List<BcmLevelBean> listToBeReturned = new ArrayList<>();
		for (BcmLevelBean lb : levelMap.values()) {
			// This means that it is a root of the tree
			if (lb.getParent() == null) {
				listToBeReturned.add(lb);
			}
		}

		return listToBeReturned;
	}

	private void mockServiceClassMethods() {
		Mockito.when(parameterService.findParameterConfigByParameter(dummyParameter)).thenReturn(dummyConfig);
		Mockito.when(functionalMapService.findOne(5, true)).thenReturn(dummyFm);
		Mockito.when(functionalMapService.fetchFunctionalMapData(dummyFm, dummyQuestionnaire)).thenReturn(fmDataBeans);
		Mockito.when(bcmLevelService.findByBcmAndLevelNumberIn(dummyFm.getBcm(), levelNumbers, false)).thenReturn(listOfBcmLevel);
		Mockito.when(bcmLevelService.generateLevelTreeFromBcmLevels(listOfBcmLevel)).thenReturn(getBcmLevelBean());
	}
	
	@Test
	public void testEvaluateParameter() {
		mockServiceClassMethods();
		
		FunctionalCoverageParameterEvaluator fcParameterEvaluator = new FunctionalCoverageParameterEvaluator(listOfParameters, parameterService, functionalMapService, bcmLevelService, dummyQuestionnaire);
		List<AssetBean> assetBeanList = new ArrayList<AssetBean>();
		assetBeanList.add(new AssetBean(dummyAsset));
		List<ParameterEvaluationResponse> listOfResponse = fcParameterEvaluator.evaluateParameter(assetBeanList);
		ParameterEvaluationResponse per = listOfResponse.get(0);
		per.fetchParamValueForAsset(new AssetBean(dummyAsset));
		Assert.assertEquals(6.33, per.fetchParamValueForAsset(new AssetBean(dummyAsset)).doubleValue(), 0.01);
	}
	
	@Test
	public void testEvaluateParameterForNonFCParameter() {
		mockServiceClassMethods();
		
		dummyParameter.setType("AP");
		
		FunctionalCoverageParameterEvaluator fcParameterEvaluator = new FunctionalCoverageParameterEvaluator(listOfParameters, parameterService, functionalMapService, bcmLevelService, dummyQuestionnaire);
		List<AssetBean> assetBeanList = new ArrayList<AssetBean>();
		assetBeanList.add(new AssetBean(dummyAsset));
		List<ParameterEvaluationResponse> listOfResponse = fcParameterEvaluator.evaluateParameter(assetBeanList);
		
		Assert.assertEquals(true, listOfResponse.isEmpty());
	}

	@Test
	public void testEvaluateParameterDoesNotThrowExceptionWithEmptyAssetList() {
		mockServiceClassMethods();
		
		dummyConfig.setParameterConfig("{\"functionalMapId\":5,\"functionalMapLevelType\":\"L3\"}");
		
		FunctionalCoverageParameterEvaluator fcParameterEvaluator = new FunctionalCoverageParameterEvaluator(listOfParameters, parameterService, functionalMapService, bcmLevelService, dummyQuestionnaire);
		List<AssetBean> assetBeanList = new ArrayList<AssetBean>();
		try {
			fcParameterEvaluator.evaluateParameter(assetBeanList);
		} catch (Exception e) {
			ex = e;
		}
		Assert.assertEquals(null, ex);
	}
	
	@Test
	public void testEvaluateBcmLevel() {
		mockServiceClassMethods();
		
		dummyConfig.setParameterConfig("{\"functionalMapId\":5,\"functionalMapLevelType\":\"L3\"}");
		
		FunctionalCoverageParameterEvaluator fcParameterEvaluator = new FunctionalCoverageParameterEvaluator(listOfParameters, parameterService, functionalMapService, bcmLevelService, dummyQuestionnaire);
		List<AssetBean> assetBeanList = new ArrayList<AssetBean>();
		assetBeanList.add(new AssetBean(dummyAsset));
		List<BcmResponseBean> listOfResponse = fcParameterEvaluator.evaluateBcmLevel(assetBeanList, dummyParameter);
		Assert.assertEquals(4, listOfResponse.size());
	}
	
	@Test
	public void testEvaluateBcmLevelWithEmptyAssetList() {
		mockServiceClassMethods();
		
		dummyConfig.setParameterConfig("{\"functionalMapId\":5,\"functionalMapLevelType\":\"L3\"}");
		
		FunctionalCoverageParameterEvaluator fcParameterEvaluator = new FunctionalCoverageParameterEvaluator(listOfParameters, parameterService, functionalMapService, bcmLevelService, dummyQuestionnaire);
		List<AssetBean> assetBeanList = new ArrayList<AssetBean>();
		List<BcmResponseBean> listOfResponse = fcParameterEvaluator.evaluateBcmLevel(assetBeanList, dummyParameter);
		Assert.assertEquals(0, listOfResponse.size());
	}
	
	@Test
	public void testEvaluateBcmLevelDoesNotThrowExceptionWithEmptyAssetList() {
		mockServiceClassMethods();
		
		dummyConfig.setParameterConfig("{\"functionalMapId\":5,\"functionalMapLevelType\":\"L3\"}");
		
		FunctionalCoverageParameterEvaluator fcParameterEvaluator = new FunctionalCoverageParameterEvaluator(listOfParameters, parameterService, functionalMapService, bcmLevelService, dummyQuestionnaire);
		List<AssetBean> assetBeanList = new ArrayList<AssetBean>();
		
		try {
			fcParameterEvaluator.evaluateBcmLevel(assetBeanList, dummyParameter);
		} catch (Exception e) {
			ex = e;
		}
		
		Assert.assertEquals(null, ex);
	}
	
	@Test(expected = NullPointerException.class)
	public void testEvaluateBcmLevelDoesNotThrowExceptionWithNullAssetList() {
		mockServiceClassMethods();
		
		dummyConfig.setParameterConfig("{\"functionalMapId\":5,\"functionalMapLevelType\":\"L3\"}");
		
		FunctionalCoverageParameterEvaluator fcParameterEvaluator = new FunctionalCoverageParameterEvaluator(listOfParameters, parameterService, functionalMapService, bcmLevelService, dummyQuestionnaire);
		List<AssetBean> assetBeanList = null;
		
		fcParameterEvaluator.evaluateBcmLevel(assetBeanList, dummyParameter);
	}
	
}
