package org.birlasoft.thirdeye.service.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.birlasoft.thirdeye.beans.AssetBean;
import org.birlasoft.thirdeye.beans.ParameterBean;
import org.birlasoft.thirdeye.beans.QuestionBean;
import org.birlasoft.thirdeye.beans.TemplateColumnBean;
import org.birlasoft.thirdeye.beans.tco.ChartOfAccountBean;
import org.birlasoft.thirdeye.entity.Asset;
import org.birlasoft.thirdeye.entity.Parameter;
import org.birlasoft.thirdeye.entity.Question;
import org.birlasoft.thirdeye.entity.Questionnaire;
import org.birlasoft.thirdeye.entity.QuestionnaireAsset;
import org.birlasoft.thirdeye.entity.QuestionnaireParameter;
import org.birlasoft.thirdeye.entity.QuestionnaireQuestion;
import org.birlasoft.thirdeye.entity.ResponseData;
import org.birlasoft.thirdeye.service.ParameterService;
import org.birlasoft.thirdeye.service.QuestionnaireAssetService;
import org.birlasoft.thirdeye.service.QuestionnaireParameterService;
import org.birlasoft.thirdeye.service.QuestionnaireQuestionService;
import org.birlasoft.thirdeye.service.TcoResponseService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import static org.mockito.Mockito.*;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class TcoResponseServiceImplTest {
	
	@Mock
	private QuestionnaireAssetService questionnaireAssetService;
	@Mock
	private QuestionnaireQuestionService questionnaireQuestionService;
	@Mock
	private QuestionnaireParameterService questionnaireParameterService;
	@Mock
	private ParameterService parameterService;
	@InjectMocks
	private TcoResponseService tcoResponseService = new TcoResponseServiceImpl();
	
	private List<QuestionnaireParameter> listOfQp = new ArrayList<>();
	private Questionnaire questionnaire;
	private AssetBean dummyAssetBean;
	private Asset dummyAsset;
	private QuestionnaireAsset questionnaireAsset;
	private Parameter dummyParameter;
	private ParameterBean dummyParameterBean;
	private Set<QuestionnaireAsset> setOfQa = new HashSet<>();
	private QuestionBean questionBean1;
	private QuestionBean questionBean2;
	private QuestionnaireQuestion qq1;
	private QuestionnaireQuestion qq2;
	
	@Before
	public void setup(){
		questionnaire = new Questionnaire();
		questionnaire.setId(21);
		questionnaire.setName("Chart of account");
		
		setupAsset();
		
		questionnaireAsset = new QuestionnaireAsset();
		questionnaireAsset.setId(21);
		questionnaireAsset.setQuestionnaire(questionnaire);
		questionnaireAsset.setAsset(dummyAsset);
		setOfQa.add(questionnaireAsset);
		
		QuestionnaireParameter questionnaireParameter = new QuestionnaireParameter();
		questionnaireParameter.setId(21);
		questionnaireParameter.setQuestionnaire(questionnaire);
		
		dummyParameter = new Parameter();
		dummyParameter.setId(21);
		questionnaireParameter.setParameterByParameterId(dummyParameter);
		
		listOfQp.add(questionnaireParameter);
		
		Question question1 = new Question();
		question1.setId(22);
		question1.setQuestionMode("FIXED");
		
		questionBean1 = new QuestionBean(question1);
		questionBean1.setType("NUMBER");
		
		
		Question question2 = new Question();
		question2.setId(21);
		question2.setQuestionMode("FIXED");
		
		questionBean2 = new QuestionBean(question2);
		questionBean2.setType("NUMBER");
		
		ParameterBean childParamBean = new ParameterBean();
		childParamBean.setDisplayName("TCO LP");
		childParamBean.setName("TCO LP");
		childParamBean.setParamType("LP");
		childParamBean.addChildQuestion(questionBean1);
		childParamBean.addChildQuestion(questionBean2);
		
		dummyParameterBean = new ParameterBean();
		dummyParameterBean.setDisplayName("TCO AP");
		dummyParameterBean.setName("TCO AP");
		dummyParameterBean.setParamType("AP");
		dummyParameterBean.addChildParameter(childParamBean);
		
		questionBean1.setParentParameter(childParamBean);
		questionBean2.setParentParameter(childParamBean);
		
		qq1 = new QuestionnaireQuestion();
		qq1.setId(21);
		qq1.setQuestionnaire(questionnaire);
		qq1.setQuestion(question1);
		qq1.setQuestionnaireParameter(questionnaireParameter);
		qq1.setQuestionnaireAsset(questionnaireAsset);
		qq1.setSequenceNumber(1);
		qq1.setResponseDatas(new HashSet<ResponseData>());
		
		qq2 = new QuestionnaireQuestion();
		qq2.setId(22);
		qq2.setQuestionnaire(questionnaire);
		qq2.setQuestion(question2);
		qq2.setQuestionnaireParameter(questionnaireParameter);
		qq2.setQuestionnaireAsset(questionnaireAsset);
		qq2.setSequenceNumber(2);
		qq2.setResponseDatas(new HashSet<ResponseData>());
		
	}
	
	private void setupAsset() {
		dummyAsset = new Asset();
		dummyAsset.setId(100);
		dummyAssetBean = new AssetBean();
		List<TemplateColumnBean> assetDatas = new ArrayList<TemplateColumnBean>();
		TemplateColumnBean tcb = new TemplateColumnBean();
		tcb.setData("Dummy Data");
		tcb.setName("Test Name");
		tcb.setSequenceNumber(1);
		assetDatas.add(tcb);
		dummyAssetBean.setAssetDatas(assetDatas);
		dummyAssetBean.setShortName("Test");
	}
	
	private void mockServiceClassMethods() {
		when(questionnaireParameterService.getListOfRootQuestionnaireParameters(questionnaire)).thenReturn(listOfQp);
		when(parameterService.findFullyLoaded(21)).thenReturn(dummyParameter);
		when(parameterService.generateParameterTree(dummyParameter)).thenReturn(dummyParameterBean);
		when(questionnaireAssetService.findByQuestionnaireLoadedAsset(questionnaire)).thenReturn(setOfQa);
		when(questionnaireParameterService.findByQuestionnaireAndParameter(questionnaireAsset.getQuestionnaire(), 
				parameterService.findOne(questionBean1.getParentParameter().getId()))).thenReturn(listOfQp);
		when(questionnaireParameterService.findByQuestionnaireAndParameter(questionnaireAsset.getQuestionnaire(), 
				parameterService.findOne(questionBean2.getParentParameter().getId()))).thenReturn(listOfQp);
		when(questionnaireQuestionService.getQqForQuestion(questionBean1, questionnaireAsset, listOfQp)).thenReturn(qq1);
		when(questionnaireQuestionService.getQqForQuestion(questionBean2, questionnaireAsset, listOfQp)).thenReturn(qq2);
	}
	
	@Test
	public void testExtractChartOfAccounts(){
		mockServiceClassMethods();
		
		List<ChartOfAccountBean> chartOfAccountBeans = tcoResponseService.extractChartOfAccounts(questionnaire);
		Assert.assertEquals(1, chartOfAccountBeans.size());
	}
	
	@Test
	public void testNumberOfCallsToGenerateParameterTree(){
		mockServiceClassMethods();
		
		tcoResponseService.extractChartOfAccounts(questionnaire);
		verify(parameterService, times(1)).generateParameterTree(dummyParameter);
	}
	
	@Test
	public void testAssetInChartOfAccountBean(){
		mockServiceClassMethods();
		
		List<ChartOfAccountBean> chartOfAccountBeans = tcoResponseService.extractChartOfAccounts(questionnaire);
		Assert.assertEquals(new AssetBean(dummyAsset), chartOfAccountBeans.get(0).getAsset());
	}
	
	@Test
	public void testNumberOfQqInChartOfAccounts(){
		mockServiceClassMethods();
		
		List<ChartOfAccountBean> chartOfAccountBeans = tcoResponseService.extractChartOfAccounts(questionnaire);
		Assert.assertEquals(2, chartOfAccountBeans.get(0).getListOfCostStructure().get(0).getChildCostStructure().get(0).getChildCostElement().size());
	}

}
