package org.birlasoft.thirdeye.beans.aid;

public class ApplicationBean {

	private String applicationName;
	
	public ApplicationBean() {
		//default constructor
	}

	public ApplicationBean(String applicationName) {
		super();
		this.applicationName = applicationName;
	}

	public String getApplicationName() {
		return applicationName;
	}

	public void setApplicationName(String applicationName) {
		this.applicationName = applicationName;
	}
	
	
}
