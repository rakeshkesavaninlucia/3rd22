package org.birlasoft.thirdeye.beans.aid;

import java.util.List;

/**
 * Wrapper for Aid report.
 * @author samar.gupta
 *
 */
public class AidSearchWrapper {
	
	private AidSearchCentralAssetWrapperBean centralAsset;
	private List<RelatedAssetSearchBean> listOfInboundAssets;
	private List<RelatedAssetSearchBean> listOfOutboundAssets;
	
	public AidSearchCentralAssetWrapperBean getCentralAsset() {
		return centralAsset;
	}
	public void setCentralAsset(AidSearchCentralAssetWrapperBean centralAsset) {
		this.centralAsset = centralAsset;
	}
	public List<RelatedAssetSearchBean> getListOfInboundAssets() {
		return listOfInboundAssets;
	}
	public void setListOfInboundAssets(
			List<RelatedAssetSearchBean> listOfInboundAssets) {
		this.listOfInboundAssets = listOfInboundAssets;
	}
	public List<RelatedAssetSearchBean> getListOfOutboundAssets() {
		return listOfOutboundAssets;
	}
	public void setListOfOutboundAssets(
			List<RelatedAssetSearchBean> listOfOutboundAssets) {
		this.listOfOutboundAssets = listOfOutboundAssets;
	}
}
