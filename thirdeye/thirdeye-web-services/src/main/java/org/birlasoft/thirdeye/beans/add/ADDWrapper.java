package org.birlasoft.thirdeye.beans.add;

import java.util.List;

/**
 * bean for ADD
 */
public class ADDWrapper  {
	
	private String name;
	private String type;
	private Integer id;
	private List<AddDependsOnBean> depends;
	private List<String> dependedOnBy;
	private String imageUrl;
	
	public List<AddDependsOnBean> getDepends() {
		return depends;
	}
	
	public void setDepends(List<AddDependsOnBean> depends) {
		this.depends = depends;
	}
	
	public List<String> getDependedOnBy() {
		return dependedOnBy;
	}
	
	public void setDependedOnBy(List<String> dependedOnBy) {
		this.dependedOnBy = dependedOnBy;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer integer) {
		this.id = integer;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ADDWrapper other = (ADDWrapper) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
	@Override
	public int hashCode() {
		final int prime = 21;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
}
