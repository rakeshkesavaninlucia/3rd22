package org.birlasoft.thirdeye.service;

import java.util.List;

public interface IncrementIndexService {
	
public void buildIncrementAssetIndex(List<Integer> assetIdlst , String tenantId );
	
	public void updateAssetParameter(Integer qusId) ;
	
	public void updateQustionnaireQues( Integer qusId);
	
	public void updateAssetRelation(List<Integer> assetIdlst, String tenantId) ;
	
	public void updateAssetAid( Integer templateId , String tenantId) ;
	
	public void updateAssetTemplate(Integer templateId, String tenantId) ;

	public void deleteAsset(Integer assetId , String tenantId) ;
	
	public void buildIncrementBCMIndex(Integer bcmId, String tenantId) ;
	
	public void updateAssetCostStucture(List<Integer> assetIdlst, String tenantId);
	
	public void updateAssetQuestionnaire(List<Integer> assetIdlst, String tenantId) ;
	
}
