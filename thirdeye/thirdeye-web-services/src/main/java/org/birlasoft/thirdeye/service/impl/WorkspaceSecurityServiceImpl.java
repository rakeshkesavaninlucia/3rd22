package org.birlasoft.thirdeye.service.impl;

import org.birlasoft.thirdeye.beans.fm.FunctionalMapBean;
import org.birlasoft.thirdeye.config.TransactionManagers;
import org.birlasoft.thirdeye.entity.Aid;
import org.birlasoft.thirdeye.entity.AssetTemplate;
import org.birlasoft.thirdeye.entity.Bcm;
import org.birlasoft.thirdeye.entity.Dashboard;
import org.birlasoft.thirdeye.entity.Graph;
import org.birlasoft.thirdeye.entity.Parameter;
import org.birlasoft.thirdeye.entity.QualityGate;
import org.birlasoft.thirdeye.entity.Question;
import org.birlasoft.thirdeye.entity.Questionnaire;
import org.birlasoft.thirdeye.entity.RelationshipType;
import org.birlasoft.thirdeye.entity.Workspace;
import org.birlasoft.thirdeye.service.CustomUserDetailsService;
import org.birlasoft.thirdeye.service.RelationshipTypeService;
import org.birlasoft.thirdeye.service.WorkspaceSecurityService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

/**
 * 
 * WorkspaceSecurityServiceImpl.java - Service Implementation class for Workspace Security
 * 
 */
@Service
@Transactional(value=TransactionManagers.TENANTTRANSACTIONMANAGER)
public class WorkspaceSecurityServiceImpl implements WorkspaceSecurityService {
	
	private static Logger logger = LoggerFactory.getLogger(WorkspaceSecurityServiceImpl.class);
	
	@Autowired
	private CustomUserDetailsService customUserDetailsService;
	@Autowired
	private RelationshipTypeService relationshipTypeService;
	
	private static final String WITH_USER = " with user "; 

	@Override
	public boolean checkAuthForTemplate(AssetTemplate oneTemplate, Workspace workspace) {
		Assert.notNull(oneTemplate, "Can not validate against a null asset template");
		
		if (workspace != null){
			return oneTemplate.getWorkspace().getId().equals(workspace.getId());
		}
		
		return false;
	}
	
	@Override
	public boolean checkAuthForQuestionnaire(Questionnaire qe,
			Workspace workspace) {
		Assert.notNull(qe, "Can not validate against a null questionnaire");

		if (workspace != null){
			return qe.getWorkspace().getId().equals(workspace.getId());
		}

		return false;
	}

	@Override
	public boolean checkAuthForQuestion(Question question, Workspace workspace) {
		Assert.notNull(question, "Can not validate against a null question");
		
		if(workspace != null && question.getWorkspace() != null){
			return question.getWorkspace().getId().equals(workspace.getId());
		}
		return true;
	}

	@Override
	public boolean checkAuthForGraph(Graph graph, Workspace workspace) {
		Assert.notNull(graph, "Can not validate against a null graph");

		if (workspace != null){
			return graph.getWorkspace().getId().equals(workspace.getId());
		}

		return false;
	}
	
	@Override
	public boolean checkAuthForAID(Aid aid, Workspace workspace) {
		Assert.notNull(aid, "Can not validate against a null Aid");

		if (workspace != null){
			return aid.getWorkspace().getId().equals(workspace.getId());
		}

		return false;
	}
	
	@Override
	public boolean checkAuthForParameter(Parameter parameter, Workspace workspace) {
		Assert.notNull(parameter, "Can not validate against a null parameter");

		if (workspace != null && parameter.getWorkspace() != null){
			return parameter.getWorkspace().getId().equals(workspace.getId());
		}

		return true;
	}
	
	private boolean checkAuthForRelationshipType(RelationshipType rType, Workspace workspace) {
		Assert.notNull(rType, "Can not validate against a null relationship suggestion");
		
		if(workspace != null && rType.getWorkspace() != null){
			return rType.getWorkspace().getId().equals(workspace.getId());
		}
		return true;
	}

	@Override
	public void authorizeGraphAccess(Graph graph) {
		if (!checkAuthForGraph(graph, customUserDetailsService.getActiveWorkSpaceForUser())){
			// You need to give an access denied
			logger.info("Blocking user access to the graph id " + graph.getId() + WITH_USER + customUserDetailsService.getCurrentUser().getEmailAddress());
			throw new AccessDeniedException("User is not allowed to access this graph");
		}
	}
	
	@Override
	public void authorizeAidAccess(Aid aid) {
		if (!checkAuthForAID(aid, customUserDetailsService.getActiveWorkSpaceForUser())){
			// You need to give an access denied
			logger.info("Blocking user access to the Aid id " + aid.getId() + WITH_USER + customUserDetailsService.getCurrentUser().getEmailAddress());
			throw new AccessDeniedException("User is not allowed to access this Aid");
		}
	}

	@Override
	public void authorizeQuestionnaireAccess(Questionnaire questionnaire) {
		if (!checkAuthForQuestionnaire(questionnaire, customUserDetailsService.getActiveWorkSpaceForUser())){
			// You need to give an access denied
			throw new AccessDeniedException("User is not allowed to access this questionnaire");
		}
	}

	@Override
	public void authorizeQuestionAccess(Question question) {
		if (!checkAuthForQuestion(question, customUserDetailsService.getActiveWorkSpaceForUser())){
			// You need to give an access denied
			logger.info("Blocking user access to the question id " + question.getId() + WITH_USER + customUserDetailsService.getCurrentUser().getEmailAddress());
			throw new AccessDeniedException("User is not allowed to access this question");
		}
	}

	@Override
	public void authorizeParameterAccess(Parameter parameter) {
		if (!checkAuthForParameter(parameter, customUserDetailsService.getActiveWorkSpaceForUser())){
			// You need to give an access denied
			logger.info("Blocking user access to the parameter id " + parameter.getId() + WITH_USER + customUserDetailsService.getCurrentUser().getEmailAddress());
			throw new AccessDeniedException("User is not allowed to access this parameter");
		}
	}

	@Override
	public void authorizeTemplateAccess(AssetTemplate assetTemplate) {
		if (!checkAuthForTemplate(assetTemplate, customUserDetailsService.getActiveWorkSpaceForUser())){
			// You need to give an access denied
			logger.info("Blocking user access to the template id " + assetTemplate.getId() + WITH_USER + customUserDetailsService.getCurrentUser().getEmailAddress());
			throw new AccessDeniedException("User is not allowed to access this template");
		}
	}
	
	
	@Override
	public void authorizeDashboardAccess(Dashboard activeDashboard) {
		if (!activeDashboard.getWorkspace().getId().equals(customUserDetailsService.getActiveWorkSpaceForUser().getId())){
			throw new AccessDeniedException("You can not create a dashboard in this workspace");
		}
	}

	@Override
	public void authorizeRelationshipTypeAccess(Integer relationshipTypeId) {
		 RelationshipType rType =  relationshipTypeService.findOne(relationshipTypeId);
		 if (!checkAuthForRelationshipType(rType, customUserDetailsService.getActiveWorkSpaceForUser())){
			// You need to give an access denied
			logger.info("Blocking user access to the relationship type id " + rType.getId() + WITH_USER + customUserDetailsService.getCurrentUser().getEmailAddress());
			throw new AccessDeniedException("User is not allowed to access this relationship type");
		 }
	}

	@Override
	public void authorizeFunctionalMapAccess(FunctionalMapBean functionalMapBean) {
		if(!checkAuthForFunctionalMap(functionalMapBean, customUserDetailsService.getActiveWorkSpaceForUser())){
			logger.info("Blocking user access to the functional map id " + functionalMapBean.getId() + WITH_USER + customUserDetailsService.getCurrentUser().getEmailAddress());
			throw new AccessDeniedException("User is not allowed to access this functional map");
		}
	}

	private boolean checkAuthForFunctionalMap(FunctionalMapBean functionalMapBean, Workspace workspace) {
		Assert.notNull(functionalMapBean, "Can not validate against a null functional map");
		
		if(workspace != null && functionalMapBean.getWorkspaceId() != null){
			return functionalMapBean.getWorkspaceId().equals(workspace.getId());
		}
		return true;
	}
	
	private boolean checkAuthForBcm(Bcm bcm, Workspace workspace) {
		Assert.notNull(bcm, "Can not validate against a null Bcm");
		
		if(workspace != null && bcm.getWorkspace() != null){
			return bcm.getWorkspace().getId().equals(workspace.getId());
		}
		return true;
	}

	@Override
	public void authorizeBcmAccess(Bcm bcm) {
		if(!checkAuthForBcm(bcm, customUserDetailsService.getActiveWorkSpaceForUser())){
			logger.info("Blocking user access to the bcm id " + bcm.getId() + WITH_USER + customUserDetailsService.getCurrentUser().getEmailAddress());
			throw new AccessDeniedException("User is not allowed to access this bcm");
		}
	}

	@Override
	public void authorizeQualityGate(QualityGate qg) {
		if (!checkAuthForQualityGate(qg, customUserDetailsService.getActiveWorkSpaceForUser())){
			// You need to give an access denied
			logger.info("Blocking user access to the quality gate id " + qg.getId() + WITH_USER + customUserDetailsService.getCurrentUser().getEmailAddress());
			throw new AccessDeniedException("User is not allowed to access this quality gate");
		 }
	}

	private boolean checkAuthForQualityGate(QualityGate qg, Workspace workspace) {
		Assert.notNull(qg, "Can not validate against a null Quality gate");
		
		if(workspace != null && qg.getWorkspace() != null){
			return qg.getWorkspace().getId().equals(workspace.getId());
		}
		return true;
	}
	
	@Override
	public void authorizeQuestionnaireType(Questionnaire questionnaire,String questionnaireType) {		
	    // You need to restrict if questionnaire type change.
		if(!questionnaire.getQuestionnaireType().equalsIgnoreCase(questionnaireType)){
			throw new AccessDeniedException("User is not allowed to modify.");
		}
	}
}
