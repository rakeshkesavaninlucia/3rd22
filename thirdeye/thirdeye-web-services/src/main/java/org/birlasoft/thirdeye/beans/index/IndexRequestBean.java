package org.birlasoft.thirdeye.beans.index;


public class IndexRequestBean{

	private String tenantURL;
	
	private String[] wsIds;
	
	
	public String getTenantURL() {
		return tenantURL;
	}

	public void setTenantURL(String tenantURL) {
		this.tenantURL = tenantURL;
	}

	public String[] getWsIds() {
		return wsIds;
	}

	public void setWsIds(String[] wsIds) {
		this.wsIds = wsIds;
	}
	
}
