package org.birlasoft.thirdeye.service.impl;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;

import org.birlasoft.thirdeye.colorscheme.service.Colorizer;
import org.birlasoft.thirdeye.config.TransactionManagers;
import org.birlasoft.thirdeye.constant.Constants;
import org.birlasoft.thirdeye.entity.AssetType;
import org.birlasoft.thirdeye.search.api.beans.AssetClassSearchBean;
import org.birlasoft.thirdeye.search.api.beans.AssetParameterWrapper;
import org.birlasoft.thirdeye.search.api.beans.ParameterSearchBean;
import org.birlasoft.thirdeye.search.api.beans.SearchConfig;
import org.birlasoft.thirdeye.service.AssetTypeService;
import org.birlasoft.thirdeye.service.HomePageService;
import org.birlasoft.thirdeye.service.ParameterService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

/**
 * Service implementation class for Home Page
 * @author dhruv.sood
 *
 */
@Service
@Transactional(value=TransactionManagers.TENANTTRANSACTIONMANAGER)
public class HomePageServiceImpl implements HomePageService{
	
	private static Logger logger = LoggerFactory.getLogger(HomePageServiceImpl.class);

	@Value("#{systemProperties['" + Constants.ELASTICSEARCH_HOST_NAME + "']}")
	private String elasticSearchHost;
	@Autowired
	private AssetTypeService assetTypeService;
	@Autowired
	private Colorizer colorizer;
	@Autowired
	private ParameterService parameterService; 
	
	private static final String INITIATING_REST_CALL="Initiating a Rest Service call";

	@Override
	public List<AssetClassSearchBean> home(String tenantId, Integer activeWorkspaceId, Map<String, List<String>> filterMap){

		List<AssetType> assetTypeList = assetTypeService.findAll();	

		final String uri = elasticSearchHost + Constants.BASESEARCHURL + "/assetClass";

		RestTemplate restTemplate = new RestTemplate();
		AssetClassSearchBean assetClassSearchBean=new AssetClassSearchBean();
		List<AssetClassSearchBean> listAssetClassSearchBean=new ArrayList<>();

		for(AssetType assetType:assetTypeList){
			if (!filterMap.isEmpty() && filterMap.containsKey("assetClass") && !filterMap.get("assetClass").contains(assetType.getAssetTypeName())){
				continue;
			}

			//Setting the search config object  
			SearchConfig searchConfig = setSearchConfig(tenantId, activeWorkspaceId);
			searchConfig.setSearchURL(assetType.getAssetTypeName());
			searchConfig.setFilterMap(filterMap);

			logger.info(INITIATING_REST_CALL);
			
			//Firing a rest request to elastic search server
			try {
				assetClassSearchBean = restTemplate.postForObject(uri,searchConfig,AssetClassSearchBean.class);
				
				//Sorting the parameters in alphabetical order of their name
				List<ParameterSearchBean> oneParameterSearchBean = new ArrayList(assetClassSearchBean.getParameters());
				oneParameterSearchBean.sort((obj1, obj2) -> obj1.getName().trim().compareTo(obj2.getName().trim()));				
				assetClassSearchBean.getParameters().clear();
				assetClassSearchBean.setParameters(new LinkedHashSet<ParameterSearchBean>(oneParameterSearchBean));	
			} catch (RestClientException e) {				
				logger.error("Exception occured in HomePageServiceImpl :: home() :"+e);
			}

			//Validating if the count of the asset types which are greater than 0
			if(assetClassSearchBean.getCount() > 0){
				listAssetClassSearchBean.add(assetClassSearchBean);
			}
		}
		return listAssetClassSearchBean;
	}

	@Override
	public ParameterSearchBean homeParameterTiles(String tenantId, Integer activeWorkspaceId, String assetTypeName, Integer parameterId, Map<String, List<String>> filterMap){

		final String uri = elasticSearchHost + Constants.BASESEARCHURL + "/home/parameter";

		RestTemplate restTemplate = new RestTemplate();
		ParameterSearchBean parameterSearchBean=new ParameterSearchBean();

		//Setting the search config object  
		SearchConfig searchConfig = setSearchConfig(tenantId, activeWorkspaceId);
		searchConfig.setSearchURL(assetTypeName);
		searchConfig.setParameterId(parameterId);
		searchConfig.setFilterMap(filterMap);
		
		logger.info(INITIATING_REST_CALL);
		
		//Firing a rest request to elastic search server
		try {
			parameterSearchBean = restTemplate.postForObject(uri,searchConfig,ParameterSearchBean.class);
		} catch (RestClientException e) {				
			logger.error("Exception occured in HomePageServiceImpl :: homeParameterTiles() :"+e);
		}	
		
		
		parameterSearchBean.setName(parameterService.findOne(parameterSearchBean.getId()).getDisplayName());
		
		//Setting the color for the parameter based on parameter value
		parameterSearchBean.setHexColor(colorizer.getHomePageParameterColor(parameterId, parameterSearchBean.getAggregate()));

		return parameterSearchBean;
	}


	@Override
	public AssetParameterWrapper assetParameterWrapper(String tenantId, Integer activeWorkspaceId, Integer parameterId, Map<String, List<String>> filterMap) {

		final String uri = elasticSearchHost + Constants.BASESEARCHURL + "/param/click";
		AssetParameterWrapper assetParameterWrapper = new AssetParameterWrapper();	 	
		RestTemplate restTemplate = new RestTemplate();

		//Setting the search config object  
		SearchConfig searchConfig = setSearchConfig(tenantId, activeWorkspaceId);		
		searchConfig.setParameterId(parameterId);
		searchConfig.setFilterMap(filterMap);
		
		logger.info(INITIATING_REST_CALL);
		
		//Firing a rest request to elastic search server
		try {
			assetParameterWrapper = restTemplate.postForObject(uri,searchConfig,AssetParameterWrapper.class);
		} catch (RestClientException e) {				
			logger.error("Exception occured in HomePageServiceImpl :: assetParameterWrapper() :"+e);
		}
		
		//Sorting the asset names in alphabetical order  		  		
		assetParameterWrapper.getValues().sort((obj1, obj2) -> obj1.getAssetName().compareTo(obj2.getAssetName()));

		return assetParameterWrapper;
	}

	/**@author dhruv.sood
	 * @param tenantId
	 * @param activeWorkspaceId	
	 * @return
	 */
	private SearchConfig setSearchConfig(String tenantId, Integer activeWorkspaceId) {
		SearchConfig searchConfig=new SearchConfig();
		List<Integer> listOfWorkspace = new ArrayList<>();
		listOfWorkspace.add(activeWorkspaceId);
		searchConfig.setListOfWorkspace(listOfWorkspace);
		List<String>list = new ArrayList<>();
		list.add(tenantId);
		searchConfig.setTenantURLs(list);
		return searchConfig;
	}

}
