package org.birlasoft.thirdeye.service;

import java.util.List;
import java.util.Set;

import org.birlasoft.thirdeye.beans.ParameterBean;
import org.birlasoft.thirdeye.entity.Parameter;
import org.birlasoft.thirdeye.entity.Questionnaire;
import org.birlasoft.thirdeye.entity.QuestionnaireParameter;

/**
 * Service interface for questionnaire parameter.
 */
public interface QuestionnaireParameterService {
	
	/**
	 * Save questionnaire parameter object.
	 * @param questionnaireParameter
	 * @return {@code QuestionnaireParameter}
	 */
	public QuestionnaireParameter save(QuestionnaireParameter questionnaireParameter);
	
	/**
	 * Delete list of questionnaire parameter in batch.
	 * @param parametersToBeDeleted
	 */
	public void deleteInBatch(Iterable<QuestionnaireParameter> parametersToBeDeleted);
	
	/**
	 * Save list of questionnaire parameter in batch.
	 * @param parameterToBeSaved
	 * @return {@code List<QuestionnaireParameter>}
	 */
	public List<QuestionnaireParameter> save(Iterable<QuestionnaireParameter> parameterToBeSaved);
	
	/**
	 * Find one questionnaire parameter object by id.
	 * @param id
	 * @return {@code QuestionnaireParameter}
	 */
	public QuestionnaireParameter findOne(Integer id);
	
	/**
	 * Find list of questionnaire parameter by questionnaire object. 
	 * @param questionnaire
	 * @return {@code List<QuestionnaireParameter>}
	 */
	public List<QuestionnaireParameter> findByQuestionnaire(Questionnaire questionnaire);	
	public Set<QuestionnaireParameter> findByQuestionnaireLoadedParameter(Questionnaire qe);

	public List<QuestionnaireParameter> findByQuestionnaireAndParameter(Questionnaire questionnaire, Parameter parameter);
 
	public  Long deleteQuestionnaireParameter(Questionnaire questionnaire,Parameter findFullyLoaded);

	public List<QuestionnaireParameter> findByQuestionnaireAndParameterByParentParameterIdIsNull(
			Questionnaire qe);

	public Set<Integer> getParameterIds(Questionnaire qe);
	public List<QuestionnaireParameter> updateParametersOnQuestionnaire(Questionnaire q,List<Parameter> newListOfParameters);

	public Set<QuestionnaireParameter> findByParameterByRootParameterIdInAndQuestionnaire(
			Set<Parameter> updatedParameterSet, Questionnaire qe);
	public Set<QuestionnaireParameter> getQuestionnaireParameterGroupByParameterId();

	public List<QuestionnaireParameter> findByQuestionnaireInAndParameterByParentParameterIdIsNull(List<Questionnaire> questionnaireList);

	public List<Questionnaire> findByParameter(ParameterBean oneParameterBean);
	
	public Set<Parameter> findByQuestionnaireIn(List<Questionnaire> questionnaireList);
	
	public List<QuestionnaireParameter> findByParameterByParameterId(Parameter parameter);
	
	/**
	 * Get {@code list} of root {@link QuestionnaireParameter} in a questionnaire
	 * @param questionnaire
	 * @return
	 */
	public List<QuestionnaireParameter> getListOfRootQuestionnaireParameters(
			Questionnaire questionnaire);
	
	/**
	 * find By Questionnaire And ParameterByParentParameterId Is Null.
	 * @param qe
	 * @param loaded
	 * @return {@code List<QuestionnaireParameter>}
	 */
	public List<QuestionnaireParameter> findByQuestionnaireAndParameterByParentParameterIdIsNull(Questionnaire qe, boolean loaded);
	/**
	 * find by questionnaire and loaded (true/false)
	 * @param questionnaire
	 * @param loaded
	 * @return {@code List<QuestionnaireParameter>}
	 */
	public List<QuestionnaireParameter> findByQuestionnaire(Questionnaire questionnaire, boolean loaded);

	public List<Questionnaire> findByParameterId(Integer oneParameter);
}
