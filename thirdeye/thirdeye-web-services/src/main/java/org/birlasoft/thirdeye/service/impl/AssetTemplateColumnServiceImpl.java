package org.birlasoft.thirdeye.service.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.birlasoft.thirdeye.comparator.SequenceNumberComparator;
import org.birlasoft.thirdeye.config.TransactionManagers;
import org.birlasoft.thirdeye.constant.Constants;
import org.birlasoft.thirdeye.constant.DataType;
import org.birlasoft.thirdeye.entity.AssetTemplate;
import org.birlasoft.thirdeye.entity.AssetTemplateColumn;
import org.birlasoft.thirdeye.entity.User;
import org.birlasoft.thirdeye.repositories.AssetDataRepository;
import org.birlasoft.thirdeye.repositories.AssetTemplateColumnRepository;
import org.birlasoft.thirdeye.service.AssetTemplateColumnService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service {@code class} for Asset template column save, update, delete and find.
 *
 */
@Service
@Transactional(value=TransactionManagers.TENANTTRANSACTIONMANAGER)
public class AssetTemplateColumnServiceImpl implements AssetTemplateColumnService {

	private AssetTemplateColumnRepository assetTemplateColumnRepository;
	private AssetDataRepository assetDataRepository;
	
	/**
	 * Constructor autowiring of dependencies
	 * @param assetTemplateColumnRepository
	 * @param assetDataRepository
	 */
	@Autowired
	public AssetTemplateColumnServiceImpl(AssetTemplateColumnRepository assetTemplateColumnRepository,
			AssetDataRepository assetDataRepository) {
		this.assetTemplateColumnRepository = assetTemplateColumnRepository;
		this.assetDataRepository = assetDataRepository;
	}

	@Override
	public AssetTemplateColumn findOne(Integer id) {
		return assetTemplateColumnRepository.findOne(id);
	}

	@Override
	public AssetTemplateColumn save(AssetTemplateColumn assetTemplateColumn) {
		return assetTemplateColumnRepository.save(assetTemplateColumn);
	}

	@Override
	public int findMaxSequenceNumberInAssetTemplateColumn(AssetTemplate cachedTemplate) {
		// find Max sequence number in asset template columns
		int maxSequenceNumber = 0;
		Set<AssetTemplateColumn> assetTemplateColumns = cachedTemplate.getAssetTemplateColumns();
		for(AssetTemplateColumn assetTemplateCol : assetTemplateColumns){
			if(assetTemplateCol != null){
				int currentSequenceNumber = assetTemplateCol.getSequenceNumber();
				if(maxSequenceNumber < currentSequenceNumber)
					maxSequenceNumber = currentSequenceNumber;
			}
		}
		return maxSequenceNumber+1;
	}
	
	@Override
	public Set<AssetTemplateColumn> sortTemplateColumnsByMandatoryChecked(Set<AssetTemplateColumn> assetTemplateColumns) {
		List<AssetTemplateColumn> sortedListOfAssetTemplateColumn = getSortedListOfTemplateColumn(assetTemplateColumns);
		Iterator<AssetTemplateColumn> assetTempIter = sortedListOfAssetTemplateColumn.iterator();
		while (assetTempIter.hasNext()) {
			AssetTemplateColumn oneTemplateCol = assetTempIter.next();
			if(oneTemplateCol.getSequenceNumber() == 2 || oneTemplateCol.getSequenceNumber() == 3) {
				if(oneTemplateCol.isMandatory() == false) {
					assetTempIter.remove();
				}
			}
		}
	
		return new LinkedHashSet<>(sortedListOfAssetTemplateColumn);
	}
	@Override
	public Set<AssetTemplateColumn> sortTemplateColumnsBySequenceNumber(Set<AssetTemplateColumn> assetTemplateColumns) {
		List<AssetTemplateColumn> sortedListOfAssetTemplateColumn = getSortedListOfTemplateColumn(assetTemplateColumns);
		return new LinkedHashSet<>(sortedListOfAssetTemplateColumn);
	}
	
	@Override
	public AssetTemplateColumn createAssetTemplateColumnObject(AssetTemplateColumn assetTemplateColumn, AssetTemplate cachedTemplate, User currentUser) {
		assetTemplateColumn.setSequenceNumber(findMaxSequenceNumberInAssetTemplateColumn(cachedTemplate));
		assetTemplateColumn.setAssetTemplate(cachedTemplate);
		assetTemplateColumn.setUserByCreatedBy(currentUser);
		assetTemplateColumn.setUserByUpdatedBy(currentUser);
		assetTemplateColumn.setCreatedDate(new Date());
		assetTemplateColumn.setUpdatedDate(new Date());
		return assetTemplateColumn;
	}

	@Override
	public List<AssetTemplateColumn> getSortedListOfTemplateColumn(Set<AssetTemplateColumn> assetTemplateColumns) {
		List<AssetTemplateColumn> listOfAssetTemplateColumn = new ArrayList<>(assetTemplateColumns);
		Collections.sort(listOfAssetTemplateColumn, new SequenceNumberComparator());
		return listOfAssetTemplateColumn;
	}

	@Override
	public List<AssetTemplateColumn> createDefaultAssetTemplateColumn(AssetTemplate assetTemplate, User currentUser) {
		List<AssetTemplateColumn> listofAsset = new ArrayList<>();
		AssetTemplateColumn assetTemplateColumn = new AssetTemplateColumn();
		assetTemplateColumn.setAssetTemplateColName(Constants.DEFAULT_COLUMN_NAME);
		assetTemplateColumn.setDataType(DataType.TEXT.name());
		assetTemplateColumn.setLength(Constants.DEFAULT_COLUMN_LENGTH);
		assetTemplateColumn.setSequenceNumber(1);
		assetTemplateColumn.setAssetTemplate(assetTemplate);
		assetTemplateColumn.setMandatory(true);
		assetTemplateColumn.setCreatedDate(new Date());
		assetTemplateColumn.setUpdatedDate(new Date());
		assetTemplateColumn.setUserByCreatedBy(currentUser);
		assetTemplateColumn.setUserByUpdatedBy(currentUser);
		
		AssetTemplateColumn assetTemplateColumn1 = new AssetTemplateColumn();
		assetTemplateColumn1.setAssetTemplateColName(Constants.DEFAULT_COUNTRY_NAME_COLUMN);
		assetTemplateColumn1.setDataType(DataType.TEXT.name());
		assetTemplateColumn1.setLength(Constants.DEFAULT_COLUMN_LENGTH);
		assetTemplateColumn1.setSequenceNumber(2);
		assetTemplateColumn1.setAssetTemplate(assetTemplate);
		assetTemplateColumn1.setMandatory(false);
		assetTemplateColumn1.setCreatedDate(new Date());
		assetTemplateColumn1.setUpdatedDate(new Date());
		assetTemplateColumn1.setUserByCreatedBy(currentUser);
		assetTemplateColumn1.setUserByUpdatedBy(currentUser);
		
		AssetTemplateColumn assetTemplateColumn2 = new AssetTemplateColumn();
		assetTemplateColumn2.setAssetTemplateColName(Constants.DEFAULT_NOOFUSERS_COLUMN);
		assetTemplateColumn2.setDataType(DataType.NUMBER.name());
		assetTemplateColumn2.setLength(Constants.DEFAULT_COLUMN_LENGTH);
		assetTemplateColumn2.setSequenceNumber(3);
		assetTemplateColumn2.setAssetTemplate(assetTemplate);
		assetTemplateColumn2.setMandatory(false);
		assetTemplateColumn2.setCreatedDate(new Date());
		assetTemplateColumn2.setUpdatedDate(new Date());
		assetTemplateColumn2.setUserByCreatedBy(currentUser);
		assetTemplateColumn2.setUserByUpdatedBy(currentUser);
		
		AssetTemplateColumn assetTemplateColumn3 = new AssetTemplateColumn();
		assetTemplateColumn3.setAssetTemplateColName(Constants.DEFAULT_LIFE_CYCLE);
		assetTemplateColumn3.setDataType(Constants.JSON_DATA_TYPE);
		assetTemplateColumn3.setLength(Constants.DEFAULT_COLUMN_LENGTH);
		assetTemplateColumn3.setSequenceNumber(4);
		assetTemplateColumn3.setAssetTemplate(assetTemplate);
		assetTemplateColumn3.setMandatory(true);
		assetTemplateColumn3.setCreatedDate(new Date());
		assetTemplateColumn3.setUpdatedDate(new Date());
		assetTemplateColumn3.setUserByCreatedBy(currentUser);
		assetTemplateColumn3.setUserByUpdatedBy(currentUser);
		
		
		listofAsset.add(assetTemplateColumn2);
		listofAsset.add(assetTemplateColumn1);
		listofAsset.add(assetTemplateColumn);
		listofAsset.add(assetTemplateColumn3);
		return listofAsset;
	}

	@Override
	public void deleteAssetData(Integer idOfTemplateColumn) {
		AssetTemplateColumn assetTemplateColumn = assetTemplateColumnRepository.findById(idOfTemplateColumn);
		if(!assetTemplateColumn.getAssetDatas().isEmpty()){
			assetDataRepository.deleteInBatch(assetTemplateColumn.getAssetDatas());
		}
	}

	@Override
	public void delete(AssetTemplateColumn assetTemplateColumn) {
		assetTemplateColumnRepository.delete(assetTemplateColumn);
	}

	@Override
	public void updateSequenceNumber(List<AssetTemplateColumn> listOfTemplateColumnToUpdate) {
		Collections.sort(listOfTemplateColumnToUpdate, new SequenceNumberComparator());
		int count = 1;
		for (AssetTemplateColumn atc : listOfTemplateColumnToUpdate) {
			atc.setSequenceNumber(count);
			count++;
		}
		saveInBatch(listOfTemplateColumnToUpdate);
	}

	@Override
	public List<AssetTemplateColumn> saveInBatch(List<AssetTemplateColumn> templateColumns) {
		return assetTemplateColumnRepository.save(templateColumns);
	}

	@Override
	public AssetTemplateColumn findOneLoadedAssetTemplate(Integer idOfTemplateColumn) {
		return assetTemplateColumnRepository.findOneLoadedAssetTemplate(idOfTemplateColumn);
	}

	@Override
	public List<AssetTemplateColumn> findByAssetTemplate(AssetTemplate assetTemplate) {
		return assetTemplateColumnRepository.findByAssetTemplate(assetTemplate);
	}

	@Override
	public void deleteInBatch(List<AssetTemplateColumn> deleteAssetTemplateColumns) {
		assetTemplateColumnRepository.deleteInBatch(deleteAssetTemplateColumns);

	}

	@Override
	public AssetTemplateColumn createAssetTemplateColumnLifeCycle(AssetTemplate assetTemplate, User currentUser,AssetTemplateColumn assetTemplateColumn) {
		AssetTemplateColumn assetTemplateColumnForLifeCycle = new AssetTemplateColumn();
		List<AssetTemplateColumn> lifeCycle = assetTemplate.getAssetTemplateColumns().stream().filter(e->e.getDataType().equals(Constants.JSON_DATA_TYPE)).collect(Collectors.toList());
		assetTemplateColumnForLifeCycle.setAssetTemplateColName(Constants.DEFAULT_LIFE_CYCLE+String.valueOf(lifeCycle.size()));
		assetTemplateColumnForLifeCycle.setSequenceNumber(findMaxSequenceNumberInAssetTemplateColumn(assetTemplate));
		assetTemplateColumnForLifeCycle.setAssetTemplate(assetTemplate);
		assetTemplateColumnForLifeCycle.setLength(Constants.DEFAULT_COLUMN_LENGTH);
		assetTemplateColumnForLifeCycle.setDataType(Constants.JSON_DATA_TYPE);
		assetTemplateColumnForLifeCycle.setMandatory(false);
		assetTemplateColumnForLifeCycle.setCreatedDate(new Date());
		assetTemplateColumnForLifeCycle.setUpdatedDate(new Date());
		assetTemplateColumnForLifeCycle.setUserByCreatedBy(currentUser);
		assetTemplateColumnForLifeCycle.setUserByUpdatedBy(currentUser);
		save(assetTemplateColumnForLifeCycle);
		return assetTemplateColumnForLifeCycle;
	}

	
}
