package org.birlasoft.thirdeye.beans;

/**
 * Bean class for showing in drop down at FR and Asset mapping.
 * @author samar.gupta
 *
 */
public class QuestionnaireFCParameterBean {

	private String questionnaireName;
	private Integer questionnaireId;
	private String fcParameterName;
	private Integer fcParameterId;
	
	public String getQuestionnaireName() {
		return questionnaireName;
	}
	public void setQuestionnaireName(String questionnaireName) {
		this.questionnaireName = questionnaireName;
	}
	public Integer getQuestionnaireId() {
		return questionnaireId;
	}
	public void setQuestionnaireId(Integer questionnaireId) {
		this.questionnaireId = questionnaireId;
	}
	public String getFcParameterName() {
		return fcParameterName;
	}
	public void setFcParameterName(String fcParameterName) {
		this.fcParameterName = fcParameterName;
	}
	public Integer getFcParameterId() {
		return fcParameterId;
	}
	public void setFcParameterId(Integer fcParameterId) {
		this.fcParameterId = fcParameterId;
	}
}
