package org.birlasoft.thirdeye.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import org.birlasoft.thirdeye.beans.AssetBean;
import org.birlasoft.thirdeye.beans.JSONMultiChoiceResponseMapper;
import org.birlasoft.thirdeye.beans.JSONQuestionOptionMapper;
import org.birlasoft.thirdeye.beans.QuestionBean;
import org.birlasoft.thirdeye.beans.widgets.BarChartConfig;
import org.birlasoft.thirdeye.config.TransactionManagers;
import org.birlasoft.thirdeye.constant.QuestionType;
import org.birlasoft.thirdeye.entity.Question;
import org.birlasoft.thirdeye.entity.Questionnaire;
import org.birlasoft.thirdeye.entity.QuestionnaireQuestion;
import org.birlasoft.thirdeye.entity.ResponseData;
import org.birlasoft.thirdeye.search.api.beans.AssetParameterBean;
import org.birlasoft.thirdeye.search.api.beans.AssetParameterWrapper;
import org.birlasoft.thirdeye.service.AssetService;
import org.birlasoft.thirdeye.service.GraphFacetsService;
import org.birlasoft.thirdeye.service.ParameterSearchService;
import org.birlasoft.thirdeye.service.QuestionnaireQuestionService;
import org.birlasoft.thirdeye.util.Utility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Implementation class for graph facets (Pie or bar chart)
 * @author samar.gupta
 */
@Service
@Transactional(value=TransactionManagers.TENANTTRANSACTIONMANAGER)
public class GraphFacetsServiceImpl implements GraphFacetsService {
	
	@Autowired
	private ParameterSearchService parameterSearchService;
	@Autowired
	private QuestionnaireQuestionService questionnaireQuestionService;

	
	@Autowired
	private AssetService assetService;
	
	@Override
	public Map<String,Integer> getMapForPlotPieOrBarChart(Questionnaire qe, Question q, Optional<BarChartConfig> optionalConfig, Map<String,List<String>> filterMap) {
		// This is the map which will contain the data for the response grouped by category (String) key.
		Map<String, Integer> mapJSONDataForPlotChart = new HashMap<>();
		
		List<QuestionnaireQuestion> qqForScanning = questionnaireQuestionService.findByParameterAndQuestionAndQuestionnaire(null, new QuestionBean(q), qe);
		
		// This simply pulls on the response data - this would work fine if you have a simple 
		// one qe one response situation. You would get cumulative response in a survey situation
		
		Map<Integer, BigDecimal> parameterValueMap = null;
		if (optionalConfig.isPresent() && optionalConfig.get().getParameterId() >0){
			Set<AssetBean> assetList = new HashSet<>();
			
			for (QuestionnaireQuestion oneQQ : qqForScanning){
				assetList.add(new AssetBean(oneQQ.getQuestionnaireAsset().getAsset()));
			}
			parameterValueMap = evaluateParameters(assetList, qe.getId(), optionalConfig.get().getParameterId(),filterMap);
		}
		
		List<Integer> listOfFilteredAssetIds = new ArrayList<>();
		if (!filterMap.isEmpty()){			
			listOfFilteredAssetIds = assetService.getFilteredAssetIdFromElasticSearch(filterMap);
		}
		
		// listOfAssetIds = 
		for (QuestionnaireQuestion oneQQ : qqForScanning){
			
			if(filterMap.isEmpty() || (!filterMap.isEmpty() && listOfFilteredAssetIds.contains(oneQQ.getQuestionnaireAsset().getAsset().getId()))){
				prepareResponseForChart(optionalConfig, mapJSONDataForPlotChart, parameterValueMap, oneQQ);
			}
		}
		
		return mapJSONDataForPlotChart;
		
	}

	/**
	 * @param optionalConfig
	 * @param mapJSONDataForPlotChart
	 * @param parameterValueMap
	 * @param oneQQ
	 */
	private void prepareResponseForChart(Optional<BarChartConfig> optionalConfig, Map<String, Integer> mapJSONDataForPlotChart, 
			Map<Integer, BigDecimal> parameterValueMap,	QuestionnaireQuestion oneQQ) {
		// Check the question type
		String queType = oneQQ.getQuestion().getQuestionType();
		
		// YOu can optionally check the response property of the responseData object 
		// if you want to restrict only to one response.
		BigDecimal parameterValue = new BigDecimal(0);
		if (optionalConfig.isPresent() && optionalConfig.get().getParameterId() > 0 && parameterValueMap != null){
			parameterValue = parameterValueMap.get(oneQQ.getQuestionnaireAsset().getAsset().getId());
		}
		
		// Go through the responses for the QQ (you will have multiple if there are multiple resposes allowed for the QE)
		for (ResponseData responseData : oneQQ.getResponseDatas()){
			List<JSONQuestionOptionMapper> optionMappers = null;
			if(queType.equals(QuestionType.MULTCHOICE.toString())){
				JSONMultiChoiceResponseMapper jsonMultiChoiceResponseMapper = Utility.convertJSONStringToObject(responseData.getResponseData(), JSONMultiChoiceResponseMapper.class);
				optionMappers = jsonMultiChoiceResponseMapper.getOptions();
			}				
			// Based on the selected option group the responses 
			groupResponsesForOptions(optionalConfig, mapJSONDataForPlotChart, parameterValue, optionMappers);
		}
	}

	/**
	 * Group responses based on MCQ options
	 * @param optionalConfig
	 * @param mapJSONDataForPlotChart
	 * @param parameterValue
	 * @param optionMappers
	 */
	private void groupResponsesForOptions(Optional<BarChartConfig> optionalConfig, Map<String, Integer> mapJSONDataForPlotChart,
			BigDecimal parameterValue, List<JSONQuestionOptionMapper> optionMappers) {
		for (JSONQuestionOptionMapper jsonQuestionOptionMapper : optionMappers) {
			String oneOption = jsonQuestionOptionMapper.getText();
			
			if(mapJSONDataForPlotChart.containsKey(oneOption)){
				Integer valueOfKey = mapJSONDataForPlotChart.get(oneOption);
				
				if (optionalConfig.isPresent() && optionalConfig.get().getParameterId() >0){
					mapJSONDataForPlotChart.put(oneOption, valueOfKey + parameterValue.intValue());
				} else {
					mapJSONDataForPlotChart.put(oneOption, valueOfKey + 1);
				}
			} else {
				if (optionalConfig.isPresent() && optionalConfig.get().getParameterId() >0){
					mapJSONDataForPlotChart.put(oneOption, parameterValue.intValue());
				} else {
					mapJSONDataForPlotChart.put(oneOption, 1);
				}
			}
		}
	}
	
	/**
	 * Evaluate parameter for set of assets.
	 * @param assetList
	 * @param questionnaireId
	 * @param parameterId
	 * @return
	 */
	private Map<Integer, BigDecimal> evaluateParameters(Set<AssetBean> assetList, int questionnaireId, int parameterId,Map<String, List<String>> filterMap) {
		Map<Integer, BigDecimal> paramMappedById = new HashMap<>();

		AssetParameterWrapper assetParameterWrapper = parameterSearchService.getParameterValueFromElasticsearch(parameterId, questionnaireId,
				assetList, filterMap);
		for (AssetParameterBean oneAssetParameterBean : assetParameterWrapper.getValues()) {
			paramMappedById.put(oneAssetParameterBean.getAssetId(), oneAssetParameterBean.getParameterValue());
		}

		return paramMappedById;
	}

}
