package org.birlasoft.thirdeye.beans.asset;


public class AssetChartRequestParamBean {
	
	private Integer assetId;
	private Integer parameterId;
	private Integer parentParameterId;
	private Integer rootParameterId;
	private Integer parentParentParameterId;
	private Integer questionnaireId;
	private String parameterName;
	private String paramType;
	
	public Integer getAssetId() {
		return assetId;
	}
	public void setAssetId(Integer assetId) {
		this.assetId = assetId;
	}
	public Integer getParameterId() {
		return parameterId;
	}
	public void setParameterId(Integer parameterId) {
		this.parameterId = parameterId;
	}
	public Integer getQuestionnaireId() {
		return questionnaireId;
	}
	public void setQuestionnaireId(Integer questionnaireId) {
		this.questionnaireId = questionnaireId;
	}
	public String getParamType() {
		return paramType;
	}
	public void setParamType(String paramType) {
		this.paramType = paramType;
	}
	public String getParameterName() {
		return parameterName;
	}
	public void setParameterName(String parameterName) {
		this.parameterName = parameterName;
	}
	public Integer getRootParameterId() {
		return rootParameterId;
	}
	public void setRootParameterId(Integer rootParameterId) {
		this.rootParameterId = rootParameterId;
	}
	public Integer getParentParameterId() {
		return parentParameterId;
	}
	public void setParentParameterId(Integer parentParameterId) {
		this.parentParameterId = parentParameterId;
	}
	public Integer getParentParentParameterId() {
		return parentParentParameterId;
	}
	public void setParentParentParameterId(Integer parentParentParameterId) {
		this.parentParentParameterId = parentParentParameterId;
	}
	
}
