package org.birlasoft.thirdeye.service.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.birlasoft.thirdeye.beans.ParameterBean;
import org.birlasoft.thirdeye.comparator.QuestionnaireParameterComparator;
import org.birlasoft.thirdeye.config.TransactionManagers;
import org.birlasoft.thirdeye.constant.ParameterType;
import org.birlasoft.thirdeye.entity.Parameter;
import org.birlasoft.thirdeye.entity.Questionnaire;
import org.birlasoft.thirdeye.entity.QuestionnaireParameter;
import org.birlasoft.thirdeye.repositories.QuestionnaireParameterRepository;
import org.birlasoft.thirdeye.service.ParameterService;
import org.birlasoft.thirdeye.service.QuestionnaireParameterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Implementation of service class for Questionnaire Parameter
 * 
 */
@Service
@Transactional(value=TransactionManagers.TENANTTRANSACTIONMANAGER)
public class QuestionnaireParameterServiceImpl implements QuestionnaireParameterService{
	@Autowired
	private QuestionnaireParameterRepository questionnaireParameterRepository;
	@Autowired
	private ParameterService pService  ;

	@Override
	public QuestionnaireParameter save(QuestionnaireParameter questionnaireParameter){
		return questionnaireParameterRepository.save(questionnaireParameter);
	}
	
	@Override
	public void deleteInBatch(Iterable<QuestionnaireParameter> parametersToBeDeleted) {
		questionnaireParameterRepository.deleteInBatch(parametersToBeDeleted);
	}
	
	@Override
	public List<QuestionnaireParameter> save(Iterable<QuestionnaireParameter> parameterToBeSaved){
		return questionnaireParameterRepository.save(parameterToBeSaved);
	}


	@Override
	public QuestionnaireParameter findOne(Integer id) {
		return questionnaireParameterRepository.findOne(id);
	}


	@Override
	public List<QuestionnaireParameter> findByQuestionnaire(Questionnaire questionnaire) {
		return questionnaireParameterRepository.findByQuestionnaire(questionnaire);
	}

	@Override
	public Set<QuestionnaireParameter> findByQuestionnaireLoadedParameter(Questionnaire qe) {
		return questionnaireParameterRepository.findByQuestionnaireLoadedParameter(qe);
	}

	@Override
	public List<QuestionnaireParameter> findByQuestionnaireAndParameter(
			Questionnaire questionnaire, Parameter parameter) {
		return questionnaireParameterRepository.findByQuestionnaireAndParameterByParameterId(questionnaire, parameter);
	}

	@Override
	public Long deleteQuestionnaireParameter(Questionnaire questionnaire,Parameter findFullyLoaded) {
	  return questionnaireParameterRepository.deleteByQuestionnaireAndParameterByRootParameterId(questionnaire,findFullyLoaded);
		
	}
	
	@Override
	public List<QuestionnaireParameter> findByQuestionnaireAndParameterByParentParameterIdIsNull(Questionnaire qe) {
		return questionnaireParameterRepository.findByQuestionnaireAndParameterByParentParameterIdIsNull(qe);
	}
	
	@Override
	public Set<Integer> getParameterIds(Questionnaire qe) {
		Set<Integer> parameterId = new HashSet<>();
		List<QuestionnaireParameter> qplist =  questionnaireParameterRepository.findByQuestionnaireAndParameterByParentParameterIdIsNull(qe);
	   if(qplist !=null && !qplist.isEmpty()){
		   for(QuestionnaireParameter qparameter : qplist ){
			   parameterId.add(qparameter.getParameterByParameterId().getId());
		   }
	   }
		return parameterId;
	}
	
	/**
	 * method to add parameter on Questionnaire
	 * @author sunil1.gupta
	 */
	@Override
	public List<QuestionnaireParameter> updateParametersOnQuestionnaire(Questionnaire q, List<Parameter> listOfParentParameters) {
		
		QuestionnaireParameter questionnaireParameter;
		List<QuestionnaireParameter> listOfItemsToSave = new ArrayList<>();
		for(Parameter oneParameter : listOfParentParameters){
			questionnaireParameter = new QuestionnaireParameter();	
			questionnaireParameter.setQuestionnaire(q);
			questionnaireParameter.setParameterByParameterId(oneParameter);
			questionnaireParameter.setParameterByRootParameterId(oneParameter);	
			listOfItemsToSave.add(questionnaireParameter);	
			if(!oneParameter.getType().equalsIgnoreCase(ParameterType.FC.toString())){
				prepareAPParameterList(q, listOfItemsToSave, oneParameter);
			}			
		}
		Set<QuestionnaireParameter> listOfWithoutDuplicateItemsToSave = new TreeSet<>(new QuestionnaireParameterComparator());
		listOfWithoutDuplicateItemsToSave.addAll(listOfItemsToSave);
		// Go for the save and goto the next step.
		return questionnaireParameterRepository.save(listOfWithoutDuplicateItemsToSave);
		
	}
	
	/**
	 * method to preapare AP parameter list for save on Questionnaire parameter
	 * @param q
	 * @param listOfItemsToSave
	 * @param oneParameter
	 */

	private void prepareAPParameterList(Questionnaire q,
			List<QuestionnaireParameter> listOfItemsToSave,
			Parameter oneParameter) {
		QuestionnaireParameter questionnaireParameter;
		ParameterBean parameterBean = pService.generateParameterTree(oneParameter);
		List<ParameterBean> parameterBeanList =parameterBean.fetchListOfAllParameters();
		for(ParameterBean pb : parameterBeanList){
			questionnaireParameter = new QuestionnaireParameter();					
			questionnaireParameter.setQuestionnaire(q);
			questionnaireParameter.setParameterByParameterId(pService.findFullyLoaded(pb.getId()));
			questionnaireParameter.setParameterByRootParameterId(oneParameter);	
			questionnaireParameter.setParameterByParentParameterId(pService.findFullyLoaded(pb.getParent().getId()));
		    listOfItemsToSave.add(questionnaireParameter);					
		}
	}

	@Override
	public Set<QuestionnaireParameter> findByParameterByRootParameterIdInAndQuestionnaire(
			Set<Parameter> parameters, Questionnaire qe) {
		return questionnaireParameterRepository.findByParameterByRootParameterIdInAndQuestionnaire(parameters, qe);
	}

	@Override
	public Set<QuestionnaireParameter> getQuestionnaireParameterGroupByParameterId() {
		return questionnaireParameterRepository.getQuestionnaireParameterGroupByParameterId();
	}

	@Override
	public List<QuestionnaireParameter> findByQuestionnaireInAndParameterByParentParameterIdIsNull(
			List<Questionnaire> questionnaireList) {
		return questionnaireParameterRepository.findByQuestionnaireInAndParameterByParentParameterIdIsNull(questionnaireList);
	}
	
	@Override
	public List<Questionnaire> findByParameter(ParameterBean oneParameterBean) {
		return questionnaireParameterRepository.findByParameterByParameterId(pService.findOne(oneParameterBean.getId()));
	
	}
	
	@Override
	public Set<Parameter> findByQuestionnaireIn(List<Questionnaire> questionnaireList) {
		return new HashSet<>(questionnaireParameterRepository.findParametersByQuestionnaireIn(questionnaireList));
	}
	
	@Override
	public List<QuestionnaireParameter> findByParameterByParameterId(Parameter parameter){
		return questionnaireParameterRepository.getByParameterByParameterId(parameter);
	}
	
	@Override
	public List<QuestionnaireParameter> getListOfRootQuestionnaireParameters(Questionnaire questionnaire) {
		if(questionnaire != null){
			return this.findByQuestionnaireAndParameterByParentParameterIdIsNull(questionnaire);
		}
		return new ArrayList<>();
	}

	@Override
	public List<QuestionnaireParameter> findByQuestionnaireAndParameterByParentParameterIdIsNull(Questionnaire qe, boolean loaded) {
		List<QuestionnaireParameter> listOfQp = questionnaireParameterRepository.findByQuestionnaireAndParameterByParentParameterIdIsNull(qe);
		
		if(loaded){
			for (QuestionnaireParameter questionnaireParameter : listOfQp) {
				questionnaireParameter.getParameterByParameterId().getUniqueName();
			}
		}
		
		return listOfQp;
	}

	@Override
	public List<QuestionnaireParameter> findByQuestionnaire(Questionnaire questionnaire, boolean loaded) {
		List<QuestionnaireParameter> listOfQp = questionnaireParameterRepository.findByQuestionnaire(questionnaire);
		
		if(loaded){
			for (QuestionnaireParameter questionnaireParameter : listOfQp) {
				questionnaireParameter.getParameterByParameterId().getUniqueName();
			}
		}
		
		return listOfQp;
	}
	
	@Override
	public List<Questionnaire> findByParameterId(Integer oneParameter) {
		return questionnaireParameterRepository.findByParameterByParameterId(pService.findOne(oneParameter));
	}
	
}
