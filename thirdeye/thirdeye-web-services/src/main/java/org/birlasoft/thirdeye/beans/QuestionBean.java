package org.birlasoft.thirdeye.beans;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.birlasoft.thirdeye.comparator.Sequenced;
import org.birlasoft.thirdeye.constant.Constants;
import org.birlasoft.thirdeye.constant.QuestionMode;
import org.birlasoft.thirdeye.constant.QuestionType;
import org.birlasoft.thirdeye.entity.Question;
import org.birlasoft.thirdeye.interfaces.QuantifiableResponse;
import org.birlasoft.thirdeye.util.Utility;


public class QuestionBean implements Sequenced {

	private Integer id;
	private String title;
	private String type;
	private String helpText;
	private String category;
	private QuestionMode mode;
	private Question parentQuestion;
	private JSONMultiChoiceQuestionMapper jsonMultiChoiceQuestionMapper;
	private JSONNumberQuestionMapper jsonNumberQuestionMapper;

	private List<QuantifiableResponse> listOfResponses = new ArrayList<>();
	
	private ParameterBean parentParameter;
	private Integer sequenceNumber;
	
	public QuestionBean(Question question, ParameterBean parentParameter ) {
		this.id = question.getId();
		this.title = question.getTitle();
		this.type = question.getQuestionType();
		this.helpText = question.getHelpText();
		this.mode = QuestionMode.valueOf(question.getQuestionMode());
		this.parentQuestion = question.getQuestion();
		
		if (question.getCategory() != null){
			this.category = question.getCategory().getName();
		}
		this.parentParameter = parentParameter;
		
		String jsonStringForQuestion = question.getQueTypeText();
		String queType = question.getQuestionType();
		if (jsonStringForQuestion != null) {
			if(queType.equals(QuestionType.MULTCHOICE.toString())){
				if(question.getBenchmark() == null)
					jsonMultiChoiceQuestionMapper = Utility.convertJSONStringToObject(jsonStringForQuestion, JSONMultiChoiceQuestionMapper.class);
				else {
					jsonMultiChoiceQuestionMapper = new JSONMultiChoiceQuestionMapper();
					JSONBenchmarkMultiChoiceQuestionMapper jsonBenchmarkMultiChoiceQuestionMapper = Utility.convertJSONStringToObject(jsonStringForQuestion, JSONBenchmarkMultiChoiceQuestionMapper.class);
					for (JSONBenchmarkQuestionOptionMapper jsonBenchmarkQuestionOptionMapper : jsonBenchmarkMultiChoiceQuestionMapper.getOptions()) {
						JSONQuestionOptionMapper jsonQuestionOptionMapper = new JSONQuestionOptionMapper();
						jsonQuestionOptionMapper.setText(jsonBenchmarkQuestionOptionMapper.getText());
						jsonQuestionOptionMapper.setQuantifier(jsonBenchmarkQuestionOptionMapper.getQuantifier());
						jsonQuestionOptionMapper.setSequenceNumber(jsonBenchmarkQuestionOptionMapper.getSequenceNumber());
						jsonMultiChoiceQuestionMapper.addOption(jsonQuestionOptionMapper);
					}
				}
			}else if(queType.equals(QuestionType.NUMBER.toString()) || queType.equals(QuestionType.CURRENCY.toString())){
				jsonNumberQuestionMapper = Utility.convertJSONStringToObject(jsonStringForQuestion, JSONNumberQuestionMapper.class);
				if(jsonNumberQuestionMapper.isLimitTo() && jsonNumberQuestionMapper.getMin() == null){
					jsonNumberQuestionMapper.setMin(Constants.DEFAULT_NUMBER_MIN);
				}
			}
		}	
		
	}
	
	public QuestionBean(Question question) {
		this.id = question.getId();
		this.title = question.getTitle();
		this.type = question.getQuestionType();
		this.helpText = question.getHelpText();
		if (question.getCategory() != null){
			this.category = question.getCategory().getName();
		}
	}

	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}

	public ParameterBean getParentParameter() {
		return parentParameter;
	}

	public void setParentParameter(ParameterBean parentParameter) {
		this.parentParameter = parentParameter;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getHelpText() {
		return helpText;
	}

	public void setHelpText(String helpText) {
		this.helpText = helpText;
	}
	
	public QuestionMode getMode() {
		return mode;
	}

	public void setMode(QuestionMode mode) {
		this.mode = mode;
	}

	public Question getParentQuestion() {
		return parentQuestion;
	}

	public void setParentQuestion(Question parentQuestion) {
		this.parentQuestion = parentQuestion;
	}

	public JSONMultiChoiceQuestionMapper getJsonMultiChoiceQuestionMapper() {
		return jsonMultiChoiceQuestionMapper;
	}

	public void setJsonMultiChoiceQuestionMapper(
			JSONMultiChoiceQuestionMapper jsonMultiChoiceQuestionMapper) {
		this.jsonMultiChoiceQuestionMapper = jsonMultiChoiceQuestionMapper;
	}

	public void addQuantifiableResponse(QuantifiableResponse oneResponse) {
		if (oneResponse != null){
			listOfResponses.add(oneResponse);
		}
	}

	
	public BigDecimal evaluate(String queType){
		if (listOfResponses.size() == 0 ){
			return new BigDecimal(0);
		}
		
		// Withing a question we will simply take the average of the
		// responses that are available
		BigDecimal questionResponseValue = new BigDecimal(0);
		int size = listOfResponses.size();
		
		for (QuantifiableResponse oneResponse : listOfResponses){
			if(oneResponse.fetchQuantifiableResponse().equals(new BigDecimal(-1))){
				size--;
			} else {				
				questionResponseValue = questionResponseValue.add(oneResponse.fetchQuantifiableResponse());
			}
		}
		if(size < 1){
			return new BigDecimal(-1);
		}
		
		BigDecimal divisor = new BigDecimal(size);
		
		return questionResponseValue.divide(divisor);
	}

	public void cleanResponses() {
		listOfResponses = new ArrayList<QuantifiableResponse>();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		QuestionBean other = (QuestionBean) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public JSONNumberQuestionMapper getJsonNumberQuestionMapper() {
		return jsonNumberQuestionMapper;
	}

	public void setJsonNumberQuestionMapper(JSONNumberQuestionMapper jsonNumberQuestionMapper) {
		this.jsonNumberQuestionMapper = jsonNumberQuestionMapper;
	}

	public void setSequenceNumber(Integer sequenceNumber) {
		this.sequenceNumber = sequenceNumber;
	}

	@Override
	public Integer getSequenceNumber() {
		return sequenceNumber;
	}
}
