/**
 * 
 */
package org.birlasoft.thirdeye.beans.bcm;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author shaishav.dixit
 *
 */
public class BCMWrapper {
	
	List<String> l1List = new ArrayList<String>();
	Set<String> l2CategoryList = new HashSet<String>();
	Set<BCMValueWrapper> l2List = new HashSet<BCMValueWrapper>();
	
	public List<String> getL1List() {
		return l1List;
	}
	public void setL1List(List<String> l1List) {
		this.l1List = l1List;
	}
	public Set<String> getL2CategoryList() {
		return l2CategoryList;
	}
	public void setL2CategoryList(Set<String> l2CategoryList) {
		this.l2CategoryList = l2CategoryList;
	}
	public Set<BCMValueWrapper> getL2List() {
		return l2List;
	}
	public void setL2List(Set<BCMValueWrapper> l2List) {
		this.l2List = l2List;
	}
}
