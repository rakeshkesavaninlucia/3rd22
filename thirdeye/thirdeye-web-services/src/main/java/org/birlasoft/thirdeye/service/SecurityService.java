package org.birlasoft.thirdeye.service;

import java.util.Collection;

import org.birlasoft.thirdeye.constant.Permissions;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

@Component(SecurityService.SECURITY_SERVICE_BEAN)
public class SecurityService {
	public static final String  SECURITY_SERVICE_BEAN = "securityService";
    public boolean hasPermission( Permissions...permissions){
        // loop over each submitted role and validate the user has at least one
        Collection<? extends GrantedAuthority> userAuthorities = SecurityContextHolder.getContext().getAuthentication().getAuthorities();
        for( Permissions permission : permissions){
            if( userAuthorities.contains( new SimpleGrantedAuthority(permission.name())))
                return true;
        }

        return false;
    }
}