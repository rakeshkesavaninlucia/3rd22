package org.birlasoft.thirdeye.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.birlasoft.thirdeye.beans.AssetBean;
import org.birlasoft.thirdeye.beans.php.PHPGroupWrapper;
import org.birlasoft.thirdeye.beans.php.PHPValueWrapper;
import org.birlasoft.thirdeye.beans.php.PHPWrapper;
import org.birlasoft.thirdeye.config.TransactionManagers;
import org.birlasoft.thirdeye.entity.AssetTemplate;
import org.birlasoft.thirdeye.entity.Questionnaire;
import org.birlasoft.thirdeye.entity.QuestionnaireAsset;
import org.birlasoft.thirdeye.search.api.beans.AssetParameterBean;
import org.birlasoft.thirdeye.search.api.beans.AssetParameterWrapper;
import org.birlasoft.thirdeye.service.AssetService;
import org.birlasoft.thirdeye.service.ExcelWriter;
import org.birlasoft.thirdeye.service.PHPService;
import org.birlasoft.thirdeye.service.ParameterSearchService;
import org.birlasoft.thirdeye.service.QuestionnaireAssetService;
import org.birlasoft.thirdeye.service.QuestionnaireService;
import org.birlasoft.thirdeye.util.ExcelUtility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(value=TransactionManagers.TENANTTRANSACTIONMANAGER)
public class PHPServiceImpl implements PHPService {
	
	@Autowired
	private QuestionnaireAssetService questionnaireAssetService;
	@Autowired
	private AssetService assetService;
	@Autowired
	private ParameterSearchService parameterSearchService;
	@Autowired
	private ExcelWriter excelWriter;
	@Autowired
	private QuestionnaireService questionnaireService;
	private static final String SHEET_PHP = "PHP";
	private static final String PHP_HEADING = "Asset/Parameter";
	
	@Override
	public Set<AssetTemplate> getAssetTemplatesFromQuestionnaire(Questionnaire questionnaire) {
		Set<AssetTemplate> assetTemplates = new HashSet<>();
		Set<QuestionnaireAsset> listOfQuestionnaireAsset = questionnaireAssetService.findByQuestionnaireLoadedAsset(questionnaire);
		for (QuestionnaireAsset questionnaireAsset : listOfQuestionnaireAsset) {
			if(!assetTemplates.contains(questionnaireAsset.getAsset().getAssetTemplate())){
				assetTemplates.add(questionnaireAsset.getAsset().getAssetTemplate());
			}
		}
		return assetTemplates;
	}
	
	@Override
	public Set<PHPWrapper> getPHPWrapperToPlot(Set<Integer> idsOfParameters, Set<Integer> idsOfAssetTemplate,
			Integer idOfQuestionnaire, Map<String, List<String>> filterMap) {
		Set<PHPWrapper> wrapperSet = new HashSet<>();
		Set<AssetBean> assetBeans = assetService.getSetOfAssetBeansByIdsAndQeId(idsOfAssetTemplate, idOfQuestionnaire);
        
        Set<PHPValueWrapper> valueWrappers = new HashSet<>();
        List<AssetParameterWrapper> assetParameterWrappers = new ArrayList<>();
        for (Integer oneParam : idsOfParameters) {			
        	AssetParameterWrapper assetParameterWrapper = parameterSearchService.getParameterValueFromElasticsearch(oneParam, idOfQuestionnaire, assetBeans, filterMap);
        	assetParameterWrappers.add(assetParameterWrapper);
		}

        List<String> parameterNames = prepareValueWrappersAndParamNames(valueWrappers, assetParameterWrappers);
        PHPWrapper phpWrapper = preaprePHPWrapper(valueWrappers,parameterNames);
        
        wrapperSet.add(phpWrapper);
        return wrapperSet;
	}
	
	private List<String> prepareValueWrappersAndParamNames(Set<PHPValueWrapper> valueWrappers,
			List<AssetParameterWrapper> assetParameterWrappers) {
		List<String> paramNames = new ArrayList<>();
        for (AssetParameterWrapper oneParameter : assetParameterWrappers) {
        	if(!oneParameter.getValues().isEmpty()) {        		
        		List<AssetParameterBean> listOfAssetBeans = oneParameter.getValues();
        		for (AssetParameterBean assetBean : listOfAssetBeans) {
        			PHPValueWrapper pvWrapper = new PHPValueWrapper();
        			pvWrapper.setRowDesc(assetBean.getAssetName());
        			pvWrapper.setColDesc(oneParameter.getParameter());
        			pvWrapper.setValue(assetBean.getParameterValue());
        			pvWrapper.setColor(assetBean.getColor());
        			valueWrappers.add(pvWrapper);
        		}
        		paramNames.add(oneParameter.getParameter());
        	}
        }
		return paramNames;
	}

	/**
	 * method to prepare PHP wrapper bean
	 * @param assetBeans
	 * @param valueWrappers
	 * @param paramNames
	 * @return {@code PHPWrapper}
	 */
	private PHPWrapper preaprePHPWrapper(Set<PHPValueWrapper> valueWrappers, List<String> paramNames) {
		Set<String> assets = new HashSet<>();
		for (PHPValueWrapper valueWrapper : valueWrappers) {
			assets.add(valueWrapper.getRowDesc());
		}
		
		List<String> assetName = new ArrayList<>();
		assetName.addAll(assets);
        List<PHPGroupWrapper> assetGroupList = new ArrayList<>();
        PHPGroupWrapper assetGroup = new PHPGroupWrapper();
        assetGroup.setKey("");
        assetGroup.setValue(assetName);
        assetGroupList.add(assetGroup);

        List<PHPGroupWrapper> paramGroupList = new ArrayList<>();
        PHPGroupWrapper paramGroup = new PHPGroupWrapper();
        paramGroup.setKey("");
        paramGroup.setValue(paramNames);
        paramGroupList.add(paramGroup);

        PHPWrapper phpWrapper = new PHPWrapper();
        phpWrapper.setAssets(assetGroupList);
        phpWrapper.setParameters(paramGroupList);
        phpWrapper.setWrappers(valueWrappers);
		return phpWrapper;
	}

	@Override
	public void downloadPHPExcel(Set<PHPWrapper> heatMapData, Integer idOfQuestionnaire, HttpServletResponse response) {
		Questionnaire questionnaire = questionnaireService.findOne(idOfQuestionnaire);
		String fileName = questionnaire.getName().replaceAll("\\s+", "_");
		XSSFWorkbook workbook = exportPHPExcel(heatMapData);
		excelWriter.writeExcel(response, workbook, fileName);
	}

	private XSSFWorkbook exportPHPExcel(Set<PHPWrapper> heatMapData) {
		XSSFWorkbook workbook = new XSSFWorkbook();
		XSSFSheet sheet1 = workbook.createSheet(SHEET_PHP);
		Row row0 = sheet1.createRow(0);
		Cell cell0 = row0.createCell(0);		
		cell0.setCellValue(PHP_HEADING);
		cell0.setCellStyle(ExcelUtility.getCellWithColor(workbook,HSSFColor.AQUA.index));	
		
		List<PHPWrapper> hmdlist = new ArrayList<>(heatMapData);
		PHPWrapper phpWrapper = hmdlist.get(0);
	
		Map<Integer,String> cellParameter = new HashMap<>();
		int paramCellCount =  populateParameter(row0, phpWrapper, cellParameter);
		
		populateAsset(sheet1, phpWrapper, paramCellCount, cellParameter);
		
		return workbook;
	}

	/**
	 * Method to populate Assets
	 * @param sheet1
	 * @param phpWrapper
	 * @param paramCellCount
	 * @param cellParameter
	 */
	private void populateAsset(XSSFSheet sheet1, PHPWrapper phpWrapper, int paramCellCount,
			Map<Integer, String> cellParameter) {
		int rowNum = 1;
		for (String oneAsset : phpWrapper.getAssets().get(0).getValue()) {
			Row row = sheet1.createRow(rowNum);
			for(int cellNum = 0; cellNum < paramCellCount; cellNum++){
				Cell cell = row.createCell(cellNum);
				if(cellNum==0){
				   cell.setCellValue(oneAsset);
				}else{
					 populateAssetParamValue(phpWrapper, cellParameter, oneAsset, cellNum, cell);
				}
			}
			rowNum++;
		}
	}

	/**
	 * Method to populate asset param value
	 * @param phpWrapper
	 * @param cellParameter
	 * @param oneAsset
	 * @param cellNum
	 * @param cell
	 */
	private void populateAssetParamValue(PHPWrapper phpWrapper, Map<Integer, String> cellParameter, String oneAsset,
			int cellNum, Cell cell) {
		for(PHPValueWrapper onePHPValueWrapper: phpWrapper.getWrappers()){
			 if(onePHPValueWrapper.getColDesc().equalsIgnoreCase(cellParameter.get(cellNum)) 
					 && onePHPValueWrapper.getRowDesc().equalsIgnoreCase(oneAsset)){
				 cell.setCellValue(onePHPValueWrapper.getValue().toString());
			 }
		 }
	}

	/**
	 * Method to populate parameters
	 * @param row0
	 * @param phpWrapper
	 * @param cellParameter
	 * @return
	 */
	private int populateParameter(Row row0, PHPWrapper phpWrapper,Map<Integer,String> cellParameter) {
		int paramCellCount =1;
		for (PHPGroupWrapper oneParamPGW : phpWrapper.getParameters()) {
			for(String oneParam : oneParamPGW.getValue()){
				Cell paramCell = row0.createCell(paramCellCount);
				paramCell.setCellValue(oneParam);
				cellParameter.put(paramCellCount, oneParam);
				paramCellCount++;
			}
		}
		return paramCellCount;
	}
}
