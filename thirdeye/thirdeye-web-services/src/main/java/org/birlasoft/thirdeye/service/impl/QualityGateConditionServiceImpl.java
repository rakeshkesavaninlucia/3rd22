package org.birlasoft.thirdeye.service.impl;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.birlasoft.thirdeye.beans.JSONQualityGateConditionMapper;
import org.birlasoft.thirdeye.beans.JSONQualityGateConditionValueMapper;
import org.birlasoft.thirdeye.beans.ParameterBean;
import org.birlasoft.thirdeye.beans.QualityGateConditionBean;
import org.birlasoft.thirdeye.config.TransactionManagers;
import org.birlasoft.thirdeye.entity.Parameter;
import org.birlasoft.thirdeye.entity.ParameterQualityGate;
import org.birlasoft.thirdeye.entity.QualityGate;
import org.birlasoft.thirdeye.entity.QualityGateCondition;
import org.birlasoft.thirdeye.entity.User;
import org.birlasoft.thirdeye.repositories.ParameterQualityGateRepository;
import org.birlasoft.thirdeye.repositories.QualityGateConditionRepository;
import org.birlasoft.thirdeye.service.CustomUserDetailsService;
import org.birlasoft.thirdeye.service.ParameterService;
import org.birlasoft.thirdeye.service.QualityGateConditionService;
import org.birlasoft.thirdeye.service.QualityGateService;
import org.birlasoft.thirdeye.util.Utility;
import org.birlasoft.thirdeye.colorscheme.constant.Condition;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service {@code class} of {@link QualityGateCondition} to {@code save, update, delete} , and 
 * find existing. It also calculates the color based on the parameter value
 * 
 * @author shaishav.dixit
 *
 */
@Service
@Transactional(value=TransactionManagers.TENANTTRANSACTIONMANAGER)
public class QualityGateConditionServiceImpl implements QualityGateConditionService {

	@Autowired
	private QualityGateConditionRepository qualityGateConditionRepository;
	
	@Autowired
	private ParameterService parameterService;
	
	@Autowired
	private QualityGateService qualityGateService;
	
	@Autowired
	private CustomUserDetailsService customUserDetailsService;
	
	@Autowired
	private ParameterQualityGateRepository parameterQualityGateRepository;
	
	@Override
	public QualityGateCondition save(QualityGateCondition qualityGateCondition) {
		QualityGateCondition qgc = qualityGateConditionRepository.save(qualityGateCondition);
		qgc.getParameter().getDisplayName();
		return qgc;
	}

	@Override
	public QualityGateCondition createConditionObject(List<Double> listOfValues, List<String> listOfColors,
			QualityGateConditionBean qualityGateCondition) {
		
		QualityGateCondition qgc = new QualityGateCondition();
		String jsonString = createJsonString(listOfValues, listOfColors);
		qgc.setConditionValues(jsonString);
		qgc.setParameter(parameterService.findOne(qualityGateCondition.getParameterId()));
		qgc.setGateCondition(Condition.LESS.toString());
		qgc.setQualityGate(qualityGateService.findOne(qualityGateCondition.getQualityGateId()));
		qgc.setWeight(new BigDecimal(qualityGateCondition.getWeight()));
		setCreatedAndUpdatedDetailsForCondition(qgc);
		return qgc;
	}

	/**
	 * @param qgc
	 */
	private void setCreatedAndUpdatedDetailsForCondition(QualityGateCondition qgc) {
		User currentUser = customUserDetailsService.getCurrentUser();
		
		qgc.setUserByCreatedBy(currentUser);
		qgc.setUserByUpdatedBy(currentUser);
		qgc.setCreatedDate(new Date());
		qgc.setUpdatedDate(new Date());
	}
	
	private String createJsonString(List<Double> listOfValues, List<String> listOfColors) {
		String jSONString;
		JSONQualityGateConditionMapper conditionMapper = generateConditionJSONMapper(listOfValues, listOfColors);
		jSONString = Utility.convertObjectToJSONString(conditionMapper);
		return jSONString;
	}

	/**
	 * This method generate a mapper object {@link JSONQualityGateConditionMapper} from 
	 * list of color and list of condition values.
	 * @param listOfValues
	 * @param listOfColors
	 * @return JSONQualityGateConditionMapper
	 */
	private JSONQualityGateConditionMapper generateConditionJSONMapper(List<Double> listOfValues, List<String> listOfColors) {
		JSONQualityGateConditionMapper conditionMapper = new JSONQualityGateConditionMapper();
		int ctr = 0;
		for (String color : listOfColors) {
			JSONQualityGateConditionValueMapper conditionValueMapper = new JSONQualityGateConditionValueMapper();
			conditionValueMapper.setColor(color);
			conditionValueMapper.setValue(listOfValues.get(ctr));
			ctr++;
			conditionMapper.addOption(conditionValueMapper);
		}
		return conditionMapper;
	}
	
	
	@Override
	public QualityGateConditionBean insertValueMapperList(QualityGateConditionBean qualityGateConditionBean,
			List<Double> listOfValues, List<String> listOfColors) {
		JSONQualityGateConditionMapper valueMapper = this.generateConditionJSONMapper(listOfValues, listOfColors);
		qualityGateConditionBean.setValueMapper(valueMapper.getValues());
		return qualityGateConditionBean;
	}

	@Override
	public List<JSONQualityGateConditionValueMapper> getGateConditions(String values) {
		List<JSONQualityGateConditionValueMapper> jsonValues = null;
		if(values != null){
			JSONQualityGateConditionMapper qualityGateConditionMapper = getQualityGateConditionMapper(values);
			jsonValues = qualityGateConditionMapper.getValues();
		}
		return jsonValues;
	}

	private JSONQualityGateConditionMapper getQualityGateConditionMapper(String values) {
		JSONQualityGateConditionMapper mappedObject = null;
		if (values != null){
			mappedObject = Utility.convertJSONStringToObject(values, JSONQualityGateConditionMapper.class);
		}
		return mappedObject;
	}

	@Override
	public List<QualityGateConditionBean> getAllConditions(Integer idOfQualityGate) {
		List<QualityGateConditionBean> listOfConditionBean = new ArrayList<>();
		List<QualityGateCondition> listOfConditions = qualityGateConditionRepository.findByQualityGate(qualityGateService.findOne(idOfQualityGate));
		for (QualityGateCondition qualityGateCondition : listOfConditions) {
			listOfConditionBean.add(createConditionBean(qualityGateCondition));
		}
		return listOfConditionBean;
	}

	@Override
	public QualityGateConditionBean createConditionBean(QualityGateCondition qualityGateCondition) {
		QualityGateConditionBean conditionBean = new QualityGateConditionBean();
		conditionBean.setId(qualityGateCondition.getId());
		conditionBean.setValueMapper(getGateConditions(qualityGateCondition.getConditionValues()));
		conditionBean.setParameterId(qualityGateCondition.getParameter().getId());
		conditionBean.setParameterName(qualityGateCondition.getParameter().getDisplayName());
		conditionBean.setQualityGateId(qualityGateCondition.getQualityGate().getId());
		conditionBean.setWeighted(this.isWeightedCondition(qualityGateCondition.getQualityGate().getId()));
		if(qualityGateCondition.getWeight() != null){
			conditionBean.setWeight(qualityGateCondition.getWeight().doubleValue());
		}
		return conditionBean;
	}

	@Override
	public Map<String, Integer> getConditionScale(QualityGate qualityGate) {
		Map<String, Integer> mapToReturn = new HashMap<>();
		List<QualityGateCondition> listOfConditions = qualityGateConditionRepository.findByQualityGate(qualityGate);
		List<JSONQualityGateConditionValueMapper> listOfValues = getGateConditions(listOfConditions.get(0).getConditionValues());
		for (int i = 0; i < listOfValues.size(); i++) {
			mapToReturn.put(listOfValues.get(i).getColor(), i+1);
		}
		return mapToReturn;
	}

	@Override
	public Map<ParameterBean, QualityGateCondition> getMapOfParameterAndCondition(QualityGate qualityGate) {
		Map<ParameterBean, QualityGateCondition> mapToReturn = new HashMap<>();
		
		List<QualityGateCondition> listOfConditions = qualityGateConditionRepository.findByQualityGate(qualityGate);
		for (QualityGateCondition qualityGateCondition : listOfConditions) {
			mapToReturn.put(new ParameterBean(qualityGateCondition.getParameter()), qualityGateCondition);
		}
		return mapToReturn;
	}

	@Override
	public String getColorBasedOnParameterValue(String conditionValues, Double parameterValue) {
		List<JSONQualityGateConditionValueMapper> listOfCondition = getGateConditions(conditionValues);
		String color = null;
		String maxConditionColor = null;
		for (JSONQualityGateConditionValueMapper jsonQualityGateConditionValueMapper : listOfCondition) {
			if (jsonQualityGateConditionValueMapper.getValue() != null && parameterValue < jsonQualityGateConditionValueMapper.getValue()){				
				color = jsonQualityGateConditionValueMapper.getColor();
				break;
			}
			if(jsonQualityGateConditionValueMapper.getValue() == null){
				maxConditionColor = jsonQualityGateConditionValueMapper.getColor();
			}
		}
		if (color == null){
			color = maxConditionColor;
		}
		return color;
	}

	@Override
	public String calculateAssetColor(Map<String, Integer> mapOfSeries, Map<QualityGateCondition, Double> mapOfConditionAndParameterValue) {
		String colorToReturn = null;
		BigDecimal weightedSum = new BigDecimal(0);
		BigDecimal divisor = new BigDecimal(0);
		for (Entry<QualityGateCondition, Double> entry : mapOfConditionAndParameterValue.entrySet()) {
			String oneColor = getColorBasedOnParameterValue(entry.getKey().getConditionValues(), entry.getValue());
			weightedSum = weightedSum.add(entry.getKey().getWeight().multiply(new BigDecimal(mapOfSeries.get(oneColor))));
			divisor = divisor.add(entry.getKey().getWeight());
		}
		
		BigDecimal avgColor = weightedSum.divide(divisor, 0, RoundingMode.HALF_UP);
		for (Entry<String, Integer> entry : mapOfSeries.entrySet()) {
			if(avgColor.intValue() == entry.getValue().intValue()){				
				colorToReturn = entry.getKey();
			}
		}
		return colorToReturn;
	}

	@Override
	public QualityGateCondition findOne(Integer idOfQualityGateCondition) {
		return qualityGateConditionRepository.findOne(idOfQualityGateCondition);
	}

	@Override
	public QualityGateConditionBean getConditionBean(Integer idOfQualityGateCondition) {
		QualityGateCondition qualityGateCondition = qualityGateConditionRepository.findOne(idOfQualityGateCondition);
		return createConditionBean(qualityGateCondition);
	}

	@Override
	public QualityGateCondition updateConditionObject(QualityGateConditionBean qualityGateConditionBean,
			List<Double> listOfValues, List<String> listOfColors) {
		User currentUser = customUserDetailsService.getCurrentUser();
		
		QualityGateCondition qgc = this.findOne(qualityGateConditionBean.getId());
		String jsonString = createJsonString(listOfValues, listOfColors);
		qgc.setConditionValues(jsonString);
		if(qualityGateConditionBean.getWeight() != null){			
			qgc.setWeight(new BigDecimal(qualityGateConditionBean.getWeight()));
		}
		qgc.setUserByUpdatedBy(currentUser);
		qgc.setUpdatedDate(new Date());
		return qgc;
	}

	@Override
	public void delete(Integer idOfQualityGateCondition) {
		qualityGateConditionRepository.delete(this.findOne(idOfQualityGateCondition));
	}

	@Override
	public void deleteInBatch(Set<QualityGateCondition> qualityGateConditions) {
		qualityGateConditionRepository.deleteInBatch(qualityGateConditions);
	}

	@Override
	public QualityGateCondition getDefaultCondition(QualityGate qualityGate, Parameter parameter) {
		
		QualityGateCondition qualityGateCondition = new QualityGateCondition();
		qualityGateCondition.setConditionValues(getDefaultConditionValues());
		qualityGateCondition.setParameter(parameter);
		qualityGateCondition.setQualityGate(qualityGate);
		qualityGateCondition.setWeight(null);
		qualityGateCondition.setGateCondition(Condition.LESS.toString());
		setCreatedAndUpdatedDetailsForCondition(qualityGateCondition);
		return qualityGateCondition;
	}

	@Override
	public String getDefaultConditionValues() {
		List<String> listOfDefaultColors = Arrays.asList("#fc8d59","#ffffbf","#91cf60");
		List<Double> listOfDefaultValues = Arrays.asList(2.0,5.0,null);
		return createJsonString(listOfDefaultValues, listOfDefaultColors);
	}

	@Override
	public boolean isWeightedCondition(Integer qualityGate) {
		if(qualityGate != null){			
			ParameterQualityGate parameterQualityGate = parameterQualityGateRepository.findByQualityGate(qualityGateService.findOne(qualityGate));
			if(parameterQualityGate != null){			
				return false;
			}
		}
		return true;
	}

	@Override
	public QualityGateConditionBean updateBean(QualityGateConditionBean qualityGateConditionBean) {
		if(qualityGateConditionBean.getQualityGateId() != null){
			qualityGateConditionBean.setWeighted(isWeightedCondition(qualityGateConditionBean.getQualityGateId()));
		} else {			
			qualityGateConditionBean.setWeighted(isWeightedCondition(extractQualityGateForCondition(qualityGateConditionBean.getId())));
		}
		if(!qualityGateConditionBean.isWeighted() && qualityGateConditionBean.getId() == null){
			List<QualityGateConditionBean> listOfConditionBeans = getAllConditions(qualityGateConditionBean.getQualityGateId());
			//there will only be one condition for parameter quality gate
			qualityGateConditionBean.setId(listOfConditionBeans.get(0).getId());
		}
		return qualityGateConditionBean;
	}

	private Integer extractQualityGateForCondition(Integer id) {
		QualityGateCondition qualityGateCondition = this.findOne(id);
		if(qualityGateCondition != null && qualityGateCondition.getQualityGate() != null){
			return qualityGateCondition.getQualityGate().getId();
		}
		return null;
	}
	
	@Override
	public Map<Integer, QualityGateCondition> getMapOfParameterIdAndCondition(QualityGate qualityGate) {
		Map<Integer, QualityGateCondition> mapToReturn = new HashMap<>();
		
		List<QualityGateCondition> listOfConditions = qualityGateConditionRepository.findByQualityGate(qualityGate);
		for (QualityGateCondition qualityGateCondition : listOfConditions) {
			mapToReturn.put(qualityGateCondition.getParameter().getId(), qualityGateCondition);
		}
		return mapToReturn;
	}

}
