package org.birlasoft.thirdeye.service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.birlasoft.thirdeye.beans.ParameterBean;
import org.birlasoft.thirdeye.beans.ParameterFunctionBean;
import org.birlasoft.thirdeye.entity.Parameter;
import org.birlasoft.thirdeye.entity.ParameterFunction;

/**
 * Service interface for parameter function.
 * @author samar.gupta
 */
public interface ParameterFunctionService {
	
    /**
     * Save the ParameterFunction Object.
     * @param parameter
     * @return {@code ParameterFunction} Object
     */
    public ParameterFunction save(ParameterFunction parameter);
    /**
     * Save list of parameter function in batch.
     * @param parameter
     * @return {@code List<ParameterFunction>}
     */
    public List<ParameterFunction> save(Iterable<ParameterFunction> parameter);
    /**
     * Find all parameter function.
     * @return {@code List<ParameterFunction>}
     */
    public List<ParameterFunction> findAll();
    /**
     * Find one parameter function by id.
     * @param id
     * @return {@code ParameterFunction}
     */
    public ParameterFunction findOne(Integer id);
    /**
     * Find list of parameter function by parameter parent id. 
     * @param parameter
     * @return {@code List<ParameterFunction>}
     */
    public List<ParameterFunction> findByParameterByParentParameterId(Parameter parameter);
    /**
     * Create new parameter function object.
     * @param savedParam
     * @param parentParameter
     * @return {@code ParameterFunction}
     */
    public ParameterFunction createNewParameterFunctionObject(Parameter savedParam, Parameter parentParameter);
    /**
     * Get list of parameter functions to be saved.
     * @param setOfQuestions
     * @param parameter
     * @param setOfMandatoryQuestion
     * @return {@code List<ParameterFunction>}
     */
    public List<ParameterFunction> getListOfParameterFunctionsToBeSaved(Set<Integer> setOfQuestions, Parameter parameter, Set<Integer> setOfMandatoryQuestion);
    /**
     * Update parameter function object.
     * @param functionBean
     * @param onePf
     * @return {@code ParameterFunction}
     */
    public ParameterFunction updateParameterFunctionObject(ParameterFunctionBean functionBean, ParameterFunction onePf);
    
    /**
     * Find by mandatory question.
     * @param isMandatoryQuestion
     * @return {@code List<ParameterFunction>}
     */
    public List<ParameterFunction> findByMandatoryQuestion(boolean isMandatoryQuestion);
    
	/**
	 * Get all child parameters
	 * @param oneParameter
	 * @return
	 */
	public String getParameterTree(Parameter oneParameter);
	
	/**
	 * Get parameter functions by parent parameter
	 * @param parameterByParameterId
	 * @return
	 */
	public List<ParameterFunction> findByParameterByParentParameterIdAndParameterByChildparameterIdIsNull(Parameter parameterByParameterId);
	
	/**
	 * Delete parameter functions in bulk
	 * @param parameterFunctions
	 */
	public void deleteInBatch(List<ParameterFunction> parameterFunctions);
	/**
	 * save or update parameter function
	 * @param parameterBean
	 * @param map
	 * @param savedParameter
	 * @param mandatoryQuestionIds
	 */
	public void saveOrUpdateParameterFuntion(ParameterBean parameterBean, Map<Integer, BigDecimal> map, Parameter savedParameter, Set<Integer> mandatoryQuestionIds);
	
	/**
     * Find list of child parameter from parameter function by list of parameter parent id
     * in case of TCO cost Structure. 
     * @param parameters
     * @return {@code List<ParameterFunction>}
     */
	
	public Set<Parameter> findByParameterByParentParameterIdIn(Set<Parameter> parameters);
	/**
	 * Fetch parameter function which has parameter as a child parameter.
	 * @param param
	 * @return {@code Set<ParameterFunction>}
	 */
	public Set<ParameterFunction> findByParameterByChildparameterId(Parameter param);
	/**
	 * Delete in batch, all parameter function
	 * @param parameterFunctionToDelete
	 */
	public void deleteInBatch(Set<ParameterFunction> parameterFunctionToDelete);
	
}
