package org.birlasoft.thirdeye.beans.add;
/**
 * Bean class fro ADD
 * 
 */
public class AddDependsOnBean {
	
	private String asset;
	private String relationshipName;
	private String direction;
	private String relationshipDisplayName;
	
	public String getAsset() {
		return asset;
	}
	
	public void setAsset(String asset) {
		this.asset = asset;
	}
	
	public String getRelationshipName() {
		return relationshipName;
	}
	
	public void setRelationshipName(String relationshipName) {
		this.relationshipName = relationshipName;
	}

	public String getDirection() {
		return direction;
	}

	public void setDirection(String direction) {
		this.direction = direction;
	}

	public String getRelationshipDisplayName() {
		return relationshipDisplayName;
	}

	public void setRelationshipDisplayName(String relationshipDisplayName) {
		this.relationshipDisplayName = relationshipDisplayName;
	}
}
