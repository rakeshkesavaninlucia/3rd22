package org.birlasoft.thirdeye.interfaces;

import java.math.BigDecimal;

public interface QuantifiableResponse {

	/**
	 * Provide a quantifiable response for the response that has
	 * been recorded by the user
	 * 
	 * @return
	 */
	BigDecimal fetchQuantifiableResponse();
}
