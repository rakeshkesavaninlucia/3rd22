package org.birlasoft.thirdeye.beans.index;

public class IncrementBcmIndexRequestBean {
	
	private Integer bcmId ;
	private String tenantId ;

	public Integer getBcmId() {
		return bcmId;
	}

	public void setBcmId(Integer bcmId) {
		this.bcmId = bcmId;
	}

	public String getTenantId() {
		return tenantId;
	}

	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}

}
