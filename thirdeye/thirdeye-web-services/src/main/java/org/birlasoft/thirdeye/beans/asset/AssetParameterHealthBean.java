package org.birlasoft.thirdeye.beans.asset;

import java.math.BigDecimal;

import org.birlasoft.thirdeye.beans.AssetBean;
import org.birlasoft.thirdeye.beans.ParameterBean;
import org.birlasoft.thirdeye.entity.Questionnaire;

public class AssetParameterHealthBean {
	
	private AssetBean assetBean;
	private Questionnaire questionnaire;
	private ParameterBean parameterBean;
	private BigDecimal parameterValue;
	public AssetBean getAssetBean() {
		return assetBean;
	}
	public void setAssetBean(AssetBean assetBean) {
		this.assetBean = assetBean;
	}
	public Questionnaire getQuestionnaire() {
		return questionnaire;
	}
	public void setQuestionnaire(Questionnaire questionnaire) {
		this.questionnaire = questionnaire;
	}
	public ParameterBean getParameterBean() {
		return parameterBean;
	}
	public void setParameterBean(ParameterBean parameterBean) {
		this.parameterBean = parameterBean;
	}
	public BigDecimal getParameterValue() {
		return parameterValue;
	}
	public void setParameterValue(BigDecimal parameterValue) {
		this.parameterValue = parameterValue;
	} 

}
