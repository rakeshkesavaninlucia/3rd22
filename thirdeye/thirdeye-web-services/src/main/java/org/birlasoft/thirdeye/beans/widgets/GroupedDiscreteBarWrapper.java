package org.birlasoft.thirdeye.beans.widgets;

import java.util.HashMap;
import java.util.Map;

public class GroupedDiscreteBarWrapper {

	private String label;
	private Map<String, Integer> map = new HashMap<>();
	
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public Map<String, Integer> getMap() {
		return map;
	}
	public void setMap(Map<String, Integer> map) {
		this.map = map;
	}
	
	public void put(String key, Integer value) {
		map.put(key, value);
	}
}
