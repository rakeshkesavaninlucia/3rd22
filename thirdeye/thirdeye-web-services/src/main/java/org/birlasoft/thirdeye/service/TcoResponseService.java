package org.birlasoft.thirdeye.service;

import java.util.List;

import org.birlasoft.thirdeye.beans.tco.ChartOfAccountBean;
import org.birlasoft.thirdeye.beans.tco.ChartOfAccountGridWrapper;
import org.birlasoft.thirdeye.entity.Questionnaire;
import org.birlasoft.thirdeye.entity.QuestionnaireQuestion;
import org.birlasoft.thirdeye.entity.Response;

public interface TcoResponseService {

	public List<ChartOfAccountBean> extractChartOfAccounts(Questionnaire questionnaire);
	
	/**
	 * @param questionnaire
	 * @param qResponse
	 * @return
	 */
	public ChartOfAccountGridWrapper getCostStructureName(Questionnaire questionnaire,Response qResponse);
	 
	/**
	 * @param questionnaireQuestions
	 * @return
	 */
	public List<String> getAssetName(List<QuestionnaireQuestion> questionnaireQuestions);

}
