/**
 * 
 */
package org.birlasoft.thirdeye.beans.php;

import java.util.List;

/**
 * @author shaishav.dixit
 *
 */
public class PHPGroupWrapper {
	
	private String key;
	List<String> value;
	
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public List<String> getValue() {
		return value;
	}
	public void setValue(List<String> value) {
		this.value = value;
	}	

}
