package org.birlasoft.thirdeye.beans.relationship;

import java.util.List;

import org.birlasoft.thirdeye.beans.AssetBean;
import org.birlasoft.thirdeye.entity.RelationshipAssetData;

/**
 * This bean class {@link RelationshipAssetDataBean} is used for 
 * display parent/child asset.
 * @author samar.gupta
 *
 */
public class RelationshipAssetDataBean {
	
	private Integer id;
	private AssetBean parentAsset;
	private AssetBean childAsset;
	private Integer assetIdOfRelationship;
	private AssetBean relationshipAsset;
	private String displayName;
	private String dataType;
	private String frequency;
	private List<AssetBean> listOfAssets;
	
	public RelationshipAssetDataBean() {
		// Default constructor
	}
	
	/**
	 * Constructor for {@code RelationshipAssetDataBean} bean from entity 
	 * @param relationshipAssetData
	 */
	public RelationshipAssetDataBean(RelationshipAssetData relationshipAssetData) {
		this.id = relationshipAssetData.getId();
		this.parentAsset = new AssetBean(relationshipAssetData.getAssetByParentAssetId());
		this.childAsset = new AssetBean(relationshipAssetData.getAssetByChildAssetId());
		this.assetIdOfRelationship = relationshipAssetData.getAssetByAssetId().getId();
		this.relationshipAsset = new AssetBean(relationshipAssetData.getAssetByAssetId());
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public AssetBean getParentAsset() {
		return parentAsset;
	}
	public void setParentAsset(AssetBean parentAsset) {
		this.parentAsset = parentAsset;
	}
	public AssetBean getChildAsset() {
		return childAsset;
	}
	public void setChildAsset(AssetBean childAsset) {
		this.childAsset = childAsset;
	}

	public Integer getAssetIdOfRelationship() {
		return assetIdOfRelationship;
	}

	public void setAssetIdOfRelationship(Integer assetIdOfRelationship) {
		this.assetIdOfRelationship = assetIdOfRelationship;
	}

	public AssetBean getRelationshipAsset() {
		return relationshipAsset;
	}

	public void setRelationshipAsset(AssetBean relationshipAsset) {
		this.relationshipAsset = relationshipAsset;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String getDataType() {
		return dataType;
	}

	public void setDataType(String dataType) {
		this.dataType = dataType;
	}

	public String getFrequency() {
		return frequency;
	}

	public void setFrequency(String frequency) {
		this.frequency = frequency;
	}
	
	public List<AssetBean> getListOfAssets() {
		return listOfAssets;
	}

	public void setListOfAssets(List<AssetBean> listOfAssets) {
		this.listOfAssets = listOfAssets;
	}
}
