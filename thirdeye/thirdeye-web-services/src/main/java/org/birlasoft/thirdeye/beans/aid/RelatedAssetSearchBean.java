package org.birlasoft.thirdeye.beans.aid;

import java.util.ArrayList;
import java.util.List;

/**
 * @author samar.gupta
 *
 */
public class RelatedAssetSearchBean {

	private Integer assetId;
	private String asset;
	private String assetStyle;
	
	private List<RelationshipAssetSearchDataBean> listOfRelationshipAssetData = new ArrayList<>();

	public String getAsset() {
		return asset;
	}

	public void setAsset(String asset) {
		this.asset = asset;
	}

	public List<RelationshipAssetSearchDataBean> getListOfRelationshipAssetData() {
		return listOfRelationshipAssetData;
	}

	public void setListOfRelationshipAssetData(List<RelationshipAssetSearchDataBean> listOfRelationshipAssetData) {
		this.listOfRelationshipAssetData = listOfRelationshipAssetData;
	}

	public Integer getAssetId() {
		return assetId;
	}

	public void setAssetId(Integer assetId) {
		this.assetId = assetId;
	}

	public String getAssetStyle() {
		return assetStyle;
	}

	public void setAssetStyle(String assetStyle) {
		this.assetStyle = assetStyle;
	}
}
