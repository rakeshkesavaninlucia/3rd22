package org.birlasoft.thirdeye.service;

import java.util.List;

import org.birlasoft.thirdeye.beans.QualityGateBean;
import org.birlasoft.thirdeye.entity.Parameter;
import org.birlasoft.thirdeye.entity.QualityGate;
import org.birlasoft.thirdeye.entity.QualityGateCondition;
import org.birlasoft.thirdeye.entity.Workspace;

/**
 * Service {@code interface} for {@code QualityGate}
 * @author shaishav.dixit
 *
 */
public interface QualityGateService {

	/**
	 * Find {@link QualityGate} by {@code name} and {@link Workspace}
	 * @param name
	 * @param workspace
	 * @return
	 */
	public QualityGate findByNameAndWorkspace(String name, Workspace workspace);

	/**
	 * Create {@link QualityGate} entity object from {@link QualityGateBean} object.
	 * @param qualityGateBean
	 * @return
	 */
	public QualityGate createQualityGateNewObject(QualityGateBean qualityGateBean);

	/**
	 * Save {@link QualityGate}
	 * @param assetQualityGate
	 * @return
	 */
	public QualityGate save(QualityGate assetQualityGate);

	/**
	 * Get all {@link QualityGate} for a {@link Workspace}
	 * @param workspace
	 * @return
	 */
	public List<QualityGate> findByWorkspace(Workspace workspace);

	/**
	 * Find {@link QualityGate} by id.
	 * @param qualityGateId
	 * @return
	 */
	public QualityGate findOne(Integer qualityGateId);

	/**
	 * Update {@link QualityGate}
	 * @param incomingQualityGate
	 * @return
	 */
	public QualityGate updateQualityGateObject(QualityGateBean incomingQualityGate);

	/**
	 * Delete {@link QualityGate} and all its associated {@link QualityGateCondition}
	 * @param idOfQualityGate
	 */
	public void delete(Integer idOfQualityGate);

	/**
	 * Create a default {@link QualityGate} for a {@link Parameter}
	 * @param parameter
	 */
	public void createDefaultQualityGateForParameter(Parameter parameter);

	/**
	 * Get a {@code List} of all {@link QualityGate} excluding {@link Parameter} {@code quality gates}
	 * @return
	 */
	public List<QualityGate> findAssetQualityGates();
	
	public QualityGate insertDescriptionMapperList(QualityGate qualityGate, List<String> listOfDescription,
			List<String> listOfColors);

	public QualityGateBean createQGBean(QualityGate qualityGate);

	public QualityGateBean getQGBean(Integer idOfQualityGate);
}
