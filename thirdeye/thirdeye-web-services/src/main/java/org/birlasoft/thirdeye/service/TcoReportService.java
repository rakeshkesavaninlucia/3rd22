package org.birlasoft.thirdeye.service;

import java.util.List;

import org.birlasoft.thirdeye.beans.tco.TCOSunburstWrapperBean;
import org.birlasoft.thirdeye.beans.tco.TCOAssetWrapper;

public interface TcoReportService {

	public TCOSunburstWrapperBean getTcoSunburstWrapperBean(Integer chartOfAccountId);

	public List<TCOAssetWrapper> getAssetDetailsForTcoReport(Integer chartOfAccountId,
			Integer costStructureId);

}
