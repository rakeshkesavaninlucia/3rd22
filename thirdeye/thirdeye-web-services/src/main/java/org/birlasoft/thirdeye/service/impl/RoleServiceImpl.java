package org.birlasoft.thirdeye.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.birlasoft.thirdeye.config.TransactionManagers;
import org.birlasoft.thirdeye.entity.Role;
import org.birlasoft.thirdeye.entity.RolePermission;
import org.birlasoft.thirdeye.entity.User;
import org.birlasoft.thirdeye.entity.UserRole;
import org.birlasoft.thirdeye.repositories.RolePermissionRepository;
import org.birlasoft.thirdeye.repositories.RoleRepository;
import org.birlasoft.thirdeye.service.RoleService;
import org.birlasoft.thirdeye.service.UserRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
@Service
@Transactional(value=TransactionManagers.TENANTTRANSACTIONMANAGER)
public class RoleServiceImpl implements RoleService {
	@Autowired
	public RoleRepository roleRepository;
	
	@Autowired
	public RolePermissionRepository rolePermissionRepository;
	@Autowired
	private UserRoleService userRoleService;
	
	@Override
	public List<Role> findAll() {
		return roleRepository.findAll();
	}

	@Override
	public Role findOne(Integer id) {
		return roleRepository.findOne(id);
	}
	
	@Override
	public Role save(Role oneRole) {
		return roleRepository.save(oneRole);
	}
	
	@Override
	public void delete(Set<RolePermission> listOfRolePermsToDelete) {
		rolePermissionRepository.deleteInBatch(listOfRolePermsToDelete);
	}
	
	@Override
	public void saveRolePermissions(Set<RolePermission> listOfRolePermsToDelete) {
		rolePermissionRepository.save(listOfRolePermsToDelete);
	}

	@Override
	public Role findByIdLoadedRolePermissions(Integer id) {
		return roleRepository.findByIdLoadedRolePermissions(id);
	}
	@Override
	public Role createUserRole(Role role){
			
		role.setRoleName(role.getRoleName());
		role.setRoleDescription(role.getRoleDescription());
		return role;
	}

	@Override
	public void softDeleteUserRole(Integer userRoleId) {
		roleRepository.updateDeleteStatus(userRoleId);
	}

	@Override
	public List<Role> findByRoleAndDeleteStatus(Boolean deleteStatus) {
		return roleRepository.findByDeleteStatus(deleteStatus);
	}

	@Override
	public boolean isUserExists(Integer roleId) {
		Boolean isEnabled = false;
		List<UserRole> listOfUserRole = userRoleService.findUserByRoleId(roleId);
		List<User> listOfUser = new ArrayList<>();
		listOfUserRole.forEach(x-> listOfUser.add(x.getUser()));
		for(User userObject : listOfUser){
			if(!listOfUserRole.isEmpty() && userObject.isDeleteStatus() == false){
			isEnabled = true;
			break;
	     	} 
		}
		return isEnabled;
	}
}
