package org.birlasoft.thirdeye.service.impl;

import java.util.Date;

import org.birlasoft.thirdeye.beans.JSONNumberQuestionMapper;
import org.birlasoft.thirdeye.config.TransactionManagers;
import org.birlasoft.thirdeye.constant.Constants;
import org.birlasoft.thirdeye.constant.QuestionMode;
import org.birlasoft.thirdeye.constant.QuestionType;
import org.birlasoft.thirdeye.entity.Question;
import org.birlasoft.thirdeye.entity.User;
import org.birlasoft.thirdeye.service.CategoryService;
import org.birlasoft.thirdeye.service.CostElementService;
import org.birlasoft.thirdeye.service.CustomUserDetailsService;
import org.birlasoft.thirdeye.service.QuestionService;
import org.birlasoft.thirdeye.util.Utility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation class for Cost Element
 * @author dhruv.sood
 */
@Service
@Transactional(value=TransactionManagers.TENANTTRANSACTIONMANAGER)
public class CostElementServiceImpl implements CostElementService {	

	@Autowired
	private QuestionService questionService;
	@Autowired
	private CustomUserDetailsService customUserDetailsService;
	@Autowired
	private CategoryService categoryService;
	

	/**Method to create Cost Element object
	 * @author dhruv.sood
	 * @param question
	 * @return question
	 */
	@Override
	public Question createCostElementObject(Question question) {
				
		//Getting the current user from workspace
		User currentUser = customUserDetailsService.getCurrentUser();		
		//Setting up the cost element attributes
		question.setQuestionMode(QuestionMode.FIXED.toString());
		question.setDisplayName(question.getTitle());
		question.setWorkspace(customUserDetailsService.getActiveWorkSpaceForUser());
		question.setQuestionType(QuestionType.CURRENCY.toString());		
		question.setLevel((short) 0);
		question.setUserByCreatedBy(currentUser);
		question.setCreatedDate(new Date());	
		question.setUserByUpdatedBy(currentUser);
		question.setUpdatedDate(new Date());
		question.setCategory(categoryService.getCategoryFromName(Constants.__SYS_TCO));		
		question.setQueTypeText(Utility.convertObjectToJSONString(new JSONNumberQuestionMapper()));
		return question;
	}
	
	/**Method to save Cost Element object
	 * @author dhruv.sood
	 * @param question	
	 */
	@Override
	public void saveCostElement(Question question) {			
		//Saving one cost element to database
		questionService.save(question);
	}

	

	
}
