package org.birlasoft.thirdeye.beans;

import java.math.BigDecimal;

import org.birlasoft.thirdeye.interfaces.QuantifiableResponse;


/**
 * This object will be used for JSON serialization
 * @author samar.gupta
 */
public class JSONNumberResponseMapper implements QuantifiableResponse{

	private Double quantifier;
	private Double responseNumber;
	
	public JSONNumberResponseMapper() {
	}

	public Double getQuantifier() {
		return quantifier;
	}

	public void setQuantifier(Double quantifier) {
		this.quantifier = quantifier;
	}

	public Double getResponseNumber() {
		return responseNumber;
	}

	public void setResponseNumber(Double responseNumber) {
		this.responseNumber = responseNumber;
	}

	@Override
	public BigDecimal fetchQuantifiableResponse() {
		BigDecimal valueToBeReturned = new BigDecimal(0);
		
		if (quantifier != null){
			valueToBeReturned = valueToBeReturned.add(new BigDecimal(quantifier));
		}
		
		
		return valueToBeReturned;
	}
}
