/**
 * 
 */
package org.birlasoft.thirdeye.interfaces;

import java.util.List;

import org.birlasoft.thirdeye.beans.AssetBean;
import org.birlasoft.thirdeye.beans.ParameterEvaluationResponse;

/**
 * Functional Interface to evaluate parameter value
 * @author shaishav.dixit
 *
 */
@FunctionalInterface
public interface ParameterEvaluator {

	List<ParameterEvaluationResponse> evaluateParameter(List<AssetBean> assets);
}
