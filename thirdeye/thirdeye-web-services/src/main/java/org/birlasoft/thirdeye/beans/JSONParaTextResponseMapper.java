package org.birlasoft.thirdeye.beans;

import java.math.BigDecimal;

import org.birlasoft.thirdeye.interfaces.QuantifiableResponse;


/**
 * This object will be used for JSON serialization
 * 
 * @author samar.gupta
 *
 */
public class JSONParaTextResponseMapper implements QuantifiableResponse{

	private String responseParaText;
	private Double score;

	public JSONParaTextResponseMapper() {
		super();
	}

	public String getResponseParaText() {
		return responseParaText;
	}

	public void setResponseParaText(String responseParaText) {
		this.responseParaText = responseParaText;
	}

	public Double getScore() {
		return score;
	}

	public void setScore(Double score) {
		this.score = score;
	}
	
	@Override
	public BigDecimal fetchQuantifiableResponse() {
		BigDecimal valueToBeReturned = new BigDecimal(0);
		
		if (score != null){
			valueToBeReturned = valueToBeReturned.add(new BigDecimal(score));
		}
		
		
		return valueToBeReturned;
	}
}
