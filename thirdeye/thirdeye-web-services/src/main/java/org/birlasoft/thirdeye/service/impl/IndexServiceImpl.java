package org.birlasoft.thirdeye.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.birlasoft.thirdeye.beans.index.IndexRequestBean;
import org.birlasoft.thirdeye.beans.index.IndexResponseBean;
import org.birlasoft.thirdeye.config.TransactionManagers;
import org.birlasoft.thirdeye.constant.Constants;
import org.birlasoft.thirdeye.entity.UserWorkspace;
import org.birlasoft.thirdeye.service.CustomUserDetailsService;
import org.birlasoft.thirdeye.service.IndexService;
import org.hibernate.context.spi.CurrentTenantIdentifierResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

/**
 * @author sunil1.gupta
 *
 */
@Service
@Transactional(value=TransactionManagers.TENANTTRANSACTIONMANAGER)
public class IndexServiceImpl implements IndexService {
	
	private static Logger logger = LoggerFactory.getLogger(IndexServiceImpl.class);
	
	@Value("#{systemProperties['" + Constants.ELASTICSEARCH_HOST_NAME + "']}")
	private String elasticSearchHost;
	
	@Autowired
	private CurrentTenantIdentifierResolver currentTenantIdentifierResolver;
	@Autowired
	private CustomUserDetailsService customUserDetailsService;

	@Override
	public String buildIndexForTenant() {
		
		String uri = elasticSearchHost + "thirdeye-search-web/api/buildFullIndex/tenant/{tenantURL}";
		String tenantURL=currentTenantIdentifierResolver.resolveCurrentTenantIdentifier();
		String[] workspaceIds =getCurruntUserWorkSpaceIds();		
		
		Map<String, String> uriParams = new HashMap<>();
		uriParams.put("tenantURL", tenantURL);
		
	    IndexRequestBean requestBean = new IndexRequestBean();
	    requestBean.setTenantURL(tenantURL);
	    requestBean.setWsIds(workspaceIds);
	    
	    RestTemplate restTemplate = new RestTemplate(); 
	    IndexResponseBean responseBean = restTemplate.postForObject(uri,requestBean,IndexResponseBean.class,uriParams);		    
	    return responseBean.getMessage();
		
	}
	
	/**
	 * method to get the current user workspaceIds
	 * @return wsIds
	 */
	private String[] getCurruntUserWorkSpaceIds() {
		Set<UserWorkspace> userWorkSpaces = customUserDetailsService.getCurrentUser().getUserWorkspaces();		
		List<String>listOfId = new ArrayList<>();
		for(UserWorkspace uw : userWorkSpaces){
			listOfId.add(String.valueOf(uw.getWorkspace().getId()));
		}
		
		String[] wsIds = listOfId.stream().toArray(String[]::new);
		return wsIds;
	}	
	
}
