package org.birlasoft.thirdeye.beans.widgets;

import org.birlasoft.thirdeye.comparator.Sequenced;
import org.birlasoft.thirdeye.constant.WidgetTypes;
import org.birlasoft.thirdeye.entity.Widget;
import org.birlasoft.thirdeye.util.Utility;
import org.springframework.util.StringUtils;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


@JsonIgnoreProperties(ignoreUnknown = true, value={"sequenceNumber","widgetType","dashboardId"})
public class BaseWidgetJSONConfig implements Sequenced {

	protected int id;
	protected int dashboardId;
	protected int sequenceNumber;
	protected WidgetTypes widgetType;
	
	protected int width;
	protected int height;
	protected String title;
	
	public BaseWidgetJSONConfig(){}
	
	public BaseWidgetJSONConfig(Widget w) {
		this.widgetType = WidgetTypes.valueOf(w.getWidgetType());
		this.id = w.getId();
		this.sequenceNumber = w.getSequenceNumber();
		
		if (!StringUtils.isEmpty(w.getWidgetConfig())){
			BaseWidgetJSONConfig deserialized = Utility.convertJSONStringToObject(w.getWidgetConfig(), BaseWidgetJSONConfig.class);
			if (deserialized != null){
				this.title = deserialized.title;
			}
		}
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Integer getSequenceNumber() {
		return sequenceNumber;
	}
	public void setSequenceNumber(int sequenceNumber) {
		this.sequenceNumber = sequenceNumber;
	}
	public int getWidth() {
		return width;
	}
	public void setWidth(int width) {
		this.width = width;
	}
	public int getHeight() {
		return height;
	}
	public void setHeight(int height) {
		this.height = height;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public int getDashboardId() {
		return dashboardId;
	}
	public void setDashboardId(int dashboardId) {
		this.dashboardId = dashboardId;
	}
	public WidgetTypes getWidgetType() {
		return widgetType;
	}
	public void setWidgetType(WidgetTypes widgetType) {
		this.widgetType = widgetType;
	}
	
}
