/**
 * 
 */
package org.birlasoft.thirdeye.beans.bcm;


/**
 * @author shaishav.dixit
 *
 */
public class BCMValueWrapper   {
	
	private String l1Desc;
	private int l1Id;
	private int l2Id;
	private String categoryDesc;
	private int categoryId;
	private String value;
	private String hexColor;
	private Integer sequenceNumber;	
	
	
	public String getL1Desc() {
		return l1Desc;
	}
	public void setL1Desc(String l1Desc) {
		this.l1Desc = l1Desc;
	}
	public int getL1Id() {
		return l1Id;
	}
	public void setL1Id(int l1Id) {
		this.l1Id = l1Id;
	}
	public int getL2Id() {
		return l2Id;
	}
	public void setL2Id(int l2Id) {
		this.l2Id = l2Id;
	}
	public String getCategoryDesc() {
		return categoryDesc;
	}
	public void setCategoryDesc(String categoryDesc) {
		this.categoryDesc = categoryDesc;
	}
	public int getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public String getHexColor() {
		return hexColor;
	}
	public void setHexColor(String hexColor) {
		this.hexColor = hexColor;
	}
	public Integer getSequenceNumber() {
		return sequenceNumber;
	}
	public void setSequenceNumber(Integer sequenceNumber) {
		this.sequenceNumber = sequenceNumber;
	}

}
