package org.birlasoft.thirdeye.beans;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Set;

import org.birlasoft.thirdeye.entity.RolePermission;
import org.birlasoft.thirdeye.entity.User;
import org.birlasoft.thirdeye.entity.UserRole;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
/**
 * 
 * Bean class for User Security
 *
 */
public class SecurityUser extends User implements UserDetails{

	private static final long serialVersionUID = -4161052457456628923L;

	/**
	 * SecurityUser parameterized constructor
	 * @param user
	 */
	public SecurityUser(User user) { 
		if(user != null) { 
			this.setId(user.getId()); 
			this.setEmailAddress(user.getEmailAddress());
			this.setPassword(user.getPassword()); 
			this.setUserRoles(user.getUserRoles());
			this.setUserWorkspaces(user.getUserWorkspaces());
			this.setFirstName(user.getFirstName());
			this.setLastName(user.getLastName());
			this.setCreatedDate(user.getCreatedDate());			
			this.setAccountEnabled(user.isAccountEnabled());
			this.setAccountExpired(user.isAccountExpired());
			this.setAccountLocked(user.isAccountLocked());
			this.setCredentialExpired(user.isCredentialExpired());
			this.setDeleteStatus(user.isDeleteStatus());
			this.setPhoto(user.getPhoto());
		}	
	} 

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		Collection<GrantedAuthority> authorities = new ArrayList<>();
		Set<UserRole> userRoles = this.getUserRoles(); 
		if(userRoles != null) { 
			for (UserRole oneRoleMapping : userRoles) {
				for (RolePermission oneRolePermission : oneRoleMapping.getRole().getRolePermissions()){
					SimpleGrantedAuthority authority = new SimpleGrantedAuthority(oneRolePermission.getPermissionname());
					authorities.add(authority); 
				}
			}
		}
		// To be fixed later!
		return authorities;
	} 

	@Override
	public String getPassword() {
		return super.getPassword();
	}

	@Override
	public String getUsername() {
		return super.getEmailAddress();
	}
	
	
	@Override
	public boolean isAccountNonExpired() {
		return !this.isAccountExpired();
	}

	@Override
	public boolean isAccountNonLocked() {
		return !this.isAccountLocked();
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return !this.isCredentialExpired();
	}

	@Override
	public boolean isEnabled() {
		return !this.isAccountEnabled();
	}
}
