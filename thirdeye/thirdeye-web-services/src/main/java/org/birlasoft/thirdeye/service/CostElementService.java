package org.birlasoft.thirdeye.service;

import org.birlasoft.thirdeye.entity.Question;
/**
 * This is {@code service} interface for cost element.
 * @author dhruv.sood
 */
public interface CostElementService {
	
	/**
	 * Method to create the Cost Element Object 
	 * @param question
	 * @return {@code Questionnaire}
	 * @author dhruv.sood
	 */
	public Question createCostElementObject(Question question);
	
	/**
	 * Method to save the Cost Element 
	 * @param question
	 * @return {@code Questionnaire}
	 * @author dhruv.sood
	 */
	public void saveCostElement(Question question);
	
	
	

	
}
