package org.birlasoft.thirdeye.beans;

public class WorldMapWrapper {
	
 private String countryName;
 private Integer numberOfUsers;
 private Integer numberOfApplications;
 private String id;
 
 public WorldMapWrapper() {

	}
public WorldMapWrapper(String countryName, Integer numberOfUsers, Integer numberOfApplications,String id) {
	super();
	this.countryName = countryName;
	this.numberOfUsers = numberOfUsers;
	this.numberOfApplications = numberOfApplications;
	this.id = id;
}
public String getCountryName() {
	return countryName;
}
public void setCountryName(String countryName) {
	this.countryName = countryName;
}
public Integer getNumberOfUsers() {
	return numberOfUsers;
}
public void setNumberOfUsers(Integer numberOfUsers) {
	this.numberOfUsers = numberOfUsers;
}
public Integer getNumberOfApplications() {
	return numberOfApplications;
}
public void setNumberOfApplications(Integer numberOfApplications) {
	this.numberOfApplications = numberOfApplications;
}
public String getId() {
	return id;
}
public void setId(String id) {
	this.id = id;
}
	
}
