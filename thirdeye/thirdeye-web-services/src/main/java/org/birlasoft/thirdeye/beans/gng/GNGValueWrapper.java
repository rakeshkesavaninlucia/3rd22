package org.birlasoft.thirdeye.beans.gng;

import java.math.BigDecimal;

public class GNGValueWrapper {
	
	private Integer assetId ;
	private String  assetName ;
	private Integer parameterId ;
	private String parameterName;
	private BigDecimal parameterValue;
	private String color;
	private String description;
	private String treatment;
	private Integer percentage;
	
	public Integer getAssetId() {
		return assetId;
	}
	public void setAssetId(Integer assetId) {
		this.assetId = assetId;
	}
	public String getAssetName() {
		return assetName;
	}
	public void setAssetName(String assetName) {
		this.assetName = assetName;
	}
	public Integer getParameterId() {
		return parameterId;
	}
	public void setParameterId(Integer parameterId) {
		this.parameterId = parameterId;
	}
	public String getParameterName() {
		return parameterName;
	}
	public void setParameterName(String parameterName) {
		this.parameterName = parameterName;
	}
	public BigDecimal getParameterValue() {
		return parameterValue;
	}
	public void setParameterValue(BigDecimal parameterValue) {
		this.parameterValue = parameterValue;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getTreatment() {
		return treatment;
	}
	public void setTreatment(String treatment) {
		this.treatment = treatment;
	}
	public Integer getPercentage() {
		return percentage;
	}
	public void setPercentage(Integer percentage) {
		this.percentage = percentage;
	}


}
