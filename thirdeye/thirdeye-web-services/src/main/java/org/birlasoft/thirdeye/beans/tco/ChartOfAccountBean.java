package org.birlasoft.thirdeye.beans.tco;

import java.util.ArrayList;
import java.util.List;

import org.birlasoft.thirdeye.beans.AssetBean;

public class ChartOfAccountBean {
	
	private AssetBean asset;
	List<CostStructureBean> listOfCostStructure = new ArrayList<>();
	
	public AssetBean getAsset() {
		return asset;
	}
	
	public void setAsset(AssetBean asset) {
		this.asset = asset;
	}
	
	public List<CostStructureBean> getListOfCostStructure() {
		return listOfCostStructure;
	}
	
	public boolean addCostStructure(CostStructureBean costStructure) {
		if (costStructure == null) {
			return false;
		}		
		return this.listOfCostStructure.add(costStructure);
	}

}
