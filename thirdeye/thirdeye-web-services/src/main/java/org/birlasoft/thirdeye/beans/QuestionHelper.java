package org.birlasoft.thirdeye.beans;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.birlasoft.thirdeye.constant.Constants;
import org.birlasoft.thirdeye.constant.QuestionType;
import org.birlasoft.thirdeye.entity.ResponseData;
import org.birlasoft.thirdeye.util.Utility;
import org.springframework.util.StringUtils;

public abstract class QuestionHelper {

	/**
	 * Get a String version of the response data for the question type
	 * 
	 * @param responseData
	 * @param questionType
	 * @return
	 */
	public static String fetchResponseAsString(Collection<ResponseData> responseDatas, QuestionType questionType){
		
		switch (questionType){
		case DATE:
			return handleDate(responseDatas);
		case MULTCHOICE:
			return handleMultichoiceQuestion(responseDatas);
		case NUMBER:
			
			return handleNumber(responseDatas);
		case PARATEXT:
			return handleParaText(responseDatas);
		case TEXT:
			return handleText(responseDatas);
		}
		
		
		
		return "";
	}

	
	private static String handleMultichoiceQuestion(Collection<ResponseData> responseDatas) {
		
		List<String> responses = new ArrayList<String>();
		for (ResponseData responseData : responseDatas){
			JSONMultiChoiceResponseMapper jsonMultiChoiceResponseMapper = Utility.convertJSONStringToObject(responseData.getResponseData(), JSONMultiChoiceResponseMapper.class);
			
			if (StringUtils.isEmpty(jsonMultiChoiceResponseMapper.getOtherOptionResponseText())){
				for (JSONQuestionOptionMapper oneOption : jsonMultiChoiceResponseMapper.getOptions()){
					responses.add(oneOption.getText());
				}
			} else {
				responses.add(Constants.OTHER_TEXT + ": " + jsonMultiChoiceResponseMapper.getOtherOptionResponseText());
			}
		}
		
		String response ="";
		if (responses.size() > 0){
			response = StringUtils.collectionToDelimitedString(responses, ", ");
		}
		
		return response;
	}
	
	private static String handleParaText(Collection<ResponseData> responseDatas) {
		
		List<String> responses = new ArrayList<String>();
		for (ResponseData responseData : responseDatas){
			JSONParaTextResponseMapper jsonMultiChoiceResponseMapper = Utility.convertJSONStringToObject(responseData.getResponseData(), JSONParaTextResponseMapper.class);
			responses.add(jsonMultiChoiceResponseMapper.getResponseParaText());
		}
		
		String response = "";
		if (responses.size() > 0){
			response = StringUtils.collectionToDelimitedString(responses, ", ");
		}
		
		return response;
	}
	
	private static String handleText(Collection<ResponseData> responseDatas) {
		
		List<String> responses = new ArrayList<String>();
		for (ResponseData responseData : responseDatas){
			JSONTextResponseMapper jsonMultiChoiceResponseMapper = Utility.convertJSONStringToObject(responseData.getResponseData(), JSONTextResponseMapper.class);
			responses.add(jsonMultiChoiceResponseMapper.getResponseText());
		}
		
		String response = "";
		if (responses.size() > 0){
			response = StringUtils.collectionToDelimitedString(responses, ", ");
		}
		
		return response;
	}
	
	private static String handleNumber(Collection<ResponseData> responseDatas) {
		
		List<String> responses = new ArrayList<String>();
		for (ResponseData responseData : responseDatas){
			JSONNumberResponseMapper jsonNumberResponse = Utility.convertJSONStringToObject(responseData.getResponseData(), JSONNumberResponseMapper.class);
			responses.add(String.valueOf(jsonNumberResponse.getResponseNumber()));
		}
		
		String response = "";
		if (responses.size() > 0){
			response = StringUtils.collectionToDelimitedString(responses, ", ");
		}
		
		return response;
	}
	
	
	private static String handleDate(Collection<ResponseData> responseDatas) {
		
		List<String> responses = new ArrayList<String>();
		for (ResponseData responseData : responseDatas){
			JSONDateResponseMapper jsonMultiChoiceResponseMapper = Utility.convertJSONStringToObject(responseData.getResponseData(), JSONDateResponseMapper.class);
			responses.add(jsonMultiChoiceResponseMapper.getResponseDate());
		}
		
		String response = "";
		if (responses.size() > 0){
			response = StringUtils.collectionToDelimitedString(responses, ", ");
		}
		
		return response;
	}
}
