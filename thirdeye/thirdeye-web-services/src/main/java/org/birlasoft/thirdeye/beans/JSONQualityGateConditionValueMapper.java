package org.birlasoft.thirdeye.beans;

public class JSONQualityGateConditionValueMapper {
	
	private String color;
	private Double value;
	
	public String getColor() {
		return color;
	}
	
	public void setColor(String color) {
		this.color = color;
	}
	
	public Double getValue() {
		return value;
	}
	
	public void setValue(Double value) {
		this.value = value;
	}

}
