package org.birlasoft.thirdeye.service;

import java.util.List;
import java.util.Set;

import org.birlasoft.thirdeye.entity.Role;
import org.birlasoft.thirdeye.entity.RolePermission;

/**
 * Service interface for role.
 */
public interface RoleService {
	/**
	 * List of all {@code Role}
	 * @return List{@code <Role>} Object
	 */
    public List<Role> findAll();
    /**
     * Find one role object by id.
     * @param id
     * @return {@code Role}
     */
    public Role findOne(Integer id);
    
	/**
	 * @param oneRole
	 * @return
	 */
	public Role save(Role oneRole);
	
	/**
	 * @param listOfRolePermsToDelete
	 */
	public void delete(Set<RolePermission> listOfRolePermsToDelete);
	
	/**
	 * @param newRolePermissionList
	 */
	public void saveRolePermissions(Set<RolePermission> newRolePermissionList);
	/**
	 * @param id
	 * @return
	 */
	public Role findByIdLoadedRolePermissions(Integer id);
	
	/**
	 * @param role
	 * @return
	 */
	public Role createUserRole(Role role);
	
	
	 /**
	 * @param userRoleId
	 */
	public void softDeleteUserRole(Integer userRoleId);
	
	/** get role by delete status
	 * @param deleteStatus
	 * @return
	 */
	public List<Role> findByRoleAndDeleteStatus(Boolean deleteStatus);
	
	 /** check if user exists for a role 
	 * @param roleId
	 * @return
	 */
	public boolean isUserExists(Integer roleId);
	
	
}
