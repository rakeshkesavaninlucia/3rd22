package org.birlasoft.thirdeye.service;

import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletResponse;

import org.birlasoft.thirdeye.beans.php.PHPWrapper;
import org.birlasoft.thirdeye.entity.AssetTemplate;
import org.birlasoft.thirdeye.entity.Questionnaire;

public interface PHPService {
	
	/**
	 * Get a set of {@link AssetTemplate} from {@link Questionnaire}.
	 * It pulls all the assets on a questionnaire and from assets it gets the different
	 * asset templates.
	 * @param questionnaire
	 * @return
	 */
	public Set<AssetTemplate> getAssetTemplatesFromQuestionnaire(Questionnaire questionnaire);
	
	/**
	 * Get {@link PHPWrapper} from parameter, questionnaire and asset templates
	 * @param idsOfParameters
	 * @param idsOfAssetTemplate
	 * @param idOfQuestionnaire
	 * @param filterMap 
	 * @return
	 */
	public Set<PHPWrapper> getPHPWrapperToPlot(Set<Integer> idsOfParameters, Set<Integer> idsOfAssetTemplate,
			Integer idOfQuestionnaire, Map<String, List<String>> filterMap);

	/**
	 * Get PHP Heat map data into Excel
	 * @param heatMapData
	 * @param idOfQuestionnaire 
	 * @param response
	 */
	public void downloadPHPExcel(Set<PHPWrapper> heatMapData, Integer idOfQuestionnaire, HttpServletResponse response);

}
